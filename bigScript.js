String.prototype.repeat = function(num) {
   return new Array(num + 1).join(this);
}

String.prototype.equalsIgnoreCase = function(s) {
   return (this.toLowerCase() == s.toLowerCase());
}

String.prototype.date = function() {
   // date string format in yyyyMMddHHmmss
   var year = parseInt(this.substring(0, 4));
   var mth = parseInt(this.substring(4, 6));
   var date = parseInt(this.substring(6, 8));
   var hour = parseInt(this.substring(8, 10));
   var minutes = parseInt(this.substring(10, 12));
   var seconds = parseInt(this.substring(12, 14));

   //return new Date(year, mth - 1, date, hour, minutes, seconds);
   return Utils.createDate(year, mth, date, hour, minutes, seconds);
}

Date.prototype.formatString = function() {
   var yyyy = "" + this.getFullYear();
   var MM = "" + (this.getMonth() + 1);
   var dd = "" + this.getDate();
   var HH = "" + this.getHours();
   var mm = "" + this.getMinutes();
   var ss = "" + this.getSeconds();

   var MM2 = Utils.right("0" + MM, 2);
   var dd2 = Utils.right("0" + dd, 2);
   var HH2 = Utils.right("0" + HH, 2);
   var mm2 = Utils.right("0" + mm, 2);
   var ss2 = Utils.right("0" + ss, 2);

   return yyyy + MM2 + dd2 + HH2 + mm2 + ss2;
}

// var sleep = function(ms) {
//    return new Promise(resolve => setTimeout(resolve, ms));
// }

/* common rounding methods */
var _R = function(v, d) {
    //return Number(Number(v).toFixed(d));
    return Number( Number( Math.round( v * Math.pow(10, d) ) / Math.pow(10, d) ).toFixed(d) );
}

var _R1 = function(v) {
    return _R(v, 1);
}

var _R2 = function(v) {
    return _R(v, 2);
}

var _R3 = function(v) {
    return _R(v, 3);
}

var _R4 = function(v) {
    return _R(v, 4);
}

var _R5 = function(v) {
    return _R(v, 5);
}

var _R6 = function(v) {
    return _R(v, 6);
}

var _R7 = function(v) {
    return _R(v, 7);
}

var _R8 = function(v) {
    return _R(v, 8);
}

var _R9 = function(v) {
    return _R(v, 9);
}

var _R10 = function(v) {
    return _R(v, 10);
}

var _R11 = function(v) {
    return _R(v, 11);
}

var _R12 = function(v) {
    return _R(v, 12);
}

var _R13 = function(v) {
    return _R(v, 13);
}

var _R14 = function(v) {
    return _R(v, 14);
}

var _R15 = function(v) {
    return _R(v, 15);
}

var _R16 = function(v) {
    return _R(v, 16);
}

var _P = function (wp, obj) {
    _D(wp, obj, "*****");
}

var _D = function (wp, obj, px) {
    // Note: cache should not be re-used by repeated calls to JSON.stringify.
    var pxp = "";
    if (px) {
        pxp = px + " ";
    }
    try {
        var result = Utils.stringify(obj);
        /*
        var cache = [];
        var result = JSON.stringify(obj, function(key, value) {
            if (typeof value === 'object' && value !== null) {
                if (cache.indexOf(value) !== -1) {
                    // Circular reference found, discard key
                    return;
                }
                // Store value in our collection
                cache.push(value);
            }
            if (_isF(value)) {
                return value.toString();
            } else {
                return value;
            }
        });
        cache = null; // Enable garbage collection
        */
        if (Utils.isNothing(px) && Utils.isNothing(wp) && Utils.isNothing(obj)) {
            console.log("");
        } else
        if (px=="" && wp=="" && obj=="") {
            console.log("");
        } else
        if (px=="" && wp=="") {
            console.log(obj);
        } else {
            console.log(pxp + wp + ": " + result);
        }
    } catch (e) {
        console.log(pxp + "Error in reading " + wp + ": " + e );
    }
}

var _DB = function(fn, msg, prefix) {
    var show = true;
    var proposal = engine.context().proposal;
    if (proposal != null) {
        show = proposal.enableDebug;
    }
    if (show) {
        _D(fn, msg, prefix);
    }
}

var _DBR = function(fn, msg, prefix, months) {
    var show = true;
    var fromYear = 0;
    var toYear = 10;
    var annualOnly = false;
    var proposal = engine.context().proposal;
    var year = Math.floor(months / 12);
    var month = (months % 12);
    if (proposal != null) {
        show = proposal.enableDebug;
        if (proposal.startDebugYear) fromYear = proposal.startDebugYear;
        if (proposal.stopDebugYear) toYear = proposal.stopDebugYear;
        if (proposal.displayEOYOnly) annualOnly = proposal.displayEOYOnly;
    }
    if (show && year >= fromYear && year <= toYear) {
        if (!annualOnly || (annualOnly && month == 11)) {
            _D(fn, msg, prefix);
        }
    }
}

var _isF = function(functionToCheck) {
    var getType = {};
    return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
}

var _SS = function(obj, level) {
    var obj2;
    if (obj != undefined) {
       var sobj = Utils.stringify(obj);
       obj2 = JSON.parse(sobj);
    } else {
       obj2 = "undefined";
    }

    var _structure = _S(obj2, level);
    var maxKeyLen = 0;
    for(idx in _structure) {
        var rec = _structure[idx];
        maxKeyLen = Math.max(maxKeyLen, rec.key.length);
    }

    if (maxKeyLen ==0) return;
    maxKeyLen = Math.max(maxKeyLen, 3);

    var border = '+' + '-'.repeat(maxKeyLen + 2) + '+' + '-'.repeat(37) + '+'
    console.log(border);
    __SS_line('key', 'value', maxKeyLen, 35);
    console.log(border);
    for(idx in _structure) {
        var rec = _structure[idx];
        __SS_line(rec.key, rec.value, maxKeyLen, 35);
    }
    console.log(border);
}

var __SS_line = function(k, v, mkl, mvl) {
    var kl = k.length;
    var vl = v.length;
    //console.log(kl);
    //console.log(vl);
    //console.log(mkl);
    //console.log(mvl);
    var pl = '| ' + k + ' '.repeat(mkl - kl + 1) + '| ' + v + ' '.repeat(mvl - vl + 1) + '|';
    console.log(pl);
}

var _S = function(obj, level, currLevel) {
    if (!currLevel) currLevel=0;

    var objKey;
    var objStructure = [];

    var cache = [];
    for(var objKey in obj) {
        var value = obj[objKey];
        if (typeof value === 'object' && value !== null) {
            // second level objects
            if (cache.indexOf(value) !== -1) {
                // circular reference found discard key
            } else {
                if (currLevel+1 < level) {
                    var _subs = _S(value, level, currLevel + 1);
                    var objKey2;
                    for(objKey2 in _subs) {
                        var rec = _subs[objKey2];
                        var row = {};
                        row["key"] = objKey + "." + rec.key;
                        row["value"] = rec.value;
                        objStructure.push(row);
                    }
                } else {
                    var row = {};
                    row["key"] = objKey;
                    row["value"] = '... more ...';
                    objStructure.push(row);
                }
            }
        } else {
            if (!_isF(value)) {
                var row = {};
                row["key"] = objKey;
                row["value"] = '' + value;
                var vo = row.value.replace('\r', '').replace('\n','').replace('\t','').trim();
                if (row.value.length > 36) vo = vo.substring(0, 36);
                //if (vo.length > 32) {
                //    var v1 = vo.substring(0, 15);
                //    var v2 = vo.substring(vo.length-15);
                //    var vv = v1 + ".." + v2;
                //    vo = vv;
                //}
                row.value = vo;
                objStructure.push(row);
            }
        }
    }
    cache = null;

    return objStructure;
}

// value by text
var _V = function( v, d ) {
    var out = v;
    if (!Utils.isNothing(v) && !Utils.isNothing(v.text)) {
        out = v.text;
    }
    // default value
    if (!Utils.isNothing(d)) {
       if (Utils.isNothing(out)) out = d;
    }
    return out;
}

// value by path
var _PV = function( v, p ) {
    if (Utils.isNothing(v)) return v;

    var pp;
    if (p instanceof Array) {
       pp = p;
    } else
    if (typeof p === 'string' || p instanceof String) {
       pp = p.split('.');
    } else {
       throw "Syntax error: _PV( obj, [String|Array] )";
    }

    var vv = v;
    for(pi in pp) {
       var ppp = pp[pi];
       if (ppp === parseInt(ppp, 10)) {
          var i = parseInt(ppp);
          vv = vv[ i ];
       } else {
          vv = vv[ ppp ];
       }
       if (Utils.isNothing( vv )) break;
    }
    return _V( vv );
}

var Utils = {

   expand: function(a, b) {
        // e.g. a = [a, b] and b = [c, d]
        var rs = [];
        if (a.length == 0) {
            for(bi in b) {
                rs.push( [ b[bi] ] );
            }
        } else {
            for(bi in b) {

                var rs2 = []

                // [ [a], [b] ]
                // [ [a], [b] ]
                for(ai in a) {
                    var ac = [];
                    for(aii in a[ai]){
                        ac.push( a[ai][aii] );
                    }
                    rs2.push( ac );
                }

                // [ [a, c], [b, c] ]
                for(ri in rs2) {
                    rs2[ri].push( b[bi] );
                    rs.push( rs2[ri] );
                }

                rs2 = null;

            }
        }
        return rs;
   },

   stringify: function(obj) {
        var cache = [];
        var result = JSON.stringify(obj, function(key, value) {
            if (typeof value === 'object' && value !== null) {
                if (cache.indexOf(value) !== -1) {
                    // Circular reference found, discard key
                    return;
                }
                // Store value in our collection
                cache.push(value);
            }
            if (_isF(value)) {
                return value.toString();
            } else {
                return value;
            }
        });
        cache = null; // Enable garbage collection
        return result;
   },

   isEmptyObject: function(o) {
      var isEmpty = true;
      for(k in o) {
         isEmpty = false;
         break;
      }
      return isEmpty;
   },

   createDate: function(y, M, d, h, m, s, S) {
      if (S) {
         return new Date(y, M-1, d, h, m, s, S);
      } else {
         return new Date(y, M-1, d, h, m, s);
      }
   },

   now: function() {
      return new Date();
   },

   isNothing: function(v) {
      return (v == undefined || v == null);
   },

   isArray: function(v) {
      return (v.constructor === Array);
   },

   extend: function() {
      for (var i = 1; i < arguments.length; i++) {
         for (var key in arguments[i]) {
            if (arguments[i].hasOwnProperty(key)) {
               arguments[0][key] = arguments[i][key];
            }
         }
      }
      return arguments[0];
   },

   mergeToInstance: function(a, b) {
      var c = {};
      for (var p in a)
         c[p] = (b[p] == null) ? a[p] : b[p];
      for (var p in b)
         if (this.isNothing(a[p]))
            c[p] = b[p];
      return c;
   },

   contains: function(obj, list) {
      var x;
      for (x in list) {
         if (list.hasOwnProperty(x) && list[x] === obj) {
            return true;
         }
      }
      return false;
   },

   leftPad: function(string, padding, length) {
      var ol = string.length;
      var ns = string;
      if (ol < length) {
         var p = padding.repeat(length);
         var ss = (p + string);
         ns = ss.substring(ss.length - length, ss.length)
      }
      return ns;
   },

   compareBits: function(source, bitvalue) {
      return ((source & bitvalue) == bitvalue);
   },

   iterator: function(v) {
      if (v instanceof Array) {
         return v;
      } else {
         return [v];
      }
   },

   matchRange: function(v, minV, maxV, undefine) {
      if (v) {
         return (minV <= v && v <= maxV);
      } else {
         return undefine;
      }
   },

   matchInList: function(v, list, undefine) {
      if (v) {
         var iter = Utils.iterator(list);
         var match = false;
         for (k in iter) {
            var kv = iter[k];
            if (kv == v || kv.text == v) {
               match = true;
               break;
            }
         }
         return match;
      } else {
         return undefine;
      }
   },

   isMemberOf: function(v, target) {
      var matched = false;
      for (k in target) {
         var vv = target[k];
         if (v == vv || vv.text == v) {
            matched = true;
            break;
         }
      }
      return matched;
   },

   right: function(s, l) {
      var L = Math.min(l, s.length);
      var begin = s.length - L;
      var end = s.length;
      return s.substring(begin, end);
   },

   left: function(s, l) {
      var L = Math.min(l, s.length);
      return s.substring(0, L);
   },

   uuid: function() {
      var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
         var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
         return v.toString(16);
      });
      return uuid;
   },

}

var Interface = {
   define: function(objInterface) {
      return {
         interfaceSpecification: objInterface,
         implementation: null,
         appliedTo: function(objImplementation) {
            // apply interface and return the static instance
            this.implementation = objImplementation;
            return this;
         },
         implementBy: function(objImplementation) {
            // apply interface and return new instance
            var newInstance = Interface.define(this.interfaceSpecification);
            return newInstance.appliedTo(objImplementation);
            //this.implementation = objImplementation;
            //return Utils.extend(true, {}, this);
         },
         signature: function(args) {
            var methodName = args[0];
            var funcDef = this.interfaceSpecification[methodName];
            if (funcDef == undefined) {
               throw "Pattern Error: method (" + methodName + ") cannot be found in interface.";
               //return null;
            }
            var params = args.length - 1;
            var _args = null;
            if (params > 1) {
               // it is assumed non-json object parameters inputted.
               _args = Utils.extend({}, funcDef.parameters);
               var i = 1;
               for (fi in _args) {
                  _args[fi] = args[i];
                  i++;
               }
            } else {
               _args = args[1];
            }
            return {
               methodName: methodName,
               arguments: _args
            };
         },
         call: function() {
            var signature = this.signature(arguments);
            if (signature == null) {
               //console.log("Pattern Error: signature is expected.");
               throw "Pattern Error: signature is expected.";
               //return null;
            }
            if (this.implementation[signature.methodName] == undefined) {
               // console.log("Pattern Error: " + signature.methodName + " is not defined.");
               throw "Pattern Error: " + signature.methodName + " is not defined.";
               //return null;
            } else {
               return this.implementation[signature.methodName](signature.arguments);
            }
         }
      }
   },
   METHOD: function(args) {
      return {
         parameters: args,
         body: function() {
            return null;
         }
      };
   }
}

var Class = {
   define: function(objClass) {
      var objClassExt = Utils.extend({
         init: function(args) {}
      }, objClass);
      //console.log(objClassExt);
      return {
         classUUID: Utils.uuid(),
         classSpecification: objClassExt,
         create: function(args) {
            var instance = Utils.extend({}, this.classSpecification);

            instance['self'] = instance;

            var _parent = instance.parent;
            while (_parent != undefined) {
               _parent['self'] = instance;
               if (args) {
                  _parent.init(args);
               } else {
                  _parent.init({});
               }
               _parent = _parent.parent;
            }

            if (args) {
               instance.init(args);
            } else {
               instance.init({});
            }
            return instance;
         },
         extend: function(objChildClass) {
            var superPrototype = Utils.extend({}, this.classSpecification);
            var prototype = Utils.extend({}, this.classSpecification);
            Utils.extend(prototype, objChildClass);

            prototype['parent'] = superPrototype;
            return Class.define(prototype);
         }
      }
   }
}

var BuilderPattern = {
   define: function(objInterface, buildProcess) {
      return {
         builderInterface: objInterface,
         builderBody: buildProcess,
         build: function(objBuilder) {
            var builder = this.builderInterface.implementBy(objBuilder);
            for (p in this.builderBody) {
               builder.call(this.builderBody[p], {});
            };
            return builder.implementation;
         }
      };
   }
}

var ServiceLocatorPattern = {
   define: function(objInterface, locatorFunction) {
      return {
         serviceInterface: objInterface,
         serviceLocator: locatorFunction,
         services: [],
         register: function(serviceId, serviceImpl) {
            this.services.push({
               serviceId: serviceId,
               serviceImpl: serviceImpl
            });
         },
         locateService: function() {
            var locatedService = null;
            if (this.serviceLocator) {
               var serviceId = this.serviceLocator();
               for (s in this.services) {
                  var sp = this.services[s];
                  if (serviceId == sp.serviceId) {
                     locatedService = sp.serviceImpl;
                  }
               }
            }
            if (locatedService && locatedService != null) {
               return this.serviceInterface.implementBy(locatedService);
            } else {
               return null;
            }
         }
      };
   }
}

var Factory = {
   define: function(interfaceClass, implementorClass) {
      return {
         interfaceClass: interfaceClass,
         implementorClass: implementorClass,
         implementation: function(args) {
            // default - generate a new instance using implementor defined
            // complicated case should be override this logic
            return implementorClass.create(args);
         },
         implementor: function(implementation) {
            this.implementation = implementation;
            return this;
         },
         create: function(args) {
            return interfaceClass.implementBy(this.implementation(args));
         }
      };
   }
}
var SystemComponents = {

   MANAGER: {
      PRODUCT_ENGINE_INSTANCE: "PRODUCT_ENGINE_INSTANCE",
      CATALOG_HELPER: "CATALOG_HELPER",
      CATALOG_FACTORY: "CATALOG_FACTORY",
      SCHEMA_HELPER: "SCHEMA_HELPER",
      SCHEMA_FACTORY: "SCHEMA_FACTORY",
      PREMIUM_CALCULATOR: "PREMIUM_CALCULATOR",
      VALIDATOR: "VALIDATOR",
      POLICY_VALUE_PROJECTOR: "POLICY_VALUE_PROJECTOR",
      MESSAGE_MANAGER: "MESSAGE_MANAGER",
      DATA_SERVICE: "DATA_SERVICE",
      RATE_MANAGER: "RATE_MANAGER",
      INPUT_TRANSFORMER: "INPUT_TRANSFORMER",
      EVENTS_MANAGER: "EVENTS_MANAGER"
   },
   DATA_SERVICE: {
      CUSTOM_DATA_SERVICE: "CUSTOM"
   }
}

// TAX_RATE must be over 1. For example, if tax rate is 5%, the value
// in constants file, is 1.05.
var PublicConstants = {
   ROUNDING: {
      DEFAULT: 2,
      REGULAR_SAVING_UVLIFE: 8,
      RATE: 4
   },
   COUNTRY: {
      TAX_RATE: 1
   },
   VALIDATION_CATALOG: {
      PRE_CALCULATION_VALIDATION: 1,
      POST_CALCULATION_VALIDATION: 2,
      POST_PROJECTION_VALIDATION: 4
   },
   TARGET_SCOPE: {
      PROPOSAL: 1,
      BASE_COVERAGE: 2,
      RIDER_COVERAGE: 4
   },
   MESSAGE_FIELD: {
      INSURED_AGE: "%INSURED_AGE%",
      MAXIMUM_ISSUE_AGE: "%MAXIMUM_ISSUE_AGE%",
      MINIMUM_ISSUE_AGE: "%MINIMUM_ISSUE_AGE%",
      COVERAGE_FACE_AMOUNT: "%COVERAGE_FACE_AMOUNT%",
      MAXIMUM_FACE_AMOUNT: "%MAXIMUM_FACE_AMOUNT%",
      MINIMUM_FACE_AMOUNT: "%MINIMUM_FACE_AMOUNT%",
      CURRENCY: "%CURRENCY%",
      PAYMENT_MODE: "%PAYMENT_MODE%",
      SMOKING_STATUS: "%SMOKING_STATUS%",
      BENEFIT_PERIOD: "%BENEFIT_PERIOD%",
      WAITING_PERIOD: "%WAITING_PERIOD%",
      MAXIMUM_BASIC_IPO_ISSUE_AGE: "%MAXIMUM_BASIC_IPO_ISSUE_AGE%",
      MINIMUM_BASIC_IPO_ISSUE_AGE: "%MINIMUM_BASIC_IPO_ISSUE_AGE%",
      PRODUCT_ID: "%PRODUCT_ID%",
      MAXIMUM_INITIAL_TOP_UP: "%MAXIMUM_INITIAL_TOP_UP%",
      MINIMUM_INITIAL_TOP_UP: "%MINIMUM_INITIAL_TOP_UP%",
      MAXIMUM_PLANNEDPREMIUM: "%MAXIMUM_PLANNEDPREMIUM%",
      MINIMUM_PLANNEDPREMIUM: "%MINIMUM_PLANNEDPREMIUM%",
      OCCUPATION: "%OCCUPATION%",
      PREMIUM_DISCOUNT: "%PREMIUM_DISCOUNT%",
      DB_LEVEL_AGE: "%DB_LEVEL_AGE%",
      IFL_AGE: "%IFL_AGE%",
      FUND_CODE: "%FUND_CODE%",
      RETURN_RATE: "%RETURN_RATE%",
      MAX_RETURN_RATE: "%MAX_RETURN_RATE%",
      MIN_RETURN_RATE: "%MIN_RETURN_RATE%",
      LOCATION: "%LOCATION%",
      YEAR: "%YEAR%",
      BILLING_METHOD: "%BILLING_METHOD%",
      MAX_SUBSCRIPTION: "%MAX_SUBSCRIPTION%",
      AMOUNT: "%AMOUNT%",
      TOTAL_AMOUNT: "%TOTAL_AMOUNT%",
      AGE: "%AGE%",
      COUNT: "%COUNT%",
      BENEFIT_OPTION_NAME:"%BENEFIT_OPTION_NAME%",
      RIDER:"%RIDER%",
      PLAN_CODE: "%PLAN_CODE%"
   },
   VALIDATION_RULES_MAPPING: {
      GLOBAL_RULES: "_ALL_"
   },
   PREMIUM_RESULT: {
      PREMIUM_RESULT_FIELD_NAME: "_premiums"
   },
   LANGUAGE: {
      en: "ENG",
      zh: "CHI"
   },
   DEBUG: true,
   AGE: {
      JUVENILE_AGE: 14,
      SINGLEPLAN_AGE: 17
   },
   FUND_RETURN: {
      LOW: "LOW",
      MEDIUM: "MEDIUM",
      HIGH: "HIGH"
   },
   FUND_CODE: {
      TARGET_INCOME: "TARGET_INCOME",
      SECURE: "SECURE",
      APAC_SECURE: "APAC_SECURE",
      ASEAN_GROWTH: "ASEAN_GROWTH"
   }
}

var WILDCARD = "--";

var RateConstants = {
   RATETYPE: {
      BASICPREMIUM: 1,
      COI: 2,
      MINIMUMPREMIUM: 3,
      POLICYFEE: 4,
      MODALFACTOR: 5,
      OCCUPATIONFACTOR: 6,
      CASHVALUE: 7,
      DIVIDEND: 8,
      PAIDUPADDITION: 9,
      NSP: 10,
      COUPON: 11,
      RETURNPREMIUM: 12,
      TAXCHARGE: 13,
      DUMPIN: 14,
      PROTECTION: 16,
      LOW_DIVIDEND: 17,
      HIGH_DIVIDEND: 18,
      TOTALDISABILITY: 19,
      SEVEREDISABILITY: 20,
      TDO: 21,
      REDUCED_PAID_UP: 22,
      ROLLING: 23,
      BOS: 24,
      FUND_GROWTH: 25,
      INITIAL_CHARGE: 26,
      SPECIAL_BONUS: 27,
      LOYALTY_BONUS: 28,
      IRR: 29,
      COMMISSION: 30,
      EXPENSE_FACTOR: 31,
      PREMIUM_ALLOCATION: 32,
      SUPPLEMENTARY: 33,
      SURRENDER: 34,
      MONTHLY_FACTOR: 35,
      DPM: 36,
      DCV: 37,
      PAYOR_BENEFIT: 38,
      CURRENT_FUND: 39,
      LOAN: 40,
      MODE_FEE: 41,
      REVERSIONARY_BONUS: 42,
      DISCOUNT_FACTOR: 43,
      MATURITY_BONUS: 44,
      TERMINAL_BONUS_DEATH: 45,
      TERMINAL_BONUS_SURRENDER: 46,
      AX_FACTOR: 47,
      MODAL_COMMISSION: 48,
      PROJECTED_IRR: 49,
      HIGH_IRR: 50,
      LOW_IRR: 51,
      AUTOMATIC_PREMIUM_LOAN: 52,
      REVERSIONARY_INTEREST: 53,
      GUARANTEED_MINIMUM_DB: 54,
      GUARANTEED_SURRENDER_VALUE: 55,
      TRAILER_FEE: 56,
      REVERSIONARY_BONUS_CV: 57,
      TOPUP_PREMIUM_ALLOCATION: 58,
      LOW_REVERSIONARY_BONUS: 59,
      PRODUCTION_CREDIT: 60,
      DEPOSIT_INTEREST: 61,
      LOW_DEPOSIT_INTEREST: 62,
      LOW_TERMINAL_BONUS_SURRENDER: 63,
      ADMINISTRATION_FEE: 64,
      LOW_COUPON: 65,
      COI_LOADING: 66,
      ADVANCE_TOPUP_BONUS: 67,
      TOPUP_COMMISSION: 70,
      RETURNTAXCHARGE: 71,
      LOW_REVERSIONARY_INTEREST: 74,
      GUARANTEED_TUITION_FUND: 82,
      GUARANTEED_EDUCATION_ALLOWANCE: 83,
      GUARANTEED_GRADUATION_GIFT: 84,
      MIN_PLANNED_PREMIUM: 85,
      MANAGEMENT_FEE: 86,
      INITIAL_PREMIUM_CHARGE: 87,
      MODAL_IRR: 88,
      TOPUP_MODE_FEE: 89,
      PREMIUM_HOLIDAY_CHARGE: 90
   },
   RATE_OPTION: {
      PER1000: 1,
      PER100: 2,
      READ_BY_ATTAINAGE: 4,
      MULTIPLE_IPOFILES: 8,
      IPO_10YEARS: 16,
      IPO_5YEARS: 32,
      PERMONTH: 64,
      READ_BY_PAYORAGE: 128,
      PERANNUAL: 256,
      YRT: 512,
      YRT_SIMULATE_LEVEL: 1024
   },
   RATE_CURRENCY: {
      HKD: 1,
      USD: 2,
      CAD: 4,
      GBP: 8,
      AUD: 16,
      SGD: 32,
      PHP: 64,
      VND: 128
   },
   LOCATION: {
      HONG_KONG: 1,
      MACAU: 2,
      SINGAPORE: 4,
      CHINA: 8,
      PHILIPPHINE: 16,
      VIETNAM: 32
   },
   SMOKING_STATUS: {
      SMOKER: 1,
      NONSMOKER: 2,
      AGGREGATE: 4,
      PREFERRED_LIVES: 8
   },
   GENDER: {
      MALE: 1,
      FEMALE: 2
   },
   WAITING_PERIOD: {
      PRD_30DAYS: 1,
      PRD_90DAYS: 2,
      PRD_180DAYS: 4,
      PRD_365DAYS: 8
   },
   BENEFIT_PERIOD: {
      PRD_5YEARS: 1,
      PRD_10YEARS: 2,
      PRD_15YEARS: 4,
      PRD_AGE65: 8
   },
   OCCUPATION_CLASS: {
      CLASS1: 1,
      CLASS2: 2,
      CLASS3: 4,
      CLASS4: 8,
      RCC: 16,
      CLASS0: 32
   },
   NO_OF_PERSONS: {
      PERSONS_1: 1,
      PERSONS_2: 2,
      PERSONS_3: 4,
      PERSONS_4: 8
   },
   OTHER_OPTION: {
      COVERAGE_CLASS_A: 16,
      COVERAGE_CLASS_B: 32,
      COVERAGE_CLASS_C: 64,
      COVERAGE_CLASS_D: 128,
      MAJOR_MEDICAL: 1,
      MAJOR_MEDICAL_N: 2
   },
   PAYMENT_MODE: {
      MONTHLY: 1,
      QUARTERLY: 2,
      SEMIANNUAL: 4,
      ANNUAL: 8
   },
   PARTICIPATE: {
      YES: 1,
      NO: 2
   },
   CHANNEL: {
      AGENCY: 1,
      BROKER: 2,
      BANK: 4,
      ACTUARIAL: 8,
      ADMIN: 16,
      ALL: 255
   }
};


var DISTRIBUTION_CHANNEL = {
   AGENCY: "Agency",
   BROKER: "Broker",
   BANK: "Bank",
   ACTUARIAL: "Actuarial",
   ADMIN: "Admin",
   ALL: "--"
};

var CURRENCY = {
   HKD: "HKD",
   USD: "USD",
   CAD: "CAD",
   GBP: "GBP",
   AUD: "AUD",
   SGD: "SGD",
   PHP: "PHP",
   VND: "VND"
};

var CatalogConstants = {
   PRODUCT_TYPE: {
      BASEPLAN: "BasePlan",
      RIDER: "Rider",
      BENEFIT: "Benefit",
      TERM: "Term",
      UVLIFE: "UVLife",
      PARTICIPATE: "Participate",
      HAS_DEATH_BENEFIT: "ByAttainAge",
      JOINLIFE: "JoinLife",
      BYATTAINAGE: "HasDeathBenefit",
      WAVIER: "Wavier",
      REGULAR_SAVING: "RegularSaving",
      REFUND: "Refund",
      COUPON: "Coupon",
      ACCIDENTAL_DEATH: "AccidentalDeath",
      HOSPITAL_BENEFIT: "HospitalBenefit",
      PAYORBENEFIT: "PayorBenefit",
      TARGET_SAVING: "TargetSaving",
      SINGLE_PREMIUM: "SinglePremium",
      RECURRENT_SINGLE_PREMIUM: "RecurrentSinglePremium",
      FAMILY: "Family",
      SPOUSE: "Spouse",
      CHILD: "Child",
      BACKEND: "BackEnd",
      INCOME: "Income",
      ROP_DEATH_BENEFIT: "ROPDeathBenefit",
      SHARE_BASIC_PLAN_FACEAMOUNT: "ShareBasicPlanFaceAmount",
      HAS_HIGH_WATER_MARK_FACEAMOUNT: "HasHighWaterMarkFaceAmount",
      DISABILITY_INCOME_PLAN: "DisabilityIncomePlan",
      MAJOR_DISEASE_BENEFIT: "MajorDiseaseBenefit",
      INCOME2: "Income2",
      HAS_SOS: "HasSOS",
      LONG_TERM_CARE: "LongTermCare",
      REDUCING_ASSURANCE: "ReducingAssurance",
      TOTAL_PERMANENT_DISABILITY: "TotalPermanentDisability",
      INCOME3: "Income3",
      GUARANTEED_ISSUE: "GuaranteedIssue",
      CONDITIONAL_GUARANTEED_ISSUE: "ConditionalGuaranteedIssue",
      EDUCATION_PLAN: "Education",
      RETURN_OF_PREMIUM: "ROP",
      PPP_NEXD_BPPP: "PPPNotExceedBPPP",
      PREMIUM_BY_NAR: "PremiumByNar",
      MULTI_DIMENSION_RATES: "MultiDimensionRates"
   },
   LOCATION: {
      HONGKONG: "HongKong",
      MACAU: "Macau",
      SINGAPORE: "Singapore",
      CHINA: "China",
      PHILIPPINE: "Philippines",
      VIETNAM: "Vietnam"
   }
};

var SchemaConstants = {
   IRR_TERM: {
      AGE: "AGE",
      PERIOD: "PERIOD"
   },
   GENDER: {
      MALE: "M",
      FEMALE: "F"
   },
   SMOKING_STATUS: {
      SMOKER: "Standard",
      NONSMOKER: "Non-Smoker",
      AGGREGATE: "Aggregate",
      PREFERRED_LIVES: "Preferred Lives"
   },
   DIVIDEND_OPTION: {
      CASH_DIVIDEND: "CashDividend",
      ACCUMULATE: "LeaveOnDeposit",
      PAID_UP_ADDITION: "PaidUpAddition",
      BONUS_PROTECTION: "BonusProtection"
   },
   PREMIUM_DISCOUNT: {
      AGE_DISCOUNT: "AgeDiscount",
      AMOUNT_DISCOUNT: "AmountDiscount",
      RATE_DISCOUNT: "RateDiscount",
      PERCENTAGE_DISCOUNT: "PercentageDiscount"
   },
   DB_OPTION: {
      LEVEL: "Level",
      INCREASE: "Increase",
      INDEXED: "Indexed",
      ROP: "ROP"
   },
   WAITING_PERIOD: {
      WP_30DAYS: "30Days",
      WP_90DAYS: "90Days",
      WP_180DAYS: "180Days",
      WP_365DAYS: "365Days"
   },
   BENEFIT_PERIOD: {
      BP_5YEARS: "5Years",
      BP_10YEARS: "10Years",
      BP_15YEARS: "15Years",
      BP_AGE65: "Age65"
   },
   OCCUPATION_CLASS: {
      CLASS_0: "0",
      CLASS_1: "1",
      CLASS_2: "2",
      CLASS_3: "3",
      CLASS_4: "4",
      RCC: "RCC"
   },
   NO_OF_PERSONS: {
      PERSONS_1: "1",
      PERSONS_2: "2",
      PERSONS_3: "3",
      PERSONS_4: "4",
   },
   COVERAGE_CLASS: {
      CLASS_A: "A",
      CLASS_B: "B",
      CLASS_C: "C",
      CLASS_D: "D",
   },
   BENEFIT_OPTION_TYPE: {
      HEALTH_MAX_PROGRAM: "HealthMaxProgram",
      ADDITIONAL_LIFE_COVERAGE: "AdditionalLifeCoverage",
      NO_VALUE: "NoValue"
   },
   PAYOUT_OPTION: {
      GRADE_SCHOOL: "GradeSchool",
      HIGH_SCHEOOL: "HighSchool",
      COLLEGE_4YRS: "4YrsCollege",
      COLLEGE_5YRS: "5YrsCollege",
      RETIREMENT_50: "RetirementAge50",
      RETIREMENT_55: "RetirementAge55",
      RETIREMENT_60: "RetirementAge60",
      RETIREMENT_65: "RetirementAge65"
   },
   PAYMENT_MODE: {
      ANNUAL: "Annual",
      SEMIANNUAL: "Semi-Annual",
      QUARTERLY: "Quarterly",
      MONTHLY: "Monthly"
   },
   PAYMENT_TYPE: {
      BASIC: "Basic",
      PLANNED: "Planned"
   },
   BILLING_MODE: {
      DIRECTBILLING: "DirectBilling",
      AUTOPAY: "AutoPay",
      PDF: "PDF",
      CREDIT_CARD: "CreditCard",
      MANUCARD: "ManuCard",
      SINGLE: "Single"
   },
   AGE_RELATION_TYPE: {
      PAYOR: "Payor",
      Insured: "Insured"
   },
   FACEAMOUNT_RELATION_TYPE: {
      FACEAMOUNT: "FaceAmount",
      PREMIUM: "Premium"
   },
   DEPENDENCY_RULE_TYPE: {
      EXCLUSIVE: "Exclusive",
      INCLUSIVE: "Inclusive",
      EXCLUSION: "Exclusion",
      INCLUSION: "Inclusion"
   },
   FUND_CHANGE_TYPE: {
      NORMAL: "Normal",
      AUTOMATIC: "Automatic",
      PREMIUM: "Premium",
      FACEAMOUNT: "FaceAmount",
      DUMPIN: "Dump-in",
      WITHDRAWAL: "Withdrawal"
   },
   CHARGE_TYPE: {
      POLICYFEE: "PolicyFee",
      BOS: "Bid-OfferSpread",
      INITIAL_CHARGE: "InitialCharge",
      COMMISSION: "Commission",
      EXPENSE_FACTOR: "ExpenseFactor",
      SUPPLEMENTARY: "Supplementary",
      SURRENDER: "Surrender",
      DPM: "DPMFactor",
      DCV: "DCVFactor",
      MODALFACTOR: "ModalFactor",
      MODE_FEE: "ModeFee",
      MODAL_COMMISSION: "ModalCommission",
      TOPUP_COMMISSION: "TopupCommission",
      ADMINISTRATION_FEE: "AdministrationFee",
      MANAGEMENT_FEE: "ManagementFee",
      INITIAL_PREMIUM_CHARGE: "InitialPremiumCharge",
      PREMIUM_HOLIDAY_CHARGE: "PremiumHolidayCharge"
   },
   PREMIUM_TYPE: {
      BASICPREMIUM: "BasicPremium",
      COSTOFINSURANCE: "CostOfInsurance",
      MINIMUMPREMIUM: "MinimumPremium",
      ALLOCATION: "PremiumAllocation",
      PAYOR_BENEFIT: "PremiumPayorBenefit",
      TOPUP_ALLOCATION: "TopupPremiumAllocation",
      MIN_PLANNED_PREMUM: "MinimumPlannedPremium"
   },
   POLICY_VALUE: {
      CASHVALUE: "CashValue",
      DIVIDEND: "Dividend",
      PAIDUPADDITION: "PaidUpAddition",
      PAIDUPINSURANCE: "PaidUpInsurance",
      COUPON: "Coupon",
      BONUSPROTECTION: "BonusProtection",
      PROTECTION: "Protection",
      TOTALDISABILITY: "TotalDisability",
      SEVEREDISABILITY: "SevereDisability",
      RETURNPREMIUM: "ReturnPremium",
      MAXBENEFIT: "MaxBenefit",
      NSP: "NetSinglePremium",
      TDO: "TDO",
      SPECIAL_BONUS: "SpecialBonus",
      LOYALTY_BONUS: "LoyaltyBonus",
      LOW_DIVIDEND: "LowerDividend",
      HIGH_DIVIDEND: "HighDividend",
      REVERSIONARY_BONUS: "ReversionaryBonus",
      DISCOUNT_FACTOR: "DiscountFactor",
      MATURITY_BONUS: "MaturityBonus",
      TERMINAL_BONUS_DEATH: "TerminalBonusDeath",
      TERMINAL_BONUS_SURRENDER: "TerminalBonusSurrender",
      AX_FACTOR: "AxFactor",
      GUARANTEED_MINIMUM_DB: "GuaranteedMinimumDB",
      GUARANTEED_SURRENDER_VALUE: "GuaranteedSurrenderValue",
      REVERSIONARY_BONUS_CV: "ReversionaryBonusCV",
      GUARANTEED_TUITION_FUND: "GuaranteedTuitionFund",
      GUARANTEED_EDUCATION_ALLOWANCE: "GuaranteedEducationAllowance",
      GUARANTEED_GRADUATION_GIFT: "GuaranteedGraduationGift",
      IRR: "IRR",
      MODAL_IRR: "ModalIRR"
   },
   SUPPORT_OPTION: {
      IPO: "IPO",
      PO: "PremiumOffset",
      CURRENCY_CHANGE: "CurrencyChange",
      BACK_DATE: "BackDate",
      MODE_CHANGE: "ModeChange",
      PDF: "PDF",
      WITHDRAWAL_BONUS: "WithdrawalBonus",
      IIO: "IIO",
      JUVENILE: "JuvenileRule",
      WAITPERIOD: "HasWaitingPeriod",
      BENEFITPERIOD: "HasBenefitPeriod",
      OCCUPATION: "HasOccupationClass",
      REQUIRE_POLICYFEE: "RequirePolicyFee",
      NOOFPERSON: "MultiplePerson",
      MAJORMEDICAL: "MajorMedical",
      RCC: "HasRCC",
      PO_W_COUPON: "POWithCoupon",
      EXTRA_RATING: "ExtraRating",
      HAS_SELF_WAVIER: "HasSelfWavier",
      TOPUPPREMIUM: "TopUpPremium",
      HIGHER_ALTDIV: "HigherAltDiv",
      LOWER_ALTDIV: "LowerAltDiv",
      DECLINED: "Declined",
      EXTRA_RATING_WITH_IPO: "ExtraRatingWithIPO",
      PAYOR_REQUIRED: "PayorRequired",
      PERSONS_LIMIT: "PersonsLimit",
      TPD: "TotalPermanentDisability",
      TI: "TerminalIllness",
      FREE_OF_CHARGE: "FreeOfCharge",
      DISALLOW_RIDER_PREMIUM_PAIDUP: "DisallowRiderPremiumPaidup",
      SHARE_BASIC_PLAN_FACEAMOUNT: "ShareBasicPlanFaceAmount",
      INCLUDE_BASIC_PLAN_MP_ONLY: "IncludeBasicPlanMPOnly",
      HAS_COMMENCEMENT_YEAR: "HasCommencementYear",
      IS_SOS_SUPPORTED: "HasSOS",
      HAS_BENEFIT_OPTION: "HasBenefitOption",
      HAS_PREMIUM_DISCOUNT: "HasPremiumDiscount",
      HOOK_SUBSCRIPTION_WITH_FA: "HookSubscriptionWithFA",
      DISALLOW_RIDER_WITH_GUARANTEED_ISSUE: "DisallowRiderWithGuaranteedIssue",
      DISALLOW_GUARANTEED_ISSUE: "DisallowGuaranteedIssue",
      HAS_LONG_TERM_CARE: "HasLongTermCare",
      INCOME_FOR_LIFE: "HasIFL",
      GWB_STEP_UP: "HasGWBStepUp",
      GWB: "HasGWB",
      DEDUCT_CHARGE_FRM_WITHDRAW: "DeductRedempChargeFromWithdraw",
      GUARANTEED_ISSUE_REQUIREMENT: "GuaranteedIssueRequirement",
      IPO_WITH_BASIC_IPO: "IPOwithBasicIPO",
      DVD_OPTION: "DividendOption",
      EXTEND_PREMIUM_PAID_PERIOD: "ExtendPremiumPaidPeriod"
   },
   GENDER_FULL: {
      MALE: "Male",
      FEMALE: "Female"
   },
   PREMIUM_CATEGORY: {
      BASIC: "Basic",
      PLANNED: "Planned",
      BASICONLY: "BasicOnly",
      TOPUP: "TopUp",
      SINGLEPREMIUM: "Single"
   },
   PREMIUM_RANGE_TYPE: {
       PREMIUMTYPE_BASICPREMIUM: "Basic",
       PREMIUMTYPE_TOPUPPREMIUM: "TopUp",
       PREMIUMTYPE_PLANNEDPREMIUM:"Planned",
       PREMIUMTYPE_SINGLEPREMIUM:"Single",
       PREMIUMTYPE_BASICPREMIUM_ONLY:"BasicOnly"
   },
   MAJOR_MEDICAL: {
      Y: "Y",
      N: "N"
   },
};

var InterfaceConstants = {
   PAYMENT_MODE: {
      ANNUAL: "A",
      SEMIANNUAL: "S",
      QUARTERLY: "Q",
      MONTHLY: "M"
   },
   LOCATION: {
      HONGKONG: "HK",
      MACAU: "MO",
      SINGAPORE: "SG",
      CHINA: "CH",
      PHILIPPINE: "PH",
      VIETNAM: "VN"
   },
   SMOKING_STATUS: {
      SMOKER: "ST",
      NONSMOKER: "NS",
      AGGREGATE: "AG",
      PREFERRED_LIVES: "PL"
   },
   OPTION_PO: {
      OPO_W_COUPON: "PO_WITH_COUPON",
      PO_W_COUPON_N: "PO_WITH_COUPON_NO",
      PO_W_REFUND: "PO_WITH_REFUND",
      PO: "Y"
   },
   OPTION_DIVIDEND: {
      DVD_CASH_DIVIDEND: "CD",
      DVD_ACCUMULATED_DIVIDEND: "AD",
      DVD_PAID_UP_ADDITION: "PUA",
      DVD_BONUS_PROTECTION: "BP",
      LOWER_ALTDIV: "LOWALTDIV",
      HIGHER_ALTDIV: "HIGHALTDIV"
   },
   OPTION_IIO: {
      OPTION_IIO_BASIC: "Basic",
      OPTION_IIO_TOPUP: "Topup"
   },
   BILLING_MODE: {
      DIRECTBILLING: "DirectBilling",
      AUTOPAY: "AutoPay",
      PDF: "PDF",
      CREDIT_CARD: "CreditCard",
      MANUCARD: "ManuCard",
      SINGLE: "Single"
   },
   DIVIDEND_OPTION: {
      CASH_DIVIDEND: "CD",
      ACCUMULATE: "AD",
      PAID_UP_ADDITION: "PUA",
      BONUS_PROTECTION: "BP"
   },
   PROJECTION_OPTIONS: {
      BASE_PLAN_ONLY: "BasePlanOnly",
      BASE_PLAN_WITH_RIDERS: "BasePlanWithRiders"
   },
   POLICY_OPTIONS: {
      CASH_OUT_OPTION: "CashOutOption",
      FUND_ACTIVITIES_OPTION: "FundActivitiesOption"
   },
   BENEFIT_OPTION_TYPE: {
      HEALTH_MAX_PROGRAM: "H",
      ADDITIONAL_LIFE_COVERAGE: "A",
      NO_VALUE: "N"
   },
   SINGLEPLAN_AGE: "17"
};

var EventGroupProductTypes = [
   CatalogConstants.PRODUCT_TYPE.UVLIFE,
   CatalogConstants.PRODUCT_TYPE.TERM,
   CatalogConstants.PRODUCT_TYPE.PARTICIPATE,
   CatalogConstants.PRODUCT_TYPE.HOSPITAL_BENEFIT
];

//    PREMIUM_TYPE : {
// 	  BASICPREMIUM : "BasicPremium",
// 	  COSTOFINSURANCE = "CostOfInsurance",
// 	  MINIMUMPREMIUM  = "MinimumPremium",
// 	  TOPUPPREMIUM    = "",
// 	  PLANNEDPREMIUM  = "",
// 	  SINGLEPREMIUM   = "",
// 	  ALLOCATION      = "PremiumAllocation",
// 	  PAYOR_BENEFIT	  = "PremiumPayorBenefit",
// 	  TOPUP_ALLOCATION  = "TopupPremiumAllocation",
// 	  MIN_PLANNED_PREMUM = "MinimumPlannedPremium",
// 	  BASICPREMIUM_ONLY = ""
//    }

// MISSING SUPPORT_OPTION
//    PLAN_CHANGE : new BitPack(4);
// 	  DEATH_BENEFIT_WITHOUT_CORRIDOR = new BitPack(51);   // <<20130802>>
// 	  PREMIUM_PAYING_PERIOD_EXTENSION = new BitPack(52);   // <<20160919>>
// 	  PARTIAL_WITHDRAWAL_NO_SURRENDER_CHARGE = new BitPack(48); // <<20161011>>
//	  PYD_BEFORE_PRODUCT_DATE       = new BitPack(31);    // PublicConstant.L2x31;
// DB_OPTION: {
//       LEVEL: "L",
//       INCREASE: "I",
//       RETURN_OF_PREMIUM: "R",
//       INDEXED: "X"
// },

//Commented as value is copied from PAYOUT_OPTION
// PREMIUM_RANGE_TYPE: {
//       BASIC_PREMIUM: "GradeSchool",
//       HIGH_SCHEOOL: "HighSchool",
//       COLLEGE_4YRS: "4YrsCollege",
//       COLLEGE_5YRS: "5YrsCollege",
//       RETIREMENT_50: "RetirementAge50",
//       RETIREMENT_55: "RetirementAge55",
//       RETIREMENT_60: "RetirementAge60",
//       RETIREMENT_65: "RetirementAge65"
//    },
// prototype overload
String.prototype.repeat = function(num) {
   return new Array(num + 1).join(this);
}

String.prototype.equalsIgnoreCase = function(s) {
   return (this.toLowerCase() == s.toLowerCase());
}

Number.prototype.format = function(n, x) {
   var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
   return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
}

Date.prototype.julianDay = function() {
   var yearBegin = new Date(this.getFullYear(), 0, 0);
   var diff = this - yearBegin;
   var days = Math.floor(diff / (1000 * 60 * 60 * 24));
   return days;
}

Date.prototype.daysInYear = function() {
   var yearBegin = new Date(this.getFullYear(), 0, 0);
   var nextYearBegin = new Date(this.getFullYear() + 1, 0, 0);
   var diff = nextYearBegin - yearBegin;
   var days = Math.floor(diff / (1000 * 60 * 60 * 24));
   return days;
}

var getPremiumRecord = function(args) {
   var coverage = args['coverage'];
   var schema = args['schema'];
   var premiumType = args['premiumType'];
   premiumType = (Utils.isNothing(premiumType) ? SchemaConstants.PREMIUM_RANGE_TYPE.PREMIUMTYPE_BASICPREMIUM : premiumType);
   var hasValue = args['hasValue'];
   hasValue = (Utils.isNothing(hasValue) ? "N" : hasValue);
   var issueAge = args['issueAge'];

   //console.log("issueAge" + issueAge);

   if (Utils.isNothing(issueAge)) {
      var insured = getPrimaryInsured({coverage: coverage});

      //_DB("insured", insured);

      if (!Utils.isNothing(insured)) {
         issueAge = insured.insuredAge;
      }
   }

   var pmRec = null;
   var pmTable = Utils.iterator(schema.ProductSchema.BasicParticular.PremiumRangeInformation.PremiumRange);
   for (recKey in pmTable) {
      var pm = pmTable[recKey];

      //_SS(pm, 5);
      //_SS(coverage, 5);

      var matched = true;
      matched = matched && (Utils.isNothing(pm.Type) ? false : premiumType == pm.Type.text);
      matched = matched && (coverage.currency.currencyPK.currencyId == pm.CurrencyPK.CurrencyId.text);
      matched = matched && (coverage.options.paymentMode == pm.PaymentMode.text);
      if (hasValue == "Y") {
         matched = matched && (Utils.isNothing(pm.HasValue) ? false : hasValue == pm.HasValue.text);
      }
      matched = matched && (Utils.isNothing(pm.MinAge) ? issueAge >= 0 : issueAge >= Number(pm.MinAge.text));
      matched = matched && (Utils.isNothing(pm.MaxAge) ? issueAge <= 99 : issueAge <= Number(pm.MaxAge.text));
      if (matched) {
         pmRec = pm;
         break;
      }
   }
   return pmRec;
}

var getFundChangeRecord = function(args) {
   var coverage = args['coverage'];
   var fundChange = args['fundChange'];
   var duration = args['duration'];
   var paymentMode = args['paymentMode'];
   var schema = args['schema'];
   fundChange = (Utils.isNothing(fundChange) ? SchemaConstants.FUND_CHANGE_TYPE.DUMPIN : fundChange);
   var matchedFundChangeRec = null;
   if (!(Utils.isNothing(schema.ProductSchema.FundChangeInformation.FundChange))) {
      var fundChangeTable = Utils.iterator(schema.ProductSchema.FundChangeInformation.FundChange);
      for (recKey in fundChangeTable) {
         var fundChangeRec = fundChangeTable[recKey];
         var matched = true;
         matched = matched && (Utils.isNothing(fundChangeRec.Type) ? false : fundChange == fundChangeRec.Type.text);
         matched = matched && (coverage.currency.currencyPK.currencyId == fundChangeRec.CurrencyPK.CurrencyId.text);
         if (!Utils.isNothing(paymentMode)) {
            var recPaymentMode = _PV(fundChangeRec, "PaymentMode");
            if (!Utils.isNothing(recPaymentMode)) {
               matched = matched && (paymentMode == recPaymentMode);
            }
         }
         matched = matched && (Utils.isNothing(fundChangeRec.FromDuration) ? duration >= -100 : duration >= Number(fundChangeRec.FromDuration.text));
         matched = matched && (Utils.isNothing(fundChangeRec.ToDuration) ? duration <= 100 : duration <= Number(fundChangeRec.ToDuration.text));
         if (matched) {
            matchedFundChangeRec = fundChangeRec;
            break;
         }
      }
   }
   return matchedFundChangeRec;
}

var getFirstInsured = function(args) {
   var coverage = args['coverage'];
   var party;
   var parties = _PV(coverage, "parties.party");

   if (parties) {
      var partyList = Utils.iterator( parties );
      for(idx in partyList) {
         var partyRec = partyList[idx];
         party = partyRec;
         break;
      }
   }
   return party;
}

var getInsured = function(args) {
   var coverage = args['coverage'];
   var type = args['type'];
   var party;
   var parties = _PV(coverage, "parties.party");

   //_DB("coverage", coverage);
   //_DB("type", type);
   //_DB("parties", parties);

   if (parties) {
      var partyList = Utils.iterator( parties );

      //_DB("partyList", partyList);

      for(idx in partyList) {
         var partyRec = partyList[idx];
         if (partyRec.type == type) {
            party = partyRec;
            break;
         }
      }
   }
   return party;
}

var getPrimaryInsured = function(args) {
   var party = getInsured({ coverage: args['coverage'], type: 'BASIC' });
   if (Utils.isNothing(party)) party = getFirstInsured( {coverage: args['coverage']} );
   return party;
}

var getSecondInsured = function(args) {
   var party = getInsured({ coverage: args['coverage'], type: 'SECOND' });
   if (Utils.isNothing(party)) party = getFirstInsured( {coverage: args['coverage']} );
   return party;
}

/*
var getJuvenileAge = function(args) {
   var location = args['location'];
   if ((location == CatalogConstants.LOCATION.HONGKONG) || (location == CatalogConstants.LOCATION.MACAU) || (location == CatalogConstants.LOCATION.CHINA))
      return 15;
   else
   if (location == LOCATION.SINGAPORE)
      return 17;
   else
      return 15; // default
}
*/

/*
var isEqualInsured = function(insured1) {
   //var insured1 = args['insured1'];
   var insured2 = this;
   if (Utils.isNothing(insured1) && Utils.isNothing(insured2)) {
      return true;
   } else if (!Utils.isNothing(insured1) && Utils.isNothing(insured2)) {
      return false;
   } else if (Utils.isNothing(insured1) && !Utils.isNothing(insured2)) {
      return false;
   }
   if (insured1.insuredId == insured2.insuredId && insured1.insuredAge == insured2.insuredAge && insured1.insuredSex == insured2.insuredSex && insured1.smokingStatus == insured2.smokingStatus && insured1.type == insured2.type) {
      return true;
   }

   return false;
}
*/

var getBenefitOptionRecord = function(args) {
   var coverage = args['coverage'];
   var schema = args['schema'];
   var benefitOptionType = args['benefitOptionType'];
   //fundChange = (Utils.isNothing(fundChange) ? SchemaConstants.FUND_CHANGE_TYPE.DUMPIN : fundChange);
   var matchedBenefitOptionRec = null;
   if (!(Utils.isNothing(schema.ProductSchema.BasicParticular.BenefitOptionInformation.BenefitOption))) {
      var benefitOptionTable = Utils.iterator(schema.ProductSchema.BasicParticular.BenefitOptionInformation.BenefitOption);
      for (recKey in benefitOptionTable) {
         var benefitOptionRec = benefitOptionTable[recKey];
         var matched = true;
         matched = matched && (Utils.isNothing(benefitOptionRec.Type) ? false : benefitOptionType == benefitOptionRec.Type.text);
         matched = matched && (coverage.currency.currencyPK.currencyId == benefitOptionRec.CurrencyPK.CurrencyId.text);
         if (matched) {
            matchedBenefitOptionRec = benefitOptionRec;
            break;
         }
      }
   }
   return matchedBenefitOptionRec;
}

var getMaxGIFaceamount = function(args) {
   var coverage = args['coverage'];
   var schema = args['schema'];
   var matchedmaxGIFaceamountRec = null;
   if (!(Utils.isNothing(schema.ProductSchema.BasicParticular.MaxGIFaceamount))) {
      var maxGIFaceamountTable = Utils.iterator(schema.ProductSchema.BasicParticular.MaxGIFaceamount);
      for (recKey in maxGIFaceamountTable) {
         var maxGIFaceamountRec = maxGIFaceamountTable[recKey];
         var matched = true;
         matched = matched && (coverage.currency.currencyPK.currencyId == maxGIFaceamountRec.currency);
         if (matched) {
            matchedmaxGIFaceamountRec = maxGIFaceamountRec;
            break;
         }
      }
   }
}
/*
var Repository = {

   managers: {},

   addManager: function(managerName, manager) {
      var newManager = this.managers;
      newManager[managerName] = manager;
      this.managers = newManager;
   },

   getManager: function(managerName) {
      return this.managers[managerName];
   }

}
*/


var RepositoryFactory = function (v) {

    return {

       repo: v,

       addManager: function(managerName, manager) {
          this.repo.call('addManager', {manager: manager, managerName: managerName});
       },

       getManager: function(managerName) {
          return this.repo.call('getManager', {managerName: managerName})
       }
    };

}

var IRepository = Interface.define({
    addManager: Interface.METHOD,
    getManager: Interface.METHOD
})

var RepositoryImpl = Class.define({
    init: function(args) {
        this.self.managers = {};
    },
    addManager: function(args) {
        var manager = args['manager'];
        var managerName = args['managerName'];
        this.self.managers[managerName] = manager;
    },
    getManager: function(args) {
        var managerName = args['managerName'];
        return this.self.managers[managerName];
    }
})

var Repository = new RepositoryFactory(IRepository.implementBy(RepositoryImpl.create()));

// interface of data field which is used for building a interface json to front-end for
// user screen interactons. In IPEService, it holds a collection of fields and then use
// iteration pattern to extract those fields into a product configuraiton object.
var IDataField = Interface.define({
   fieldname: Interface.METHOD,
   readField: Interface.METHOD({
      language: null,
      effectiveDate: null,
      location: null,
      catalog: null,
      schema: null
   })
});

// it is the interface of builder process definition for generating product configurations
// to front-end
var IProductListBuilder = Interface.define({
   matchSchema: Interface.METHOD,
   loadSchema: Interface.METHOD,
   extractFields: Interface.METHOD
});

// IProductEngine Interface
var IProductEngine = Interface.define({
   init: Interface.METHOD({
      managers: []
   }),
   setProposal: Interface.METHOD({
      proposal: null
   }),
   getContext: Interface.METHOD,
   validate: Interface.METHOD,
   calculatePremiums: Interface.METHOD,
   runProjection: Interface.METHOD({
      projectionOptions: null
   }),
   listBaseProducts: Interface.METHOD({
      planCodes: null,
      language: PublicConstants.LANGUAGE.en,
      effectiveDate: null,
      location: null
   }),
   listRiders: Interface.METHOD({
      planCodes: null,
      language: PublicConstants.LANGUAGE.en,
      effectiveDate: null,
      location: null
   }),
   calculateInsuredAge: Interface.METHOD({
      dateOfBirth: null,
      policyYearDate: null
   })
});

// IExecutable specification
// the interface is designed for system use only
var IExecutable = Interface.define({
   execute: Interface.METHOD
});

// IPEService specification
// - loadCatalog is the method to retrieve catalog, which is useful for lookup a appropriate product schema
// - loadSchema is the method to retrieve schema based on the schema key information
// - readRates is the method to load a set of rates (premium, policy values and etc.)
// - readRate is the method to read a single rate (premium, policy values and etc.)
var IPEService = Interface.define({
   createProductListBuilder: Interface.METHOD,
   loadCatalog: Interface.METHOD({
      productCode: null
   }),
   loadSchema: Interface.METHOD({
      productId: null,
      schemaInfo: null
   }),
   listBaseProducts: Interface.METHOD({
      planCodes: null,
      language: PublicConstants.LANGUAGE.en,
      effectiveDate: null,
      location: null
   }),
   listRiders: Interface.METHOD({
      planCodes: null,
      language: PublicConstants.LANGUAGE.en,
      effectiveDate: null,
      location: null
   }),
   readRates: Interface.METHOD({
      schemaPk: null,
      searchRequest: null
   }),
   readRate: Interface.METHOD({
      schemaPk: null,
      searchRequest: null
   })
});

// IBasicPremiumFormula specification
// - calculate is the method of normal premium calculation
// - calculateExtraPremium is the method for calculating the premium of extra loading
var IBasicPremiumFormula = Interface.define({
   formulaName: Interface.METHOD,
   productId: Interface.METHOD({
      coverage: null
   }),
   methodId: Interface.METHOD({
      coverage: null
   }),
   productTypes: Interface.METHOD({
      coverage: null
   }),
   rateList: Interface.METHOD({
      coverage: null,
      ipo:0
   }),
   modalRateList: Interface.METHOD({
      coverage: null,
      ipo: 0
   }),
   calculate: Interface.METHOD({
      coverage: null,
      rates: null,
      rounding: 2,
      overridePremiumRate: null
   }),
   calculateExtraPremium: Interface.METHOD({
      coverage: null,
      rates: null,
      rounding: 2,
      policyYear: 0
   }),
   calculateCompletePremiumRecords: Interface.METHOD({
      coverage: null,
      rates: null,
      rounding: 2,
      policyYear: 0
   })
});

// IValidationRule specification
// - ruleName is the name of rule for identification
// - executionPoint is the timing in process to do the validation (PRE, POST, POST_PROJECT)
// - targetScope is to extract the right information from proposal to test
// - shouldTest is to determination function to decide if the rule should be applied in same target scope
// - validate is the major logic of validation rule
var IValidationRule = Interface.define({
   ruleName: Interface.METHOD,
   shouldTest: Interface.METHOD,
   scope: Interface.METHOD,
   validate: Interface.METHOD({
      target: null
   })
});

// IValidationBuilder specification
var IValidationBuilder = Interface.define({
   init: Interface.METHOD,
   getErrors: Interface.METHOD({
      language: "en"
   }),
   validateProposal: Interface.METHOD({
      proposal: null
   }),
   validateBasePlan: Interface.METHOD({
      basePlan: null
   }),
   validateRider: Interface.METHOD({
      rider: null
   })
});

// ICatalogManager interface
var ICatalogManager = Interface.define({
   lookup: Interface.METHOD({
      productCode: null
   }),
   load: Interface.METHOD({
      productCode: null
   }),
   prepareInstance: Interface.METHOD({
      catalog: null
   })
});

// ISchemaManager interface
var ISchemaManager = Interface.define({
   lookup: Interface.METHOD({
      coverage: null
   }),
   load: Interface.METHOD({
      coverage: null
   }),
   lookupCache: Interface.METHOD({
      productCode: null,
      schemaId: null
   }),
   getCatalogManager: Interface.METHOD({
      productCode: null
   }),
   loadIntoCache: Interface.METHOD({
      coverage: null,
      schemaInfo: null
   }),
   prepareInstance: Interface.METHOD({
      schema: null
   })
});

// IMessageManager interface
var IMessageManager = Interface.define({
   init: Interface.METHOD({
      messages: []
   }),
   getMessage: Interface.METHOD({
      error: null,
      language: null
   })
});

// IValidator interface
var IValidator = Interface.define({
   init: Interface.METHOD({
      preValidationRules: null,
      postValidationRules: null,
      postProjectionValidationRules: null,
      productValidationMapping: null
   }),
   addValidationRule: Interface.METHOD({
      rule: null
   }),
   showRuleNames: Interface.METHOD,
   validate: Interface.METHOD({
      proposal: null
   }),
   preCalculationValidate: Interface.METHOD({
      proposal: null
   }),
   postCalculationValidate: Interface.METHOD({
      proposal: null
   }),
   postProjectionValidate: Interface.METHOD({
      proposal: null,
      context: null,
      projectionResult: null
   })
});

// IPremiumCalculator interface
var IPremiumCalculator = Interface.define({
   init: Interface.METHOD({
      methods: null
   }),
   addMethod: Interface.METHOD({
      productCode: null,
      methodId: null,
      method: null
   }),
   showFormulas: Interface.METHOD,
   lookupCalculator: Interface.METHOD({
      productCode: null,
      methodId: null
   }),
   calculate: Interface.METHOD({
      proposal: null,
   }),
   calculateCoverage: Interface.METHOD({
      coverage: null,
      policyYear: 0,
      ipo: 0
   })
});

// ICatalog interface
var ICatalog = Interface.define({
   init: Interface.METHOD({
      catalogDefintion: null
   }),
   getCatalog: Interface.METHOD,
   lookupSchema: Interface.METHOD({
      coverage: null
   }),
   isUVL: Interface.METHOD,
   isSinglePremium: Interface.METHOD,
   isJoinLife: Interface.METHOD,
   isRegularUVLPlans: Interface.METHOD({
      productCode: null
   }),
   hasTaxCharge: Interface.METHOD,
   isBenefit: Interface.METHOD,
   rounding: Interface.METHOD({
      coverage: null
   }),
   isCoupon: Interface.METHOD,
   isRegularSaving: Interface.METHOD,
   isFundCodeSupported: Interface.METHOD({
      fund: null
   }),
   isTerm: Interface.METHOD,
   isIncome3: Interface.METHOD,
   isSOS: Interface.METHOD,
   isGuaranteedIssue: Interface.METHOD,
   isConditionalGuaranteedIssue: Interface.METHOD,
   isFamilyProductType: Interface.METHOD,
   isSpouseProductType: Interface.METHOD,
   isChildProductType: Interface.METHOD,
   isRiderProductType: Interface.METHOD
});

// ISchema interface
var ISchema = Interface.define({
   getSchema: Interface.METHOD,
   isRefundPremiumSupported: Interface.METHOD,
   isChargeTypeSupported: Interface.METHOD({
      chargeType: null
   }),

   isCurrencySupported: Interface.METHOD({
      currencyId: null
   }),
   hasSupportOption: Interface.METHOD({
      option: null
   }),
   calculateBanding: Interface.METHOD({
      coverage: null
   }),
   matchPremiumInfoRecord: Interface.METHOD({
      coverage: null,
      banding: null,
      premiumType: null
   }),
   matchChargeInfoRecord: Interface.METHOD({
      coverage: null,
      banding: null,
      chargeType: null
   }),
   matchPolicyValueInfoRecord: Interface.METHOD({
      coverage: null,
      banding: null,
      policyValueType: null
   }),
   matchProductionCreditInfoRecord: Interface.METHOD({
      coverage: null,
      banding: null
   }),
   readRefundRates: Interface.METHOD({
      coverage: null,
      policyYear: 0,
      ipo: 0
   }),
   isCurrencyPaymentModeSupported: Interface.METHOD({
      currencyId: null,
      paymentMode: null
   }),
   isInsuredSexSupported: Interface.METHOD({
      gender: null
   }),
   isInsuredSmokingStatusSupported: Interface.METHOD({
      smokingStatus: null
   }),
   isWaitPeriodSupported: Interface.METHOD({
      waitPeriod: null
   }),
   isBenefitPeriodSupported: Interface.METHOD({
      benefitPeriod: null
   }),
   isOccupationSupported: Interface.METHOD({
      occupation: null
   }),
   isBillingMethodPaymentModeSupported: Interface.METHOD({
      billingMethod: null,
      paymentMode: null
   }),
   isCurrencyBillingMethodSupported: Interface.METHOD({
      billingMethod: null,
      currencyId: null
   }),
   calculatePremiumPayingPeriod: Interface.METHOD({
      coverage: null
   }),
   calculateBenefitPeriod: Interface.METHOD({
      coverage: null
   }),
   isDividendOptionSupported: Interface.METHOD({
      dividendOption: null
   }),
   availablePaymentModes: Interface.METHOD({
      premiumType: SchemaConstants.PREMIUM_CATEGORY.BASIC
   }),
   hasExtraRating: Interface.METHOD({
      coverage: null
   }),
   isDBOptionSupported: Interface.METHOD({
      dbOption: null
   }),
   isValidIIO: Interface.METHOD({
      basicIncrePerc: null
   }),
   isIpoDBOptionSupported: Interface.METHOD({
      ipoDbOption: null
   }),
   isMajorMedicalOptionSupported: Interface.METHOD({
      majorMedicalOption: null
   }),
   isNoOfPersonSupported: Interface.METHOD({
      noOfPersonOption: null
   }),
   isClassOtherOptionSupported: Interface.METHOD({
      classOtherOption: null
   })

});

/*
var ICalculatorFactory = Interface.define({
   createCalculator: Interface.METHOD({
      formula: null,
      coverage: null,
      context: null,
      ipo: 0,
      rounding: 2
   })
});
*/

var ICalculateMethod = Interface.define({
   formulaName: Interface.METHOD,
   formulaScope: Interface.METHOD({
      coverage: null
   }),
   calculate: Interface.METHOD({
      policyYear: 0
   })
});

var IConditionalCalculateMethod = Interface.define({
   init: Interface.METHOD({
      formula: null,
      coverage: null,
      context: null,
      ipo: 0,
      rounding: 2
   }),
   isMatched: Interface.METHOD,
   prepareRates: Interface.METHOD({
      policyYear: 0
   }),
   calculate: Interface.METHOD({
      policyYear: 0
   })
});

// in both lookupRates and lookupRate, either rateType or resourceKey is required. If both are provided,
// rateType has priority to be used for lookuping the handler. If rateType cannot lookup any and
// resourceKey is provided, resourceKey will be used for lookup.
var IRateManager = Interface.define({
   lookupHandler: Interface.METHOD({
      rateType: null
   }),
   lookupHandlerByResourceKey: Interface.METHOD({
      resourceKey: null
   }),
   lookupRates: Interface.METHOD({
      coverage: null,
      rateType: null,
      resourceKey: null,
      ipo: 0
   }),
   lookupRate: Interface.METHOD({
      coverage: null,
      rateType: null,
      resourceKey: null,
      policyYear: 0,
      ipo: 0
   })
});

// IInputTransformer interface
var IInputTransformer = Interface.define({

   /*transformLocation: Interface.METHOD,
   transformGender:Interface.METHOD,
   transformPaymentMode:Interface.METHOD,
   transformSmokingStatus:Interface.METHOD,*/
   setReturnRatesByCurrency: Interface.METHOD({
      proposal: null
   }),
   transform: Interface.METHOD({
      coverage: null
   })
});

// Projection Related Interface
var IProjectionManager = Interface.define({
   runProjection: Interface.METHOD({
      proposal: null,
      projectionOptions: null,
      policyOptions: null
   })
});

var IEvent = Interface.define({
   eventName: Interface.METHOD,
   run: Interface.METHOD({
      context: null
   }),
   shouldBeRun: Interface.METHOD({
      context: null
   })
});

var IEventColumn = Interface.define({
   columnName: Interface.METHOD,
   calculatedValue: Interface.METHOD({
      context: null
   }),
   shouldBeShown: Interface.METHOD({
      context: null
   })
});

var IEventsProvider = Interface.define({
   providerName: Interface.METHOD,
   matchProductId: Interface.METHOD,
   matchProductType: Interface.METHOD,
   iterations: Interface.METHOD,
   iterationUnit: Interface.METHOD,
   events: Interface.METHOD,
   columns: Interface.METHOD,
   makeSnapshot: Interface.METHOD
});

var IEventsManager = Interface.define({
   addEventProvider: Interface.METHOD({
      eventGroup: null
   }),
   showEventProviders: Interface.METHOD,
   events: Interface.METHOD({
      proposal: null
   })
});

var IFund = Interface.define({
   init: Interface.METHOD({
      fundId: "unknown"
   }),
   dumpin: Interface.METHOD({
      amount: 0
   }),
   withdraw: Interface.METHOD({
      amount: 0
   }),
   payout: Interface.METHOD,
   rollingInterest: Interface.METHOD,
   fundBalances: Interface.METHOD,
   dumpins: Interface.METHOD,
   withdraws: Interface.METHOD,
   depositPayouts: Interface.METHOD,
   surrenderValues: Interface.METHOD
});

var IDataDrivenCalculator = Interface.define({
   init: Interface.METHOD({
      drivenKeys: null,
      drivenDatas: null
   }),
   data: Interface.METHOD({
      drivenKey: null
   }),
   calculate: Interface.METHOD,
   result: Interface.METHOD
});

var IColumn = Interface.define({
   acceptValue: Interface.METHOD,
   column: Interface.METHOD
});

// Core Rules definitions
var ICoreRules = Interface.define({
   create: Interface.METHOD
});
// field definitions
var FieldPlanName = Class.define({
   fieldname: function(args) {
      return "planname";
   },
   readField: function(args) {
      var language = args['language'];
      var effectiveDate = args['effectiveDate'];
      var location = args['location'];
      var catalog = args['catalog'];
      var schema = args['schema'];

      var result = null;
      var nameList = Utils.iterator(catalog.ProductCatalog.ProductName.Name);
      for (i in nameList) {
         var n = nameList[i];
         if (Utils.isNothing(language) || n.code == language) {
            result = n.text;
            break;
         }
      }
      return result;
   }
});

var FieldPlanId = Class.define({
   fieldname: function(args) {
      return "planid";
   },
   readField: function(args) {
      var language = args['language'];
      var effectiveDate = args['effectiveDate'];
      var location = args['location'];
      var catalog = args['catalog'];
      var schema = args['schema'];

      return catalog.ProductCatalog.ProductPK.ProductId.text;
   }
});

var FieldFunds = Class.define({
   fieldname: function(args) {
      return "funds";
   },
   readField: function(args) {
      var language = args['language'];
      var effectiveDate = args['effectiveDate'];
      var location = args['location'];
      var catalog = args['catalog'];
      var schema = args['schema'];

      var result = [];
      if (!Utils.isNothing(catalog.ProductCatalog.FundMapInformation.FundCode)) {
         var fundCodeList = Utils.iterator(catalog.ProductCatalog.FundMapInformation.FundCode);
         for (fundInfoIdx in fundCodeList) {
            var fundInfo = fundCodeList[fundInfoIdx];
            result.push(fundInfo.text);
         }
      }
      return result;
   }
});

var FieldProductTypes = Class.define({
   fieldname: function(args) {
      return "producttypes";
   },
   readField: function(args) {
      var language = args['language'];
      var effectiveDate = args['effectiveDate'];
      var location = args['location'];
      var catalog = args['catalog'];
      var schema = args['schema'];

      var result = [];
      var productTypeList = Utils.iterator(catalog.ProductCatalog.ProductType.ProductTypeCode);
      for (prodTypeIdx in productTypeList) {
         var productType = productTypeList[prodTypeIdx].text;
         result.push(productType);
      }
      return result;
   }
});

var FieldIssueAge = Class.define({
   fieldname: function(args) {
      return "issueage";
   },
   readField: function(args) {
      var language = args['language'];
      var effectiveDate = args['effectiveDate'];
      var location = args['location'];
      var catalog = args['catalog'];
      var schema = args['schema'];

      var result = {};
      result["max"] = schema.ProductSchema.BasicParticular.IssueAge.Max.text;
      result["min"] = schema.ProductSchema.BasicParticular.IssueAge.Min.text;
      return result;
   }
});

var FieldGenderOptions = Class.define({
   fieldname: function(args) {
      return "gender";
   },
   readField: function(args) {
      var language = args['language'];
      var effectiveDate = args['effectiveDate'];
      var location = args['location'];
      var catalog = args['catalog'];
      var schema = args['schema'];

      var result = [];
      var genderList = Utils.iterator(schema.ProductSchema.BasicParticular.SupportSex.Sex);
      for (idx in genderList) {
         var sex = genderList[idx];
         if (sex.text == "Male") {
            result.push(SchemaConstants.GENDER.MALE);
         }
         if (sex.text == "Female") {
            result.push(SchemaConstants.GENDER.FEMALE);
         }
      }
      return result;
   }
});

var FieldPremiumPayingPeriod = Class.define({
   fieldname: function(args) {
      return "premiumpayperiod";
   },
   readField: function(args) {
      var language = args['language'];
      var effectiveDate = args['effectiveDate'];
      var location = args['location'];
      var catalog = args['catalog'];
      var schema = args['schema'];

      var result = {};
      result["minperiod"] = schema.ProductSchema.BasicParticular.PremiumPaidPeriod.MinPremiumPaidPeriod.text;
      result["maxage"] = schema.ProductSchema.BasicParticular.PremiumPaidPeriod.MaxPremiumPaidAge.text;
      return result;
   }
});

var FieldBenefitPeriod = Class.define({
   fieldname: function(args) {
      return "benefitperiod";
   },
   readField: function(args) {
      var language = args['language'];
      var effectiveDate = args['effectiveDate'];
      var location = args['location'];
      var catalog = args['catalog'];
      var schema = args['schema'];

      var result = {};
      result["maxage"] = schema.ProductSchema.BasicParticular.BenefitPeriod.MaxBenefitAge.text;
      result["minperiod"] = schema.ProductSchema.BasicParticular.BenefitPeriod.MinBenefitPeriod.text;
      return result;
   }
});

var FieldPremiumRange = Class.define({
   fieldname: function(args) {
      return "premiumrange";
   },
   readField: function(args) {
      var language = args['language'];
      var effectiveDate = args['effectiveDate'];
      var location = args['location'];
      var catalog = args['catalog'];
      var schema = args['schema'];

      // premium range
      var currencies = [];
      var currencyCodes = {};

      var result = {};
      var premiumRangeList = Utils.iterator(schema.ProductSchema.BasicParticular.PremiumRangeInformation.PremiumRange);
      for (idx in premiumRangeList) {
         var pr = premiumRangeList[idx];

         var premRange = null;
         if (pr.Type.text == SchemaConstants.PAYMENT_TYPE.BASIC) {
            if (result[SchemaConstants.PAYMENT_TYPE.BASIC] == undefined) {
               result[SchemaConstants.PAYMENT_TYPE.BASIC] = {};
            }
            if (result[SchemaConstants.PAYMENT_TYPE.BASIC][pr.CurrencyPK.CurrencyId.text] == undefined) {
               result[SchemaConstants.PAYMENT_TYPE.BASIC][pr.CurrencyPK.CurrencyId.text] = {};
            }
            premRange = result[SchemaConstants.PAYMENT_TYPE.BASIC][pr.CurrencyPK.CurrencyId.text];
         } else
         if (pr.Type.text == SchemaConstants.PAYMENT_TYPE.PLANNED) {
            if (result[SchemaConstants.PAYMENT_TYPE.PLANNED] == undefined) {
               result[SchemaConstants.PAYMENT_TYPE.PLANNED] = {};
            }
            if (result[SchemaConstants.PAYMENT_TYPE.PLANNED][pr.CurrencyPK.CurrencyId.text] == undefined) {
               result[SchemaConstants.PAYMENT_TYPE.PLANNED][pr.CurrencyPK.CurrencyId.text] = {};
            }
            premRange = result[SchemaConstants.PAYMENT_TYPE.PLANNED][pr.CurrencyPK.CurrencyId.text];
         } else {
            console.log("iOSservice error: " + pr.Type.text + " is not defined");
         }

         if (!Utils.isNothing(premRange)) {

            if (premRange[pr.PaymentMode.text] == undefined) {
               premRange[pr.PaymentMode.text] = {};
            }

            bpRec = premRange[pr.PaymentMode.text];

            if (!Utils.isNothing(bpRec)) {
               var billingMode = [];
               if (pr.BillingMode) {
                  var billingModeList = Utils.iterator(pr.BillingMode);
                  for (idx in billingModeList) {
                     var bm = billingModeList[idx];

                     //console.log(bm);

                     if (bm) {
                        billingMode.push(bm.text);
                     }
                  }
               }
               bpRec["billingMode"] = billingMode;
               bpRec["max"] = pr.MaxPremium.text;
               bpRec["min"] = pr.MinPremium.text;

               if (currencyCodes[pr.CurrencyPK.CurrencyId.text] == undefined) {
                  currencyCodes[pr.CurrencyPK.CurrencyId.text] = "Y";
                  currencies.push(pr.CurrencyPK.CurrencyId.text);
               }
            }
         }
      }
      return result;
   }
});

var FieldCurrency = Class.define({
   fieldname: function(args) {
      return "currency";
   },
   readField: function(args) {
      var language = args['language'];
      var effectiveDate = args['effectiveDate'];
      var location = args['location'];
      var catalog = args['catalog'];
      var schema = args['schema'];

      // premium range
      var currencies = [];
      var currencyCodes = {};

      var premiumRangeList = Utils.iterator(schema.ProductSchema.BasicParticular.PremiumRangeInformation.PremiumRange);
      for (idx in premiumRangeList) {
         var pr = premiumRangeList[idx];

         if (currencyCodes[pr.CurrencyPK.CurrencyId.text] == undefined) {
            currencyCodes[pr.CurrencyPK.CurrencyId.text] = "Y";
            currencies.push(pr.CurrencyPK.CurrencyId.text);
         }
      }
      return currencies;
   }
});

var FieldSupportOptions = Class.define({
   fieldname: function(args) {
      return "supportoptions";
   },
   readField: function(args) {
      var language = args['language'];
      var effectiveDate = args['effectiveDate'];
      var location = args['location'];
      var catalog = args['catalog'];
      var schema = args['schema'];

      var result = [];
      if (schema.ProductSchema.BasicParticular.SupportOption) {
         for (optionKey in schema.ProductSchema.BasicParticular.SupportOption) {
            if (schema.ProductSchema.BasicParticular.SupportOption[optionKey] != undefined && schema.ProductSchema.BasicParticular.SupportOption[optionKey].text == "Y") {
               result.push(optionKey);
            }
         }
      }
      return result;
   }
});

var FieldInitialPremium = Class.define({
   fieldname: function(args) {
      return "initialpremium";
   },
   readField: function(args) {
      var language = args['language'];
      var effectiveDate = args['effectiveDate'];
      var location = args['location'];
      var catalog = args['catalog'];
      var schema = args['schema'];

      var result = {};
      result["min"] = {};
      if (schema.ProductSchema.BasicParticular.MinInitialDumpin) {
         var minInitialPremiumList = Utils.iterator(schema.ProductSchema.BasicParticular.MinInitialDumpin);
         for (idx in minInitialPremiumList) {
            var minIP = minInitialPremiumList[idx];
            result.min[minIP.currency] = minIP.text;
         }
      }
      return result;
   }
});

var FieldFaceAmount = Class.define({
   fieldname: function(args) {
      return "faceamount";
   },
   readField: function(args) {
      var language = args['language'];
      var effectiveDate = args['effectiveDate'];
      var location = args['location'];
      var catalog = args['catalog'];
      var schema = args['schema'];

      var result = {};
      var bandInfoList = Utils.iterator(schema.ProductSchema.BandInformation.BandRecord);
      for (idx in bandInfoList) {
         var pr = bandInfoList[idx];

         //console.log(JSON.stringify(pr));

         if (Utils.isNothing(result[pr.CurrencyPK.CurrencyId.text])) {
            result[pr.CurrencyPK.CurrencyId.text] =
               {maxTopup:0, minTopup:9999999999,
                maxFaceAmount:0, minFaceAmount:9999999999,
                maxGuaranteeRatio:0,
                maxContractPremium: 0, minContractPremium: 9999999999};
         }
         var rec = result[pr.CurrencyPK.CurrencyId.text];

         rec.maxTopup = Math.max(rec.maxTopup, pr.MaxTopup.text);
         rec.minTopup = Math.min(rec.minTopup, pr.MinTopup.text);
         rec.maxFaceAmount = Math.max(rec.maxFaceAmount, pr.MaxFaceAmount.text);
         rec.minFaceAmount = Math.min(rec.minFaceAmount, pr.MinFaceAmount.text);
         rec.maxContractPremium = Math.max(rec.maxContractPremium, pr.MaxContractPremium.text);
         rec.minContractPremium = Math.min(rec.minContractPremium, pr.MinContractPremium.text);
         rec.maxGuaranteeRatio = Math.max(rec.maxGuaranteeRatio, pr.MaxGuarRatio.text);
      }
      return result;
   }
});

var FieldDeathBenefitOptions = Class.define({
   fieldname: function(args) {
      return "deathbenefitoptions";
   },
   readField: function(args) {
      var language = args['language'];
      var effectiveDate = args['effectiveDate'];
      var location = args['location'];
      var catalog = args['catalog'];
      var schema = args['schema'];

      var result = [];
      if (!Utils.isNothing(schema.ProductSchema.BasicParticular.SupportDeathBenefit.DeathBenefit)) {
         var deathBenefitList = Utils.iterator(schema.ProductSchema.BasicParticular.SupportDeathBenefit.DeathBenefit);
         for (idx in deathBenefitList) {
            var dbOption = deathBenefitList[idx];
            result.push(dbOption.text);
         }
      }
      return result;
   }
});

var FieldFundChange = Class.define({
   fieldname: function(args) {
      return "fundchange";
   },
   readField: function(args) {
      var language = args['language'];
      var effectiveDate = args['effectiveDate'];
      var location = args['location'];
      var catalog = args['catalog'];
      var schema = args['schema'];

      var fundchange = {};
      if (!Utils.isNothing( schema.ProductSchema.FundChangeInformation.FundChange )) {
         var fundChangeList = Utils.iterator(schema.ProductSchema.FundChangeInformation.FundChange);
         for (idx in fundChangeList) {
            var fc = fundChangeList[idx];

            console.log(JSON.stringify(fc));

            if (Utils.isNothing( fundchange[fc.CurrencyPK.CurrencyId.text] )) {
               fundchange[fc.CurrencyPK.CurrencyId.text] = {};
            }
            fundchangeCurrency = fundchange[fc.CurrencyPK.CurrencyId.text];
            if (Utils.isNothing( fundchangeCurrency[fc.Type.text] )) {
               fundchangeCurrency[fc.Type.text] = {};
            }
            fundchangeCurrencyType = fundchangeCurrency[fc.Type.text];
            fundchangeCurrencyType["max"] = fc.MaxIncrease.text;
            fundchangeCurrencyType["min"] = fc.MinIncrease.text;
         }
      }
      return fundchange;
   }
});

// builder
var ProductListBuilder = Class.define({
   context: {},
   init: function(args) {
      this.self.context['planCodes'] = args["planCodes"];
      this.self.context['language'] = args["language"];
      this.self.context['effectiveDate'] = args["effectiveDate"];
      this.self.context['location'] = args["location"];
      this.self.context['catalog'] = args["catalog"];
      this.self.context['now'] = args['now'];
      this.self.context['fields'] = fields = [
         FieldPlanName.create(),
         FieldPlanId.create(),
         FieldFunds.create(),
         FieldProductTypes.create(),
         FieldIssueAge.create(),
         FieldGenderOptions.create(),
         FieldPremiumPayingPeriod.create(),
         FieldBenefitPeriod.create(),
         FieldPremiumRange.create(),
         FieldCurrency.create(),
         FieldSupportOptions.create(),
         FieldInitialPremium.create(),
         FieldFaceAmount.create(),
         FieldDeathBenefitOptions.create(),
         FieldFundChange.create()
      ];
   },
   matchSchema: function(args) {
      var catalog = this.self.context.catalog;
      var latestSchema = null;
      var catalogRecordList = Utils.iterator(catalog.ProductCatalog.ProductCatalogDetail.CatalogRecord);
      for (prodDtlIdx in catalogRecordList) {
         var productDetail = catalogRecordList[prodDtlIdx];

         //console.log(JSON.stringify(productDetail) );

         if (Utils.isNothing(this.self.context.location) || (this.self.context.location == productDetail.Location.text || productDetail.Location.text == WILDCARD)) {
            if ((productDetail.EffectiveDate.text + "000000") <= this.self.context.now) {
               // effective product only
               if (latestSchema == null) {
                  latestSchema = productDetail;
               } else {
                  if (latestSchema.EffectiveDate.text <= productDetail.EffectiveDate.text) {
                     latestSchema = productDetail;
                  }
               }
            }
         }
      }
      this.self.context['latestSchema'] = latestSchema;
   },
   loadSchema: function(args) {
      if (!Utils.isNothing( this.self.context.latestSchema )) {
         var catalog = this.self.context.catalog;
         var latestSchema = this.self.context.latestSchema;
         this.self.context['schema'] = PEservice.loadSchema(
            catalog.ProductCatalog.ProductPK.ProductId.text,
            { schemaId: latestSchema.SchemaId }
         );
      }
   },
   extractFields: function(args) {
      if (!Utils.isNothing( this.self.context.schema )) {
         if (Utils.isNothing())
         this.self.context['product'] = {};
         for (fieldIdx in fields) {
            var fieldImpl = IDataField.appliedTo(fields[fieldIdx]);
            var fieldname = fieldImpl.call("fieldname", {});
            var fieldvalue = null;
            //console.log('fieldname = ' + fieldname);
            try {
               fieldvalue = fieldImpl.call("readField", {
                  planCodes: this.self.context.planCodes,
                  language: this.self.context.language,
                  effectiveDate: this.self.context.effectiveDate,
                  location: this.self.context.location,
                  catalog: this.self.context.catalog,
                  schema: this.self.context.schema
               });
            } catch ( e ) {
               console.log('field (' + fieldname + ') cannot be read. Skip the field - ' + e);
            }
            this.self.context.product[fieldname] = fieldvalue;
         }
      }
   }
});
var PEServiceStubImpl = Class.define({

   loadCatalog: function(args) {
		var productCode = args['productCode'];
      // TODO - implement the actual logic to obtain catalog information
      //        from native code layer in string format or JSON format

      var catalog = JSON.parse(basePlanCatalog);
      //var catalog = JSON.parse(basePlanCatalog, (k, v) => {
      //   return v;
      //});
      return catalog;
   },

   loadSchema: function(args) {
      var schemaInfo = args['productId'];
		var schemaInfo = args['schemaInfo'];
      // TODO - implement the actual logic to obtain schema information
      //        from native code layer, using product code and schema id.
      //        the value can be either JSON or string format

      var schema = JSON.parse(basePlanSchema);
      //var schemas = JSON.parse(basePlanSchema, (k, v) => {
      //   return v;
      //});

      // var schemaFound = null;
      // for (schKey in schemas) {
      //    var schema = schemas[schKey];
      //    if (schema.ProductSchema.ProductSchemaPK.SchemaId.text == schemaInfo.schemaId.text) {
      //       schemaFound = schema;
      //       break;
      //    }
      // }
      return schema;
   },

   listProducts: function(args) {
      return [];
   },

   readRates: function(args) {
      var productId = args['schemaPk'];
		var searchRequest = args['searchRequest'];
      // TODO - implement the actual logic to call RateManager to obtain
      //        rates
      var productRates = rates[searchRequest.rateId];
      var _rates = productRates[searchRequest.rateType];

      if (searchRequest.rateType == RateConstants.RATETYPE.MODALFACTOR) {
         _rates = _rates[searchRequest.paymentMode];
      }

      // create local caches to avoid unnecessary rate loading
      // TODO - build local cache
      // For YRT product, it might be necessary to create a looping here
      // to read all rates from native - need consider.

      return _rates;
   },

   readRate: function(args) {
      // assume readRates to return all rates to JS context already
      // need further consider about YRT and ATTAIN-AGE rates situations
      var _rates = this.readRates(args);
      return _rates.rates[0];
   }

});
var PEServiceIOSImpl = Class.define({

   loadCatalog: function(args) {
      var productCode = args['productCode'];
      var catalog = iOSservice.readProductByProductId(productCode);
      return catalog;
   },

   loadSchema: function(args) {
      var productId = args['productId'];
      var schemaInfo = args['schemaInfo'];
      var schemaFound = iOSservice.readPlanByProductIdSchemaId(productId, Utils.leftPad(schemaInfo.schemaId.text, '0', 2));
      return schemaFound;
   },

   createProductListBuilder: function(args) {
      var planCodes = args["planCodes"];
      var language = args["language"];
      var effectiveDate = args["effectiveDate"];
      var location = args["location"];
      var now = args['now'];
      var catalog = args['catalog'];

      return ProductListBuilder.create(
         {
            planCodes: planCodes,
            language: language,
            effectiveDate: effectiveDate,
            location: location,
            now: now,
            catalog: catalog
         }
      );
   },

   listBaseProducts: function(args) {
      var planCodes = args["planCodes"];
      var language = args["language"];
      var effectiveDate = args["effectiveDate"];
      var location = args["location"];

      console.log("iOSservice (listBaseProducts) --- start")
      console.log(planCodes);
      console.log(language);
      console.log(effectiveDate);
      console.log(location);
      console.log("iOSservice (listBaseProducts)--- end")

      var products = [];
      var now = null;
      if (Utils.isNothing(effectiveDate)) {
         now = Utils.now().formatString();
      } else {
         now = effectiveDate.formatString();
      }
      var buildProcess = BuilderPattern.define(IProductListBuilder, ['matchSchema', 'loadSchema', 'extractFields'] );
      var catalogList = iOSservice.getPlansByProductIds(planCodes);
      for (catalogIdx in catalogList) {
         var catalog = catalogList[catalogIdx];
         var isBasePlan = Utils.matchInList(CatalogConstants.PRODUCT_TYPE.BASEPLAN, catalog.ProductCatalog.ProductType.ProductTypeCode, false);
         if (isBasePlan) {
            var result = buildProcess.build( this.self.createProductListBuilder(
               {
                  planCodes: planCodes,
                  language: language,
                  effectiveDate: effectiveDate,
                  location: location,
                  now: now,
                  catalog: catalog
               }
            ) );
            //console.log("product = " + JSON.stringify(result.product));
            if (!Utils.isNothing( result.context.product )) {
               products.push(result.context.product);
            }
         }
      }
      return products;
   },

   listRiders: function(args) {
      var planCodes = args["planCodes"];
      var language = args["language"];
      var effectiveDate = args["effectiveDate"];
      var location = args["location"];

      console.log("iOSservice (listRiders) --- start")
      console.log(planCodes);
      console.log(language);
      console.log(effectiveDate);
      console.log(location);
      console.log("iOSservice (listRiders)--- end")

      var products = [];
      var now = null;
      if (Utils.isNothing(effectiveDate)) {
         now = Utils.now().formatString();
      } else {
         now = effectiveDate.formatString();
      }
      var buildProcess = BuilderPattern.define(IProductListBuilder, ['matchSchema', 'loadSchema', 'extractFields'] );
      var catalogList = iOSservice.getRidersByProductIds(planCodes);
      for (catalogIdx in catalogList) {
         var product = {};
         var catalog = catalogList[catalogIdx];
         var isBasePlan = Utils.matchInList(CatalogConstants.PRODUCT_TYPE.BASEPLAN, catalog.ProductCatalog.ProductType.ProductTypeCode, false);
         if (!isBasePlan) {
            var result = buildProcess.build( this.self.createProductListBuilder(
               {
                  planCodes: planCodes,
                  language: language,
                  effectiveDate: effectiveDate,
                  location: location,
                  now: now,
                  catalog: catalog
               }
            ) );
            //console.log("product = " + JSON.stringify(result.product));
            if (!Utils.isNothing( result.context.product )) {
               products.push(result.context.product);
            }
         }
      }
      return products;
   },

   readRates: function(args) {
      var schemaPk = args['schemaPk'];
      var searchRequest = args['searchRequest'];


      var context = engine.context();
      //_DB("PE.context: ", context);
      var debug = false;
      if (context && context.proposal && context.proposal.enableDebug) {
         debug = context.proposal.enableDebug;
      }

      //if (PublicConstants.DEBUG) {
      //   console.log(schemaPk);
      //   console.log(searchRequest);
      //}
      var requestString = JSON.stringify({
         schemaPk: schemaPk,
         request: searchRequest
      });
      //if (debug) {
      _DB("readRates.requestString (try 1): " + requestString, "", "PEServiceIOSImpl->");
      //}
      //if (PublicConstants.DEBUG) {
      //   console.log(requestString);
      //}

      var _rates;
      var isEmpty = true;
      try {
         _rates = iOSservice.readRates(requestString);
         if (!Utils.isNothing(_rates)) {
            if (typeof _rates === 'string' || _rates instanceof String) {
               _rates = JSON.parse(_rates);
            }

            _DB("requestString", requestString, "PEServiceIOSImpl->");
            for(r in _rates.rates) {
               if (Number(_rates.rates[r]) > Number(0)) {
                  isEmpty = false;
                  break;
               }
            }

            //isEmpty = (_rates.size() == 0)
         }
      } catch (e) {
         _D("Exception (try 1): " + e)
      }

      //console.log("isEmpty = " + isEmpty);
      if (isEmpty && (Number(searchRequest.band) > Number(0))) {
         searchRequest.band = 0;
         requestString = JSON.stringify({
            schemaPk: schemaPk,
            request: searchRequest
         });

         //if (debug) {
         _D("readRates.requestString (try 2): " + requestString, "", "PEServiceIOSImpl->");
         //}

         try {
            var _rates2 = iOSservice.readRates(requestString);
            if (!Utils.isNothing(_rates2)) {
               //console.log("_rates = " + JSON.stringify(_rates));
               if (typeof _rates2 === 'string' || _rates2 instanceof String) {
                  _rates2 = JSON.parse(_rates2);
               }
               _rates = _rates2;
            }
         } catch ( e ) {
            _D("Exception (try 2): " + e)
         }
      } else {
         //console.log("_rates = " + JSON.stringify(_rates));
      }

      return _rates;
   },

   readRate: function(args) {
      var _rates = this.readRates(args);
      return _rates.rates[0];
   }

});
var PEServiceWEBImpl = PEServiceIOSImpl.extend({

  loadCatalog: function(args) {
     var catalogString = this.self.parent.loadCatalog(args);
     //console.log('loadCatalog - catalogString : ' + catalogString);
     var catalog = JSON.parse(catalogString);
     //console.log('loadCatalog - catalog : ' + JSON.stringify(catalog));
     return catalog;
  },

  loadSchema: function(args) {
     var schemaString = this.self.parent.loadSchema(args);
     var schema = JSON.parse(schemaString);
     return schema;
  },

  readRates: function(args) {
     //var _ratesString = this.self.parent.readRates(args);
     //var _rates = JSON.parse(_ratesString);
     //return _rates;
     return this.self.parent.readRates(args);
  },

  readRate: function(args) {
     var _rates = this.self.readRates(args);
     return _rates.rates[0];
  }

});
var BPM1 = Class.define({
    formulaName: function(args) {
        return 'BPM1';
    },
    productId: function(args) {
        var coverage = args['coverage'];
        return null;
    },
    methodId: function(args) {
        var coverage = args['coverage'];
        return 'BPM1';
    },
    productTypes: function(args) {
        var coverage = args['coverage'];
        return null;
    },
    rateList: function(args) {
        var coverage = args['coverage'];
        var ipo = args['ipo'];
        return [{
                rateName: 'premiumRate',
                rateType: RateConstants.RATETYPE.BASICPREMIUM,
                unit: true,
                hasRate: true
            },
            {
                rateName: 'occupationFactorRate',
                rateType: RateConstants.RATETYPE.OCCUPATIONFACTOR,
                unit: false,
                hasRate: coverage.schema.call("hasSupportOption", {
                    option: SchemaConstants.SUPPORT_OPTION.OCCUPATION
                })
            }
        ];
    },
    modalRateList: function(args) {
        var coverage = args['coverage'];
        var ipo = args['ipo'];
        return [{
            rateName: 'modalFactor',
            rateType: RateConstants.RATETYPE.MODALFACTOR,
            unit: false,
            hasRate: true
        }];
    },
    rounding: function(args) {
        var coverage = args['coverage'];
        return coverage.catalog.call("rounding", {
            coverage: coverage
        });
    },
    calculate: function(args) {
        var coverage = args['coverage'];
        var rates = args['rates'];
        var overridePremiumRate = args['overridePremiumRate'];

        var rounding = this.self.rounding(args);
        var rateUnit = rates.rateUnit;
        var modalFactor = rates.modalFactor;
        var occupationFactor = rates.occupationFactorRate;

        var rate = overridePremiumRate;
        if (!(rate)) {
            rate = rates.premiumRate;
        }

        if (occupationFactor) {
            rate = _R(rate * occupationFactor, rounding);
        }

        var modalRate = _R(rate * modalFactor, rounding);
        if (coverage.catalog.call("hasTaxCharge", null)) {
            modalRate = _R(modalRate * PublicConstants.COUNTRY.TAX_RATE, rounding);
        }

        var premium = _R(coverage.faceAmount * modalRate / rateUnit, rounding);
        return premium;
    },
    calculateExtraPremium: function(args) {
        var coverage = args['coverage'];
        var rates = args['rates'];
        var policyYear = args['policyYear'];

        var rateUnit = rates.rateUnit;
        var rate = rates.premiumRate;
        var modalFactor = rates.modalFactor;

        var xpremium = 0;
        var premiumX = {};
        if (coverage.extraRating) {
            if (coverage.extraRating.tempFlat) {
                if (coverage.extraRating.tempFlatDuration > policyYear) {
                    var args2 = Utils.extend(args, {
                        overridePremiumRate: coverage.extraRating.tempFlat
                    });

                    var p = this.calculate(args2);
                    premiumX = Utils.extend(premiumX, {
                        temporaryFlatExtra: p
                    });
                    xpremium += p;
                }
            }
            if (coverage.extraRating.flatExtra) {
                var args2 = Utils.extend(args, {
                    overridePremiumRate: coverage.extraRating.flatExtra
                });

                var p = this.calculate(args2);
                premiumX = Utils.extend(premiumX, {
                    permanentFlatExtra: p
                });
                xpremium += p;
            }
            if (coverage.extraRating.percentage) {
                if (coverage.extraRating.percentage.rate) {
                    var extraRate = _R(rate * (coverage.extraRating.percentage.rate - 1), rounding);
                    var args2 = Utils.extend(args, {
                        overridePremiumRate: extraRate
                    });

                    var p = this.calculate(args2);
                    premiumX = Utils.extend(premiumX, {
                        percentageExtra: p
                    });
                    xpremium += p;
                }
            }
        }
        return xpremium;
    }
});

var SPSM = Class.define({
   formulaName: function(args) {
      return 'SPSM';
   },
   productId: function(args) {
      var coverage = args['coverage'];
      return null;
   },
   methodId: function(args) {
      var coverage = args['coverage'];
      return null;
   },
   productTypes: function(args) {
      var coverage = args['coverage'];
      return [CatalogConstants.PRODUCT_TYPE.UVLIFE, CatalogConstants.PRODUCT_TYPE.SINGLE_PREMIUM];
   },
   rateList: function(args) {
      var coverage = args['coverage'];
      var ipo = args['ipo'];
      return [];
   },
   modalRateList: function(args) {
      var coverage = args['coverage'];
      var ipo = args['ipo'];
      return [];
   },
   rounding: function(args) {
      var coverage = args['coverage'];
      return coverage.catalog.call("rounding", {coverage: coverage});
   },
   calculate: function(args) {
      var coverage = args['coverage'];
      var rounding = this.self.rounding(args);
      return _R(coverage.initialDumpIn, rounding);
   },
   calculateExtraPremium: function(args) {
      //return {temporaryFlatExtra : 0, permanentFlatExtra: 0, percentageExtra: 0};
      return 0;
   }
});
var CommonBpm = Class.define({
   init: function(args) {
      this.self.formula = args['formula'];
      this.self.premiums = [];
      this.self.extraPremiums = [];
      this.self.extraPremiumDetails = [];
      this.self.totalPremiums = [];
   },
   readModalFactors: function(args) {
      var coverage = args['coverage'];
      //var policyYear = args['policyYear'];
      var paymentMode = args['paymentMode'];
      var ipo = args['ipo'];
      var rateType = args['rateType'];

      var _paymentMode = coverage.options.paymentMode;
      if (paymentMode) {
         coverage.options.paymentMode = paymentMode;
      }
      var PEratemanager = Repository.getManager(SystemComponents.MANAGER.RATE_MANAGER);
      var _rates = IRateManager.appliedTo(PEratemanager).call("lookupRates",
         {coverage: coverage, rateType: rateType, ipo: ipo});
      coverage.options.paymentMode = _paymentMode;
      return _rates;
   },
   prepareRates: function(args) {
      var coverage = args['coverage'];
      var ipo = args['ipo'];
      var policyYear = args["policyYear"];
      // prepare annualize rates
      // hasRate, rateType, variableName, policyYear, callback
      var currentYearRates = {};

      var rateList = IBasicPremiumFormula.appliedTo(this.self.formula).call('rateList', {coverage: coverage, ipo: ipo});
      for(rateIndex in rateList) {
         var rateRequired = rateList[rateIndex];

         //if (coverage.proposal.enableDebug) {
         _DB('prepareRates requesting rate: ', rateRequired, 'CommonBpm ->');
         //}

         var rateName = rateRequired.rateName;
         var rateType = rateRequired.rateType;
         var rateUnit = rateRequired.unit;
         var hasRate = rateRequired.hasRate;

         if (hasRate) {
            var rates = this.self.loadSingleSetRates(
               {
                  coverage: coverage,
                  ipo: ipo,
                  rateType: rateType,
                  callback: function(rates) {
                     if (rateUnit) {
                        currentYearRates["rateUnit"] = rates.rateUnit();
                     }
                  }
               }
            );
            if (rates) {
               currentYearRates[rateName] = rates.rates[0];
               //if (coverage.proposal.enableDebug) {
               _DB('prepareRates(' + rateType + '): ', currentYearRates[rateName], 'CommonBpm ->')
               //}
            }
         }
      }
      return currentYearRates;
   },
   prepareModalRates: function(args) {
      // e.g. modal factor, policy fee
      // mode = A, S, Q, M
      // console.log("prepareModalRates: mode = " + mode);

      // policyYear, mode, coverage, ipo
      var coverage = args['coverage'];
      var ipo = args['ipo'];
      var policyYear = args['policyYear'];
      var mode = args['mode'];

      var modalRates = {};

      var rateList = IBasicPremiumFormula.appliedTo(this.self.formula).call('modalRateList', {coverage: coverage, ipo: ipo});
      for(rateIndex in rateList) {
         var rateRequired = rateList[rateIndex];

         //if (coverage.proposal.enableDebug) {
         _DB('prepareModalRates requesting rate: ', rateRequired, 'CommonBpm ->');
         //}

         var rateName = rateRequired.rateName;
         var rateType = rateRequired.rateType;
         var rateUnit = rateRequired.unit;
         var hasRate = rateRequired.hasRate;

         if (hasRate) {
            var rates = this.self.readModalFactors({
               coverage: coverage,
               paymentMode: mode,
               rateType: rateType,
               ipo: ipo
            });
            if (rates) {
               modalRates[rateName] = rates.rates[0];
               //if (coverage.proposal.enableDebug) {
               _DB('prepareModalRates(' + rateType + '): ', modalRates[rateName], 'CommonBpm ->')
               //}
            }
         }
      }
      return modalRates;
   },
   loadSingleSetRates: function(args) {
      // hasRate, rateType, variableName, policyYear, callback
      var coverage = args['coverage'];
      var ipo = args['ipo'];
      var rateType = args['rateType'];
      var callback = args['callback'];
      //var hasRate = args['hasRate'];
      //var variableName = args['variableName'];
      //var policyYear = args['policyYear'];

      var PEratemanager = Repository.getManager(SystemComponents.MANAGER.RATE_MANAGER);
      var _rates = IRateManager.appliedTo(PEratemanager).call("lookupRates",
         {coverage: coverage, rateType: rateType, ipo: ipo});
      if (callback) callback(_rates);
      return _rates;
   },
   createPremiumResults: function(args) {
      /*
      var premiumResult = args["premiumResult"];
      var extraPremiumResult = args["extraPremiumResult"];
      var extraPremiumResultDetails = args["extraPremiumResultDetails"];
      var totalPremiumResult = args["totalPremiumResult"];

      this.self.premiums = premiumResult;
      this.self.extraPremiums = extraPremiumResult;
      this.self.extraPremiumDetails = extraPremiumResultDetails;
      this.self.totalPremiums = totalPremiumResult;

      var consolidatedPremiums = {
         premiums: this.self.premiums,
         extraPremiums: this.self.extraPremiums,
         extraPremiumDetails: this.self.extraPremiumDetails,
         totalPremiums: this.self.totalPremiums
      };
      */
      var consolidatedPremiums = {};
      for( var premKey in args ) {
          this.self[premKey] = args[premKey];
          consolidatedPremiums[premKey] = args[premKey];
      }
      console.log("Total Premiums = " + JSON.stringify(consolidatedPremiums));
      return consolidatedPremiums;
   },
   formulaName: function(args) {
      return IBasicPremiumFormula.appliedTo(this.self.formula).call('formulaName', {});
   },
   formulaScope: function(args) {
      var coverage = args['coverage'];
      var methodId = IBasicPremiumFormula.appliedTo( this.self.formula ).call('methodId', {coverage: coverage});
      var productId = IBasicPremiumFormula.appliedTo( this.self.formula ).call('productId', {coverage: coverage});
      var productTypes = IBasicPremiumFormula.appliedTo( this.self.formula ).call('productTypes', {coverage: coverage});
      return {methodId: methodId, productId: productId, productTypes: productTypes};
   },
   calculate: function(args) {
      var policyYear = args["policyYear"];
      var coverage = args['coverage'];
      var context = args['context'];
      var ipo = args['ipo'];
      //var rounding = args['rounding'];

      // prepare all necessary rates
      var currentYearRates = this.self.prepareRates({coverage: coverage, ipo: ipo, policyYear : policyYear});

      var premiumResult = [];
      var extraPremiumResult = [];
      var extraPremiumResultDetails = [];
      var totalPremiumResult = [];

      var premiumModes = coverage.schema.call("availablePaymentModes",
         {premiumType: SchemaConstants.PREMIUM_CATEGORY.BASIC});
      //console.log(JSON.stringify(premiumModes));
      if (Utils.isEmptyObject( premiumModes )) {
         _DB("calculate.paymentModes: ", SchemaConstants.PREMIUM_CATEGORY.BASIC + " cannot be found. Try seach " + SchemaConstants.PREMIUM_CATEGORY.PLANNED + ".", "CommonBpm->");
         premiumModes = coverage.schema.call("availablePaymentModes",
             {premiumType: SchemaConstants.PREMIUM_CATEGORY.PLANNED});
      }
      if (Utils.isEmptyObject( premiumModes )) {
         _DB("calculate.paymentModes: ", SchemaConstants.PREMIUM_CATEGORY.PLANNED + " cannot be found. Try seach " + SchemaConstants.PREMIUM_CATEGORY.BASICONLY + ".", "CommonBpm->");
         premiumModes = coverage.schema.call("availablePaymentModes",
             {premiumType: SchemaConstants.PREMIUM_CATEGORY.BASICONLY});
      }
      //if (coverage.proposal.enableDebug) {
      _DB("calculate.premiumModes: ", premiumModes, "CommonBpm->");
      //}

      if ( Utils.isNothing( _PV( this.self.formula, "calculateCompletePremiumRecords" ) ) ) {
         for (modeKey in premiumModes) {
            var mode = premiumModes[modeKey];

            // console.log("mode = " + mode);
            var totalPremium = 0;
            currentYearRates = Utils.extend(currentYearRates, this.self.prepareModalRates({coverage: coverage, ipo: ipo, policyYear: policyYear, mode: mode }));

            //console.log("normal premium start");
            var premium = IBasicPremiumFormula.appliedTo(this.self.formula).call(
               'calculate', {
                  coverage: coverage,
                  rates: currentYearRates,
                  policyYear: policyYear,
                  paymentMode: mode
               });
            _DB("premium[" + mode + "]", premium, "CommonBpm->");
            //console.log("premium = " + JSON.stringify(premium));
            //console.log("normal premium end");

            premiumResult.push({
               paymentMode: mode,
               premium: premium
            });
            totalPremium += premium;

            if (coverage.extraRating) {
               //console.log("extraRating start");
               //var extraPremiumDetails = this.self.formulaX(policyYear);

               //var extraPremiumDetails = IBasicPremiumFormula.appliedTo(this.self.formula).call(
               var extra = IBasicPremiumFormula.appliedTo(this.self.formula).call(
                  'calculateExtraPremium', {
                     coverage: coverage,
                     rates: currentYearRates,
                     policyYear: policyYear,
                     paymentMode: mode
                  });

               /*
               extraPremiumResultDetails.push({
                  paymentMode: mode,
                  extraPremiumDetails: extraPremiumDetails
               });
               var extra = 0;
               if (extraPremiumDetails.temporaryFlatExtra) {
                  extra += extraPremiumDetails.temporaryFlatExtra;
               }
               if (extraPremiumDetails.permanentFlatExtra) {
                  extra += extraPremiumDetails.permanentFlatExtra;
               }
               if (extraPremiumDetails.percentageExtra) {
                  extra += extraPremiumDetails.percentageExtra;
               }
               */

               if (!Utils.isNothing(extra)) {
                  extraPremiumResult.push({
                     paymentMode: mode,
                     extraPremium: extra
                  });
                  totalPremium += (extra);
               }
               _DB("extraPremium[" + mode + "]", extra, "CommonBpm->");
               //console.log("extraPremium (" + extra + ") = " + JSON.stringify(extraPremiumDetails));
               //console.log("extraRating end");
            }

            totalPremiumResult.push({
               paymentMode: mode,
               totalPremium: totalPremium
            });
         }

         return this.self.createPremiumResults({
            premiums: premiumResult,
            extraPremiums: extraPremiumResult,
            extraPremiumDetails: extraPremiumResultDetails,
            totalPremiums: totalPremiumResult
         })
      } else {
         premiumResult = {};
         for (modeKey in premiumModes) {
            var mode = premiumModes[modeKey];

            // console.log("mode = " + mode);
            var totalPremium = 0;
            currentYearRates = Utils.extend(currentYearRates, this.self.prepareModalRates({coverage: coverage, ipo: ipo, policyYear: policyYear, mode: mode }));

            var modePremiums = IBasicPremiumFormula.appliedTo(this.self.formula).call(
               'calculateCompletePremiumRecords', {
                  coverage: coverage,
                  rates: currentYearRates,
                  policyYear: policyYear,
                  paymentMode: mode
               });
            // e.g. of modePremiums
            // { premium: 100, extraPremium: 0 }

            if (coverage.proposal.enableDebug) {
               console.log("[ " + mode + " Premiums ]");
               _SS(modePremiums);
            }

            for(var premKey in modePremiums) {
               if (Utils.isNothing(premiumResult[premKey + 's'])) {
                  premiumResult[premKey + 's'] = [];
               }

               var premRec = premiumResult[premKey + 's'];
               var rec = {};
               rec['paymentMode'] = mode;
               rec[premKey] = modePremiums[premKey];
               premRec.push( rec );
            }
            // e.g. of premiumResult
            // { premiums: [ { paymentMode: 'Annual', premium: 100 } ]
         }
         return this.self.createPremiumResults(premiumResult);
      }
   }
});
var CatalogImpl = Class.define({

    init: function(args) {
        var _catalog = args["catalogDefintion"];
        if (Utils.isNothing(_catalog)) {
            console.log("ERROR: Catalog is initialized by a empty definition.");
        } else {
            this.self.catalog = _catalog;
        }
    },

    getCatalog: function(args) {
        return this.self.catalog;
    },

    lookupSchema: function(args) {
        var coverage = args['coverage'];
        var debugMode = coverage.proposal.enableDebug;

        var _proposal = coverage.proposal;
        var _coverage = coverage;

        var conditions = [{
            catalogField: "Location",
            requestField: (_coverage.product.productKey.location)
        }, {
            catalogField: "BasicProductId",
            requestField: _coverage.product.productKey.associateProduct.productPK.productId
        }];

        var matchSchema = function(dtl, cvg) {
            if (cvg.product.productKey.valueDate < dtl.EffectiveDate.text) {
                return false;
            }
            for (condKey in conditions) {
                var cond = conditions[condKey];

                //if (debugMode) {
                _DB('schema record: ', dtl, 'matchSchema->');
                //}
                if (dtl[cond.catalogField].text != WILDCARD) {
                    if (cond.requestField) {
                        if (cond.requestField.toLowerCase() != dtl[cond.catalogField].text.toLowerCase()) {
                            //if (debugMode) {
                            _DB('>>> match field (' + cond.catalogField + ') - NOT MATCH: ', cond.requestField.toLowerCase() + ' vs ' + dtl[cond.catalogField].text.toLowerCase() + '=>' + (cond.requestField.toLowerCase() == dtl[cond.catalogField].text.toLowerCase()), 'matchSchema->');
                            //}
                            return false;
                        }
                    } else {
                        //if (debugMode) {
                        _DB('>>> match field (' + cond.catalogField + ') - MISSING FIELD: ', dtl[cond.catalogField].text.toLowerCase(), 'matchSchema->');
                        //}
                        return false;
                    }
                } else {
                    //if (debugMode) {
                    _DB('>>> match field (' + cond.catalogField + ') - SKIP: ', dtl[cond.catalogField].text, 'matchSchema->');
                    //}
                }
            }
            return true;
        };

        var schemaInfo = null;
        var iter = Utils.iterator(this.self.catalog.ProductCatalog.ProductCatalogDetail.CatalogRecord);
        for (key in iter) {
            dtl = iter[key];
            if (matchSchema(dtl, _coverage)) {
                schemaInfo = {
                    "schemaId": dtl.SchemaId
                };
            }
        }

        //if (debugMode) {
        _DB('schema located: ', schemaInfo, 'matchSchema->');
        //}
        return schemaInfo;
    },

    isUVL: function() {
        return Utils.matchInList(CatalogConstants.PRODUCT_TYPE.UVLIFE, this.self.catalog.ProductCatalog.ProductType.ProductTypeCode, false);
    },

    isSinglePremium: function() {
        return Utils.matchInList(CatalogConstants.PRODUCT_TYPE.SINGLE_PREMIUM, this.self.catalog.ProductCatalog.ProductType.ProductTypeCode, false);
    },

    isJoinLife: function() {
        return Utils.matchInList(CatalogConstants.PRODUCT_TYPE.JOINLIFE, this.self.catalog.ProductCatalog.ProductType.ProductTypeCode, false);
    },

    isRegularUVLPlans: function(args) {
        var planCode = args['productCode'];

        // TODO - remove those hardcoded plans by refactoring
        var planCodes = "|UN105|UN110|UN199|UN205|UN210|UN299|UO105|UO110|UO199|UO205|UO210|UO299" +
            "|UH105|UH110|UH199|UH205|UH210|UH299|UI105|UI110|UI199|UI205|UI210|UI299" +
            "|CE105|CE110|CA199|CB199|CC199|CE205|CE210|CA299|CB299|CC299" +
            "|DE105|DE110|DA199|DB199|DC199|DE205|DE210|DA299|DB299|DC299|";
        return (planCodes.indexOf("|" + planCodes + "|") > 0);
    },

    hasTaxCharge: function() {
        return Utils.matchInList(CatalogConstants.PRODUCT_TYPE.TAXCHARGE, this.self.catalog.ProductCatalog.ProductType.ProductTypeCode, false);
    },

    isBenefit: function() {
        return Utils.matchInList(CatalogConstants.PRODUCT_TYPE.BENEFIT, this.self.catalog.ProductCatalog.ProductType.ProductTypeCode, false);
    },

    rounding: function(args) {
        var coverage = args["coverage"];
        if (coverage != coverage.proposal.coverageInfo) {
            var basePlanCatalog = coverage.proposal.coverageInfo.catalog.call("getCatalog", {});
            if (Utils.matchInList(CatalogConstants.PRODUCT_TYPE.UVLIFE, basePlanCatalog.ProductCatalog.ProductType.ProductTypeCode, false) &&
                Utils.matchInList(CatalogConstants.PRODUCT_TYPE.REGULAR_SAVING, basePlanCatalog.ProductCatalog.ProductType.ProductTypeCode, false)) {
                if (!this.self.isRegularUVLPlans({productCode: coverage.product.productKey.primaryProduct.productPK.productId})) {
                    return PublicConstants.ROUNDING.REGULAR_SAVING_UVLIFE;
                }
            }
        } else {
            if (Utils.matchInList(CatalogConstants.PRODUCT_TYPE.UVLIFE, this.self.catalog.ProductCatalog.ProductType.ProductTypeCode, false) &&
                Utils.matchInList(CatalogConstants.PRODUCT_TYPE.REGULAR_SAVING, this.self.catalog.ProductCatalog.ProductType.ProductTypeCode, false)) {
                return PublicConstants.ROUNDING.REGULAR_SAVING_UVLIFE;
            }
        }
        return PublicConstants.ROUNDING.DEFAULT;
    },

    isCoupon: function() {
        return Utils.matchInList(CatalogConstants.PRODUCT_TYPE.COUPON, this.self.catalog.ProductCatalog.ProductType.ProductTypeCode, false);
    },

    isFundCodeSupported: function(args) {
        var fund = args["fund"];
        if (Utils.isNothing(this.self.catalog.ProductCatalog.FundMapInformation.FundCode)) {
            return false;
        }
        var fundTable = Utils.iterator(this.self.catalog.ProductCatalog.FundMapInformation.FundCode);
        //console.log("this.self.catalog.ProductCatalog.FundMapInformation.FundCode = " + this.self.catalog.ProductCatalog.FundMapInformation.FundCode);
        //console.log("this.self.catalog.ProductCatalog.FundMapInformation.FundCode.length = " + this.self.catalog.ProductCatalog.FundMapInformation.FundCode.length);
        //console.log("fundTable = " + JSON.stringify(fundTable));
        //console.log("fundTable.length = " + fundTable.length);
        if (fundTable.length > 0) {
            for (fundi in fundTable) {
                var fundRec = fundTable[fundi];
                if (fundRec.text.toLowerCase() == (fund).toLowerCase()) {
                    return true;
                }
            }
        }
        return false;
    },

    isSOS: function() {
        return Utils.matchInList(CatalogConstants.PRODUCT_TYPE.HAS_SOS, this.self.catalog.ProductCatalog.ProductType.ProductTypeCode, false);
    },

    isIncome3: function() {
        return Utils.matchInList(CatalogConstants.PRODUCT_TYPE.INCOME3, this.self.catalog.ProductCatalog.ProductType.ProductTypeCode, false);
    },

    isTerm: function() {
        return Utils.matchInList(CatalogConstants.PRODUCT_TYPE.TERM, this.self.catalog.ProductCatalog.ProductType.ProductTypeCode, false);
    },

    isRegularSaving: function() {
        return Utils.matchInList(CatalogConstants.PRODUCT_TYPE.REGULAR_SAVING, this.self.catalog.ProductCatalog.ProductType.ProductTypeCode, false);
    },

    isParticipate: function() {
        return Utils.matchInList(CatalogConstants.PRODUCT_TYPE.PARTICIPATE, this.self.catalog.ProductCatalog.ProductType.ProductTypeCode, false);
    },

    isGuaranteedIssue: function() {
        return Utils.matchInList(CatalogConstants.PRODUCT_TYPE.GUARANTEED_ISSUE, this.self.catalog.ProductCatalog.ProductType.ProductTypeCode, false);
    },

    isConditionalGuaranteedIssue: function() {
        return Utils.matchInList(CatalogConstants.PRODUCT_TYPE.CONDITIONAL_GUARANTEED_ISSUE, this.self.catalog.ProductCatalog.ProductType.ProductTypeCode, false);
    },
    isFamilyProductType: function() {
        return Utils.matchInList(CatalogConstants.PRODUCT_TYPE.FAMILY, this.self.catalog.ProductCatalog.ProductType.ProductTypeCode, false);
    },
    isSpouseProductType: function() {
        return Utils.matchInList(CatalogConstants.PRODUCT_TYPE.SPOUSE, this.self.catalog.ProductCatalog.ProductType.ProductTypeCode, false);
    },
    isChildProductType: function() {
        return Utils.matchInList(CatalogConstants.PRODUCT_TYPE.CHILD, this.self.catalog.ProductCatalog.ProductType.ProductTypeCode, false);
    },
    isRiderProductType: function() {
        return Utils.matchInList(CatalogConstants.PRODUCT_TYPE.RIDER, this.self.catalog.ProductCatalog.ProductType.ProductTypeCode, false);
    }
});
var SchemaImpl = Class.define({

   init: function(args) {
      var _schema = args['schemaDefinition'];
      this.self.schema = _schema;
   },

   getSchema: function() {
      return this.self.schema;
   },

   isRefundPremiumSupported: function() {
      if (Utils.isNothing( _PV(this.self.schema, 'ProductSchema.PolicyValue.PolicyValueTable') )) {
         return false;
      }
      var pvTable = Utils.iterator(this.self.schema.ProductSchema.PolicyValue.PolicyValueTable);
      for (pvi in pvTable) {
         var pv = pvTable[pvi];
         if (pv.PolicyValueType.text.toLowerCase() == SchemaConstants.POLICY_VALUE.RETURNPREMIUM.toLowerCase()) {
            return true;
         }
      }
      return false;
   },

   isChargeTypeSupported: function(args) {
      var chargeType = args['chargeType'];
      if (!Utils.isNothing(chargeType)) {
         if (Utils.isNothing( _PV(this.self.schema, 'ProductSchema.ChargeInfo.ChargeScale') )) {
            return false;
         }
         var chgTable = Utils.iterator(this.self.schema.ProductSchema.ChargeInfo.ChargeScale);
         //console.log("chgTable = " + JSON.stringify(chgTable));
         //console.log("chgTable.length = " + chgTable.length);
         for (chgi in chgTable) {
            //console.log("chgi = " + chgi);
            //console.log("chgTable[" + chgi + "] = " + chgTable[chgi]);
            var charge = chgTable[chgi];
            for (chgti in charge.ChargeType) {
               var chgType = charge.ChargeType[chgti];
               if (!Utils.isNothing(chgType) && !Utils.isNothing(chgType.text)) {
                  if (chgType.text.toLowerCase() == chargeType.toLowerCase()) {
                     return true;
                  }
               }
            }
         }
      }
      return false;
   },

   isCurrencySupported: function(args) {
      var currencyId = args['currencyId'];
      if (Utils.isNothing( _PV(this.self.schema, 'ProductSchema.PremiumInfo.PremiumTable') )) {
         return false;
      }
      var premiumTable = Utils.iterator(this.self.schema.ProductSchema.PremiumInfo.PremiumTable);
      for (premiumKey in premiumTable) {
         var premiumRec = premiumTable[premiumKey];
         if (currencyId == premiumRec.CurrencyId.text) {
            return true;
         }
      }
      return false;
   },

   hasSupportOption: function(args) {
      var option = args['option'];
      if (Utils.isNothing(option)) return false;
      if (Utils.isNothing( _PV(this.self.schema, 'ProductSchema.BasicParticular.SupportOption') )) {
         return false;
      }
      var opt = this.self.schema.ProductSchema.BasicParticular.SupportOption[option];
      if (opt) {
         if (opt.text.toLowerCase() == "y") {
            return true;
         } else {
            return false;
         }
      } else {
         return false;
      }
   },

   calculateBanding: function(args) {
      var coverage = args['coverage'];
      var insuredAge = args['insuredAge'];

      var showLog = coverage.proposal.enableDebug;

      // if no insured age is passed in, use primary insured age
      if (Utils.isNothing(insuredAge)) {
         var insured = getPrimaryInsured({coverage: coverage});
         if (!Utils.isNothing(insured)) {
             insuredAge = insured.insuredAge;
         }
      }
      // ----

      if (Utils.isNothing( _PV(this.self.schema, 'ProductSchema.BandInformation.BandRecord') )) {
         return null;
      }

      var banding = null;
      var bandTable = Utils.iterator(this.self.schema.ProductSchema.BandInformation.BandRecord);
      for (bandKey in bandTable) {
         var band = bandTable[bandKey];
         var matched = true;

         if (showLog) console.log("calculateBanding - begin");

         matched = matched && Utils.matchRange(coverage.plannedPremium, Number(band.MinContractPremium.text), Number(band.MaxContractPremium.text), true);
         if (showLog) {
             console.log("1. contract premium - matched = " + matched);
             if (!matched) {
                console.log("coverage.plannedPremium = " + coverage.plannedPremium);
                console.log("band.MinContractPremium.text = " + band.MinContractPremium.text);
                console.log("band.MaxContractPremium.text = " + band.MaxContractPremium.text);
             }
          }
         matched = matched && Utils.matchRange(coverage.faceAmount, Number(band.MinFaceAmount.text), Number(band.MaxFaceAmount.text), true);
         if (showLog) {
             console.log("2. face amount - matched = " + matched);
             if (!matched) {
                console.log("coverage.faceAmount = " + coverage.faceAmount);
                console.log("band.MinFaceAmount.text = " + band.MinFaceAmount.text);
                console.log("band.MaxFaceAmount.text = " + band.MaxFaceAmount.text);
             }
         }
         matched = matched && (coverage.currency.currencyPK.currencyId == band.CurrencyPK.CurrencyId.text);
         if (showLog) {
             console.log("3. currency - matched = " + matched);
             if (!matched) {
                console.log("coverage.currency.currencyPK.currencyId = " + coverage.currency.currencyPK.currencyId);
             }
         }
         matched = matched && Utils.matchRange(insuredAge, Number(band.MinIssueAge.text), Number(band.MaxIssueAge.text), true);
         if (showLog) {
             console.log("4. issue age - matched = " + matched);
             if (!matched) {
                console.log("coverage.parties.party.insuredAge = " + insuredAge);
                console.log("band.MinIssueAge.text = " + band.MinIssueAge.text);
                console.log("band.MaxIssueAge.text = " + band.MaxIssueAge.text);
             }
         }
         matched = matched && Utils.matchRange(coverage.topUpPremium, Number(band.MinTopup.text), Number(band.MaxTopup.text), true);
         if (showLog) {
             console.log("5. topup amount - matched = " + matched);
             if (!matched) {
                console.log("coverage.topUpPremium = " + coverage.topUpPremium);
                console.log("band.MinTopup.text = " + band.MinTopup.text);
                console.log("band.MaxTopup.text = " + band.MaxTopup.text);
             }
         }
         if (!Utils.isNothing(band.MinSinglePremium) && !Utils.isNothing(band.MaxSinglePremium)) {
            matched = matched && Utils.matchRange(coverage.initialDumpIn, Number(band.MinSinglePremium.text), Number(band.MaxSinglePremium.text), true);
            if (showLog) {
                console.log("6. single premium - matched = " + matched);
                if (showLog) {
                   console.log("coverage.initialDumpIn = " + coverage.initialDumpIn);
                   console.log("band.MinSinglePremium.text = " + band.MinSinglePremium.text);
                   console.log("band.MaxSinglePremium.text = " + band.MaxSinglePremium.text);
                }
            }
         }
         if (showLog) console.log("calculateBanding - end");
         if (matched) {
            banding = band;
            break;
         }
      }
      return banding;
   },

   matchPremiumInfoRecord: function(args) {
      var coverage = args['coverage'];
      var banding = args['banding'];
      var premiumType = args['premiumType'];

      //console.log("matchPremiumInfoRecord - begin");
      //console.log(JSON.stringify(banding));
      //console.log("matchPremiumInfoRecord - end");

      if (Utils.isNothing(banding)) return null;
      if (Utils.isNothing(premiumType)) return null;
      if (Utils.isNothing( _PV(this.self.schema, 'ProductSchema.PremiumInfo.PremiumTable') )) {
         return null;
      }

      var premInfoRec = null;
      var premTable = Utils.iterator(this.self.schema.ProductSchema.PremiumInfo.PremiumTable);
      for (recKey in premTable) {
         var premRec = premTable[recKey];

         var matched = true;
         matched = matched && Utils.matchInList(premiumType, premRec.PremiumType, false);
         matched = matched && (banding == premRec.Band.text);
         matched = matched && (coverage.currency.currencyPK.currencyId == premRec.CurrencyId.text);
         if (matched) {
            premInfoRec = premRec;
            break;
         }
      }
      if (premInfoRec == null) {
         for (recKey in premTable) {
            var premRec = premTable[recKey];

            var matched = true;
            matched = matched && Utils.matchInList(premiumType, premRec.PremiumType, false);
            matched = matched && (coverage.currency.currencyPK.currencyId == premRec.CurrencyId.text);
            if (matched) {
               premInfoRec = premRec;
               break;
            }
         }
      }
      return premInfoRec;
   },

   matchChargeInfoRecord: function(args) {
      var coverage = args['coverage'];
      var banding = args['banding'];
      var chargeType = args['chargeType'];

      if (Utils.isNothing(banding)) return null;
      if (Utils.isNothing(chargeType)) return null;
      if (Utils.isNothing( _PV(this.self.schema, 'ProductSchema.ChargeInfo.ChargeScale') )) {
         return null;
      }

      var chargeInfoRec = null;
      var chargeTable = Utils.iterator(this.self.schema.ProductSchema.ChargeInfo.ChargeScale);
      for (recKey in chargeTable) {
         var chargeRec = chargeTable[recKey];

         var matched = true;
         matched = matched && Utils.matchInList(chargeType, chargeRec.ChargeType, false);
         matched = matched && (banding == chargeRec.Band.text || chargeRec.Band.text == "0");
         matched = matched && (coverage.currency.currencyPK.currencyId == chargeRec.CurrencyId.text);
         if (matched) {
            chargeInfoRec = chargeRec;
            break;
         }
      }
      return chargeInfoRec;
   },

   matchPolicyValueInfoRecord: function(args) {
      var coverage = args['coverage'];
      var banding = args['banding'];
      var policyValueType = args['policyValueType'];

      if (Utils.isNothing(banding)) return null;
      if (Utils.isNothing(policyValueType)) return null;
      if (Utils.isNothing( _PV(this.self.schema, 'ProductSchema.PolicyValue.PolicyValueTable') )) {
         return null;
      }

      var policyValueRecord = null;
      var policyValueTable = Utils.iterator(this.self.schema.ProductSchema.PolicyValue.PolicyValueTable);
      for (recKey in policyValueTable) {
         var pvRec = policyValueTable[recKey];

         var matched = true;
         matched = matched && Utils.matchInList(policyValueType, pvRec.PolicyValueType, false);
         matched = matched && (banding == pvRec.Band.text || pvRec.Band.text == "0");
         matched = matched && (coverage.currency.currencyPK.currencyId == pvRec.CurrencyId.text);
         if (matched) {
            policyValueRecord = pvRec;
            break;
         }
      }
      return policyValueRecord;
   },

   matchProductionCreditInfoRecord: function(args) {
      var coverage = args['coverage'];
      var banding = args['banding'];

      if (Utils.isNothing(banding)) return null;
      if (Utils.isNothing( _PV(this.self.schema, 'ProductSchema.ProductionCredit.PCScale') )) {
         return null;
      }

      var pcRec = null;
      var pcTable = Utils.iterator( this.self.schema.ProductSchema.ProductionCredit.PCScale );
      for (recKey in pcTable) {
         var pc = pcTable[recKey];

         var matched = true;
         matched = matched && (banding == pc.Band.text || pc.Band.text == "0");
         matched = matched && (coverage.proposal.channel == pc.Channel.text || pc.Channel.text == "");
         matched = matched && (coverage.currency.currencyPK.currencyId == pc.CurrencyId.text);
         if (matched) {
            pcRec = pc;
            break;
         }
      }
      return pcRec;
   },

   readRefundRates: function(args) {
      var coverage = args['coverage'];
      var policyYear = args['policyYear'];
      var ipo = args['ipo'];

      var refundRates = null;
      if (coverage.occupation) {
         if (coverage.occupation == SchemaConstants.OCCUPATION_CLASS.CLASS_4) {
            var coverageCopy = Utils.extend({}, coverage);
            coverageCopy.occupation == SchemaConstants.OCCUPATION_CLASS.CLASS_0;
            refundRates = this.self.readPremiumRates(coverageCopy, policyYear, ipo);
         } else {
            refundRates = this.self.readPremiumRates(coverage, policyYear, ipo);
         }
      }
      return refundRates;
   },

   isCurrencyPaymentModeSupported: function(args) {
      var currencyId = args['currencyId'];
      var paymentMode = args['paymentMode'];

      if (Utils.isNothing(currencyId)) return false;
      if (Utils.isNothing(paymentMode)) return false;
      if (Utils.isNothing( _PV(this.self.schema, 'ProductSchema.BasicParticular.PremiumRangeInformation.PremiumRange') )) {
         return false;
      }

      var premiumRangeTable = Utils.iterator( this.self.schema.ProductSchema.BasicParticular.PremiumRangeInformation.PremiumRange );
      for (premiumKey in premiumRangeTable) {
         var premiumRangeRec = premiumRangeTable[premiumKey];
         //this.getPaymentModeFullName
         if ((currencyId == premiumRangeRec.CurrencyPK.CurrencyId.text) && ((paymentMode) == premiumRangeRec.PaymentMode.text)) {
            return true;
         }
      }
      return false;
   },

   readModalFactors: function(args) {
      var coverage = args['coverage'];
      var policyYear = args['policyYear'];
      var paymentMode = args['paymentMode'];
      var ipo = args['ipo'];

      var _paymentMode = coverage.options.paymentMode;
      if (paymentMode) {
         coverage.options.paymentMode = paymentMode;
      }
      var _rates = PEratemanager.lookupRates(coverage, RateConstants.RATETYPE.MODALFACTOR, policyYear, ipo);
      coverage.options.paymentMode = _paymentMode;
      return _rates;
   },

   isInsuredSexSupported: function(args) {
      var gender = args['gender'];
      if (Utils.isNothing(gender)) return false;
      if (Utils.isNothing( _PV(this.self.schema, 'ProductSchema.BasicParticular.SupportSex.Sex') )) {
         return false;
      }

      var genderTable = Utils.iterator( this.self.schema.ProductSchema.BasicParticular.SupportSex.Sex );
      for (genderi in genderTable) {
         var genderRec = genderTable[genderi];
         //this.getGenderFullName
         if (genderRec.text.toLowerCase() == (gender).toLowerCase()) {
            return true;
         }
      }
      return false;
   },

   isInsuredSmokingStatusSupported: function(args) {
      var smokingstatus = args['smokingStatus'];
      if (Utils.isNothing(smokingstatus)) return false;
      if (Utils.isNothing( _PV(this.self.schema, 'ProductSchema.BasicParticular.SupportSmokingStatus.SmokingStatus') )) {
         return false;
      }

      var smokingStatusTable = Utils.iterator( this.self.schema.ProductSchema.BasicParticular.SupportSmokingStatus.SmokingStatus );
      for (smokingStatusi in smokingStatusTable) {
         var smokingStatusRec = smokingStatusTable[smokingStatusi];
         //this.getSmokingStatusFullName
         if (smokingStatusRec.text.toLowerCase() == (smokingstatus).toLowerCase()) {
            return true;
         }
      }
      return false;
   },
   isWaitPeriodSupported: function(args) {
      var waitPeriod = args['waitPeriod'];
      if (Utils.isNothing(waitPeriod)) return false;
      if (Utils.isNothing( _PV(this.self.schema, 'ProductSchema.BasicParticular.SupportWaitingPeriod.WaitingPeriod') )) {
         return false;
      }

      var waitPeriodTable = Utils.iterator(this.self.schema.ProductSchema.BasicParticular.SupportWaitingPeriod.WaitingPeriod);
      for (waitPeriodi in waitPeriodTable) {
         var waitPeriodRec = waitPeriodTable[waitPeriodi];
         if (waitPeriodRec.text.toLowerCase() == (waitPeriod).toLowerCase()) {
            return true;
         }
      }
      return false;
   },
   isBenefitPeriodSupported: function(args) {
      var benefitPeriod = args['benefitPeriod'];
      if (Utils.isNothing(benefitPeriod)) return false;
      if (Utils.isNothing( _PV(this.self.schema, 'ProductSchema.BasicParticular.SupportBenefitPeriod.BenefitPeriod') )) {
         return false;
      }

      var benefitPeriodTable = Utils.iterator(this.self.schema.ProductSchema.BasicParticular.SupportBenefitPeriod.BenefitPeriod);
      for (benefitPeriodi in benefitPeriodTable) {
         var benefitPeriodRec = benefitPeriodTable[benefitPeriodi];
         if (benefitPeriodRec.text.toLowerCase() == (benefitPeriod).toLowerCase()) {
            return true;
         }
      }
      return false;
   },
   hasExtraRating: function(args) {
      var coverage = args['coverage'];
      var matched = true;
      matched = matched && (coverage.extraRating.flatExtra != "0.00");
      matched = matched && (coverage.extraRating.tempFlat != "0.00");
      matched = matched && (coverage.extraRating.tempFlatDur > 0);
      matched = matched && (coverage.extraRating.tempPercentDur > 0);
      matched = matched && (coverage.extraRating.percentageExtra > 1);
      matched = matched && (coverage.extraRating.tempPercent > 1);
      matched = matched && ("Y".equalsIgnoreCase(coverage.extraRating.exclution));
      return matched;
   },
   isOccupationSupported: function(args) {
      var occupation = args['occupation'];
      if (Utils.isNothing(occupation)) {
         return false;
      }
      if (Utils.isNothing(_PV( this.self.schema, 'ProductSchema.BasicParticular.SupportOccupationClass.OccupationClass') )) {
         return false;
      }
      var occupationTable = Utils.iterator(this.self.schema.ProductSchema.BasicParticular.SupportOccupationClass.OccupationClass);
      for (occupationi in occupationTable) {
         var occupationRec = occupationTable[occupationi];
         if (occupationRec.text.toLowerCase() == (occupation).toLowerCase()) {
            return true;
         }
      }
      return false;
   },
   isPremiumDiscountSupported: function(args) {
      var discountType = args['discountType'];
      if (Utils.isNothing(discountType)) {
         return false;
      }
      if (Utils.isNothing( _PV(this.self.schema, 'ProductSchema.BasicParticular.SupportPremiumDiscount.PremiumDiscount') )) {
         return false;
      }
      var discountTypeTable = Utils.iterator(this.self.schema.ProductSchema.BasicParticular.SupportPremiumDiscount.PremiumDiscount);
      for (discountTypei in discountTypeTable) {
         var discountTypeRec = discountTypeTable[discountTypei];
         if (discountTypeRec.text.toLowerCase() == (discountType).toLowerCase()) {
            return true;
         }
      }
      return false;
   },
   isBillingMethodPaymentModeSupported: function(args) {
      var billingMethod = args['billingMethod'];
      var paymentMode = args['paymentMode'];
      if (Utils.isNothing(paymentMode)) {
         return false;
      }
      if (Utils.isNothing(billingMethod)) {
         return false;
      }
      if (Utils.isNothing( _PV(this.self.schema, 'ProductSchema.BasicParticular.PremiumRangeInformation.PremiumRange') )) {
         return false;
      }
      var premiumRangeTable = Utils.iterator(this.self.schema.ProductSchema.BasicParticular.PremiumRangeInformation.PremiumRange);
      for (premiumKey in premiumRangeTable) {
         var premiumRangeRec = premiumRangeTable[premiumKey];
         //this.getPaymentModeFullName
         if ((Utils.matchInList(billingMethod, premiumRangeRec.BillingMode, false)) && (paymentMode.equalsIgnoreCase(premiumRangeRec.PaymentMode.text))) {
            return true;
         }
      }
      return false;
   },
   isCurrencyBillingMethodSupported: function(args) {
      var billingMethod = args['billingMethod'];
      var currencyId = args['currencyId'];
      if (Utils.isNothing(billingMethod)) {
         return false;
      }
      if (Utils.isNothing(currencyId)) {
         return false;
      }
      if (Utils.isNothing( _PV(this.self.schema, 'ProductSchema.BasicParticular.PremiumRangeInformation.PremiumRange') )) {
         return false;
      }
      var premiumRangeTable = Utils.iterator(this.self.schema.ProductSchema.BasicParticular.PremiumRangeInformation.PremiumRange);
      for (premiumKey in premiumRangeTable) {
         var premiumRangeRec = premiumRangeTable[premiumKey];
         //this.getPaymentModeFullName
         if ((currencyId.equalsIgnoreCase(premiumRangeRec.CurrencyPK.CurrencyId.text)) && (Utils.matchInList(billingMethod, premiumRangeRec.BillingMode, false))) {
            return true;
         }
      }
      return false;
   },

   calculatePremiumPayingPeriod: function(args) {
      var coverage = args['coverage'];
      var issueAge = args['issueAge'];

      if (Utils.isNothing(issueAge)) {
         var insured = getPrimaryInsured({coverage: coverage});
         if (!Utils.isNothing(insured)) {
            issueAge = insured.insuredAge;
         }
      }

      var maxPremiumPaidAge = Number(this.self.schema.ProductSchema.BasicParticular.PremiumPaidPeriod.MaxPremiumPaidAge.text);
      var minPremiumPaidPeriod = Number(this.self.schema.ProductSchema.BasicParticular.PremiumPaidPeriod.MinPremiumPaidPeriod.text);
      var premiumPayingPeriod = Number(maxPremiumPaidAge - issueAge);

      var maxDuration = Number(100);
      premiumPayingPeriod = Math.max(premiumPayingPeriod, minPremiumPaidPeriod);
      if ((premiumPayingPeriod + issueAge) > maxDuration) {
         premiumPayingPeriod = maxDuration - issueAge;
      }

      var payorAge = coverage.payerAge;
      if (payorAge) {
         var maxPayorAge = Number(this.self.schema.ProductSchema.BasicParticular.PremiumPaidPeriod.MaxPayorAge.text);
         if (payorAge) {
            var _payorAge = Number(payorAge);
            if (_payorAge > 0) {
               premiumPayingPeriod = Math.min((maxPayorAge - _payorAge), premiumPayingPeriod);
            }
         }
      }
      return premiumPayingPeriod;
   },

   isDividendOptionSupported: function(args) {
      var dividendOption = args['dividendOption'];
      if (Utils.isNothing(_PV( this.self.schema, 'ProductSchema.BasicParticular.SupportDividendOption.DividendOption'))) {
         return false;
      }
      var dividendOptionTable = Utils.iterator(this.self.schema.ProductSchema.BasicParticular.SupportDividendOption.DividendOption);
      for (dividendOptioni in dividendOptionTable) {
         var dividendOptionRec = dividendOptionTable[dividendOptioni];
         if (dividendOptionRec.text.toLowerCase() == (dividendOption).toLowerCase()) {
            return true;
         }
      }
      return false;
   },

   isDBOptionSupported: function(args) {
      var dbOption = args['dbOption'];
      if (Utils.isNothing(_PV( this.self.schema, 'ProductSchema.BasicParticular.SupportDeathBenefit.DeathBenefit'))) {
         return false;
      }
      var dbOptionTable = Utils.iterator(this.self.schema.ProductSchema.BasicParticular.SupportDeathBenefit.DeathBenefit);
      for (dbOptioni in dbOptionTable) {
         var dbOptionRec = dbOptionTable[dbOptioni];
         if (dbOptionRec.text.toLowerCase() == (dbOption).toLowerCase()) {
            return true;
         }
      }
      return false;
   },
   calculateBenefitPeriod: function(args) {
      var coverage = args['coverage'];
      var issueAge = args['issueAge'];

      if (Utils.isNothing(issueAge)) {
         var insured = getPrimaryInsured({coverage: coverage});
         if (!Utils.isNothing(insured)) {
            issueAge = insured.insuredAge;
         }
      }

      var maxBenefitAge = Number(this.self.schema.ProductSchema.BasicParticular.BenefitPeriod.MaxBenefitAge.text);
      var minBenefitPeriod = Number(this.self.schema.ProductSchema.BasicParticular.BenefitPeriod.MinBenefitPeriod.text);

      // console.log('issueAge = ' + issueAge);
      // console.log('maxBenefitAge = ' + maxBenefitAge);
      // console.log('minBenefitPeriod = ' + minBenefitPeriod);

      var benefitPeriod = Math.min(100, Math.max(maxBenefitAge - issueAge, minBenefitPeriod));
      // console.log('benefitPeriod = ' + benefitPeriod);

      if (coverage.catalog.call("isTerm")) {
         var renewalPeriod = this.self.schema.ProductSchema.BasicParticular.renewalPeriod;
         // console.log('renewalPeriod = ' + renewalPeriod);
         if (renewalPeriod) {
            var rp = Number(renewalPeriod.text);
            if (rp > 1) {
               benefitPeriod = benefitPeriod - (benefitPeriod % rp);
            }
         }
      }
      return benefitPeriod;
   },
   availablePaymentModes: function(args) {
      var premiumType = args["premiumType"];
      var schema = this.self.schema.ProductSchema;
      //console.log(JSON.stringify(schema));

      if (Utils.isNothing( _PV(schema, 'BasicParticular.PremiumRangeInformation.PremiumRange') )) {
         return {};
      }

      var premiumModes = {};
      var premiumIterator = Utils.iterator(schema.BasicParticular.PremiumRangeInformation.PremiumRange);
      for (pii in premiumIterator) {
         var pi = premiumIterator[pii];
         if (Utils.isNothing(premiumType) || (premiumType == pi.Type.text)) {
            premiumModes[pi.PaymentMode.text] = pi.PaymentMode.text;
         }
      }
      return premiumModes;
   },

   isIpoDBOptionSupported: function(args) {
      var ipoDbOption = args['ipoDbOption'];
      if (Utils.isNothing(ipoDbOption)) return false;
      if (Utils.isNothing(_PV( this.self.schema, 'ProductSchema.BasicParticular.SupportIPODeathBenefit.IPODeathBenefit'))) {
         return false;
      }
      var ipoDbOptionTable = Utils.iterator(this.self.schema.ProductSchema.BasicParticular.SupportIPODeathBenefit.IPODeathBenefit);
      for (ipoDbOptioni in ipoDbOptionTable) {
         var ipoDbOptionRec = ipoDbOptionTable[ipoDbOptioni];
         if (ipoDbOptionRec.text.toLowerCase() == (ipoDbOption).toLowerCase()) {
            return true;
         }
      }
      return false;
   },

   isMajorMedicalOptionSupported: function(args) {
      var majorMedicalOption = args['majorMedicalOption'];
      if (Utils.isNothing(majorMedicalOption)) return false;
      if (Utils.isNothing(_PV( this.self.schema, 'ProductSchema.BasicParticular.SupportOtherOptions.MajorMedical'))) {
         return false;
      }
      var majorMedicalTable = Utils.iterator(this.self.schema.ProductSchema.BasicParticular.SupportOtherOptions.MajorMedical);
      for (majorMedicali in majorMedicalTable) {
         var dividendOptionRec = majorMedicalTable[majorMedicali];
         if (dividendOptionRec.text.toLowerCase() == (majorMedicalOption).toLowerCase()) {
            return true;
         }
      }
      return false;
   },

   isNoOfPersonSupported: function(args) {
      var noOfPersonOption = args['noOfPersonOption'];
      if (Utils.isNothing(noOfPersonOption)) return false;
      if (Utils.isNothing(_PV( this.self.schema, 'ProductSchema.BasicParticular.SupportNoOfPersons.NoOfPerson'))) {
         return false;
      }
      var noOfPersonTable = Utils.iterator(this.self.schema.ProductSchema.BasicParticular.SupportNoOfPersons.NoOfPerson);
      for (noOfPersoni in noOfPersonTable) {
         var noOfPersonRec = noOfPersonTable[majorMedicali];
         if (noOfPersonRec.text.toLowerCase() == (noOfPersonOption).toLowerCase()) {
            return true;
         }
      }
      return false;
   },

   isClassOtherOptionSupported: function(args) {
      var classOtherOption = args['classOtherOption'];
      if (Utils.isNothing(classOtherOption)) return false;
      if (Utils.isNothing(_PV( this.self.schema, 'ProductSchema.BasicParticular.SupportOtherOptions.CoverageClass'))) {
         return false;
      }
      var classOtherOptionTable = Utils.iterator(this.self.schema.ProductSchema.BasicParticular.SupportOtherOptions.CoverageClass);
      for (classOtherOptioni in classOtherOptionTable) {
         var classOtherOptionRec = classOtherOptionTable[majorMedicali];
         if (classOtherOptionRec.text.toLowerCase() == (classOtherOption).toLowerCase()) {
            return true;
         }
      }
      return false;
   }
});
var eventAvyInitialization = Class.define({
   eventName: function(args) {
      // reset data for AVY beginning
      return "Initialize AVY";
   },
   run: function(args) {
      var context = args['context'];
      var proposal = context.proposal;
      var basePlan = proposal.coverageInfo;
      var metadata = context.events.metadata;
      var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
      //this.self.context['cashOutAccount'] = FundImpl.create();
      var runningPolicyValues = context.runningPolicyValues;
      //console.log("runningPolicyValues = " + (runningPolicyValues.fundPool));
      var year = runningPolicyValues.year;
      var months = runningPolicyValues.months;
      var month = months % 12;

      var fundConfig;
      fundConfig = {
         fundId: ""
      };
      fundConfig['annualInterestRate'] = {};
      fundConfig[PublicConstants.FUND_RETURN.LOW] = Number(0);
      fundConfig[PublicConstants.FUND_RETURN.MEDIUM] = Number(0);
      fundConfig[PublicConstants.FUND_RETURN.HIGH] = Number(0);
      fundConfig.annualInterestRate[PublicConstants.FUND_RETURN.LOW] = Number(0);
      fundConfig.annualInterestRate[PublicConstants.FUND_RETURN.MEDIUM] = Number(0);
      fundConfig.annualInterestRate[PublicConstants.FUND_RETURN.HIGH] = Number(0);
      fundConfig['policyOptions'] = context.policyOptions;

      if (months == 0) {
         if (!Utils.isNothing(proposal.funds.fundRecord)) {
            //runningPolicyValues['fundPool'] = FundPoolImpl.create({context : this.self.context});
            runningPolicyValues['fundPool'] = PortfolioImpl.create({
               context: context
            });
         }

         runningPolicyValues['totalPremiums'] = 0;
         runningPolicyValues['initialFaceAmountRatio'] = _R(basePlan.faceAmount / proposal.coverageInfo.initialDumpIn,2);
         console.log("initialFaceAmountRatio = " + runningPolicyValues.initialFaceAmountRatio);

         var fundSelections = Utils.iterator(proposal.funds.fundRecord);
         //console.log("fundSelections : " + JSON.stringify(fundSelections));
         for (fundId in fundSelections) {
            var fundRecord = fundSelections[fundId];
            //var fundRecord = proposal.funds.fundRecord[PublicConstants.FUND_CODE.TARGET_INCOME];
            if (fundRecord._code == PublicConstants.FUND_CODE.TARGET_INCOME && Number(fundRecord.allocation) > 0) {
               //console.log("fundRecord.returnRate[" + fundRecord._code + "] = " + JSON.stringify(fundRecord));
               fundConfig = {
                  fundId: fundRecord._code
               };
               if (fundRecord.returnRate > 0) {
                  fundConfig[PublicConstants.FUND_RETURN.LOW] = _R(Math.pow(1 + fundRecord.returnRate / 100, 1 / metadata.iterationUnit) - 1,15);
               }
               if (fundRecord.returnRateMedium > 0) {
                  fundConfig[PublicConstants.FUND_RETURN.MEDIUM] = _R(Math.pow(1 + fundRecord.returnRateMedium / 100, 1 / metadata.iterationUnit) - 1,15);
               }
               if (fundRecord.returnRateHigh > 0) {
                  fundConfig[PublicConstants.FUND_RETURN.HIGH] = _R(Math.pow(1 + fundRecord.returnRateHigh / 100, 1 / metadata.iterationUnit) - 1,15);
               }
               fundConfig['payoutRatio'] = fundRecord.targetPayoutRate;
               fundConfig['annualInterestRate'] = {};
               fundConfig.annualInterestRate[PublicConstants.FUND_RETURN.LOW] = fundRecord.returnRate;
               fundConfig.annualInterestRate[PublicConstants.FUND_RETURN.MEDIUM] = fundRecord.returnRateMedium;
               fundConfig.annualInterestRate[PublicConstants.FUND_RETURN.HIGH] = fundRecord.returnRateHigh;
               fundConfig['isSurrenderChargeApplicable'] = false;
            }
         }
         runningPolicyValues['cashoutAccount'] = FundImpl.create({
            fundConfig: fundConfig,
            context: context
         });
      }

      if (month == 0) {
         runningPolicyValues['initialDumpIn'] = 0;
         runningPolicyValues['newPremium'] = 0;
         runningPolicyValues['withdrawal'] = 0;
         runningPolicyValues['firstMonthCoi'] = Number(0);
         runningPolicyValues['cashOutPayments'] = {};
         runningPolicyValues['cois'] = {};
         runningPolicyValues['annualCois'] = {};
         runningPolicyValues['accumulatedcashOutPayments'] = {};

         for (returnTypeKey in PublicConstants.FUND_RETURN) {
            runningPolicyValues['cashOutPayments'][returnTypeKey] = Number(0);
            runningPolicyValues['cois'][returnTypeKey] = Number(0);
            runningPolicyValues['annualCois'][returnTypeKey] = Number(0);
            runningPolicyValues['accumulatedcashOutPayments'][returnTypeKey] = Number(0);
         }
      }
   },
   shouldBeRun: function(args) {
      // it will be executed regardless the calculate options
      return true;
   }
});

var eventReceiveInitialPremiums = Class.define({
   eventName: function(args) {
      // eventReceiveInitialPremiums
      // - receive premium and dump in the amount to accounts
      return "Receive Initial Premiums";
   },
   run: function(args) {
      var context = args["context"];

      var metadata = context.events.metadata;
      var proposal = context.proposal;
      var basePlan = proposal.coverageInfo;
      var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
      var runningPolicyValues = context.runningPolicyValues;
      var newPremium = 0;

      var year = runningPolicyValues.year;
      var months = runningPolicyValues.months;
      //var month = months % 12;

      if (months == 0) {
         newPremium = Number(newPremium) + Number(proposal.coverageInfo.initialDumpIn);
         // if (!Utils.isNothing(proposal.fundActivities) && !Utils.isNothing(proposal.fundActivities.fundActivity)) {
         //    var activities = Utils.iterator( proposal.fundActivities.fundActivity );
         //    //var activities = proposal.fundActivities.fundActivity;
         //    var parties = Utils.iterator(basePlan.parties.party);
         //    for (idx in activities) {
         //       var activity = activities[idx];
         //       if (Number(activity.topupPremium) > 0 && Number(activity.attainAge) == (Number(parties[0].insuredAge) + Number(year))) {
         //          newPremium = Number( newPremium ) + Number( activity.topupPremium );
         //          break;
         //       }
         //    }
         // }
         console.log("initialDumpIn = " + newPremium);
      }
      if (Number(newPremium) > 0) {
         var allocationRates = context.rates[productId][SchemaConstants.PREMIUM_TYPE.ALLOCATION];

         //console.log("allocationRates = " + JSON.stringify(allocationRates));

         var newPremiumRounding = proposal.coverageInfo.catalog.call("rounding", {
            coverage: basePlan
         });
         var netNewPremium = _R(newPremium * _R4(1 - allocationRates.rates[year]),newPremiumRounding);

         console.log("netNewPremium = " + netNewPremium);

         if (Number(netNewPremium) > 0) {
            runningPolicyValues.initialDumpIn = Number(runningPolicyValues.initialDumpIn) + Number(newPremium);
            runningPolicyValues.totalPremiums = Number(runningPolicyValues.totalPremiums) + Number(newPremium);
            IFund.appliedTo(runningPolicyValues.fundPool).call("dumpin", {
               amount: netNewPremium
            });
         }
      }
   },
   shouldBeRun: function(args) {
      // it will be executed regardless the calculate options
      return true;
   }
});

var eventCalculateNAR = Class.define({
   eventName: function(args) {
      // eventCalculateNAR
      // - calculate the Net Amount at Risk
      return "Calculate NAR";
   },
   run: function(args) {
      var context = args["context"];

      var metadata = context.events.metadata;
      var proposal = context.proposal;
      var basePlan = proposal.coverageInfo;
      var runningPolicyValues = context.runningPolicyValues;

      var narCalc = DataDrivenCalculatorImpl.create({
         drivenKeys: PublicConstants.FUND_RETURN,
         drivenDatas: [IFund.appliedTo(runningPolicyValues.fundPool).call("fundBalances", {}), IFund.appliedTo(runningPolicyValues.cashoutAccount).call("fundBalances", {})]
      });
      var nars = IDataDrivenCalculator.appliedTo(narCalc).call("calculate", {
         calculateBlock: function(fund) {
           _DBR("dbLevel", basePlan.options.dbLevel, "eventCalculateNAR->", context.runningPolicyValues.months);
            if (basePlan.options.dbLevel == SchemaConstants.DB_OPTION.INCREASE) {
               if (basePlan.catalog.call("isSinglePremium", {})) {
                  var nar = _R(runningPolicyValues.totalPremiums * runningPolicyValues.initialFaceAmountRatio, 15);
                  _DBR("fa = totalPremiums(" + runningPolicyValues.totalPremiums + ") * initialFaceAmountRatio(" + runningPolicyValues.initialFaceAmountRatio + ")", nar, "eventCalculateNAR->", context.runningPolicyValues.months);
                  return nar;
               } else {
                  var nar = Number(basePlan.faceAmount);
                  _DBR("fa = faceAmount(" + basePlan.faceAmount + ")", nar, "eventCalculateNAR->", context.runningPolicyValues.months);
                  return nar;
               }
               _DBR("NAR", narValue, "eventCalculateNAR->", context.runningPolicyValues.months);
               return narValue
            }
            if (basePlan.options.dbLevel == SchemaConstants.DB_OPTION.LEVEL) {
               var fundBalance = fund[0];
               var cashOutAccountBalance = fund[1];
               var totalFundBalance = Number(fundBalance) + Number(cashOutAccountBalance);
               var narValue = 0;
               _DBR("fundBalance", fundBalance, "eventCalculateNAR->", context.runningPolicyValues.months);
               _DBR("cashOutAccountBalance", cashOutAccountBalance, "eventCalculateNAR->", context.runningPolicyValues.months);
               _DBR("totalFundBalance", totalFundBalance, "eventCalculateNAR->", context.runningPolicyValues.months);
               if (basePlan.catalog.call("isSinglePremium", {})) {
                  var fa = _R(runningPolicyValues.totalPremiums * runningPolicyValues.initialFaceAmountRatio, 15);
                  var nar = Math.max(Number(0), _R(Number(fa) - Number(totalFundBalance),15));
                  //console.log("months :" + runningPolicyValues.months + ", fundBalance :" + fundBalance + " ,cahoutAccountBalance :" + cahoutAccountBalance + " ,fa :" + fa + ", nar :" + nar);
                  _DBR("fa = Max(0, totalPremiums(" + runningPolicyValues.totalPremiums + ") * initialFaceAmountRatio(" + runningPolicyValues.initialFaceAmountRatio + ") - totalFundBalance(" + totalFundBalance + "))", nar, "eventCalculateNAR->", context.runningPolicyValues.months);
                  return nar;
               } else {
                  var nar = _R(Math.max(Number(0), Number(basePlan.faceAmount) - Number(totalFundBalance)), 15);
                  _DBR("fa = Max(0, faceAmount(" + basePlan.faceAmount + ") - totalFundBalance(" + totalFundBalance + "))", nar, "eventCalculateNAR->", context.runningPolicyValues.months);
                  return nar;
               }
            }
         }
      });
      runningPolicyValues['nars'] = nars;

   },
   shouldBeRun: function(args) {
      // it will be executed regardless the calculate options
      return true;
   }
});

var eventCalculateCOI = Class.define({
   eventName: function(args) {
      // eventCalculateCOI
      // - calculate COI based on the NAR result
      return "Calculate COI";
   },
   run: function(args) {
      var context = args["context"];

      var metadata = context.events.metadata;
      var proposal = context.proposal;
      var basePlan = proposal.coverageInfo;
      var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
      var runningPolicyValues = context.runningPolicyValues;
      var year = runningPolicyValues.year;
      var month = runningPolicyValues.months;
      var coiVal = Number(0);

      var coiRates = context.rates[basePlan.product.productKey.primaryProduct.productPK.productId][SchemaConstants.PREMIUM_TYPE.COSTOFINSURANCE];
      var coiRate;
      if (year > coiRates.rates.length - 1) {
         coiRate = coiRates.rates[coiRates.rates.length - 1];
      } else {
         coiRate = coiRates.rates[year];
      }
      _DBR("coiRate", coiRate, "eventCalculateCOI->", context.runningPolicyValues.months);
      var coiCalc = DataDrivenCalculatorImpl.create({
         drivenKeys: PublicConstants.FUND_RETURN,
         drivenDatas: runningPolicyValues.nars
      });
      var cois = IDataDrivenCalculator.appliedTo(coiCalc).call("calculate", {
         calculateBlock: function(nar) {
            var coi = _R(nar * coiRate / 12000,15);
            //console.log("nar = " + nar);
            //console.log("coiRate = " + coiRate);
            //console.log("coi = " + coi);
            _DBR("coi = nar(" + nar + ") * coiRate(" + coiRate + ")", coi, "eventCalculateCOI->", context.runningPolicyValues.months);
            coiVal = coi;
            return coi;
         }
      });
      runningPolicyValues['cois'] = cois;
      runningPolicyValues['coiRate'] = coiRate;
      if (month == "0") {
         runningPolicyValues['firstMonthCoi'] = coiVal;
      }
   },
   shouldBeRun: function(args) {
      // it will be executed regardless the calculate options
      return true;
   }
});

var eventAccumulateCOI = Class.define({
   eventName: function(args) {
      // eventCalculateCOI
      // - calculate COI based on the NAR result
      return "Accumulate COI";
   },
   run: function(args) {
      var context = args["context"];

      var metadata = context.events.metadata;
      var proposal = context.proposal;
      var basePlan = proposal.coverageInfo;
      var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
      var runningPolicyValues = context.runningPolicyValues;
      var year = runningPolicyValues.year;
      var month = runningPolicyValues.months;

      var accumCalc = DataDrivenCalculatorImpl.create({
         drivenKeys: PublicConstants.FUND_RETURN,
         drivenDatas: [runningPolicyValues.cois, runningPolicyValues.annualCois]
      });

      var annualCois = IDataDrivenCalculator.appliedTo(accumCalc).call("calculate", {
         calculateBlock: function(args) {
            var coi = args[0];
            var annualCoi = args[1];
            var accumCoi = _R(Number(annualCoi) + Number(coi), 15);
            _DBR("accumCoi = annualCoi(" + annualCoi + ") + coi(" + coi + ")", accumCoi, "eventCalculateCOI->", context.runningPolicyValues.months);
            return accumCoi;
         }
      });
      runningPolicyValues['annualCois'] = annualCois;
   },
   shouldBeRun: function(args) {
      // it will be executed regardless the calculate options
      return true;
   }
});

var eventDeductCOI = Class.define({
   eventName: function(args) {
      // eventDeductCOI
      // - deduct COI from accounts
      return "Deduct COI";
   },
   run: function(args) {
      var context = args["context"];
      var runningPolicyValues = context.runningPolicyValues;
      IFund.appliedTo(runningPolicyValues.fundPool).call("withdraw", {
         amounts: runningPolicyValues.cois
      });
   },
   shouldBeRun: function(args) {
      // it will be executed regardless the calculate options
      return true;
   }
});

var eventRollInterest = Class.define({
   eventName: function(args) {
      // eventRollInterest
      // - roll interest of accounts
      return "Roll interest";
   },
   run: function(args) {
      var context = args["context"];
      var runningPolicyValues = context.runningPolicyValues;
      //console.log("runningPolicyValues.fundPool : ");
      IFund.appliedTo(runningPolicyValues.fundPool).call("rollingInterest", {});

      if (runningPolicyValues.cashoutAccount) {
         //console.log("runningPolicyValues.cashoutAccount : ");
         IFund.appliedTo(runningPolicyValues.cashoutAccount).call("rollingInterest", {});
      }
   },
   shouldBeRun: function(args) {
      // it will be executed regardless the calculate options
      return true;
   }
});

var eventReceiveTopupPremium = Class.define({
   eventName: function(args) {
      // eventReceiveTopupPremium
      // - deduct withdrawal and redemption fee from accounts
      return "Receive Topup Premium";
   },
   run: function(args) {
      var context = args["context"];
      var metadata = context.events.metadata;
      var proposal = context.proposal;
      var basePlan = proposal.coverageInfo;
      var primary = getPrimaryInsured({coverage: basePlan});

      var runningPolicyValues = context.runningPolicyValues;
      var year = context.runningPolicyValues.year;
      var month = context.runningPolicyValues.months % 12;

      var topupPremium = 0;
      if (!Utils.isNothing(proposal.fundActivities) && !Utils.isNothing(proposal.fundActivities.fundActivity)) {
         var activities = Utils.iterator(proposal.fundActivities.fundActivity);
         //var parties = Utils.iterator(basePlan.parties.party);
         for (idx in activities) {
            var activity = activities[idx];
            if (Number(activity.topupPremium) > Number(0) && Number(activity.attainAge) == (Number(primary.insuredAge) + Number(year)) && (month == 0)) {
               topupPremium = activity.topupPremium;
               break;
            }
         }
      }
      if (Number(topupPremium) > Number(0)) {
         var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
         var allocationRates = context.rates[productId][SchemaConstants.PREMIUM_TYPE.TOPUP_ALLOCATION];

         var newPremiumRounding = proposal.coverageInfo.catalog.call("rounding", {
            coverage: basePlan
         });
         var netTopupPremium = _R(topupPremium * _R4(1 - allocationRates.rates[year]), newPremiumRounding);

         runningPolicyValues.newPremium = Number(runningPolicyValues.newPremium) + Number(topupPremium);
         runningPolicyValues.totalPremiums = Number(runningPolicyValues.totalPremiums) + Number(topupPremium);
         IFund.appliedTo(runningPolicyValues.fundPool).call("dumpin", {
            amount: netTopupPremium
         });
      }
   },
   shouldBeRun: function(args) {
      var context = args["context"];
      var policyOptions = context.policyOptions;
      return Utils.matchInList(InterfaceConstants.POLICY_OPTIONS.FUND_ACTIVITIES_OPTION, policyOptions, false);
   }
});

var eventDeductWithdrawal = Class.define({
   eventName: function(args) {
      // eventPerformWithdrawal
      // - deduct withdrawal and redemption fee from accounts
      return "Deduct withdrawal from accounts";
   },
   run: function(args) {
      var context = args["context"];
      var metadata = context.events.metadata;
      var proposal = context.proposal;
      var basePlan = proposal.coverageInfo;
      var primary = getPrimaryInsured({coverage: basePlan});

      var runningPolicyValues = context.runningPolicyValues;
      var year = context.runningPolicyValues.year;
      var month = context.runningPolicyValues.months % 12;

      var withdrawalAmount = 0;
      if (!Utils.isNothing(proposal.fundActivities) && !Utils.isNothing(proposal.fundActivities.fundActivity)) {
         var activities = Utils.iterator(proposal.fundActivities.fundActivity);
         //var parties = Utils.iterator(basePlan.parties.party);
         for (idx in activities) {
            var activity = activities[idx];
            if (Number(activity.withdrawal) > Number(0) && Number(activity.attainAge) == (Number(primary.insuredAge) + Number(year)) && (month == 0)) {
               withdrawalAmount = activity.withdrawal;
               break;
            }
         }
      }
      if (Number(withdrawalAmount) > Number(0)) {
         runningPolicyValues.withdrawal = runningPolicyValues.withdrawal + withdrawalAmount;
         IFund.appliedTo(runningPolicyValues.fundPool).call("withdraw", {
            amount: withdrawalAmount
         });
      }
   },
   shouldBeRun: function(args) {
      var context = args["context"];
      var policyOptions = context.policyOptions;
      return Utils.matchInList(InterfaceConstants.POLICY_OPTIONS.FUND_ACTIVITIES_OPTION, policyOptions, false);
   }
});

var eventCalculateDeathBenefit = Class.define({
   eventName: function(args) {
      // eventCalculateDeathBenefit
      // - calculate redemption charges base on the withdrawal amount and policy year
      return "Calculate death benefit";
   },
   run: function(args) {
      var context = args["context"];

      var metadata = context.events.metadata;
      var proposal = context.proposal;
      var basePlan = proposal.coverageInfo;
      var runningPolicyValues = context.runningPolicyValues;

      var narCalc = DataDrivenCalculatorImpl.create({
         drivenKeys: PublicConstants.FUND_RETURN,
         drivenDatas: PublicConstants.FUND_RETURN
      });
      var deathBenefits = IDataDrivenCalculator.appliedTo(narCalc).call("calculate", {
         calculateBlock: function(fund) {
            // death benefit = basePlan.faceAmount
            // the logic can be further extended
            var db = _R(Number(runningPolicyValues.totalPremiums) * Number(runningPolicyValues.initialFaceAmountRatio),0)
            _DBR("DB = round(totalPremiums(" + runningPolicyValues.totalPremiums + ") * initialFaceAmountRatio(" + runningPolicyValues.initialFaceAmountRatio + "),0)", db, "eventCalculateDeathBenefit->", runningPolicyValues.months);
            return db;
         }
      });
      runningPolicyValues['deathBenefits'] = deathBenefits;
   },
   shouldBeRun: function(args) {
      return true;
   }
});

var eventCalculateTotalDeathBenefit = Class.define({
   eventName: function(args) {
      // eventCalculateTotalDeathBenefit
      // - calculate redemption charges base on the withdrawal amount and policy year
      return "Calculate total death benefit";
   },
   run: function(args) {
      var context = args["context"];

      var metadata = context.events.metadata;
      var proposal = context.proposal;
      var basePlan = proposal.coverageInfo;
      var runningPolicyValues = context.runningPolicyValues;

      var narCalc = DataDrivenCalculatorImpl.create({
         drivenKeys: PublicConstants.FUND_RETURN,
         drivenDatas: [runningPolicyValues.deathBenefits, IFund.appliedTo(runningPolicyValues.fundPool).call("fundBalances", {}), IFund.appliedTo(runningPolicyValues.cashoutAccount).call("fundBalances", {})]
      });
      var totalDeathBenefits = IDataDrivenCalculator.appliedTo(narCalc).call("calculate", {
         calculateBlock: function(data) {
            var db = data[0];
            var fundBalance = data[1];
            var cashOutAccountBalance = data[2];
            var totalFundBalance = Number(fundBalance) + Number(cashOutAccountBalance);
            var tdb = 0;
            if (basePlan.options.dbLevel == SchemaConstants.DB_OPTION.LEVEL) {
               tdb = Math.max(_R2(db), _R2(totalFundBalance));
               _DBR("TDB = Max(R2(DB(" + db + ")), R2(totalFundBalance(" + totalFundBalance + ")))", tdb, "eventCalculateDeathBenefit->", runningPolicyValues.months);
            } else {
               tdb = _R2(Number(db) + Number(totalFundBalance));
               _DBR("TDB = DB(" + db + ") + totalFundBalance(" + totalFundBalance + ")", tdb, "eventCalculateDeathBenefit->", runningPolicyValues.months);
            }
            return tdb;
         }
      });
      runningPolicyValues['totalDeathBenefits'] = totalDeathBenefits;
   },
   shouldBeRun: function(args) {
      return true;
   }
});

var eventCalcPayout = Class.define({
   eventName: function(args) {
      //eventCalcPayout
      return "Withdraw dividends / target income fund";
   },
   run: function(args) {
      var context = args["context"];
      var metadata = context.events.metadata;
      var proposal = context.proposal;
      var basePlan = proposal.coverageInfo;
      var runningPolicyValues = context.runningPolicyValues;
      var year = context.runningPolicyValues.year;
      var month = context.runningPolicyValues.months % 12;
      var payoutStartYear = Number(proposal.coverageInfo.dividendWithdrawnStartYear) - 1;
      if (payoutStartYear <= year) {
         runningPolicyValues.cashOut = true;
         var totalPayout = IFund.appliedTo(runningPolicyValues.fundPool).call("payout", {});
         for (cashOutKey in totalPayout) {
            if (Utils.isNothing(runningPolicyValues.cashOutPayments[cashOutKey])) {
               runningPolicyValues.cashOutPayments[cashOutKey] = Number(0);
            }
            if (Utils.isNothing(runningPolicyValues.accumulatedcashOutPayments[cashOutKey])) {
               runningPolicyValues.accumulatedcashOutPayments[cashOutKey] = Number(0);
            }
            runningPolicyValues.cashOutPayments[cashOutKey] = Number(totalPayout[cashOutKey]);
            runningPolicyValues.accumulatedcashOutPayments[cashOutKey] = runningPolicyValues.accumulatedcashOutPayments[cashOutKey] + Number(totalPayout[cashOutKey]);
         }
      } else {
         runningPolicyValues.cashOut = false;
      }
      //console.log("eventCalcPayout = " + JSON.stringify(runningPolicyValues.cashOutPayments));
   },
   shouldBeRun: function(args) {
      var context = args["context"];
      var policyOptions = context.policyOptions;
      return true;
      //return Utils.matchInList(InterfaceConstants.POLICY_OPTIONS.CASH_OUT_OPTION, policyOptions, false);
   }
});

var eventDepositPayout = Class.define({
   eventName: function(args) {
      // eventDepositPayout
      return "Deposit Payout";
   },
   run: function(args) {
      var context = args["context"];
      var metadata = context.events.metadata;
      var proposal = context.proposal;
      var basePlan = proposal.coverageInfo;

      var runningPolicyValues = context.runningPolicyValues;
      var year = context.runningPolicyValues.year;
      var month = context.runningPolicyValues.months % 12;
      var payoutStartYear = Number(proposal.coverageInfo.dividendWithdrawnStartYear) - 1;
      if (payoutStartYear <= year) {
         //IFund.appliedTo(runningPolicyValues.fundPool).call("dumpins", {amounts: runningPolicyValues.cashOutPayments});
         IFund.appliedTo(runningPolicyValues.cashoutAccount).call("dumpins", {
            amounts: runningPolicyValues.cashOutPayments
         });
      }
      //console.log("dumpins = " + JSON.stringify(runningPolicyValues.cashOutPayments));
   },
   shouldBeRun: function(args) {
      var context = args["context"];
      var policyOptions = context.policyOptions;
      //return true;
      return !Utils.matchInList(InterfaceConstants.POLICY_OPTIONS.CASH_OUT_OPTION, policyOptions, false);
   }
});

var eventCoreAccumulatePremiums = Class.define({
    eventName: function(args) {
        return "eventCoreAccumulatePremiums";
    },
    shouldBeRun: function(args) {
        var context = args["context"];
        return true;
    },
    run: function(args) {
        var context = args["context"];
        var running = context.runningPolicyValues;
        running.accPremiums = running.accPremiums + running.totalPremiums;
        return;
    }
});

var eventCoreCalculatePremiumXXXXX = Class.define({
    eventName: function(args) {
        return "eventCoreCalculatePremiumXXXXX";
    },
    shouldBeRun: function(args) {
        var context = args["context"];
        return true;
    },
    run: function(args) {
        var context = args["context"];
        var rates = context.rates;
        var running = context.runningPolicyValues;
        var months = running.months;
        var methodId = this.self.premiumMethod(args);

        //if (months == 0) {
        //    _SS( rates, 10 );
        //}

        var basePlan = context.proposal.coverageInfo;
        var basePlanSchema = basePlan.schema.call("getSchema");
        var baseProductId = _PV(basePlan, "product.productKey.primaryProduct.productPK.productId");

        var annualPremium = this.self.calculate({
            context: context,
            coverage: basePlan
        });
        running.basePlanPremiums += annualPremium;
        running.totalPremiums += annualPremium;

        _DBR("running.totalPremiums", running.totalPremiums, "eventCalculatePremium" + methodId + "->", months);

        return;
    },
    premiumMethod: function(args) {
        return "XXXXX";
    },
    calculatePremium: function(args) {
        var coverage = args["coverage"];
        var context = args["context"];
        var methodId = this.self.premiumMethod(args);

        var rates = context.rates;
        var running = context.runningPolicyValues;
        var months = running.months;
        var productId = _PV(coverage, "product.productKey.primaryProduct.productPK.productId");
        var year = running.year;

        // annual mode rate
        var premRate = rates[productId][SchemaConstants.PREMIUM_TYPE.BASICPREMIUM].rates[year];

        _DBR("year", year, "eventCalculatePremium" + methodId + "->", months);
        _DBR("premRate", premRate, "eventCalculatePremium" + methodId + "->", months);
        _DBR("coverage.faceAmount", coverage.faceAmount, "eventCalculatePremium" + methodId + "->", months);

        var annualPremium = _R2(coverage.faceAmount * premRate * coverage.extraRating.percentageExtra / 1000);
        if (!Utils.isNothing(coverage.extraRating.flatExtra)) {
            annualPremium = annualPremium + _R2(coverage.faceAmount * coverage.extraRating.flatExtra / 1000);
        }
        if (!Utils.isNothing(coverage.extraRating.tempFlat) && !Utils.isNothing(coverage.extraRating.tempFlatDuration)) {
            if (year < coverage.extraRating.tempFlatDuration) {
                annualPremium = annualPremium + _R2(coverage.faceAmount * coverage.extraRating.tempFlat / 1000);
            }
        }
        _DBR("annualPremium", annualPremium, "eventCalculatePremium" + methodId + "->", months);
        return annualPremium;
    },
    calculate: function(args) {
        var coverage = args["coverage"];
        var context = args["context"];
        var methodId = this.self.premiumMethod(args);
        var running = context.runningPolicyValues;
        var months = running.months;
        var productId = _PV(coverage, "product.productKey.primaryProduct.productPK.productId");

        var covPremium = 0;

        var schema = coverage.schema.call("getSchema");
        var premiumTable = _V(_PV(schema, "ProductSchema.PremiumInfo.PremiumTable"), []);
        var premiumTableRec;
        for (recIdx in premiumTable) {
            var premiumRec = premiumTable[recIdx];
            var premiumType = _V(premiumRec.PremiumType);
            if (premiumType == SchemaConstants.PREMIUM_TYPE.BASICPREMIUM) {
                var methodId = _V(premiumRec.Method);
                if (methodId == this.self.premiumMethod(args)) {
                    covPremium = this.self.calculatePremium({
                        coverage: coverage,
                        context: context
                    });
                    _DBR("Product (" + productId + ")", covPremium, "eventCalculatePremium" + methodId + "->", months);
                    break;
                }
            }
        }
        return covPremium;
    }
});

var eventCoreMvyAvyInitialization = Class.define({
    eventName: function(args) {
        return "eventCoreMvyAvyInitialization";
    },
    shouldBeRun: function(args) {
        var context = args["context"];
        return true;
    },
    MVY: function(args) {
        var context = args["context"];
        var running = context.runningPolicyValues;
        var months = running.months;
        // do something
    },
    AVY: function(args) {
        var context = args["context"];
        var running = context.runningPolicyValues;
        var months = running.months;
        // do something
        running['basePlanPremiums'] = 0;
        running['riderPremiums'] = 0;
        running['totalPremiums'] = 0;
    },
    initialSetup: function(args) {
        var context = args["context"];
        var running = context.runningPolicyValues;
        var months = running.months;
        // do something
        running['accPremiums'] = 0;
    },
    run: function(args) {
        var context = args["context"];
        var running = context.runningPolicyValues;
        var months = running.months;
        this.self.MVY(args);
        if (months == 0) {
            this.self.initialSetup(args);
        }
        if (months % 12 == 0) {
            this.self.AVY(args);
        }
        return;
    }
});

var eventCoreCalculateRiderPremiumXXXXX = eventCoreCalculatePremiumXXXXX.extend({
    eventName: function(args) {
        return "eventCoreCalculateRiderPremiumXXXXX";
    },
    shouldBeRun: function(args) {
        var context = args["context"];
        var projOptions = context.projectionOptions;
        return Utils.matchInList(InterfaceConstants.PROJECTION_OPTIONS.BASE_PLAN_WITH_RIDERS, projOptions, false);
    },
    run: function(args) {
        var context = args["context"];
        var methodId = this.self.premiumMethod(args);
        var rates = context.rates;
        var running = context.runningPolicyValues;
        var months = running.months;

        //if (months == 0) {
        //    _SS( rates, 10 );
        //}

        var riders = _V(_PV(context.proposal, "riders.coverageInfo"), []);
        for (riderIdx in riders) {
            var riderCov = riders[riderIdx];
            var riderPremium = this.self.calculate({
                context: context,
                coverage: riderCov
            });
            running.riderPremiums += riderPremium;
            running.totalPremiums += riderPremium;
        }
        _DBR("running.totalPremiums", running.totalPremiums, "eventCalculatePremium" + methodId + "->", months);
        return;
    }
});
var columnYear = Class.define({
   columnName: function(args) {
      return "Year";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = _R(Number(record.year) + Number(1),0);
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var columnAge = Class.define({
   columnName: function(args) {
      return "Age";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = _R(Number(record.age) + Number(1),0); // match printing requirements
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colAccountLow= Class.define({
   columnName: function(args) {
      return "Account Value (LOW)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.accountValues[PublicConstants.FUND_RETURN.LOW] + record.cashoutAccount[PublicConstants.FUND_RETURN.LOW];
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colAccountMedium= Class.define({
   columnName: function(args) {
      return "Account Value (MEDIUM)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.accountValues[PublicConstants.FUND_RETURN.MEDIUM] + record.cashoutAccount[PublicConstants.FUND_RETURN.MEDIUM];
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colAccountHigh= Class.define({
   columnName: function(args) {
      return "Account Value (HIGH)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.accountValues[PublicConstants.FUND_RETURN.HIGH] + record.cashoutAccount[PublicConstants.FUND_RETURN.HIGH];
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colNarLow= Class.define({
   columnName: function(args) {
      return "NAR (LOW)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.nars[PublicConstants.FUND_RETURN.LOW];
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colNarMedium= Class.define({
   columnName: function(args) {
      return "NAR (MEDIUM)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.nars[PublicConstants.FUND_RETURN.MEDIUM];
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colNarHigh= Class.define({
   columnName: function(args) {
      return "NAR (HIGH)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.nars[PublicConstants.FUND_RETURN.HIGH];
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colCoiRate= Class.define({
   columnName: function(args) {
      return "COI RATE";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.coiRate;
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colCoiLow= Class.define({
   columnName: function(args) {
      return "COI (LOW)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.cois[PublicConstants.FUND_RETURN.LOW];
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colCoiMedium= Class.define({
   columnName: function(args) {
      return "COI (MEDIUM)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.cois[PublicConstants.FUND_RETURN.MEDIUM];
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colCoiHigh= Class.define({
   columnName: function(args) {
      return "COI (HIGH)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.cois[PublicConstants.FUND_RETURN.HIGH];
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colWithdrawal= Class.define({
   columnName: function(args) {
      return "Withdrawal";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.withdrawal;
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colInitialDumpin= Class.define({
   columnName: function(args) {
      return "Initial Dumpin";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.initialDumpIn;
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colPremium= Class.define({
   columnName: function(args) {
      return "Premium";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.newPremium;
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colTotalPremium= Class.define({
   columnName: function(args) {
      return "Total Premium";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.totalPremiums;
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colSurValueLow= Class.define({
   columnName: function(args) {
      return "Surrender Value (LOW)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = Number(Number(record.surrenderValues[PublicConstants.FUND_RETURN.LOW]) + Number(record.surrenderValuesFromCashoutAccount[PublicConstants.FUND_RETURN.LOW]));
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colSurValueMedium= Class.define({
   columnName: function(args) {
      return "Surrender Value (MEDIUM)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = Number(Number(record.surrenderValues[PublicConstants.FUND_RETURN.MEDIUM]) + Number(record.surrenderValuesFromCashoutAccount[PublicConstants.FUND_RETURN.MEDIUM]));
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colSurValueHigh= Class.define({
   columnName: function(args) {
      return "Surrender Value (HIGH)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = Number(Number(record.surrenderValues[PublicConstants.FUND_RETURN.HIGH]) + Number(record.surrenderValuesFromCashoutAccount[PublicConstants.FUND_RETURN.HIGH]));
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colDbLow= Class.define({
   columnName: function(args) {
      return "Death Benefit (LOW)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.deathBenefits[PublicConstants.FUND_RETURN.LOW];
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colDbMedium= Class.define({
   columnName: function(args) {
      return "Death Benefit (MEDIUM)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.deathBenefits[PublicConstants.FUND_RETURN.MEDIUM];
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colDbHigh= Class.define({
   columnName: function(args) {
      return "Death Benefit (HIGH)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.deathBenefits[PublicConstants.FUND_RETURN.HIGH];
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colTotalDbLow= Class.define({
   columnName: function(args) {
      return "Total Death Benefit (LOW)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.totalDeathBenefits[PublicConstants.FUND_RETURN.LOW];
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colTotalDbMedium= Class.define({
   columnName: function(args) {
      return "Total Death Benefit (MEDIUM)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.totalDeathBenefits[PublicConstants.FUND_RETURN.MEDIUM];
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colTotalDbHigh= Class.define({
   columnName: function(args) {
      return "Total Death Benefit (HIGH)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.totalDeathBenefits[PublicConstants.FUND_RETURN.HIGH];
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colCashOut= Class.define({
   columnName: function(args) {
      return "Cash Out Indicator";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.cashOut;
      return calValue;
   },
   shouldBeShown: function(args) {
     var context = args["context"];
     var policyOptions = context.policyOptions;
     return Utils.matchInList(InterfaceConstants.POLICY_OPTIONS.CASH_OUT_OPTION, policyOptions, false);
   }
});

var colCashOutPaymentLow= Class.define({
   columnName: function(args) {
      return "Cash Payment (LOW)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.cashOutPayments[PublicConstants.FUND_RETURN.LOW];
      return calValue;
   },
   shouldBeShown: function(args) {
     var context = args["context"];
     var policyOptions = context.policyOptions;
     return Utils.matchInList(InterfaceConstants.POLICY_OPTIONS.CASH_OUT_OPTION, policyOptions, false);
   }
});

var colCashOutPaymentMedium= Class.define({
   columnName: function(args) {
      return "Cash Payment (MEDIUM)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.cashOutPayments[PublicConstants.FUND_RETURN.MEDIUM];
      return calValue;
   },
   shouldBeShown: function(args) {
     var context = args["context"];
     var policyOptions = context.policyOptions;
     return Utils.matchInList(InterfaceConstants.POLICY_OPTIONS.CASH_OUT_OPTION, policyOptions, false);
   }
});

var colCashOutPaymentHigh= Class.define({
   columnName: function(args) {
      return "Cash Payment (HIGH)";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.cashOutPayments[PublicConstants.FUND_RETURN.HIGH];
      return calValue;
   },
   shouldBeShown: function(args) {
     var context = args["context"];
     var policyOptions = context.policyOptions;
     return Utils.matchInList(InterfaceConstants.POLICY_OPTIONS.CASH_OUT_OPTION, policyOptions, false);
   }
});

var colFirstMonthCoi= Class.define({
   columnName: function(args) {
      return "First Month COI";
   },
   calculatedValue: function(args) {
      var record = args["record"];
      var calValue = record.firstMonthCoi;
      return calValue;
   },
   shouldBeShown: function(args) {
      return true;
   }
});

var colTradAccumulatePremiums = Class.define({
    columnName: function(args) {
        return "Accumulative Premiums";
    },
    shouldBeShown: function(args) {
        var context = args["context"];
        return true;
    },
    calculatedValue: function(args) {
        var record = args["record"];
        return _V(record.accumulatePremiums, 0.00);
    }
});

var colTradBasePlanPremiums = Class.define({
    columnName: function(args) {
        return "colBasePlanPremiums";
    },
    shouldBeShown: function(args) {
        var context = args["context"];
        return true;
    },
    calculatedValue: function(args) {
        var record = args["record"];
        return _V(record.basePlanPremiums, 0.00);
    }
});

var colTradRiderPremiums = Class.define({
    columnName: function(args) {
        return "colRiderPremiums";
    },
    shouldBeShown: function(args) {
        var context = args["context"];
        return true;
    },
    calculatedValue: function(args) {
        var record = args["record"];
        return _V(record.riderPremiums, 0.00);
    }
});

var colTradTotalPremiums = Class.define({
    columnName: function(args) {
        return "Total Premiums";
    },
    shouldBeShown: function(args) {
        var context = args["context"];
        return true;
    },
    calculatedValue: function(args) {
        var record = args["record"];
        return _V(record.totalPremiums, 0.00);
    }
});

var CommonEvents = Class.define({
   init: function(args) {
      this.self['name'] = 'CommonEvents';
      this.self['productId'] = null;
      this.self['productType'] = [];
   },
   iterationUnit: function(args) {
      var proposal = args['proposal'];
      var basePlan = proposal.coverageInfo;
      if (basePlan.catalog.call("isUVL", {})) {
         return Number(1);
      } else {
         return Number(12);
      }
   },
   iterations: function(args) {
      var proposal = args['proposal'];
      var basePlan = proposal.coverageInfo;
      var benefitPeriod = basePlan.schema.call("calculateBenefitPeriod", {
         coverage: basePlan
      });
      var iterations = benefitPeriod * 12;
      return iterations;
   },
   providerName: function(args) {
      return this.self['name'];
   },
   matchProductId: function(args) {
      // return true or false;
      var productId = args['productId'];
      var catalog = args['catalog'];
      if (!Utils.isNothing(this.self.productId)) {
         return (this.self.productId == productId);
      } else {
         return false;
      }
   },
   matchProductType: function(args) {
      // return matches
      var productId = args['productId'];
      var catalog = args['catalog'];

      var matchedTypes = 0;
      if (this.self['productType']) {
          for(ptIndex in this.self['productType']) {
              var prodType = this.self['productType'][ptIndex];
              if (Utils.matchInList(prodType, catalog.call('getCatalog',{}).ProductCatalog.ProductType.ProductTypeCode, false)) {
                  matchedTypes += 1;
              }
          }
      }
      if (matchedTypes == this.self['productType'].length) {
          return matchedTypes;
      } else {
          return 0;
      }
   }
});

var UVLife_Events = CommonEvents.extend({
   init: function(args) {
      this.self['name'] = 'UVLife_Events';
      this.self['productId'] = null;
      //this.self['productType'] = [CatalogConstants.PRODUCT_TYPE.UVLIFE];
      this.self['productType'] = [];
   },
   events: function(args) {
      var events = [
         eventAvyInitialization.create(),
         eventReceiveInitialPremiums.create(),
         eventReceiveTopupPremium.create(),
         eventDeductWithdrawal.create(),
         eventCalculateNAR.create(),
         eventCalculateCOI.create(),
         eventAccumulateCOI.create(),
         eventDeductCOI.create(),
         eventRollInterest.create(),
         eventCalcPayout.create(),
         eventDepositPayout.create(),
         eventCalculateDeathBenefit.create(),
         eventCalculateTotalDeathBenefit.create()
      ];
      return events;
   },
   columns: function(args) {
      var columns = [
         columnYear.create(),
         columnAge.create(),
         colAccountLow.create(),
         colAccountMedium.create(),
         colAccountHigh.create(),
         colNarLow.create(),
         colNarMedium.create(),
         colNarHigh.create(),
         colCoiRate.create(),
         colCoiLow.create(),
         colCoiMedium.create(),
         colCoiHigh.create(),
         colWithdrawal.create(),
         colInitialDumpin.create(),
         colPremium.create(),
         colTotalPremium.create(),
         colSurValueLow.create(),
         colSurValueMedium.create(),
         colSurValueHigh.create(),
         colDbLow.create(),
         colDbMedium.create(),
         colDbHigh.create(),
         colTotalDbLow.create(),
         colTotalDbMedium.create(),
         colTotalDbHigh.create(),
         colCashOut.create(),
         colCashOutPaymentLow.create(),
         colCashOutPaymentMedium.create(),
         colCashOutPaymentHigh.create(),
         colFirstMonthCoi.create()
      ];
      return columns;
   },
   makeSnapshot: function(args) {
      var context = args['context'];
      var snapshot = {
          year: context.runningPolicyValues.year,
          month: (context.runningPolicyValues.months % 12),
          age: Number(context.insuredAge) + Number(context.runningPolicyValues.year),
          accountValues: IFund.appliedTo(context.runningPolicyValues.fundPool).call("fundBalances", {}),
          nars: context.runningPolicyValues.nars,
          cois: context.runningPolicyValues.annualCois,
          coiRate: context.runningPolicyValues.coiRate,
          withdrawal: context.runningPolicyValues.withdrawal,
          initialDumpIn: context.runningPolicyValues.initialDumpIn,
          newPremium: context.runningPolicyValues.newPremium,
          totalPremiums: context.runningPolicyValues.totalPremiums,
          deathBenefits: context.runningPolicyValues.deathBenefits,
          totalDeathBenefits: context.runningPolicyValues.totalDeathBenefits,
          cashOut: context.runningPolicyValues.cashOut,
          cashOutPayments: context.runningPolicyValues.accumulatedcashOutPayments,
          firstMonthCoi: context.runningPolicyValues.firstMonthCoi,
          cashoutAccount: IFund.appliedTo(context.runningPolicyValues.cashoutAccount).call("fundBalances", {}),
          surrenderValuesFromCashoutAccount: IFund.appliedTo(context.runningPolicyValues.cashoutAccount).call("surrenderValues", {}),
          surrenderValues: IFund.appliedTo(context.runningPolicyValues.fundPool).call("surrenderValues", {})
      };
      return snapshot;
   }
});

var TradTermCore_Events = CommonEvents.extend({
    events: function(args) {
        var proposal = args["proposal"];
        var events = [
            eventTradAvyInitialization.create(),
            eventCalculatePremiumBPSG1.create(),
            eventCalculateRiderPremiumBPSG1.create(),
            eventAccumulatePremiums.create()
        ];
        return events;
    },
    columns: function(args) {
        var proposal = args["proposal"];
        var columns = [
            columnYear.create(),
            columnAge.create(),
            colBasePlanPremiums.create(),
            colRiderPremiums.create(),
            colTotalPremiums.create(),
            colAccumulatePremiums.create()
        ];
        return columns;
    },
    init: function(args) {
        this.self['name'] = 'TradTerm_Events';
        this.self['productId'] = null;
        //this.self['productType'] = [CatalogConstants.PRODUCT_TYPE.TERM];
        this.self['productType'] = [];
    },
    makeSnapshot: function(args) {
        var context = args['context'];
        var snapshot = {
            year: context.runningPolicyValues.year,
            month: (context.runningPolicyValues.months % 12),
            age: Number(context.insuredAge) + Number(context.runningPolicyValues.year),
            totalPremiums: context.runningPolicyValues.totalPremiums,
            accumulatePremiums: context.runningPolicyValues.accPremiums,
            basePlanPremiums: context.runningPolicyValues.basePlanPremiums,
            riderPremiums: context.runningPolicyValues.riderPremiums
        };
        return snapshot;
    }
});

// coverage attributes reader for rate
var RateSearchAttributeReader = function( coverage, ipo ) {

   //banding = coverage.schema.call("calculateBanding", {coverage: coverage});
   banding = coverage.banding;
   return {
      catalog : coverage.catalog,
      schema : coverage.schema,
      coverage : coverage,
      banding : banding,
      ipo : ipo,
      insured: function() {
         return getPrimaryInsured({coverage: this.coverage});
      },
      map : function(v, valueMap) {
         var o = 0;
         if (v) {
            for(k in valueMap) {
               var vp = valueMap[k];
               var match = false;
               //if (v.length) {
               //   match = Utils.matchInList(vp, v, false);
               //} else {
               //   match = v.equalsIgnoreCase(vp.key);
               //}
               if (v.equalsIgnoreCase(vp.key)) {
                  o = o | vp.value;
               }
            }
         }
         return o;
      },
      participate : function() {
         var catalog = this.coverage.catalog.implementation;
         if (catalog.isParticipate()) {
             return RateConstants.PARTICIPATE.YES;
         } else {
             return RateConstants.PARTICIPATE.NO;
         }
      },
      sex : function() {
         //console.log("this.coverage.parties.party.insuredSex = " + this.coverage.parties.party.insuredSex);
         var _sex = this.map(this.insured().insuredSex,
            [
               {key : SchemaConstants.GENDER_FULL.MALE, value : RateConstants.GENDER.MALE},
                {key : SchemaConstants.GENDER_FULL.FEMALE, value : RateConstants.GENDER.FEMALE}
            ]
         );
         return _sex;
      },
      location : function() {
         var _location = this.map(_V(_PV(this.coverage, "product.productKey.location")),
            [
               {key: CatalogConstants.LOCATION.HONGKONG, value: RateConstants.LOCATION.HONG_KONG},
               {key: CatalogConstants.LOCATION.MACAU, value: RateConstants.LOCATION.MACAU},
               {key: CatalogConstants.LOCATION.SINGAPORE, value: RateConstants.LOCATION.SINGAPORE},
               {key: CatalogConstants.LOCATION.CHINA, value: RateConstants.LOCATION.CHINA},
               {key: CatalogConstants.LOCATION.PHILIPPINE, value: RateConstants.LOCATION.PHILIPPHINE},
               {key: CatalogConstants.LOCATION.VIETNAM, value: RateConstants.LOCATION.VIETNAM}
            ]
         );

         //console.log('this.coverage.product.productKey.location = ' + this.coverage.product.productKey.location);
         //console.log('_location = ' + _location);

         return _location;
      },
      channel : function() {
         var _channel = this.map(_V(_PV(this.coverage, "proposal.channel")),
         [
            {key: DISTRIBUTION_CHANNEL.AGENCY, value: RateConstants.CHANNEL.AGENCY},
            {key: DISTRIBUTION_CHANNEL.BROKER, value: RateConstants.CHANNEL.BROKER},
            {key: DISTRIBUTION_CHANNEL.BANK, value: RateConstants.CHANNEL.BANK},
            {key: DISTRIBUTION_CHANNEL.ACTUARIAL, value: RateConstants.CHANNEL.ACTUARIAL},
            {key: DISTRIBUTION_CHANNEL.ADMIN, value: RateConstants.CHANNEL.ADMIN},
            {key: DISTRIBUTION_CHANNEL.ALL, value: RateConstants.CHANNEL.ALL}
         ]);
         return _channel;
      },
      smokingStatus : function() {
         var _smokingStatus = this.map(this.insured().smokingStatus,
            [
               {key: SchemaConstants.SMOKING_STATUS.SMOKER, value: RateConstants.SMOKING_STATUS.SMOKER},
               {key: SchemaConstants.SMOKING_STATUS.NONSMOKER, value: RateConstants.SMOKING_STATUS.NONSMOKER},
               {key: SchemaConstants.SMOKING_STATUS.AGGREGATE, value: RateConstants.SMOKING_STATUS.AGGREGATE},
               {key: SchemaConstants.SMOKING_STATUS.PREFERRED_LIVES, value: RateConstants.SMOKING_STATUS.PREFERRED_LIVES}
            ]
         );
         return _smokingStatus;
      },
      waitingPeriod : function() {
         var _waitingPeriod = this.map(_V(_PV(this.coverage, "otherOptions.waitingPeriod")),
            [
               {key: SchemaConstants.WAITING_PERIOD.WP_30DAYS, value: RateConstants.WAITING_PERIOD.PRD_30DAYS},
               {key: SchemaConstants.WAITING_PERIOD.WP_90DAYS, value: RateConstants.WAITING_PERIOD.PRD_90DAYS},
               {key: SchemaConstants.WAITING_PERIOD.WP_180DAYS, value: RateConstants.WAITING_PERIOD.PRD_180DAYS},
               {key: SchemaConstants.WAITING_PERIOD.WP_365DAYS, value: RateConstants.WAITING_PERIOD.PRD_365DAYS}
            ]
         );
         return _waitingPeriod;
      },
      benefitPeriod : function() {
         var _benefitPeriod = this.map(_V(_PV(this.coverage, "otherOptions.benefitPeriod")),
            [
               {key: SchemaConstants.BENEFIT_PERIOD.BP_5YEARS, value: RateConstants.BENEFIT_PERIOD.PRD_5YEARS},
               {key: SchemaConstants.BENEFIT_PERIOD.BP_10YEARS, value: RateConstants.BENEFIT_PERIOD.PRD_10YEARS},
               {key: SchemaConstants.BENEFIT_PERIOD.BP_15YEARS, value: RateConstants.BENEFIT_PERIOD.PRD_15YEARS},
               {key: SchemaConstants.BENEFIT_PERIOD.BP_AGE65, value: RateConstants.BENEFIT_PERIOD.PRD_AGE65}
            ]
         );
         return _benefitPeriod;
      },
      occupation : function() {
         var _occupation = this.map(this.coverage.occupation,
            [
               {key: SchemaConstants.OCCUPATION_CLASS.CLASS_0, value: RateConstants.OCCUPATION_CLASS.CLASS0},
               {key: SchemaConstants.OCCUPATION_CLASS.CLASS_1, value: RateConstants.OCCUPATION_CLASS.CLASS1},
               {key: SchemaConstants.OCCUPATION_CLASS.CLASS_2, value: RateConstants.OCCUPATION_CLASS.CLASS2},
               {key: SchemaConstants.OCCUPATION_CLASS.CLASS_3, value: RateConstants.OCCUPATION_CLASS.CLASS3},
               {key: SchemaConstants.OCCUPATION_CLASS.CLASS_4, value: RateConstants.OCCUPATION_CLASS.CLASS4}
            ]
         );
         if (this.coverage.rcc) {
            if (this.coverage.rcc.equalsIgnoreCase("Y")) {
               _occupation = _occupation | RateConstants.OCCUPATION_CLASS.RCC;
            }
         }
         return _occupation;
      },
      noOfPerson : function() {
         var _noOfPerson = this.map(_V(_PV(this.coverage, "otherOptions.noOfPerson")),
            [
               {key: SchemaConstants.NO_OF_PERSONS.PERSONS_1, value: RateConstants.NO_OF_PERSONS.PERSONS_1},
               {key: SchemaConstants.NO_OF_PERSONS.PERSONS_2, value: RateConstants.NO_OF_PERSONS.PERSONS_2},
               {key: SchemaConstants.NO_OF_PERSONS.PERSONS_3, value: RateConstants.NO_OF_PERSONS.PERSONS_3},
               {key: SchemaConstants.NO_OF_PERSONS.PERSONS_4, value: RateConstants.NO_OF_PERSONS.PERSONS_4}
            ]
         );
         return _noOfPerson;
      },
      otherOption : function() {
         var _otherOption = this.map(_V(_PV(this.coverage, "otherOptions.coverageClass")),
            [
               {key: SchemaConstants.COVERAGE_CLASS.CLASS_A, value: RateConstants.OTHER_OPTION.COVERAGE_CLASS_A},
               {key: SchemaConstants.COVERAGE_CLASS.CLASS_B, value: RateConstants.OTHER_OPTION.COVERAGE_CLASS_B},
               {key: SchemaConstants.COVERAGE_CLASS.CLASS_C, value: RateConstants.OTHER_OPTION.COVERAGE_CLASS_C},
               {key: SchemaConstants.COVERAGE_CLASS.CLASS_D, value: RateConstants.OTHER_OPTION.COVERAGE_CLASS_D}
            ]
         );
         var _majorMedical = this.map(_V(_PV(this.coverage, "otherOptions.majorMedical")),
            [
               {key: "Y", value: RateConstants.OTHER_OPTION.MAJOR_MEDICAL},
               {key: "N", value: RateConstants.OTHER_OPTION.MAJOR_MEDICAL_N}
            ]
         );
         var result = (_otherOption | _majorMedical);
         return result;
      },
      paymentMode : function() {
         var _paymentMode = this.map(_V(_PV(this.coverage, "options.paymentMode")),
            [
               {key: SchemaConstants.PAYMENT_MODE.ANNUAL, value: RateConstants.PAYMENT_MODE.ANNUAL},
               {key: SchemaConstants.PAYMENT_MODE.SEMIANNUAL, value: RateConstants.PAYMENT_MODE.SEMIANNUAL},
               {key: SchemaConstants.PAYMENT_MODE.QUARTERLY, value: RateConstants.PAYMENT_MODE.QUARTERLY},
               {key: SchemaConstants.PAYMENT_MODE.MONTHLY, value: RateConstants.PAYMENT_MODE.MONTHLY}
            ]
         )
         return _paymentMode;
      },
      rateId : function( resourceKey ) {
         if (resourceKey) {
            if (Utils.isMemberOf(resourceKey, SchemaConstants.CHARGE_TYPE)) {
               var rec = this.schema.call("matchChargeInfoRecord", {coverage: this.coverage, banding: banding.Band.text, chargeType: resourceKey});
               return rec.ChargePK.ChargeId.text;
            }
            if (Utils.isMemberOf(resourceKey, SchemaConstants.PREMIUM_TYPE)) {
               var rec = this.schema.call("matchPremiumInfoRecord", {coverage: this.coverage, banding: banding.Band.text, premiumType: resourceKey});
               return rec.PremiumPK.PremiumId.text;
            }
            if (Utils.isMemberOf(resourceKey, SchemaConstants.POLICY_VALUE)) {
               var rec = this.schema.call("matchPolicyValueInfoRecord", {coverage: this.coverage, banding: banding.Band.text, policyValueType: resourceKey});
               return rec.PolicyValuePK.PolicyValueId.text;
            }
            var _schema = this.schema.call("getSchema", null);
            return _schema.ProductSchema.ProductSchemaPK.ProductPK.ProductId.text +
               Utils.leftPad(_schema.ProductSchema.ProductSchemaPK.SchemaId);
         } else {
            var _schema = this.schema.call("getSchema", null);
            return _schema.ProductSchema.ProductSchemaPK.ProductPK.ProductId.text +
               Utils.leftPad(_schema.ProductSchema.ProductSchemaPK.SchemaId);
         }
      }
   };

}

// super function for generate default search VO
SearchVO = function( coverage, rateType, resourceKey, policyYear, ipo ) {
   var coverageForRateSearch = new RateSearchAttributeReader( coverage, ipo);

   // var assPrd = (coverage.product.productKey.associateProduct.productPK) ? coverage.product.productKey.associateProduct.productPK.productId : null;
   var location = coverageForRateSearch.location();
   var rateId = coverageForRateSearch.rateId(resourceKey);
   /*
   return {
           rateId : rateId,
           currency : coverage.currency.currencyPK.currencyId,
           location : location,
           rateType : rateType,
           valueDate : coverage.product.productKey.valueDate,
           issueDate : Utils.now(),
           associateProductPK : assPrd,
           attainAge : (Number(coverage.parties.party.insuredAge)  + Number(policyYear)),

           calcMethod : null,
           renewalPeriod : 0,
         };
    */
   var insured = getPrimaryInsured({coverage: coverage});
   return {
           rateId : rateId,
           currency : coverage.currency.currencyPK.currencyId,
           location : location,
           rateType : rateType,
           valueDate : coverage.product.productKey.valueDate,
           issueDate : Utils.now(),
           attainAge : (Number(insured.insuredAge)  + Number(policyYear)),

           calcMethod : null,
           renewalPeriod : 0,
         };
};

// premium rates
var handlerType1 = function(rateType, resourceKey) {
   return {
      rateType: rateType,
      resourceKey: resourceKey,
      createSearchRequest : function( coverage, policyYear, ipo ) {
         var searchVO = new SearchVO( coverage, this.rateType, this.resourceKey, policyYear, ipo );
         var insured = getPrimaryInsured({coverage: coverage});
         var bp = coverage.schema.call("calculateBenefitPeriod", {coverage:coverage, issueAge: insured.issueAge});

         var coverageForRateSearch = new RateSearchAttributeReader(coverage, ipo);
         var _sex = coverageForRateSearch.sex();
         var _channel = coverageForRateSearch.channel();
         var _smokingStatus = coverageForRateSearch.smokingStatus();
         var _waitingPeriod = coverageForRateSearch.waitingPeriod();
         var _benefitPeriod = coverageForRateSearch.benefitPeriod();
         var _occupation = coverageForRateSearch.occupation();
         var _noOfPerson = coverageForRateSearch.noOfPerson();
         var _otherOption = coverageForRateSearch.otherOption();
         var _waitingPeriod = 0;
         if (coverage.waiverPeriod) {
            _waitingPeriod = coverage.waiverPeriod;
         }
         var searchVOx = {
            coverageBenefitPeriod : bp,
            channel : _channel,
            issueAge : insured.insuredAge,
            sex : _sex,
            smokingStatus : _smokingStatus,
            band : coverage.banding.Band.text,
            ipo : ipo,
            waitingPeriod : _waitingPeriod,
            benefitPeriod : _benefitPeriod,
            occupation : _occupation,
            noOfPerson : _noOfPerson,
            otherOption : _otherOption,
            waiverPeriod : _waitingPeriod
         };

         var svo = Utils.extend(searchVO, searchVOx);
            //if (coverage.proposal.enableDebug) {
            _DB("handlerType1.svo: ", svo, "PE_ratemanager->");
            //}
         return svo;
      }
   };
};

// modal factor
var handlerType2 = function(rateType, resourceKey) {
   return {
      rateType: rateType,
      resourceKey: resourceKey,
      createSearchRequest: function( coverage, policyYear, ipo ) {
         searchVO = new SearchVO( coverage, this.rateType, this.resourceKey, policyYear, ipo );

         var coverageForRateSearch = new RateSearchAttributeReader(coverage, ipo);
         var _paymentMode = coverageForRateSearch.paymentMode();
         var searchVOx = {
            paymentMode : _paymentMode
         };
         var svo = Utils.extend( searchVO, searchVOx );
            //if (coverage.proposal.enableDebug) {
            _DB("handlerType2.svo: ", svo, "PE_ratemanaer->");
            //}
         return svo;
      }
   };
};

// modal factor
var handlerType3 = function(rateType, resourceKey) {
   return {
      rateType: rateType,
      resourceKey: resourceKey,
      createSearchRequest: function( coverage, policyYear, ipo ) {
         searchVO = new SearchVO( coverage, this.rateType, this.resourceKey, policyYear, ipo );

         var coverageForRateSearch = new RateSearchAttributeReader(coverage, ipo);
         var _paymentMode = coverageForRateSearch.paymentMode();
         var _participate = coverageForRateSearch.participate();
         var searchVOx = {
            paymentMode : _paymentMode,
            participate : _participate
         };
         var svo = Utils.extend( searchVO, searchVOx );
            //if (coverage.proposal.enableDebug) {
            _DB("handlerType2.svo: ", svo, "PE_ratemanaer->");
            //}
         return svo;
      }
   };
};

// refund premium rates
var handlerType4 = function(rateType, resourceKey) {
   return {
      rateType: rateType,
      resourceKey: resourceKey,
      createSearchRequest : function( coverage, policyYear, ipo ) {
         var schema = coverage.schema.implementation;
         var searchVO = new SearchVO( coverage, this.rateType, this.resourceKey, policyYear, ipo );
         var insured = getPrimaryInsured({coverage: coverage});

         var coverageForRateSearch = new RateSearchAttributeReader(coverage, ipo);
         var _sex = coverageForRateSearch.sex();
         var _channel = coverageForRateSearch.channel();
         var _smokingStatus = coverageForRateSearch.smokingStatus();
         var _waitingPeriod = coverageForRateSearch.waitingPeriod();
         var _benefitPeriod = coverageForRateSearch.benefitPeriod();
         var _occupation = coverageForRateSearch.occupation();
         var _noOfPerson = coverageForRateSearch.noOfPerson();
         var _otherOption = coverageForRateSearch.otherOption();
         var _refundPeriod = schema.ProductSchema.BasicParticular.RefundPeriod.text;
         var _otherOption = coverageForRateSearch.otherOption();
         var _waitingPeriod = 0;
         if (coverage.waiverPeriod) {
            _waitingPeriod = coverage.waiverPeriod;
         }
         var searchVOx = {
            channel : _channel,
            issueAge : insured.insuredAge,
            sex : _sex,
            smokingStatus : _smokingStatus,
            band : coverage.banding.Band.text,
            ipo : ipo,
            waitingPeriod : _waitingPeriod,
            benefitPeriod : _benefitPeriod,
            occupation : _occupation,
            noOfPerson : _noOfPerson,
            otherOption : _otherOption,
            waiverPeriod : _waiverPeriod,
            refundPeriod: _refundPeriod
         };

         var svo = Utils.extend(searchVO, searchVOx);
            //if (coverage.proposal.enableDebug) {
            _DB("handlerType4.svo: ", svo, "PE_ratemanager->");
            //}
         return svo;
      }
   };
};

// commission rates
var handlerType5 = function(rateType, resourceKey) {
   return {
      rateType: rateType,
      resourceKey: resourceKey,
      createSearchRequest : function( coverage, policyYear, ipo ) {
         var searchVO = new SearchVO( coverage, this.rateType, this.resourceKey, policyYear, ipo );
         var insured = getPrimaryInsured({coverage: coverage});

         var coverageForRateSearch = new RateSearchAttributeReader(coverage, ipo);
         var _sex = coverageForRateSearch.sex();
         var _channel = coverageForRateSearch.channel();
         var _smokingStatus = coverageForRateSearch.smokingStatus();
         var _waitingPeriod = coverageForRateSearch.waitingPeriod();
         var _benefitPeriod = coverageForRateSearch.benefitPeriod();
         var _occupation = coverageForRateSearch.occupation();
         var _noOfPerson = coverageForRateSearch.noOfPerson();
         var _otherOption = coverageForRateSearch.otherOption();
         var _waitingPeriod = 0;
         if (coverage.waiverPeriod) {
            _waitingPeriod = coverage.waiverPeriod;
         }
         var _paymentMode = coverageForRateSearch.paymentMode();
         var searchVOx = {
            channel : _channel,
            issueAge : insured.insuredAge,
            sex : _sex,
            smokingStatus : _smokingStatus,
            band : coverage.banding.Band.text,
            ipo : ipo,
            waitingPeriod : _waitingPeriod,
            benefitPeriod : _benefitPeriod,
            occupation : _occupation,
            noOfPerson : _noOfPerson,
            otherOption : _otherOption,
            waiverPeriod : _waitingPeriod,
            paymentMode : _paymentMode
         };

         var svo = Utils.extend(searchVO, searchVOx);
            //if (coverage.proposal.enableDebug) {
            _DB("handlerType1.svo: ", svo, "PE_ratemanager->");
            //}
         return svo;
      }
   };
};

var PERateManager = Class.define({

   init: function(args) {
      var handlersList = null;

      if (!Utils.isNothing(args)) {
         handlersList = args["handlers"];
      }

      if (Utils.isNothing(handlersList)) {
         handlersList = this.self.defaultHandlers(args);
      }

      var handlerResourceMap = {};
      var handlerMap = {};
      for(handlerKey in handlersList) {
         var handlerRec = handlersList[handlerKey];
         handlerMap[handlerRec.rateType] = handlerRec;
         handlerResourceMap[handlerRec.resourceKey] = handlerRec;
      }

      this.self.handlers = handlerMap;
      this.self.handlersByResourceKey = handlerResourceMap;
   },

   defaultHandlers: function(args) {
       /*
      var coreHandlers = [
         new handlerType1(RateConstants.RATETYPE.BASICPREMIUM, SchemaConstants.PREMIUM_TYPE.BASICPREMIUM),
         new handlerType2(RateConstants.RATETYPE.MODALFACTOR, SchemaConstants.CHARGE_TYPE.MODALFACTOR),
         new handlerType1(RateConstants.RATETYPE.COI, SchemaConstants.PREMIUM_TYPE.COSTOFINSURANCE),
         new handlerType1(RateConstants.RATETYPE.PREMIUM_ALLOCATION, SchemaConstants.PREMIUM_TYPE.ALLOCATION),
         new handlerType1(RateConstants.RATETYPE.TOPUP_PREMIUM_ALLOCATION, SchemaConstants.PREMIUM_TYPE.TOPUP_ALLOCATION),
         new handlerType1(RateConstants.RATETYPE.SURRENDER, SchemaConstants.CHARGE_TYPE.SURRENDER),
         new handlerType1(RateConstants.RATETYPE.COMMISSION, SchemaConstants.CHARGE_TYPE.COMMISSION),
         new handlerType1(RateConstants.RATETYPE.OCCUPATIONFACTOR),
         new handlerType3(RateConstants.RATETYPE.POLICYFEE, SchemaConstants.CHARGE_TYPE.POLICYFEE),
      ];
      */
      var coreHandlers = [
         new handlerType1(RateConstants.RATETYPE.BASICPREMIUM, SchemaConstants.PREMIUM_TYPE.BASICPREMIUM),
         new handlerType1(RateConstants.RATETYPE.COI, SchemaConstants.PREMIUM_TYPE.COSTOFINSURANCE),
         new handlerType1(RateConstants.RATETYPE.MINIMUMPREMIUM, SchemaConstants.PREMIUM_TYPE.MINIMUMPREMIUM),
         new handlerType3(RateConstants.RATETYPE.POLICYFEE, SchemaConstants.CHARGE_TYPE.POLICYFEE),
         new handlerType2(RateConstants.RATETYPE.MODALFACTOR, SchemaConstants.CHARGE_TYPE.MODALFACTOR),
         new handlerType1(RateConstants.RATETYPE.OCCUPATIONFACTOR),
         new handlerType1(RateConstants.RATETYPE.CASHVALUE, SchemaConstants.POLICY_VALUE.CASHVALUE),
         new handlerType1(RateConstants.RATETYPE.DIVIDEND, SchemaConstants.POLICY_VALUE.DIVIDEND),
         new handlerType1(RateConstants.RATETYPE.PAIDUPADDITION, SchemaConstants.POLICY_VALUE.PAIDUPADDITION),
         new handlerType1(RateConstants.RATETYPE.NSP, SchemaConstants.POLICY_VALUE.NSP),
         new handlerType1(RateConstants.RATETYPE.COUPON, SchemaConstants.POLICY_VALUE.COUPON),
         new handlerType4(RateConstants.RATETYPE.RETURNPREMIUM, SchemaConstants.POLICY_VALUE.RETURNPREMIUM),
         new handlerType1(RateConstants.RATETYPE.TAXCHARGE),
         new handlerType1(RateConstants.RATETYPE.DUMPIN),
         new handlerType1(RateConstants.RATETYPE.PROTECTION, SchemaConstants.POLICY_VALUE.PROTECTION),
         new handlerType1(RateConstants.RATETYPE.LOW_DIVIDEND, SchemaConstants.POLICY_VALUE.LOW_DIVIDEND),
         new handlerType1(RateConstants.RATETYPE.HIGH_DIVIDEND, SchemaConstants.POLICY_VALUE.HIGH_DIVIDEND),
         new handlerType1(RateConstants.RATETYPE.TOTALDISABILITY, SchemaConstants.POLICY_VALUE.TOTALDISABILITY),
         new handlerType1(RateConstants.RATETYPE.SEVEREDISABILITY, SchemaConstants.POLICY_VALUE.SEVEREDISABILITY),
         new handlerType1(RateConstants.RATETYPE.TDO, SchemaConstants.POLICY_VALUE.TDO),
         new handlerType1(RateConstants.RATETYPE.REDUCED_PAID_UP),
         new handlerType1(RateConstants.RATETYPE.ROLLING),
         new handlerType1(RateConstants.RATETYPE.BOS, SchemaConstants.CHARGE_TYPE.BOS),
         new handlerType1(RateConstants.RATETYPE.FUND_GROWTH),
         new handlerType1(RateConstants.RATETYPE.INITIAL_CHARGE, SchemaConstants.CHARGE_TYPE.INITIAL_CHARGE),
         new handlerType1(RateConstants.RATETYPE.SPECIAL_BONUS, SchemaConstants.POLICY_VALUE.SPECIAL_BONUS),
         new handlerType1(RateConstants.RATETYPE.LOYALTY_BONUS, SchemaConstants.POLICY_VALUE.LOYALTY_BONUS),
         new handlerType1(RateConstants.RATETYPE.IRR, SchemaConstants.POLICY_VALUE.IRR),
         new handlerType1(RateConstants.RATETYPE.COMMISSION, SchemaConstants.CHARGE_TYPE.COMMISSION),
         new handlerType1(RateConstants.RATETYPE.EXPENSE_FACTOR, SchemaConstants.CHARGE_TYPE.EXPENSE_FACTOR),
         new handlerType1(RateConstants.RATETYPE.PREMIUM_ALLOCATION, SchemaConstants.PREMIUM_TYPE.ALLOCATION),
         new handlerType1(RateConstants.RATETYPE.SUPPLEMENTARY, SchemaConstants.CHARGE_TYPE.SUPPLEMENTARY),
         new handlerType1(RateConstants.RATETYPE.SURRENDER, SchemaConstants.CHARGE_TYPE.SURRENDER),
         new handlerType1(RateConstants.RATETYPE.MONTHLY_FACTOR),
         new handlerType1(RateConstants.RATETYPE.DPM, SchemaConstants.CHARGE_TYPE.DPM),
         new handlerType1(RateConstants.RATETYPE.DCV, SchemaConstants.CHARGE_TYPE.DCV),
         new handlerType1(RateConstants.RATETYPE.PAYOR_BENEFIT, SchemaConstants.PREMIUM_TYPE.PAYOR_BENEFIT),
         new handlerType1(RateConstants.RATETYPE.CURRENT_FUND),
         new handlerType1(RateConstants.RATETYPE.LOAN),
         new handlerType2(RateConstants.RATETYPE.MODE_FEE, SchemaConstants.CHARGE_TYPE.MODE_FEE),
         new handlerType1(RateConstants.RATETYPE.REVERSIONARY_BONUS, SchemaConstants.POLICY_VALUE.REVERSIONARY_BONUS),
         new handlerType1(RateConstants.RATETYPE.DISCOUNT_FACTOR, SchemaConstants.POLICY_VALUE.DISCOUNT_FACTOR),
         new handlerType1(RateConstants.RATETYPE.MATURITY_BONUS, SchemaConstants.POLICY_VALUE.MATURITY_BONUS),
         new handlerType1(RateConstants.RATETYPE.TERMINAL_BONUS_DEATH, SchemaConstants.POLICY_VALUE.TERMINAL_BONUS_DEATH),
         new handlerType1(RateConstants.RATETYPE.TERMINAL_BONUS_SURRENDER, SchemaConstants.POLICY_VALUE.TERMINAL_BONUS_SURRENDER),
         new handlerType1(RateConstants.RATETYPE.AX_FACTOR, SchemaConstants.POLICY_VALUE.AX_FACTOR),
         new handlerType5(RateConstants.RATETYPE.MODAL_COMMISSION, SchemaConstants.CHARGE_TYPE.MODAL_COMMISSION),
         new handlerType1(RateConstants.RATETYPE.PROJECTED_IRR),
         new handlerType1(RateConstants.RATETYPE.HIGH_IRR),
         new handlerType1(RateConstants.RATETYPE.AUTOMATIC_PREMIUM_LOAN),
         new handlerType1(RateConstants.RATETYPE.REVERSIONARY_INTEREST),
         new handlerType1(RateConstants.RATETYPE.GUARANTEED_MINIMUM_DB, SchemaConstants.POLICY_VALUE.GUARANTEED_MINIMUM_DB),
         new handlerType1(RateConstants.RATETYPE.GUARANTEED_SURRENDER_VALUE, SchemaConstants.POLICY_VALUE.GUARANTEED_SURRENDER_VALUE),
         new handlerType1(RateConstants.RATETYPE.TRAILER_FEE),
         new handlerType1(RateConstants.RATETYPE.REVERSIONARY_BONUS_CV, SchemaConstants.POLICY_VALUE.REVERSIONARY_BONUS_CV),
         new handlerType1(RateConstants.RATETYPE.TOPUP_PREMIUM_ALLOCATION, SchemaConstants.PREMIUM_TYPE.TOPUP_ALLOCATION),
         new handlerType1(RateConstants.RATETYPE.LOW_REVERSIONARY_BONUS),
         new handlerType1(RateConstants.RATETYPE.PRODUCTION_CREDIT),
         new handlerType1(RateConstants.RATETYPE.DEPOSIT_INTEREST),
         new handlerType1(RateConstants.RATETYPE.LOW_DEPOSIT_INTEREST),
         new handlerType1(RateConstants.RATETYPE.LOW_TERMINAL_BONUS_SURRENDER),
         new handlerType1(RateConstants.RATETYPE.ADMINISTRATION_FEE),
         new handlerType1(RateConstants.RATETYPE.LOW_COUPON),
         new handlerType1(RateConstants.RATETYPE.COI_LOADING),
         new handlerType1(RateConstants.RATETYPE.ADVANCE_TOPUP_BONUS),
         new handlerType5(RateConstants.RATETYPE.TOPUP_COMMISSION, SchemaConstants.CHARGE_TYPE.TOPUP_COMMISSION),
         new handlerType1(RateConstants.RATETYPE.RETURNTAXCHARGE),
         new handlerType1(RateConstants.RATETYPE.LOW_REVERSIONARY_INTEREST),
         new handlerType1(RateConstants.RATETYPE.GUARANTEED_TUITION_FUND, SchemaConstants.POLICY_VALUE.GUARANTEED_TUITION_FUND),
         new handlerType1(RateConstants.RATETYPE.GUARANTEED_EDUCATION_ALLOWANCE, SchemaConstants.POLICY_VALUE.GUARANTEED_EDUCATION_ALLOWANCE),
         new handlerType1(RateConstants.RATETYPE.GUARANTEED_GRADUATION_GIFT, SchemaConstants.POLICY_VALUE.GUARANTEED_GRADUATION_GIFT),
         new handlerType1(RateConstants.RATETYPE.MIN_PLANNED_PREMIUM, SchemaConstants.PREMIUM_TYPE.MIN_PLANNED_PREMUM),
         new handlerType1(RateConstants.RATETYPE.MANAGEMENT_FEE, SchemaConstants.CHARGE_TYPE.MANAGEMENT_FEE),
         new handlerType1(RateConstants.RATETYPE.INITIAL_PREMIUM_CHARGE, SchemaConstants.CHARGE_TYPE.INITIAL_PREMIUM_CHARGE),
         new handlerType5(RateConstants.RATETYPE.MODAL_IRR, SchemaConstants.POLICY_VALUE.MODAL_IRR),
         new handlerType2(RateConstants.RATETYPE.TOPUP_MODE_FEE),
         new handlerType1(RateConstants.RATETYPE.PREMIUM_HOLIDAY_CHARGE, SchemaConstants.CHARGE_TYPE.PREMIUM_HOLIDAY_CHARGE)
      ];
      return coreHandlers;
   },

   lookupHandler: function( rateType ) {
      //return this.handlers.get( rateType );
      return this.self.handlers[rateType];
   },

   lookupHandlerByResourceKey: function( resourceKey ) {
      //return this.handlers.get( rateType );
      return this.self.handlersByResourceKey[resourceKey];
   },

   lookupRates: function( args ) {
      var coverage = args["coverage"];
      var rateType = args["rateType"];
      var resourceKey = args["resourceKey"];
      var ipo = args["ipo"];

      var handler;
      if (!Utils.isNothing(rateType)) {
         handler = this.self.lookupHandler( rateType );
      }
      if (Utils.isNothing(handler)) {
         if (!Utils.isNothing(resourceKey)) {
            handler = this.self.lookupHandlerByResourceKey( resourceKey );
         }
      }
       //console.log("handler = " + JSON.stringify( handler));
      if (handler) {
         var svo = handler.createSearchRequest( coverage, 0, ipo );

         //console.log("svo[" + handler.resourceKey + "] = " + JSON.stringify(svo));

         var schema = coverage.schema.call("getSchema", null);
         var _rates = PEservice.readRates( schema.ProductSchema.ProductSchemaPK, svo );
         if ( Utils.compareBits(_rates.header.options, RateConstants.RATE_OPTION.READ_BY_ATTAINAGE ) ) {
            _DB("", "INFO: (1) It is ATTAINAGE rates.", "");
            if ( !Utils.compareBits(_rates.header.options, RateConstants.RATE_OPTION.YRT) ) {
               _DB("", "INFO: (2) It is NOT YRT rates.", "");
               if ( !Utils.isNothing( svo.coverageBenefitPeriod ) ) {
                  _DB("", "INFO: (3) coverageBenefitPeriod is found.", "");
                  if ( _rates.rates.length == 1 ) {
                     _DB("", "INFO: (3) only 1 rate is returned in first call.", "");
                     var debug = coverage.proposal.enableDebug;

                     coverage.proposal.enableDebug = false;
                     var svo2 = Utils.extend({}, svo);
                     for(var attainAgeInc=1; attainAgeInc<svo.coverageBenefitPeriod; attainAgeInc++) {
                        svo2.attainAge = svo2.issueAge + attainAgeInc;
                        var _rates2 = PEservice.readRates( schema.ProductSchema.ProductSchemaPK, svo2 );
                        for(var rateIdx in _rates2.rates) {
                           _rates.rates.push(_rates2.rates[rateIdx]);
                        }
                     }
                     coverage.proposal.enableDebug = debug;
                  }
               }
            }
         }
         var _xrates = Utils.extend(_rates, ratesMethods);
         return _xrates;
      } else {
         if (rateType) {
            throw "rate handler (" + rateType + ") cannot be find.";
         }
         if (resourceKey) {
            throw "rate handler (" + resourceKey + ") cannot be find.";
         }
      }
   },

   lookupRate: function( coverage, rateType, policyYear, ipo ) {
      var coverage = args["coverage"];
      var rateType = args["rateType"];
      var resourceKey = args["resourceKey"];
      var policyYear = args["policyYear"];
      var ipo = args["ipo"];

      //var handler = this.self.lookupHandler( rateType );
      var handler;
      if (!Utils.isNothing(rateType)) {
         handler = this.self.lookupHandler( rateType );
      }
      if (Utils.isNothing(handler)) {
         if (!Utils.isNothing(resourceKey)) {
            handler = this.self.lookupHandlerByResourceKey( resourceKey );
         }
      }
      if (handler) {
         var svo = handler.createSearchRequest( coverage, policyYear, ipo );
         var schema = coverage.schema.call("getSchema", null);
         var _rate = PEservice.readRate( schema.ProductSchema.ProductSchemaPK, svo );
         return _rate;
      } else {
         if (rateType) {
            throw "rate handler (" + rateType + ") cannot be find.";
         }
         if (resourceKey) {
            throw "rate handler (" + resourceKey + ") cannot be find.";
         }
      }
   }

});
// abstract factory
var PEservice = {
   dataSource: "WebService",
   serviceImpl: function() {
      return peServiceLocator.locateService();
   },
   loadCatalog: function(productCode) {
      return this.serviceImpl().call("loadCatalog", productCode, null);
   },
   loadSchema: function(productId, schemaInfo) {
      return this.serviceImpl().call("loadSchema", productId, schemaInfo, null);
   },
   listBaseProducts: function(planCodes, language, effectiveDate, location) {
      return this.serviceImpl().call("listBaseProducts", {planCodes: planCodes, language: language, effectiveDate: effectiveDate, location: location});
   },
   listRiders: function(planCodes, language, effectiveDate, location) {
      return this.serviceImpl().call("listRiders", {planCodes: planCodes, language: language, effectiveDate: effectiveDate, location: location});
   },
   readRates: function(schemaPk, searchRequest) {
      return this.serviceImpl().call("readRates", schemaPk, searchRequest, null);
   },
   readRate: function(schemaPk, searchRequest) {
      return this.serviceImpl().call("readRate", schemaPk, searchRequest, null);
   }
};

//var dataSource = "IOS";
var peServiceLocator = ServiceLocatorPattern.define(IPEService,
   function() {
      return PEservice.dataSource;
   }
);

peServiceLocator.register("StubService", PEServiceStubImpl.create(null));
peServiceLocator.register("IosService", PEServiceIOSImpl.create(null));
peServiceLocator.register("WebService", PEServiceWEBImpl.create(null));
// core prototype extension
var CatalogManagerImpl = Class.define({

   init: function(args) {
      this.self.cache = [];
   },

   lookup: function(args) {
      var productCode = args["productCode"];

      for (key in this.self.self.cache) {
         var cacheRecord = this.self.self.cache[key];
         if (cacheRecord.productCode == productCode) {
            return cacheRecord.manager;
         }
      }
      return this.self.load({productCode: productCode});

   },

   load: function(args) {
      var productCode = args["productCode"];

      var _catalog = PEservice.loadCatalog(productCode);
      var _instance = this.self.prepareInstance({catalog: _catalog});

      this.self.cache.push({
         productCode: productCode,
         manager: _instance
      });
      return _instance;

   },

   prepareInstance: function(args) {
      var catalog = args["catalog"];
      var cf = Repository.getManager(SystemComponents.MANAGER.CATALOG_FACTORY);
      return cf.create({catalogDefintion: catalog});
   }

});
var SchemaManagerImpl = Class.define( {

   init: function(args) {
      this.self.cache = [];
   },

   lookup: function(args) {
      var coverage = args["coverage"];

      var catalog = this.self.getCatalog({productCode: coverage.product.productKey.primaryProduct.productPK.productId});
      var schemaInfo = catalog.call("lookupSchema", {coverage: coverage});
      var schema = this.self.lookupCache({coverage: coverage.product.productKey.primaryProduct.productPK.productId, schemaId: schemaInfo.schemaId.text});
      if (schema == null) {
         schema = this.self.loadIntoCache({coverage: coverage, schemaInfo: schemaInfo});
      }
      return schema;
   },

   load: function(args) {
      var coverage = args["coverage"];

      var catalog = this.self.getCatalog(coverage.product.productKey.primaryProduct.productPK.productId);
      var schemaInfo = catalog.call("lookupSchema", {coverage: coverage});
      return this.self.loadIntoCache({coverage: coverage, schemaInfo: schemaInfo});
   },

   lookupCache: function(args) {
      var productCode = args["productCode"];
      var schemaId = args["schemaId"];

      for (key in this.self.cache) {
         var cacheRecord = this.self.cache[key];
         if (cacheRecord.productCode == productCode && cacheRecord.schemaId == schemaId) {
            return cacheRecord.manager;
         }
      }
      return null;

   },

   getCatalog: function(args) {
      var productCode = args["productCode"];

      var cm = Repository.getManager(SystemComponents.MANAGER.CATALOG_HELPER);
      var catalogManager = cm.call("lookup", {
         productCode: productCode
      });
      if (!(catalogManager)) {
         cm.call("load", {productCode: productCode});
         catalogManager = cm.call("lookup", {
            productCode: productCode
         });
      }
      return catalogManager;
   },

   loadIntoCache: function(args) {
      var coverage = args["coverage"];
      var schemaInfo = args["schemaInfo"];

      var schema = PEservice.loadSchema(coverage.product.productKey.primaryProduct.productPK.productId, schemaInfo);
      var schemaManager = this.self.prepareInstance({schema: schema});
      this.self.cache.push({
         productCode: coverage.product.productKey.primaryProduct.productPK.productId,
         schemaId: schemaInfo.schemaId.text,
         manager: schemaManager
      });
      return schemaManager;
   },

   prepareInstance: function(args) {
      var schema = args["schema"];
      var sf = Repository.getManager(SystemComponents.MANAGER.SCHEMA_FACTORY);
      return sf.create({schemaDefinition: schema});
   }

} );
var MessageManagerImpl = Class.define( {

   init: function(args) {
      this.self.messages = [];
      var messages = args["messages"];
      if (messages != undefined && messages != null) {
         this.self.messages.push(messages);
      }
   },

   getMessage: function(args) {
      var error = args["error"];
      var language = args["language"];

      if (this.self.messages.length > 0) {
         var msg = this.self.messages[0][error.code];
         if (msg) {
            var warning = false;
            if (!Utils.isNothing(msg['warning'])) {
                warning = msg['warning'];
            }
            var localMessage = msg[language];
            if (!localMessage) localMessage = msg["en"];
            if (!localMessage) localMessage = "Error " + code + " occured!!";
            userMessage = localMessage.replace(/%\w+%/g, function(all) {
               return error.arguments[all] || all;
            });
            return {
               parameters: error.arguments,
               error: error.code,
               code: msg.uiCode,
               warning: warning,
               message: userMessage
            };
         } else {
            console.log("ERROR: error code (" + error.code + ") cannot be found in messages!");
            return {
               parameters: error.arguments,
               error: error.code,
               code: null,
               warning: false,
               message: "Error " + error.code + " occured but no such error is defined in system."
            };
         }
      } else {
         console.log("ERROR: no error message is loaded into engine.");
         return null;
      }
   }

} );
// EventsManager
// - determine which event list is for proposal
// - return the event list instance
var EventsManagerImpl = Class.define({
   eventProviders: [],
   init: function(args) {
      this.self.eventProviders.push(UVLife_Events.create());
   },
   locateProviders: function(args) {
      var proposal = args['proposal'];
      var basePlan = proposal.coverageInfo;

      var events = null;
      var provider = null;
      // try matchProductId
      for (ep in this.self.eventProviders) {
         var eventProviderInstance = this.self.eventProviders[ep];
         var eventProvider = IEventsProvider.appliedTo(eventProviderInstance);
         var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
         var catalog = basePlan.catalog;

         var providerName = eventProvider.call("providerName", args);
         console.log('matching ProductId - eventProvider: ' + providerName);
         var isMatch = eventProvider.call("matchProductId", {productId:productId, catalog:catalog});
         if (isMatch) {
            provider = eventProviderInstance;
            break;
         }
      }
      // try matchProductType
      if (Utils.isNothing(provider)) {
         var providerMatches = 0;
         for (ep in this.self.eventProviders) {
            var eventProviderInstance = this.self.eventProviders[ep];
            var eventProvider = IEventsProvider.appliedTo(eventProviderInstance);
            var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
            var catalog = basePlan.catalog;

            var providerName = eventProvider.call("providerName", args);
            console.log('matching ProductTypes - eventProvider: ' + providerName);
            var matches = eventProvider.call("matchProductType", {productId:productId, catalog:catalog});
            if (matches > 0 && matches > providerMatches) {
               providerMatches = matches;
               provider = eventProviderInstance;
            }
            console.log('matching ProductTypes - eventProvider (' + providerName + ') results: ' + matches);
         }
      }
      if (Utils.isNothing(provider)) {
         throw "Event provider cannot be located."
      }
      var providerName = IEventsProvider.appliedTo(provider).call("providerName", args);
      console.log("EventManagerImpl locateProviders: " + providerName);
      return provider;
   },
   //iterationUnit: function(args) {
   //   var proposal = args['proposal'];
   //   var basePlan = proposal.coverageInfo;
   //   if (basePlan.catalog.call("isUVL", {})) {
   //      return Number(1);
   //   } else {
   //      return Number(12);
   //   }
   //},
   //calculateIterationPeriod: function(args) {
   //   var benefitPeriod = args['benefitPeriod'];
   //   var iterationUnit = args['iterationUnit'];
   //   var iterations = _R(benefitPeriod * iterationUnit, 0);
   //   return iterations;
   //},
   addEventProvider: function(args) {
      var provider = args["provider"];
      if (provider) {
         this.self.eventProviders.push(provider);
      }
   },
   showEventProviders: function(args) {
      _D("", '[ Event Providers ]', "");
      for(ei in this.self.eventProviders) {
         var eg = this.self.eventProviders[ei];
         var egName = IEventsProvider.appliedTo(eg).call('providerName', {});
         _D("", 'event provider: ' + egName, "");
      }
   },
   events: function(args) {
      var proposal = args['proposal'];
      var basePlan = proposal.coverageInfo;

      var provider = this.self.locateProviders(args);
      var benefitPeriod = basePlan.schema.call("calculateBenefitPeriod", {
         coverage: basePlan
      });
      var ppp = basePlan.schema.call("calculatePremiumPayingPeriod", {
         coverage: basePlan
      });
      //var iterationUnit = this.self.iterationUnit({
      //   proposal: proposal
      //});
      //var iterations = this.self.calculateIterationPeriod({
      //   benefitPeriod: benefitPeriod,
      //   iterationUnit: 12
      //});

      var eventItems = IEventsProvider.appliedTo(provider).call("events", args);
      var columnItems = IEventsProvider.appliedTo(provider).call("columns", args);
      var iterations = IEventsProvider.appliedTo(provider).call("iterations", args);
      var iterationUnit = IEventsProvider.appliedTo(provider).call("iterationUnit", args);

      return {
         provider: provider,
         items: eventItems,
         columnItems: columnItems,
         metadata: {
            iterations: iterations,
            iterationUnit: iterationUnit,
            benefitPeriod: benefitPeriod,
            premiumPayingPeriod: ppp
         }
      };
   }
});
// Data key driven calculations
// mainly design for different return types
var DataDrivenCalculatorImpl = Class.define({
   init: function(args) {
      this.self.context = {};
      this.self.context.drivenKeys = args["drivenKeys"];
      this.self.context.drivenDatas = args["drivenDatas"];
      this.self.result = {};
   },
   data: function(args) {
      var key = args["drivenKey"];
      return this.self.context.drivenDatas[key];
   },
   calculate: function(args) {
      var calculateBlock = args["calculateBlock"];

      var dataSeries = Utils.iterator(this.self.context.drivenDatas);
      //console.log(dataSeries);

      for (idx in this.self.context.drivenKeys) {
         var drivenKey = this.self.context.drivenKeys[idx];

         var dataList = [];
         for (seriesIdx in dataSeries) {
            var data = dataSeries[seriesIdx];
            dataList.push(data[drivenKey]);
         }

         //console.log(dataList);
         if (dataList.length == 1) {
            this.self.result[drivenKey] = calculateBlock(dataList[0]);
         } else {
            this.self.result[drivenKey] = calculateBlock(dataList);
         }
      }
      return this.self.result;
   },
   result: function(args) {
      return this.self.result;
   }
});

// fund proposal
var FundImpl = Class.define({
   init: function(args) {
      if (Utils.isNothing(this.self.context)) {
         this.self.context = {};
      }

      var context = args["context"];
      var metadata = context.events.metadata;
      var proposal = context.proposal;
      var basePlan = proposal.coverageInfo;
      var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
      var precision = context.precision;
      if (!precision) {
          precision = 15;
      }

      this.self.context['iteratorContext'] = context;

      this.self.context.noOfRolloingPerYear = metadata.iterationUnit;
      this.self.context.noOfInterestRolling = 0;
      this.self.context.surrenderValueRates = context.rates[productId][SchemaConstants.CHARGE_TYPE.SURRENDER];
      this.self.context.precision = precision;

      //console.log("args = " + JSON.stringify(args));
      var fundConfig = args['fundConfig'];
      var fundId = fundConfig["fundId"];
      this.self.context["fundId"] = fundId;
      //console.log("fundId = " + fundId);
      this.self.context['fund'] = {};
      this.self.context['interestRate'] = {};
      this.self.context['annualInterestRate'] = {};
      for (returnTypeKey in PublicConstants.FUND_RETURN) {
         var returnType = PublicConstants.FUND_RETURN[returnTypeKey];
         this.self.context.interestRate[returnType] = fundConfig[returnType];
         this.self.context.annualInterestRate[returnType] = fundConfig['annualInterestRate'][returnType];
         this.self.context.fund[returnType] = Number(0);
      }
      this.self.context['payoutRatio'] = fundConfig['payoutRatio'];
      //this.self.context['policyOptions'] = fundConfig['policyOptions'];
      this.self.context['surrenderRate'] = fundConfig['surrenderRate'];
      this.self.context['isSurrenderChargeApplicable'] = fundConfig['isSurrenderChargeApplicable'];

      //console.log(JSON.stringify(this.self.context.fund));
      //if (this.self.context.proposal.enableDebug) {
      console.log("--------------------------------------------------");
      _DB("fundId", this.self.context.fundId, "FundImpl->");
      _DB("interestRate", this.self.context.interestRate, "FundImpl->");
      _DB("annualInterestRate", this.self.context.annualInterestRate, "FundImpl->");
      _DB("payoutRatio", this.self.context.payoutRatio, "FundImpl->");
      _DB("precision", this.self.context.precision, "FundImpl->");
      //_DB("policyOptions", this.self.context.policyOptions, "FundImpl->");
      _DB("surrenderRate", this.self.context.surrenderValueRates, "FundImpl->");
      _DB("isSurrenderChargeApplicable", this.self.context.isSurrenderChargeApplicable, "FundImpl->");
      console.log("--------------------------------------------------");
      //}
   },
   dumpin: function(args) {
      var amount = args['amount'];
      for (returnTypeKey in PublicConstants.FUND_RETURN) {
         var returnType = PublicConstants.FUND_RETURN[returnTypeKey];
         var currentBalance = Number(this.self.context.fund[returnType]);
         this.self.context.fund[returnType] = currentBalance + Number(amount);
         _DBR("fundBalance(" + returnType +  ") = currentBalance(" + currentBalance + ") + dumpin(" + amount + ")", this.self.context.fund[returnType], "FundImpl->" ,this.self.context.iteratorContext.runningPolicyValues.months);
      }
   },
   withdraw: function(args) {
      var amount = args['amount'];
      for (returnTypeKey in PublicConstants.FUND_RETURN) {
         var returnType = PublicConstants.FUND_RETURN[returnTypeKey];
         var currentBalance = Number(this.self.context.fund[returnType]);
         this.self.context.fund[returnType] = currentBalance - Number(amount);
         _DBR("fundBalance(" + returnType +  ") = currentBalance(" + currentBalance + ") - withdraw(" + amount + ")", this.self.context.fund[returnType], "FundImpl->" ,this.self.context.iteratorContext.runningPolicyValues.months);
      }
   },
   withdraws: function(args) {
      var amounts = args['amounts'];
      for (returnTypeKey in PublicConstants.FUND_RETURN) {
         var returnType = PublicConstants.FUND_RETURN[returnTypeKey];
         if (!Utils.isNothing(amounts[returnType])) {
            var currentBalance = Number(this.self.context.fund[returnType]);
            this.self.context.fund[returnType] = currentBalance - Number(amounts[returnType]);
            _DBR("fundBalance(" + returnType +  ") = currentBalance(" + currentBalance + ") - withdraw(" + Number(amounts[returnType]) + ")", this.self.context.fund[returnType], "FundImpl->" ,this.self.context.iteratorContext.runningPolicyValues.months);
         }
      }
   },
   dumpins: function(args) {
      var amounts = args['amounts'];
      for (returnTypeKey in PublicConstants.FUND_RETURN) {
         var returnType = PublicConstants.FUND_RETURN[returnTypeKey];
         if (!Utils.isNothing(amounts[returnType])) {
            var currentBalance = Number(this.self.context.fund[returnType]);
            this.self.context.fund[returnType] = currentBalance + Number(amounts[returnType]);
            _DBR("fundBalance(" + returnType +  ") = currentBalance(" + currentBalance + ") + dumpin(" + Number(amounts[returnType]) + ")", this.self.context.fund[returnType], "FundImpl->" ,this.self.context.iteratorContext.runningPolicyValues.months);
         }
      }
   },
   payout: function(args) {
      var payoutCash = {};
      var payRate = {};
      if (!Utils.isNothing(this.self.context['payoutRatio'])) {
         for (returnTypeKey in PublicConstants.FUND_RETURN) {
            var returnType = PublicConstants.FUND_RETURN[returnTypeKey];
            var payRate = Math.min(Number(this.self.context.payoutRatio), _R(Number(this.self.context.annualInterestRate[returnType]) / Number(12), 15));
            if (Number(payRate) > 0) {
               //var effectiveInterestRate = Number(Number(this.self.context.interestRate[returnType]) - (Number(payRate) / Number(100))).toFixed(15);
               //this.self.context.effectiveInterestRate[returnType]= Number(effectiveInterestRate);
               var payoutOut = _R( Number(this.self.context.fund[returnType]) * Number(payRate) / Number(100), this.context.precision);
               payoutCash[returnType] = Number(payoutOut).valueOf();
            }
         }
         this.self.withdraws({
            amounts: payoutCash
         });
      }
      return payoutCash;
   },
   rollingInterest: function(args) {
      for (returnTypeKey in PublicConstants.FUND_RETURN) {
         var returnType = PublicConstants.FUND_RETURN[returnTypeKey];
         if (!Utils.isNothing(this.self.context.interestRate[returnType])) {
            var currentBalance = this.self.context.fund[returnType];
            if (currentBalance >= 0) {
                var interest = _R(Number(this.self.context.interestRate[returnType]) * Number(currentBalance), this.context.precision);
                _DBR("interest(" + returnType + ") = round( interestRate(" + this.self.context.interestRate[returnType] + ") * fund(" + this.self.context.fund[returnType] + "), " + this.context.precision + ")", interest, "FundImpl->" ,this.self.context.iteratorContext.runningPolicyValues.months);

                this.self.context.fund[returnType] = _R( Number(currentBalance) + Number(interest) ,this.context.precision);
                _DBR("fund(" + returnType + ") = interest(" + interest + ") + fund(" + currentBalance + ")", this.self.context.fund[returnType], "FundImpl->" ,this.self.context.iteratorContext.runningPolicyValues.months);
            }
         }
      }
      //_DB("fundBalances after interest rolling", this.self.context.fund, "FundImpl->");
      this.self.context.noOfInterestRolling++;
   },
   fundBalances: function(args) {
      var fundValues = {};
      for (returnTypeKey in PublicConstants.FUND_RETURN) {
         var returnType = PublicConstants.FUND_RETURN[returnTypeKey];
         fundValues[returnType] = _R(this.self.context.fund[returnType], this.context.precision);
      }
      return fundValues;
   },
   surrenderValues: function(args) {
      var year = parseInt(String(this.self.context.noOfInterestRolling / this.self.context.noOfRolloingPerYear));
      if (this.self.context.isSurrenderChargeApplicable) {
         if (!Utils.isNothing(this.self.context.surrenderValueRates)) {
            var surrenderRate = this.self.context.surrenderValueRates.rates[year];
            var surrenderValues = {};
            for (returnTypeKey in PublicConstants.FUND_RETURN) {
               var returnType = PublicConstants.FUND_RETURN[returnTypeKey];
               surrenderValues[returnType] = _R(this.self.context.fund[returnType] * (1 - surrenderRate), this.context.precision);
            }
            return surrenderValues;
         } else {
            return this.self.fundBalances(args);
         }
      } else {
         return this.self.fundBalances(args);
      }
   }
});

// fund proposal
var FundPoolImpl = Class.define({
   init: function(args) {
      if (Utils.isNothing(this.self.context)) {
         this.self.context = {};
      }
      this.self.context.fundPools = {};
      this.self.context.allocations = {};
      var context = args["context"];
      var metadata = context.events.metadata;
      var proposal = context.proposal;
      var basePlan = proposal.coverageInfo;
      var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
      var precision = context.precision;
      if (!precision) {
          precision = 15;
      }
      var interestRatePrecision = context.interestRatePrecision;
      if (!interestRatePrecision) {
          interestRatePrecision = 15;
      }

      this.self.context['iteratorContext'] = context;

      this.self.context.noOfRolloingPerYear = metadata.iterationUnit;
      this.self.context.noOfInterestRolling = 0;
      this.self.context.surrenderValueRates = context.rates[productId][SchemaConstants.CHARGE_TYPE.SURRENDER];
      this.self.context.precision = precision;
      this.self.context.interestRatePrecision = interestRatePrecision;

      var fundSelections = Utils.iterator(proposal.funds.fundRecord);
      //console.log("fundSelections : " + JSON.stringify(fundSelections));
      for (fundId in fundSelections) {
         var fundRecord = fundSelections[fundId];
         if (Number(fundRecord.allocation) > 0) {
            //console.log("fundRecord.returnRate[" + fundRecord._code + "] = " + JSON.stringify(fundRecord));
            var fundConfig = {
               fundId: fundRecord._code
            };
            if (fundRecord.returnRate > 0) {
               fundConfig[PublicConstants.FUND_RETURN.LOW] = _R(Math.pow(1 + fundRecord.returnRate / 100, 1 / (12/metadata.iterationUnit)) - 1, interestRatePrecision);
            }
            if (fundRecord.returnRateMedium > 0) {
               fundConfig[PublicConstants.FUND_RETURN.MEDIUM] = _R(Math.pow(1 + fundRecord.returnRateMedium / 100, 1 / (12/metadata.iterationUnit)) - 1, interestRatePrecision);
            }
            if (fundRecord.returnRateHigh > 0) {
               fundConfig[PublicConstants.FUND_RETURN.HIGH] = _R(Math.pow(1 + fundRecord.returnRateHigh / 100, 1 / (12/metadata.iterationUnit)) - 1, interestRatePrecision);
            }
            fundConfig['payoutRatio'] = fundRecord.targetPayoutRate;

            fundConfig['annualInterestRate'] = {};
            fundConfig.annualInterestRate[PublicConstants.FUND_RETURN.LOW] = fundRecord.returnRate;
            fundConfig.annualInterestRate[PublicConstants.FUND_RETURN.MEDIUM] = fundRecord.returnRateMedium;
            fundConfig.annualInterestRate[PublicConstants.FUND_RETURN.HIGH] = fundRecord.returnRateHigh;
            //fundConfig['policyOptions'] = context.policyOptions;
            this.self.context.allocations[fundRecord._code] = fundRecord.allocation;
            fundConfig['isSurrenderChargeApplicable'] = true;
            this.self.context.fundPools[fundRecord._code] = FundImpl.create({
               fundConfig: fundConfig,
               context: context
            });
         }
      }
   },
   allocations: function(args) {
      var allocationRatios = {};
      var balanceTotals = this.self.fundBalances();

      for (fundPoolKey in this.self.context.fundPools) {
         //console.log(fundPoolKey);

         if (Utils.isNothing(allocationRatios[fundPoolKey])) {
            allocationRatios[fundPoolKey] = {};
         }
         var balances = IFund.appliedTo(this.self.context.fundPools[fundPoolKey]).call("fundBalances", {});
         //console.log(JSON.stringify(balances));

         for (balanceIdx in balances) {
            if (Number(balanceTotals[balanceIdx]) == 0) {
               allocationRatios[fundPoolKey][balanceIdx] = _R(this.self.context.allocations[fundPoolKey] / 100, 15);
            } else {
               allocationRatios[fundPoolKey][balanceIdx] = _R(balances[balanceIdx] / balanceTotals[balanceIdx], 15);
            }
         }
      }
      return allocationRatios;
   },
   dumpin: function(args) {
      var amount = args['amount'];
      var amounts = args['amounts'];
      var allocationRatios = this.self.allocations();

      var remainAmount = amount;
      var remainAmounts = Utils.extend({}, amounts);

      //console.log("allocationRatios (i.e. pool allocation ratio) = " + JSON.stringify(allocationRatios));
      var i = 0;
      _DBR("allocationRatios", allocationRatios, "FundPoolImpl->" ,this.self.context.iteratorContext.runningPolicyValues.months);
      for (fundPoolKey in this.self.context.fundPools) {
         i = i + 1;
         var lastOne = (i == this.self.context.fundPools.length);

         var pool = this.self.context.fundPools[fundPoolKey];
         for (returnType in pool.context.fund) {
            if (Utils.isNothing(pool.context.fund[returnType])) {
               pool.context.fund[returnType] = 0;
            }
            //console.log("fund before = " + pool.context.fund[returnType]);
            var allocatedAmount = Number(0);
            if (!Utils.isNothing(amounts)) {
               if (!lastOne) {
                   allocatedAmount = _R(Number(amounts[returnType]) * Number(allocationRatios[fundPoolKey][returnType]), this.context.precision);
                   remainAmounts[returnType] = remainAmounts[returnType] - allocatedAmount;
               } else {
                   allocatedAmount = remainAmounts[returnType];
               }
            } else {
               if (!lastOne) {
                   allocatedAmount = _R(amount * allocationRatios[fundPoolKey][returnType], this.context.precision);
                   remainAmount = remainAmount - allocatedAmount;
               } else {
                   allocatedAmount = remainAmount;
               }
            }

            var currentBalance = Number(pool.context.fund[returnType]);
            pool.context.fund[returnType] = _R(currentBalance + Number(allocatedAmount), this.context.precision);

            if (allocatedAmount < 0) {
                _DBR("fundBalance(" + fundPoolKey + ")[" + returnType + "] = currentBalance(" + currentBalance +  ") - amount(" + Number(-allocatedAmount) + ")", pool.context.fund[returnType], "FundPoolImpl->" ,this.self.context.iteratorContext.runningPolicyValues.months);
            } else {
                _DBR("fundBalance(" + fundPoolKey + ")[" + returnType + "] = currentBalance(" + currentBalance +  ") + amount(" + Number(allocatedAmount) + ")", pool.context.fund[returnType], "FundPoolImpl->" ,this.self.context.iteratorContext.runningPolicyValues.months);
            }
            //console.log("fund after = " + pool.context.fund[returnType]);
         }
      }
   },
   withdraw: function(args) {
      var amount = args['amount'];
      var amounts = args['amounts'];
      if (!Utils.isNothing(amounts)) {
         var deductions = {};
         for (amountsIdx in amounts) {
            deductions[amountsIdx] = Number(-1 * amounts[amountsIdx]);
         }
         //console.log("deductions = " + JSON.stringify(deductions));
         this.self.dumpin({
            amounts: deductions
         });
      } else {
         this.self.dumpin({
            amount: -amount
         });
      }
   },

   withdraws: function(args) {
      var amounts = args['amounts'];
      if (!Utils.isNothing(amounts)) {
         for (fundPoolKey in this.self.context.fundPools) {
            var pool = this.self.context.fundPools[fundPoolKey];
            IFund.appliedTo(fundPool).call("withdraw", {
               amounts: amounts
            });
         }
      }
   },

   dumpins: function(args) {
      var amounts = args['amounts'];
      for (fundPoolKey in this.self.context.fundPools) {
         var pool = this.self.context.fundPools[fundPoolKey];
         IFund.appliedTo(pool).call("dumpins", {
            amounts: amounts
         });
      }
   },

   payout: function(args) {
      var totalCashOuts = {};
      for (fundPoolKey in this.self.context.fundPools) {
         var pool = this.self.context.fundPools[fundPoolKey];
         var cashOut = IFund.appliedTo(pool).call("payout", {});
         for (cashOutKey in cashOut) {
            if (Utils.isNothing(totalCashOuts[cashOutKey])) {
               totalCashOuts[cashOutKey] = Number(0);
            }
            totalCashOuts[cashOutKey] = Number(totalCashOuts[cashOutKey]) + Number(cashOut[cashOutKey]);
         }
      }
      return totalCashOuts;
   },
   rollingInterest: function(args) {
      for (fundPoolKey in this.self.context.fundPools) {
         var pool = this.self.context.fundPools[fundPoolKey];
         IFund.appliedTo(pool).call("rollingInterest", {});
      }
      this.self.context.noOfInterestRolling++;
   },
   fundBalances: function(args) {
      var fundReturns = {};
      for (fundPoolKey in this.self.context.fundPools) {
         var pool = this.self.context.fundPools[fundPoolKey];
         var poolFunds = IFund.appliedTo(pool).call("fundBalances", {});
         //console.log("poolFunds = " + JSON.stringify(poolFunds));

         for (fundKey in poolFunds) {
            if (Utils.isNothing(fundReturns[fundKey])) {
               fundReturns[fundKey] = 0;
            }
            fundReturns[fundKey] = Number(fundReturns[fundKey]) + Number(poolFunds[fundKey]);
         }
      }
      //console.log("Pool.fundBalances = " + JSON.stringify(fundReturns));
      return fundReturns;
   },
   surrenderValues: function(args) {
      var surrenderVal = {};
      if (!Utils.isNothing(this.self.context.surrenderValueRates)) {
         for (fundPoolKey in this.self.context.fundPools) {
            var pool = this.self.context.fundPools[fundPoolKey];
            var poolSurrenders = IFund.appliedTo(pool).call("surrenderValues");
            for (fundKey in poolSurrenders) {
               if (Utils.isNothing(surrenderVal[fundKey])) {
                  surrenderVal[fundKey] = 0;
               }
               surrenderVal[fundKey] = Number(surrenderVal[fundKey]) + Number(poolSurrenders[fundKey]);
            }
         }
         return surrenderVal;
      } else {
         return this.self.fundBalances(args);
      }
   }

});

// fund proposal
var PortfolioImpl = Class.define({
   init: function(args) {
      if (Utils.isNothing(this.self.context)) {
         this.self.context = {};
      }
      this.self.context.fundPools = [];
      this.self.context.projectionContext = args['context'];
      if (!this.self.context.projectionContext) {
          this.self.context.projectionContext.precision = 15;
      }
   },
   allocations: function(args) {
      var allocationRatios = [];
      var portfolioTotals = this.self.fundBalances();

      // sum of porfolio
      var totalPortfolioAmount = 0;
      for (returnType in portfolioTotals) {
         totalPortfolioAmount = _R(Number(totalPortfolioAmount) + Number(portfolioTotals[returnType]), 15);
      }

      for (poolIdx in this.self.context.fundPools) {
         var pool = this.self.context.fundPools[poolIdx];
         var balanceTotals = IFund.appliedTo(pool).call("fundBalances", {});

         // sum of fund pool
         var totalPoolAmount = 0;
         for (returnType in balanceTotals) {
            totalPoolAmount = _R(Number(totalPoolAmount) + Number(balanceTotals[returnType]), 15);
         }
         var ratio = _R(totalPoolAmount / totalPortfolioAmount, 15);
         allocationRatios.push(ratio);
      }
      return allocationRatios;
   },
   dumpin: function(args) {
      //console.log('new fund pool is created - begin');
      var newFundPool = FundPoolImpl.create({
         context: this.self.context.projectionContext
      });
      //console.log('new fund pool is created - end');

      //console.log('dumpin is processed - begin');
      IFund.appliedTo(newFundPool).call("dumpin", args);
      //console.log('dumpin is processed - end');

      //console.log('show balances - begin');
      //console.log(JSON.stringify(IFund.appliedTo(newFundPool).call("fundBalances", args)));
      //console.log('show balances - end');

      this.self.context.fundPools.push(newFundPool);
      //console.log('funds in fundPools = ' + this.self.context.fundPools.length);
   },
   withdraw: function(args) {
      var amount = args['amount'];
      var amounts = args['amounts'];
      var allocationRatios = this.self.allocations();

      //console.log("allocationRatios = " + allocationRatios);

      if (!Utils.isNothing(amounts)) {
         var i = 0;
         var remainAmounts = Utils.extend({}, amounts);
         for (poolIdx in this.self.context.fundPools) {
            i = i + 1;
            var lastOne = (i == this.self.context.fundPools.length);

            var fundPool = this.self.context.fundPools[poolIdx];

            var deductions = {};
            if (!lastOne) {
                for (amountsIdx in amounts) {
                   deductions[amountsIdx] = _R(amounts[amountsIdx] * allocationRatios[poolIdx], this.self.context.projectionContext.precision);
                   //console.log("amounts[amountsIdx] = " + amounts[amountsIdx]);
                   //console.log("deductions[amountsIdx] = " + deductions[amountsIdx]);
                   remainAmounts[amountsIdx] = remainAmounts[amountsIdx] - deductions[amountsIdx];
                }
            } else {
                deductions = remainAmounts;
            }

            IFund.appliedTo(fundPool).call("withdraw", {
               amounts: deductions
            });
         }
      } else {
         for (poolIdx in this.self.context.fundPools) {
            var fundPool = this.self.context.fundPools[poolIdx];
            IFund.appliedTo(fundPool).call("withdraw", {
               amount: _R(amount * allocationRatios[poolIdx], 15)
            });
         }
      }
   },
   withdraws: function(args) {
      var amounts = args['amounts'];
      if (!Utils.isNothing(amounts)) {
         for (poolIdx in this.self.context.fundPools) {
            var fundPool = this.self.context.fundPools[poolIdx];
            var deductions = {};
            IFund.appliedTo(fundPool).call("withdraw", {
               amounts: amounts
            });
         }
      }
   },
   dumpins: function(args) {
      var amounts = args['amounts'];
      if (!Utils.isNothing(amounts)) {
         for (poolIdx in this.self.context.fundPools) {
            var fundPool = this.self.context.fundPools[poolIdx];
            var deductions = {};
            IFund.appliedTo(fundPool).call("dumpins", {
               amounts: amounts
            });
         }
      }
   },
   payout: function(args) {
      var totalPayoutCash = {};
      for (poolIdx in this.self.context.fundPools) {
         var fundPool = this.self.context.fundPools[poolIdx];
         var totalCashOut = IFund.appliedTo(fundPool).call("payout", {});
         for (cashOutKey in totalCashOut) {
            if (Utils.isNothing(totalPayoutCash[cashOutKey])) {
               totalPayoutCash[cashOutKey] = Number(0);
            }
            totalPayoutCash[cashOutKey] = Number(totalPayoutCash[cashOutKey]) + Number(totalCashOut[cashOutKey]);
         }
      }
      return totalPayoutCash;
   },
   rollingInterest: function(args) {
      for (fundPoolKey in this.self.context.fundPools) {
         var pool = this.self.context.fundPools[fundPoolKey];
         IFund.appliedTo(pool).call("rollingInterest", {});
      }
   },
   fundBalances: function(args) {
      var fundReturns = {};
      for (fundPoolKey in this.self.context.fundPools) {
         var pool = this.self.context.fundPools[fundPoolKey];
         var poolFunds = IFund.appliedTo(pool).call("fundBalances", {});
         //console.log("poolFunds = " + JSON.stringify(poolFunds));

         for (fundKey in poolFunds) {
            if (Utils.isNothing(fundReturns[fundKey])) {
               fundReturns[fundKey] = 0;
            }
            fundReturns[fundKey] = Number(fundReturns[fundKey]) + Number(poolFunds[fundKey]);
         }
      }
      //console.log("Pool.fundBalances = " + JSON.stringify(fundReturns));
      return fundReturns;
   },

   surrenderValues: function(args) {
      var surrenderFunds = {};
      for (fundPoolKey in this.self.context.fundPools) {
         var pool = this.self.context.fundPools[fundPoolKey];
         var surrenderBalances = IFund.appliedTo(pool).call("surrenderValues", {});
         //console.log("poolFunds = " + JSON.stringify(poolFunds));
         for (fundKey in surrenderBalances) {
            if (Utils.isNothing(surrenderFunds[fundKey])) {
               surrenderFunds[fundKey] = 0;
            }
            surrenderFunds[fundKey] = Number(surrenderFunds[fundKey]) + Number(surrenderBalances[fundKey]);
         }
      }
      //console.log("Pool.fundBalances = " + JSON.stringify(fundReturns));
      return surrenderFunds;
   }
});

// Column Implementation
var ColumnImpl = Class.define({
   init: function(args) {
      this.self['columnName'] = args["columnName"];
      this.self['columnItem'] = args["columnItem"];
      this.self['values'] = [];
   },
   acceptValue: function(args) {
      var year = args['year'];
      var age = args['age'];
      var value = args['value'];
      this.self.values.push({
         year: year,
         age: age,
         value: value
      });
   },
   column: function(args) {
      return {
         Name: this.self.columnName,
         Values: this.self.values
      };
   }
});

// Projection Manager Implementation
var ProjectionManagerImpl = Class.define({
   init: function(args) {
      this.self.context = {};
      this.self.context['runningPolicyValues'] = {};
      this.self.context['result'] = {};
      this.self.context['rates'] = {};
      this.self.context.monthlyResult = false;
      this.self.context.stepChanges = false;
   },
   loadProductRates: function(args) {
      _DB("","","");
      _DB("", "ProjectionManagerImpl.loadProductRates","");
      _DB("","--------------------------------","");
      // base plan rates
      var basePlan = this.self.context.proposal.coverageInfo;
      var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
      var schemaObject = basePlan.schema.call("getSchema", {});
      var PEratemanager = Repository.getManager(SystemComponents.MANAGER.RATE_MANAGER);

      if (Utils.isNothing(this.self.context[productId])) {
         this.self.context.rates[productId] = {};
      };

      var configInfos = {
         PremiumType: _V(_PV( schemaObject, "ProductSchema.PremiumInfo.PremiumTable" ), []),
         PolicyValueType: _V(_PV( schemaObject, "ProductSchema.PolicyValue.PolicyValueTable" ), []),
         ChargeType: _V(_PV( schemaObject, "ProductSchema.ChargeInfo.ChargeScale" ), [])
      };
      for (configKey in configInfos) {
         var config = configInfos[configKey];

         var configIterator = Utils.iterator(config);
         for (idx in configIterator) {
            var configElement = configIterator[idx];
            if (configElement.CurrencyId.text == basePlan.currency.currencyPK.currencyId) {
               console.log("configElement = " + configElement[configKey].text);
               if (Utils.isNothing(this.self.context.rates[productId][configElement[configKey].text])) {
                  var rates = IRateManager.appliedTo(PEratemanager).call("lookupRates", {
                     coverage: basePlan,
                     resourceKey: configElement[configKey].text,
                     ipo: 0
                  });
                  //console.log("rates[" + premInfo.PremiumType.text + "] = " + JSON.stringify(rates));
                  _DB("","","");
                  this.self.context.rates[productId][configElement[configKey].text] = rates;
                  _DB("rates[" + productId + "][" + configElement[configKey].text + "]", rates, "");
               }
            }
         }
      }

      for(riderIdx in this.self.context.proposal.riders.coverageInfo) {
         var rider = this.self.context.proposal.riders.coverageInfo[riderIdx];
         var riderId = rider.product.productKey.primaryProduct.productPK.productId;
         var riderSchemaObj = rider.schema.call("getSchema", {});

         if (Utils.isNothing(this.self.context[riderId])) {
            this.self.context.rates[riderId] = {};
         };

         var configInfos = {
            PremiumType: _V( _PV( riderSchemaObj, "ProductSchema.PremiumInfo.PremiumTable" ), [] ),
            PolicyValueType: _V( _PV( riderSchemaObj, "ProductSchema.PolicyValue.PolicyValueTable" ), [] ),
            ChargeType: _V( _PV( riderSchemaObj, "ProductSchema.ChargeInfo.ChargeScale" ), [] )
         };
         for (configKey in configInfos) {
            var config = configInfos[configKey];

            var configIterator = Utils.iterator(config);
            for (idx in configIterator) {
               var configElement = configIterator[idx];
               if (configElement.CurrencyId.text == rider.currency.currencyPK.currencyId) {
                  console.log("configElement = " + configElement[configKey].text);
                  if (Utils.isNothing(this.self.context.rates[riderId][configElement[configKey].text])) {
                     var rates = IRateManager.appliedTo(PEratemanager).call("lookupRates", {
                        coverage: rider,
                        resourceKey: configElement[configKey].text,
                        ipo: 0
                     });
                     //console.log("rates[" + premInfo.PremiumType.text + "] = " + JSON.stringify(rates));
                     _DB("","","");
                     this.self.context.rates[riderId][configElement[configKey].text] = rates;
                     _DB("rates[" + riderId + "][" + configElement[configKey].text + "]", rates, "");
                  }
               }
            }
         }

      }
      _DB("","--------------------------------","");

      // console.log("this.self.context.rates = " + JSON.stringify(this.self.context.rates));
   },
   runProjection: function(args) {
      this.self.init(args);

      var proposal = args['proposal'];
      var projectionOptions = args['projectionOptions'];
      var policyOptions = args['policyOptions'];
      //var parties = Utils.iterator(proposal.coverageInfo.parties.party);

      this.self.context['proposal'] = proposal;
      this.self.context['insuredAge'] = _V( _PV( getPrimaryInsured({coverage: proposal.coverageInfo}), 'insuredAge' ) );
      this.self.context['projectionOptions'] = projectionOptions;
      this.self.context['policyOptions'] = policyOptions;

      // load rates
      this.self.loadProductRates();

      // locate event manager
      var em = Repository.getManager(SystemComponents.MANAGER.EVENTS_MANAGER);
      var events = em.call("events", {
         proposal: proposal
      });
      this.self.context['events'] = events;

      if (proposal.enableDebug) {
         console.log('Event List - begin');
         console.log('------------------')
         console.log('benefitPeriod = ' + events.metadata.benefitPeriod);
         console.log('premiumPayingPeriod = ' + events.metadata.premiumPayingPeriod);
         console.log('iterations = ' + events.metadata.iterations);
         console.log('iterationUnit = ' + events.metadata.iterationUnit);
         for (idx in events.items) {
            var eventInstance = events.items[idx];
            var eventImpl = IEvent.appliedTo(eventInstance);
            console.log('' + idx + '. ' + eventImpl.call('eventName', {}));
         }
         console.log('Event List - end');
      }

      /*
      if (!Utils.isNothing(proposal.funds.fundRecord)) {
         //this.self.context.runningPolicyValues['fundPool'] = FundPoolImpl.create({context : this.self.context});
         this.self.context.runningPolicyValues['fundPool'] = PortfolioImpl.create({
            context: this.self.context
         });
      }
      */

      var hasCashOut = Utils.matchInList(InterfaceConstants.POLICY_OPTIONS.CASH_OUT_OPTION, this.self.context.policyOptions, false);
      var tryRuns = 10000;
      var year = 0;
      var months = 0;
      var i;
      for (i = 0; i < Math.min(tryRuns, events.metadata.iterations); i+=events.metadata.iterationUnit) {
         if (proposal.enableDebug) {
            _DBR("","","",i);
            _DBR("","@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@","",i);
            _DBR("","@@@@ YEAR " + year + " / MONTH " + (i % 12), "",i);
            _DBR("","@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@","",i);
         }

         this.self.context.runningPolicyValues['year'] = year;
         this.self.context.runningPolicyValues['months'] = months;

         // if (year == 10) {
         //    this.self.context.monthlyResult = true;
         //    this.self.context.stepChanges = true;
         // } else {
         //    this.self.context.monthlyResult = false;//    this.self.context.stepChanges = false;
         // }
         for (eventIdx in events.items) {
            var event = events.items[eventIdx];
            var shouldRun = IEvent.appliedTo(event).call("shouldBeRun", {
               context: this.self.context
            });
            if (shouldRun) {
               if (proposal.enableDebug) {
                  _DBR("","","",i);
                  _DBR("","##### Event (" + IEvent.appliedTo(event).call("eventName", {}) + ") started #####","",i);
               }
               IEvent.appliedTo(event).call("run", {
                  context: this.self.context
               });
               if (this.self.context.stepChanges) {
                  var snapshot = this.self.makeSnapshotValues();
                   if (proposal.enableDebug) {
                       _DBR("","Event (" + IEvent.appliedTo(event).call("eventName", {}) + ") completed","",i);
                   }
               }
            }
         }

         months += events.metadata.iterationUnit;
         year = parseInt( months / 12 );

         //if ((i + 1) % events.metadata.iterationUnit == 0) {
         //    year = year + 1;
         //}
         //if ((i + 1) % events.metadata.iterationUnit == 0 || this.self.context.monthlyResult) {
         if (months % 12 == 0 || this.self.context.monthlyResult) {
            var snapshot = this.self.makeSnapshotValues();
            if (snapshot) {
               this.self.saveSnapshot({
                  snapshot: snapshot
               });
            }
         }
      }
      //console.log(JSON.stringify(this.self.context.result[this.self.context.projectionOptions]));
      var resultData = this.self.transformResult();
      //console.log("RESULT = \n" + JSON.stringify(resultData));
      var finalResult = {
         projectionOptions: this.self.context.projectionOptions,
         policyOptions: this.self.context.policyOptions,
         columns: resultData
      };

      var validator = Repository.getManager(SystemComponents.MANAGER.VALIDATOR);
      if (!Utils.isNothing(validator)) {
         // validator is defined
         var validationResult = validator.call("postProjectionValidate", {
            proposal: proposal,
            context: this.self.context,
            projectionResult: finalResult
         });
         finalResult["validationResult"] = validationResult;
      }

      //console.log("RESULT = \n" + JSON.stringify(finalResult));
      return finalResult;
   },


   transformResult: function(args) {
      var columnSet = [];
      var columnResults = [];

      for (columnIdx in this.self.context.events.columnItems) {
         var column = this.self.context.events.columnItems[columnIdx];
         var shouldShown = IEventColumn.appliedTo(column).call("shouldBeShown", {
            context: this.self.context
         });
         if (shouldShown) {
            var columnName = IEventColumn.appliedTo(column).call("columnName", {});
            columnSet.push(ColumnImpl.create({
               columnName: columnName,
               columnItem: column
            }));
         }
      }

      var result = this.self.context.result[this.self.context.projectionOptions];
      for (i in result) {
         var record = result[i];
         var year = _R(Number(record.year) + Number(1), 0); // match printing requirements
         var age = _R(Number(record.age) + Number(1), 0); // match printing requirements
         for (columnIdx in columnSet) {
            var column = columnSet[columnIdx];
            var colVal = IEventColumn.appliedTo(column.columnItem).call("calculatedValue", {
               record: record
            });
            IColumn.appliedTo(column).call("acceptValue", {
               year: year,
               age: age,
               value: colVal
            });
         }
      }

      for (columnIdx in columnSet) {
         var column = columnSet[columnIdx];
         columnResults.push(IColumn.appliedTo(column).call("column", {}));
      }
      return columnResults;
   },

   makeSnapshotValues: function(args) {
      //months: this.self.context.runningPolicyValues.months - (this.self.context.runningPolicyValues.year * 12),
      //console.log("cashOut = " + this.self.context.runningPolicyValues.cashOut);
      /*
      var returnValues = {
         year: this.self.context.runningPolicyValues.year,
         month: (this.self.context.runningPolicyValues.months % 12),
         age: Number(this.self.context.insuredAge) + Number(this.self.context.runningPolicyValues.year),
         accountValues: IFund.appliedTo(this.self.context.runningPolicyValues.fundPool).call("fundBalances", {}),
         nars: this.self.context.runningPolicyValues.nars,
         cois: this.self.context.runningPolicyValues.annualCois,
         coiRate: this.self.context.runningPolicyValues.coiRate,
         withdrawal: this.self.context.runningPolicyValues.withdrawal,
         initialDumpIn: this.self.context.runningPolicyValues.initialDumpIn,
         newPremium: this.self.context.runningPolicyValues.newPremium,
         totalPremiums: this.self.context.runningPolicyValues.totalPremiums,
         deathBenefits: this.self.context.runningPolicyValues.deathBenefits,
         totalDeathBenefits: this.self.context.runningPolicyValues.totalDeathBenefits,
         cashOut: this.self.context.runningPolicyValues.cashOut,
         cashOutPayments: this.self.context.runningPolicyValues.accumulatedcashOutPayments,
         firstMonthCoi: this.self.context.runningPolicyValues.firstMonthCoi,
         cashoutAccount: IFund.appliedTo(this.self.context.runningPolicyValues.cashoutAccount).call("fundBalances", {}),
         surrenderValuesFromCashoutAccount: IFund.appliedTo(this.self.context.runningPolicyValues.cashoutAccount).call("surrenderValues", {}),
         surrenderValues: IFund.appliedTo(this.self.context.runningPolicyValues.fundPool).call("surrenderValues", {})
      };
      //console.log(JSON.stringify(returnValues));
      */
      var events = this.self.context.events;
      var provider = events.provider;
      var returnValues = IEventsProvider.appliedTo(provider).call("makeSnapshot", { context: this.self.context });
      return returnValues;
   },
   saveSnapshot: function(args) {
      var snapshot = args['snapshot'];
      if (Utils.isNothing(this.self.context.result[this.self.context.projectionOptions])) {
         this.self.context.result[this.self.context.projectionOptions] = [];
      }
      this.self.context.result[this.self.context.projectionOptions].push(Utils.extend({}, snapshot));
      //console.log('result record[' + i + '] = ' + JSON.stringify(snapshot));
   }
});
var ratesMethods = {

   attributes : {

   },

   supportOption : function(option) {
      return (option & this.header.options == option);
   },

   rateUnit : function() {
      if (!(this.attributes.rateUnit)) {
         var unit;
         if (!(unit) && this.supportOption(RateConstants.RATE_OPTION.PER1000)) {
            unit = 1000;
         }
         if (!(unit) && this.supportOption(RateConstants.RATE_OPTION.PER100)) {
            unit = 100;
         }
         if (!(unit)) {
            // default value
            unit = 1;
         }
         this.attributes = Utils.extend(this.attributes, { rateUnit: unit });
      }
      return this.attributes.rateUnit;
   }

}
var PremiumCalculatorImpl = Class.define({
   init: function(args) {
      this.self.calculateMethods = [];
      this.self.calculateMethods.push(CommonBpm.create({formula: BPM1.create()}));
      this.self.calculateMethods.push(CommonBpm.create({formula: SPSM.create()}));

      var calculateMethods = args["methods"];
      if (!Utils.isNothing(calculateMethods)) {
         for (index in calculateMethods) {
            var methodDefinition = calculateMethods[index];
            this.self.calculateMethods.push(methodDefinition);
         }
      }
   },
   addMethod: function(args) {
      var newMethod = args['method'];
      this.self.calculateMethods.push(newMethod);
   },
   lookupCalculator: function(args) {
      var coverage = args['coverage'];

      var calculator;
      // lookup by Product Id
      for(methodIndex in this.self.calculateMethods) {
         var method = this.self.calculateMethods[methodIndex];
         var productId = ICalculateMethod.appliedTo(method).call('formulaScope', {coverage: coverage}).productId;

         if (coverage.proposal.enableDebug) {
            var formulaName = ICalculateMethod.appliedTo(method).call('formulaName', {});
            _DB('lookupCalculator by Product Id: ', '(' + formulaName + ').productId(' + productId + ') compare to coverage.productId('
               + coverage.product.productKey.primaryProduct.productPK.productId + ') = '
               + (productId == coverage.product.productKey.primaryProduct.productPK.productId), 'PremiumCalculatorImpl->');
         }

         if (!Utils.isNothing(productId)) {
            if (productId == coverage.product.productKey.primaryProduct.productPK.productId) {
               calculator = method;
               break;
            }
         }
      }

      // lookup by method Id;
      if (!calculator) {
         var premiumInfo = coverage.schema.call("matchPremiumInfoRecord", {
            coverage: coverage,
            banding: coverage.banding.Band.text,
            premiumType: SchemaConstants.PREMIUM_TYPE.BASICPREMIUM
         });

         if (premiumInfo) {
            var searchMethodId = premiumInfo.Method.text;
            for(methodIndex in this.self.calculateMethods) {
               var method = this.self.calculateMethods[methodIndex];
               var methodId = ICalculateMethod.appliedTo(method).call('formulaScope', {coverage: coverage}).methodId;

               if (coverage.proposal.enableDebug) {
                  var formulaName = ICalculateMethod.appliedTo(method).call('formulaName', {});
                  _DB('lookupCalculator by Method Id: ', '(' + formulaName + ').methodId(' + methodId + ') compare to premiumInfo.methodId('
                     + searchMethodId + ') = '
                     + (methodId == searchMethodId), 'PremiumCalculatorImpl->');
               }

               if (!Utils.isNothing(methodId)) {
                  if (methodId == searchMethodId) {
                     calculator = method;
                     break;
                  }
               }
            }
         }
      }

      // lookup by productTypes;
      if (!calculator) {
         var mostMatchingMethod = null;
         var mostMatchingScore = 0;

         var availableProductTypes = coverage.catalog.implementation.catalog.ProductCatalog.ProductType.ProductTypeCode;
         for(methodIndex in this.self.calculateMethods) {
            var method = this.self.calculateMethods[methodIndex];
            var productTypes = ICalculateMethod.appliedTo(method).call('formulaScope', {coverage: coverage}).productTypes;
            var formulaName = ICalculateMethod.appliedTo(method).call('formulaName', {});

            //if (coverage.proposal.enableDebug) {
            _DB('lookupCalculator by Product Types: ', '(' + formulaName + ').productTypes(' + productTypes + ') compare to catalog.availableProductTypes('
               + JSON.stringify(availableProductTypes) + ')', 'PremiumCalculatorImpl->');
            //}

            var score = 0;
            if (!Utils.isNothing(productTypes)) {
               for(typeIdx in productTypes) {
                  var found = false;
                  var productType = productTypes[typeIdx];
                  for(availableProductIndex in availableProductTypes) {
                     var avaProdType = availableProductTypes[availableProductIndex];
                     if (avaProdType.text == productType) {
                        found = true;
                        break;
                     }
                  }
                  if (found) score = score + 1;
                  //if (coverage.proposal.enableDebug) {
                  _DB('lookupCalculator by Product Types: ', '(' + formulaName + ').productType(' + productType + ') found = ' + found, 'PremiumCalculatorImpl->');
                  //}
               }
            }

            if (coverage.proposal.enableDebug) {
               var formulaName = ICalculateMethod.appliedTo(method).call('formulaName', {});
               _DB('lookupCalculator by Product Types: ', '(' + formulaName + ').productTypes(' + productTypes + ') compare to catalog.availableProductTypes('
                  + JSON.stringify(availableProductTypes) + ') = '
                  + score + ((!Utils.isNothing(productTypes) && score==productTypes.length) ? ' [MATCHED]' : ' [NOT MATCHED]'), 'PremiumCalculatorImpl->');
            }

            if (!Utils.isNothing(productTypes) && score==productTypes.length && score > mostMatchingScore) {
               mostMatchingScore = score;
               mostMatchingMethod = method;
            }
         }

         if (mostMatchingMethod) {
            calculator = mostMatchingMethod;
         }
      }

      if (calculator) {
         return ICalculateMethod.implementBy( calculator );
      } else {
         return;
      }
   },
   showFormulas: function(args) {
      for(methodIndex in this.self.calculateMethods) {
         var method = this.self.calculateMethods[methodIndex];
         var formulaName = ICalculateMethod.appliedTo(method).call('formulaName', {});
         _D("showFormulas.formulaName: ", formulaName, "PremiumCalculatorImpl->");
      }
   },
   calculate: function(args) {
      var proposal = args['proposal'];

      var basePlanPremiums = this.self.calculateCoverage({
         coverage: proposal.coverageInfo,
         policyYear: 0,
         ipo: 0
      });
      proposal.coverageInfo[PublicConstants.PREMIUM_RESULT.PREMIUM_RESULT_FIELD_NAME] = basePlanPremiums;
      if (!Utils.isNothing(proposal.riders)) {
         var riderPremiums = [];
         for (riderIndex in proposal.riders.coverageInfo) {
            var rider = proposal.riders.coverageInfo[riderIndex];
            var schema = rider.schema.call("getSchema", {});
            var freeRider = _V( _PV( schema, "ProductSchema.BasicParticular.SupportOption.FreeOfCharge"), "N" );
            if (freeRider != "Y") {
                var riderPremium = this.self.calculateCoverage({
                   coverage: rider,
                   policyYear: 0,
                   ipo: 0
                });
                var riderPremiumRecord = {
                   riderCode: rider.product.productKey.primaryProduct.productPK.productId,
                   premiums: riderPremium
                };
                riderPremiums.push(riderPremiumRecord);
                rider[PublicConstants.PREMIUM_RESULT.PREMIUM_RESULT_FIELD_NAME] = riderPremium;
            }
         }
      }
      //if (proposal.enableDebug) {
      _DB("basePlanPremiums = ", basePlanPremiums, "PremiumCalculatorImpl->");
      _DB("riderPremiums = ", riderPremiums, "PremiumCalculatorImpl->");
      //}
      var consolidatedPremiums = {
         basePlan: basePlanPremiums,
         basePlanCode: proposal.coverageInfo.product.productKey.primaryProduct.productPK.productId,
         riders: riderPremiums
      };
      console.log("consolidatedPremiums = " + JSON.stringify(consolidatedPremiums));
      return consolidatedPremiums;
   },
   calculateCoverage: function(args) {
      var coverage = args["coverage"];
      var policyYear = args["policyYear"];
      var ipo = args["ipo"];
      var calculator = this.self.lookupCalculator({coverage: coverage});
      if (calculator) {
         return calculator.call("calculate",
            {
               coverage: coverage,
               ipo: ipo,
               context: PE.context,
               policyYear: policyYear
            }
         );
      } else {
         throw "ERROR: Premium calculator (" + coverage.product.productKey.primaryProduct.productPK.productId + "," +
            coverage.product.productKey.associateProduct.productPK.productId + "," +
            coverage.product.productKey.basicProduct.productPK.productId +
            ")  cannot be found or premium information is not defined";
      }
   }
});
var IssueAgeValidationRule = Class.define({
   ruleName: function(args) {
      return "IssueAgeValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE | PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');
      var catalog = coverage.catalog.call("getCatalog", null);
      var schema = coverage.schema.call("getSchema", null);
      //console.log("Base Product = " + catalog.ProductCatalog.ProductPK.ProductId.text);
      //var riderCatalog = coverage.proposal.coverageInfo.catalog.call("getCatalog", null);
      //var riderSchema = coverage.proposal.coverageInfo.schema.call("getSchema", null);
      //console.log("Rider Product = " + riderCatalog.ProductCatalog.ProductPK.ProductId.text);
      var insureds = _PV( coverage, 'parties.party' );
      if (Utils.isNothing(insureds)) {
          return {
             code: "ERRC0213",
             arguments: {"%PRODUCT_ID%" : productId}
          };
      }
      var insuredList = Utils.iterator( insureds );
      for (insuredIdx in insuredList) {
          var insured = insuredList[insuredIdx];
          var insuredAge = insured.insuredAge;
          _DB("insuredAge("+ insuredIdx + ")", insuredAge);

          var isDeclined = false;
          var minDeclineAge = 9999999999;
          var maxDeclineAge = 0;
          var minDeclAge = _PV( schema, "ProductSchema.BasicParticular.MinDeclinedAge" );
          var maxDeclAge = _PV( schema, "ProductSchema.BasicParticular.MaxDeclinedAge" );
          if (!(Utils.isNothing(minDeclAge))) {
             minDeclineAge = Math.min( minDeclineAge, Number(minDeclAge) );
          }
          if (!(Utils.isNothing(maxDeclAge))) {
             maxDeclineAge = Math.max( maxDeclineAge, Number(maxDeclAge) );
          }

          _DB("maxDeclineAge", maxDeclineAge);
          _DB("minDeclineAge", minDeclineAge);

          var a = function( v, d ) {
             if (v) return parseInt(v);
             return d;
          }

          var maxIssueAge = 0;
          var minIssueAge = 9999999999;
          if (schema) {
             maxIssueAge = Math.max( maxIssueAge, a( _PV(schema, 'ProductSchema.BasicParticular.IssueAge.Max'), maxIssueAge ) );
             minIssueAge = Math.min( minIssueAge, a( _PV(schema, 'ProductSchema.BasicParticular.IssueAge.Min'), minIssueAge ) );
          }
          var specialMaxIssueAge = a( _PV( schema, "ProductSchema.BasicParticular.specialMaxIssueAge" ), 0);

          _DB("maxIssueAge", maxIssueAge);
          _DB("minIssueAge", minIssueAge);
          _DB("specialMaxIssueAge", specialMaxIssueAge);

          if (insuredAge < minIssueAge) {
             // Error: Issue age is below the minimum.
             return {
                code: "ERRC0001",
                arguments: {"%INSURED%" : insured}
             };
          }
          if (insuredAge > maxIssueAge) {
             if (insuredAge <= specialMaxIssueAge && specialMaxIssueAge > 0) {
                // Warning: Issue age is greater than the maximum.
                return {
                   code: "ERRC0003",
                   arguments: {"%INSURED%" : insured}
                };
             } else {
                // Error: Issue age is greater than the maximum.
                return {
                   code: "ERRC0002",
                   arguments: {"%INSURED%" : insured}
                };
             }
          }
          //}

          if (coverage.schema.call("hasSupportOption", {option: SchemaConstants.SUPPORT_OPTION.DECLINED})) {
             if ((insuredAge >= minDeclineAge) && (insuredAge <= maxDeclineAge)) {
                isDeclined = true;
                // Warning: This is a declined or over-insurance case.
                return {
                   code: "ERRC0004",
                   arguments: {"%INSURED%" : insured}
                };
             }
          }

      }
   }

});

var FaceAmountValidationRule = Class.define({
   ruleName: function(args) {
      return "FaceAmountValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   getMaxDeclinedFaceAmount: function(args) {
      var coverage = args['coverage'];
      var schema = args['schema'];
      //Currency Filter - To Do Test
      if (Utils.isNothing(schema.ProductSchema.BasicParticular.MaxDeclinedFaceAmount)) {
         return 0;
      }
      var faceAmountTable = Utils.iterator(schema.ProductSchema.BasicParticular.MaxDeclinedFaceAmount);
      for (recKey in faceAmountTable) {
         var recInfo = faceAmountTable[recKey];
         if (recInfo._Currency != null) {
            return recInfo.text;
         }
         return 0;
      }
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE | PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   getFaceAmountLimits: function(args) {
      var coverage = args['coverage'];
      var schema = coverage.schema.call("getSchema", null);
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var primary = getPrimaryInsured({coverage: coverage});
      if (Utils.isNothing(primary)) {
         return {
            code: "ERRC0213",
            arguments: {"%PRODUCT_ID%" : productId}
         };
      }
      var insuredAge = primary.insuredAge;

      var minFA = 99989898989898.999;
      var maxFA = -99989898989898.999;
      var result = {};
      for (bandKey in schema.ProductSchema.BandInformation.BandRecord) {
         var bandRec = schema.ProductSchema.BandInformation.BandRecord[bandKey];
         if (!Utils.isNothing(bandRec.CurrencyPK)) {
            //console.log("productRules.length = " + bandRec.CurrencyPK.CurrencyId.text);
            if (coverage.currency.currencyPK.currencyId == bandRec.CurrencyPK.CurrencyId.text) {
               if (insuredAge >= Number(bandRec.MinIssueAge.text) && insuredAge <= Number(bandRec.MaxIssueAge.text)) {
                  minFA = Math.min(minFA, Number(bandRec.MinFaceAmount.text));
                  maxFA = Math.max(maxFA, Number(bandRec.MaxFaceAmount.text));
               }
            }
         }
      }

      var currencyBasedLimits = Utils.iterator( _V( _PV( schema, "ProductSchema.CurrencyBaseLimits.CurrencyLimitRecord" ), [] ) );
      var bpFaceAmount = _V( _PV( coverage, "proposal.coverageInfo.faceAmount" ) );
      var currencyId = _PV( coverage, "currency.currencyPK.currencyId" );
      _DB("currencyBasedLimits(" + productId + ")", currencyBasedLimits, "FaceAmountValidationRule->");
      _DB("currencyId", currencyId, "FaceAmountValidationRule->");
      if (!Utils.isNothing(bpFaceAmount)) {
         var minBpRatio;
         var maxBpRatio;
         var bpFA = Number( bpFaceAmount );
         for(limitRecIdx in currencyBasedLimits) {
            var limitRec = currencyBasedLimits[limitRecIdx];
            var limitRecCurrency = _V( _PV( limitRec, "CurrencyPK.CurrencyId" ) );
            var limitNature =  _V( limitRec.LimitNature );

            _DB("limitRecCurrency", limitRecCurrency, "FaceAmountValidationRule->");
            _DB("limitNature", limitNature, "FaceAmountValidationRule->");

            if (currencyId == limitRecCurrency) {
                if (limitNature == "Min") {
                    minBpRatio =  _V( limitRec.BpFaRatio );
                }
                if (limitNature == "Max") {
                    maxBpRatio =  _V( limitRec.BpFaRatio );
                }
            }
         }
         _DB("", "Face amount ratio logic", "")
         _DB("bpFA", bpFA, "FaceAmountValidationRule->");
         _DB("minBpRatio", minBpRatio, "FaceAmountValidationRule->");
         _DB("maxBpRatio", maxBpRatio, "FaceAmountValidationRule->");
         if (!Utils.isNothing(minBpRatio)) {
            minFA = Math.max(minFA, (bpFA * Number(minBpRatio)));
         }
         if (!Utils.isNothing(maxBpRatio)) {
            maxFA = Math.min(maxFA, (bpFA * Number(maxBpRatio)));
         }
         _DB("minFA", minFA, "FaceAmountValidationRule->");
         _DB("maxFA", maxFA, "FaceAmountValidationRule->");
      }

      return {minLimit: minFA, maxLimit: maxFA};
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      var catalog = coverage.catalog.call("getCatalog", null);

      var amountFA = 0;
      var isDeclined = false;
      var basicFaceAmountValid = true;
      var basicFaceAmount = 0;
      var minDeclineAge = 99989898989898.999;
      var maxDeclineAge = 0;

      basicFaceAmount = Utils.isNothing(coverage.faceAmount) ? 0 : Number(coverage.faceAmount);

      if (!(Utils.isNothing(schema.ProductSchema.BasicParticular.MinDeclineAge))) {
         minDeclineAge = Math.min(minDeclineAge, schema.ProductSchema.BasicParticular.MinDeclineAge.text);
      }
      if (!(Utils.isNothing(schema.ProductSchema.BasicParticular.MinDeclineAge))) {
         maxDeclineAge = Math.max(maxDeclineAge, schema.ProductSchema.BasicParticular.MaxDeclineAge.text);
      }

      var insureds = _PV( coverage, 'parties.party' );
      if (Utils.isNothing(insureds)) {
          return {
             code: "ERRC0213",
             arguments: {"%PRODUCT_ID%" : productId}
          };
      }

      //To Do confimation required on default min and max value
      var minFA;
      var maxFA;
      var result = this.self.getFaceAmountLimits({ coverage: coverage })
      if (!Utils.isNothing(result)) {
         minFA = result.minLimit;
         maxFA = result.maxLimit;
      }
      var insuredList = Utils.iterator( insureds );
      for (insuredIdx in insuredList) {
          var insured = insuredList[insuredIdx];
          var insuredAge = insured.insuredAge;
          _DB("insuredAge("+ insuredIdx + ")", insuredAge);

          if (coverage.schema.call("hasSupportOption", {
                option: SchemaConstants.SUPPORT_OPTION.DECLINED
             })) {
            if ((insuredAge >= minDeclineAge) && (insuredAge >= maxDeclineAge)) {
               isDeclined = true;

            }
          }
          var isCurrencySupported = (coverage.schema.call("isCurrencySupported", {
             currencyId: coverage.currency.currencyPK.currencyId
          }));

          if (isCurrencySupported) {
             //for (bandKey in schema.ProductSchema.BandInformation.BandRecord) {
             //   var bandRec = schema.ProductSchema.BandInformation.BandRecord[bandKey];
             //   if (!Utils.isNothing(bandRec.CurrencyPK)) {
             //      //console.log("productRules.length = " + bandRec.CurrencyPK.CurrencyId.text);
             //      if (coverage.currency.currencyPK.currencyId == bandRec.CurrencyPK.CurrencyId.text) {
             //         if (insuredAge >= Number(bandRec.MinIssueAge.text) && insuredAge <= Number(bandRec.MaxIssueAge.text)) {
             //            minFA = Math.min(minFA, Number(bandRec.MinFaceAmount.text));
             //            maxFA = Math.max(maxFA, Number(bandRec.MaxFaceAmount.text));
             //         }
             //      }
             //   }
             //}

             if (isDeclined) {
                maxFA = this.getMaxDeclinedFaceAmount({coverage: coverage, schema: schema});
             }

             //if (minFA + maxFA == 0) {
             if (Utils.isNothing(minFA) || Utils.isNothing(maxFA)) {
                if (basicFaceAmount > 0) {
                   basicFaceAmountValid = false;
                   //message = "Error: No face amount is required.";
                   return {
                      code: "ERRC0113",
                      arguments: {"%INSURED%" : insured, "%PRODUCT_ID%" : productId}
                   };
                }
             } else {
                if (basicFaceAmount < minFA) {
                   basicFaceAmountValid = false;
                   var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                   //variables[PublicConstants.MESSAGE_FIELD.COVERAGE_FACE_AMOUNT] = coverage.currency.currencyPK.currencyId + coverage.faceAmount;
                   variables[PublicConstants.MESSAGE_FIELD.MINIMUM_FACE_AMOUNT] = coverage.currency.currencyPK.currencyId + minFA;
                   if (!coverage.catalog.call("isRegularSaving")) {
                      // Error: The face amount is below minimum.
                      return {
                         code: "ERRC0106",
                         arguments: variables
                      };
                   } else {
                      // M313", "Error: The Base Saving premium is below the minimum ($_).
                      return {
                         code: "ERRC0107",
                         arguments: variables
                      };
                   }
                }
                // check if the face amount is less than minimum after fa reduced
                if (coverage.reduceFaPercent > 0) {
                   if (coverage.catalog.call("isIncome3") && coverage.startAnnuityAge != null && coverage.startAnnuityAge != 0) {
                      var reduceFA = basicFaceAmount * (100 - coverage.reduceFaPercent) / 100;
                      if (reduceFA < minFA) {
                         basicFaceAmountValid = false;
                         // Error: 266 Reduced face amount is below the minimum requirement.
                         var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                         //variables[PublicConstants.MESSAGE_FIELD.COVERAGE_FACE_AMOUNT] = coverage.currency.currencyPK.currencyId + coverage.faceAmount;
                         variables[PublicConstants.MESSAGE_FIELD.MINIMUM_FACE_AMOUNT] = coverage.currency.currencyPK.currencyId + minFA;
                         return {
                            code: "ERRC0112",
                            arguments: variables
                         };
                      }
                   }
                }
                if (basicFaceAmount > maxFA) {
                   basicFaceAmountValid = false;
                   var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                   variables[PublicConstants.MESSAGE_FIELD.MAXIMUM_FACE_AMOUNT] = coverage.currency.currencyPK.currencyId + maxFA;
                   if (!coverage.catalog.call("isRegularSaving")) {
                      // Error: The face amount is greater than maximum.
                      return {
                         code: "ERRC0108",
                         arguments: variables
                      };
                   } else {
                      // M314", "Error: The Base Saving premium is greater than the maximum ($_).
                      return {
                         code: "ERRC0109",
                         arguments: variables
                      };
                   }
                }
             }
          } else {
             basicFaceAmountValid = false;
             if (coverage.catalog.call("isRegularSaving") || coverage.catalog.call("isUVL")) {
                // Error: The face amount cannot be check due to invalid currency entered.
                return {
                   code: "ERRC0110",
                   arguments: {"%INSURED%" : insured, "%PRODUCT_ID%" : productId}
                };
             } else {
                //M317", "Error: The Base Saving premium cannot be check due to invalid currency entered.
                return {
                   code: "ERRC0111",
                   arguments: {"%INSURED%" : insured, "%PRODUCT_ID%" : productId}
                };
             }
          }

      }
   }

});

var CurrencyValidationRule = Class.define({
   ruleName: function(args) {
      return "CurrencyValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE | PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');
      //var schema = coverage.schema.call("getSchema", null);
      //var catalog = coverage.catalog.call("getCatalog", null);
      if (!(Utils.isNothing(coverage)) && !(Utils.isNothing(coverage.currency)) && !(Utils.isNothing(coverage.currency.currencyPK))) {
         if (!(coverage.schema.call("isCurrencySupported", {
               currencyId: coverage.currency.currencyPK.currencyId
            }))) {
            //var variables = {};
            //variables[PublicConstants.MESSAGE_FIELD.CURRENCY] = coverage.currency.currencyPK.currencyId;
            return {
               code: "ERRC0007",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
      }
   }
});

var PaymentModeValidationRule = Class.define({
   ruleName: function(args) {
      return "PaymentModeValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      if (!(Utils.isNothing(coverage)) && !(Utils.isNothing(coverage.currency)) && !(Utils.isNothing(coverage.currency.currencyPK))) {
         if (!(coverage.schema.call("isCurrencySupported", {
               currencyId: coverage.currency.currencyPK.currencyId
            }))) {
            return {
               code: "ERRC0009",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         } else {
            var variables = {"%PRODUCT_ID%" : productId};
            variables[PublicConstants.MESSAGE_FIELD.CURRENCY] = coverage.currency.currencyPK.currencyId;
            variables[PublicConstants.MESSAGE_FIELD.PAYMENT_MODE] = coverage.options.paymentMode;
            if (!(coverage.schema.call("isCurrencyPaymentModeSupported", {
                  currencyId: coverage.currency.currencyPK.currencyId,
                  paymentMode: coverage.options.paymentMode
               }))) {
               return {
                  code: "ERRC0010",
                  arguments: variables
               };
            }
         }
      }
   }
});

var IssuedCountValidationRule = Class.define({
   ruleName: function(args) {
      return "IssuedCountValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');
      //Clarification Required on parties struture
      var insureds = _PV( coverage, 'parties.party' );
      if (Utils.isNothing(insureds)) {
          return {
             code: "ERRC0213",
             arguments: {"%PRODUCT_ID%" : productId}
          };
      }
      var insuredList = Utils.iterator(insureds);
      if (coverage.catalog.call("isJoinLife", null)) {
         if (insuredList.length != 2) {
             return {
                code: "ERRC0011",
                arguments: {"%PRODUCT_ID%" : productId}
             };
         }
      } else {
         if (insuredList.length != 1) {
             return {
                code: "ERRC0012",
                arguments: {"%PRODUCT_ID%" : productId}
             };
         }
      }
   }
});

var InsuredSexValidationRule = Class.define({
   ruleName: function(args) {
      return "InsuredSexValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE | PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');
      var schema = coverage.schema.call("getSchema", null);

      var insureds = _PV( coverage, 'parties.party' );
      if (Utils.isNothing(insureds)) {
          return {
             code: "ERRC0213",
             arguments: {"%PRODUCT_ID%" : productId}
          };
      }
      var insuredList = Utils.iterator( insureds );
      for (insuredIdx in insuredList) {
         var insured = insuredList[insuredIdx];
         var insuredSex = insured.insuredSex;
         _DB("insuredSex("+ insuredIdx + ")", insuredSex);

         if (!(coverage.schema.call("isInsuredSexSupported", { gender: insuredSex }))) {
            return {
               code: "ERRC0013",
               arguments: {"%INSURED%" : insured, "%PRODUCT_ID%" : productId}
            };
         }
      }
   }
});

var InsuredSmokingStatusValidationRule = Class.define({
   ruleName: function(args) {
      return "InsuredSmokingStatusValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   getJuvenileAge: function(args) {
      var location = args['location'];
      if ((location == CatalogConstants.LOCATION.HONGKONG) || (location == CatalogConstants.LOCATION.MACAU) || (location == CatalogConstants.LOCATION.CHINA))
         return 15;
      else
      if (location == CatalogConstants.LOCATION.SINGAPORE)
         return 17;
      else
         return 15; // default
   },
   performAggregateSmokingStatusChecking: function(args) {
      var coverage = args["coverage"];
      var insured = args['insured'];
      var insuredAge = args['insuredAge'];
      var smokingStatus = args['smokingStatus'];

      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');
      var schema = coverage.schema.call("getSchema", null);

      var location = _PV( coverage, ['product','productKey','location'] );
      var juvenileAge = this.self.getJuvenileAge({ location: location });

      var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
      variables[PublicConstants.MESSAGE_FIELD.SMOKING_STATUS] = "" + smokingStatus;

      if ((coverage.schema.call("isInsuredSmokingStatusSupported", { smokingStatus: InterfaceConstants.SMOKING_STATUS.AGGREGATE }))) {
         if (insuredAge > juvenileAge) {
            if (smokingStatus == InterfaceConstants.SMOKING_STATUS.AGGREGATE) {
               // message = "Error: Smoking status is not supported for age 16 or above.";
               return {
                  code: "ERRC0016",
                  arguments: variables
               };
            }
         } else {
            // 0-15
            if (smokingStatus != InterfaceConstants.SMOKING_STATUS.AGGREGATE) {
               return {
                  code: "ERRC0017",
                  arguments: variables
               };
            }
         }
      } else {
         // std, non smoke
         if (coverage.schema.call("hasSupportOption", { option: SchemaConstants.SUPPORT_OPTION.JUVENILE })) {
            if (smokingStatus != InterfaceConstants.SMOKING_STATUS.AGGREGATE) {
               if (insuredAge <= juvenileAge) {
                  return {
                     code: "ERRC0017",
                     arguments: variables
                  };
               }
            }
         }
      }
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');
      var schema = coverage.schema.call("getSchema", null);

      var insureds = _PV( coverage, 'parties.party' );
      if (Utils.isNothing(insureds)) {
          return {
             code: "ERRC0213",
             arguments: {"%PRODUCT_ID%" : productId}
          };
      }
      var insuredList = Utils.iterator( insureds );
      for (insuredIdx in insuredList) {
         var insured = insuredList[insuredIdx];
         var insuredAge = insured.insuredAge;
         var smokingStatus = insured.smokingStatus;
         _DB("insuredAge("+ insuredIdx + ")", insuredAge);
         _DB("smokingStatus("+ insuredIdx + ")", smokingStatus);

         var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
         variables[PublicConstants.MESSAGE_FIELD.SMOKING_STATUS] = "" + smokingStatus;

         if (!(coverage.schema.call("isInsuredSmokingStatusSupported", {
               smokingStatus: smokingStatus
            }))) {
            return {
               code: "ERRC0015",
               arguments: variables
            };
         } else {
            return this.self.performAggregateSmokingStatusChecking({coverage: coverage, insured: insured, insuredAge: insuredAge, smokingStatus: smokingStatus});
         }
      }
   }
});

var WaitingPeriodValidationRule = Class.define({
   ruleName: function(args) {
      return "WaitingPeriodValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE | PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');
      var schema = coverage.schema.call("getSchema", null);
      if (coverage.schema.call("hasSupportOption", {
            option: SchemaConstants.SUPPORT_OPTION.WAITPERIOD
         })) {
         if ((!Utils.isNothing(coverage.otherOptions)) && !(Utils.isNothing(coverage.otherOptions.waitingPeriod))) {
            if (!(coverage.otherOptions.waitingPeriod == SchemaConstants.WAITING_PERIOD.WP_30DAYS || coverage.otherOptions.waitingPeriod == SchemaConstants.WAITING_PERIOD.WP_90DAYS || coverage.otherOptions.waitingPeriod == SchemaConstants.WAITING_PERIOD.WP_180DAYS || coverage.otherOptions.waitingPeriod == SchemaConstants.WAITING_PERIOD.WP_365DAYS)) {
               return {
                  // Error: No wait period is set.
                  code: "ERRC0018",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            }
            var variables = {"%PRODUCT_ID%" : productId};
            variables[PublicConstants.MESSAGE_FIELD.WAITING_PERIOD] = coverage.otherOptions.waitingPeriod;
            if (!(coverage.schema.call("isWaitPeriodSupported", {
                  waitPeriod: coverage.otherOptions.waitingPeriod
               }))) {
               //message = "Error: It does not support wait period " + modeStr + ".";
               return {
                  code: "ERRC0031",
                  arguments: variables
               };
            }
         }

      } else {
         if ((!Utils.isNothing(coverage.otherOptions)) && !(Utils.isNothing(coverage.otherOptions.waitingPeriod))) {
            //message = "Error: It does not support any wait period.";
            return {
               code: "ERRC0032",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
      }

   }
});

var BenefitPeriodValidationRule = Class.define({
   ruleName: function(args) {
      return "BenefitPeriodValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE | PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');
      var schema = coverage.schema.call("getSchema", null);
      if (coverage.schema.call("hasSupportOption", {
            option: SchemaConstants.SUPPORT_OPTION.BENEFITPERIOD
         })) {
         if ((!Utils.isNothing(coverage.otherOptions)) && !(Utils.isNothing(coverage.otherOptions.benefitPeriod))) {
            if (!(coverage.otherOptions.benefitPeriod == SchemaConstants.BENEFIT_PERIOD.BP_5YEARS || coverage.otherOptions.benefitPeriod == SchemaConstants.BENEFIT_PERIOD.BP_10YEARS || coverage.otherOptions.benefitPeriod == SchemaConstants.BENEFIT_PERIOD.BP_15YEARS || coverage.otherOptions.benefitPeriod == SchemaConstants.BENEFIT_PERIOD.BP_AGE65)) {
               return {
                  code: "ERRC0019",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            }

            if (!(coverage.schema.call("isBenefitPeriodSupported", {
                  benefitPeriod: coverage.otherOptions.benefitPeriod
               }))) {

               var variables = {"%PRODUCT_ID%" : productId};
               variables[PublicConstants.MESSAGE_FIELD.BENEFIT_PERIOD] = coverage.otherOptions.benefitPeriod;
               if (coverage.otherOptions.benefitPeriod == SchemaConstants.BENEFIT_PERIOD.BP_AGE65) {
                  return {
                     code: "ERRC0029",
                     arguments: variables
                  };
               } else {
                  return {
                     code: "ERRC0028",
                     arguments: variables
                  };
               }
            }
         }

      } else {
         //If not support but selected
         if ((!Utils.isNothing(coverage.otherOptions)) && !(Utils.isNothing(coverage.otherOptions.benefitPeriod))) {
            return {
               code: "ERRC0030",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
      }

   }
});

var InitialDumpInValidationRule = Class.define({
   ruleName: function(args) {
      return "InitialDumpInValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   minDumpinAmount: function(args) {
      var result = null;

      var schema = args['schema'];
      var coverage = args['coverage'];
      var minInitialDumpin = schema.ProductSchema.BasicParticular.MinInitialDumpin;
      if (minInitialDumpin) {
          var iterMinInitialDumpin = Utils.iterator(minInitialDumpin);
          for(dpIdx in iterMinInitialDumpin) {
              var minInitDumpinRec = iterMinInitialDumpin[dpIdx];
              if (minInitialDumpin.currency) {
                  if (minInitialDumpin.currency == coverage.currency.currencyPK.currencyId) {
                      result = Number(minInitDumpinRec.text);
                      break;
                  }
              }
          }
      }
      var currencyLimits = schema.ProductSchema.CurrencyBaseLimits.CurrencyLimitRecord;
      if (currencyLimits) {
          var iterCurrencyLimits = Utils.iterator(currencyLimits);
          for(limitIdx in iterCurrencyLimits) {
              var currencyLimitRec = iterCurrencyLimits[limitIdx];
              if (currencyLimitRec.LimitNature.text == "Min") {
                  if (currencyLimitRec.CurrencyPK.CurrencyId.text) {
                      if (currencyLimitRec.CurrencyPK.CurrencyId.text == coverage.currency.currencyPK.currencyId) {
                          result = Number( currencyLimitRec.InitialDumpin.text );
                          break;
                      }
                  }
              }
          }
      }

      return result;
   },
   maxDumpinAmount: function(args) {
      var result = null;

      var schema = args['schema'];
      var coverage = args['coverage'];
      var maxInitialDumpin = schema.ProductSchema.BasicParticular.MaxInitialDumpin;
      if (maxInitialDumpin) {
          var iterMaxInitialDumpin = Utils.iterator(maxInitialDumpin);
          for(dpIdx in iterMaxInitialDumpin) {
              var maxInitDumpinRec = iterMaxInitialDumpin[dpIdx];
              if (maxInitialDumpin.currency) {
                  if (maxInitialDumpin.currency == coverage.currency.currencyPK.currencyId) {
                      result = Number(maxInitDumpinRec.text);
                      break;
                  }
              }
          }
      }
      var currencyLimits = schema.ProductSchema.CurrencyBaseLimits.CurrencyLimitRecord;
      if (currencyLimits) {
          var iterCurrencyLimits = Utils.iterator(currencyLimits);
          for(limitIdx in iterCurrencyLimits) {
              var currencyLimitRec = iterCurrencyLimits[limitIdx];
              if (currencyLimitRec.LimitNature.text == "Max") {
                  if (currencyLimitRec.CurrencyPK.CurrencyId.text) {
                      if (currencyLimitRec.CurrencyPK.CurrencyId.text == coverage.currency.currencyPK.currencyId) {
                          result = Number( currencyLimitRec.InitialDumpin.text );
                          break;
                      }
                  }
              }
          }
      }

      return result;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      var catalog = coverage.catalog.call("getCatalog", null);
      if ((coverage.catalog.call("isUVL"))) {
         if (!(Utils.isNothing(coverage)) && !(Utils.isNothing(coverage.initialDumpIn))) {
            //if ((coverage.initialDumpIn) > 0 || coverage.catalog.call("isSinglePremium")) {
            if (coverage.initialDumpIn != 0) {

               if (!(coverage.schema.call("isCurrencySupported", {
                     currencyId: coverage.currency.currencyPK.currencyId
                  }))) {
                  return {
                     code: "ERRC0021",
                     arguments: {"%PRODUCT_ID%" : productId}
                  };
               } else {
                  // min dump , max dump validation
                  //var minDumpIn = schema.ProductSchema.BasicParticular.MinInitialDumpin.text;
                  //var maxDumpIn = schema.ProductSchema.BasicParticular.MaxInitialDumpin.text;

                  var minDumpIn = this.self.minDumpinAmount({coverage:coverage, schema:schema});
                  //if (minDumpIn == null) throw "Minimum dump-in setting is not available in schema.";
                  var maxDumpIn = this.self.maxDumpinAmount({coverage:coverage, schema:schema});
                  //if (maxDumpIn == null) throw "Maximum dump-in setting is not available in schmea";

                  if (!Utils.isNothing(minDumpIn) && !Utils.isNothing(maxDumpIn)) {
                     if ((coverage.initialDumpIn) < minDumpIn) {
                        var variables = {"%PRODUCT_ID%" : productId};
                        variables[PublicConstants.MESSAGE_FIELD.MINIMUM_INITIAL_TOP_UP] =
                           coverage.currency.currencyPK.currencyId + " " + minDumpIn;
                        if (coverage.catalog.call("isSinglePremium")) {
                           //"Error: Basic Single Premium is below the minimum requirement."
                           return {
                              code: "ERRC0044",
                              arguments: variables
                           };
                        } else if (coverage.catalog.call("isRegularSaving") || coverage.catalog.call("isUVL")) {
                           //var variables = {"%PRODUCT_ID%" : productId};
                           //variables[PublicConstants.MESSAGE_FIELD.MINIMUM_INITIAL_TOP_UP] = minDumpIn;
                           //"Error: Initial Top-up Premium is below the minimum ($_)."
                           return {
                              code: "ERRC0046",
                              arguments: variables
                           };
                        }
                     }
                     if ((coverage.initialDumpIn) > maxDumpIn) {
                        var variables = {"%PRODUCT_ID%" : productId};
                        variables[PublicConstants.MESSAGE_FIELD.MAXIMUM_INITIAL_TOP_UP] =
                           coverage.currency.currencyPK.currencyId + " " + maxDumpIn;
                        if (coverage.catalog.call("isSinglePremium")) {
                           //"Error: Initial Subscription (Initial Dump-in) is above the maximum requirement."
                           return {
                              code: "ERRC0045",
                              arguments: variables
                           };
                        } else if (coverage.catalog.call("isRegularSaving") || coverage.catalog.call("isUVL")) {
                           //var variables = {"%PRODUCT_ID%" : productId};
                           //variables[PublicConstants.MESSAGE_FIELD.MAXIMUM_INITIAL_TOP_UP] = maxDumpIn;
                           //"Error: Initial Top-up Premium is above the maximum ($_)."
                           return {
                              code: "ERRC0047",
                              arguments: variables
                           };
                        }

                     }

                  }
               }

            }
         }
      } else {
         if (!(Utils.isNothing(coverage)) && !(Utils.isNothing(coverage.initialDumpIn))) {
            if (Utils.isNothing(coverage.initialDumpIn) > 0) {
               return {
                  code: "ERRC0020",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            }
         }
      }

   }
});

var CouponValidationRule = Class.define({
   ruleName: function(args) {
      return "CouponValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      if ((!Utils.isNothing(coverage.options)) && !(Utils.isNothing(coverage.options.PO))) {
         if (coverage.options.PO == InterfaceConstants.OPTION_PO.PO_W_COUPON) {
            if (!(coverage.catalog.call("isCoupon"))) {
               return {
                  //Coupon is not supported.
                  code: "ERRC0022",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            } else {
               if (!coverage.schema.call("hasSupportOption", {
                     option: SchemaConstants.SUPPORT_OPTION.PO_W_COUPON
                  })) {
                  return {
                     //"PO with Coupon is not allowed."
                     code: "ERRC0023",
                     arguments: {"%PRODUCT_ID%" : productId}
                  };
               }
               if ((!Utils.isNothing(coverage.options)) && !(Utils.isNothing(coverage.options.prePayment))) {
                  if (coverage.options.prePayment == "Y") {
                     return {
                        //"PO with Coupon is not allowed with Prepayment."
                        code: "ERRC0024",
                        arguments: {"%PRODUCT_ID%" : productId}
                     };
                  }
               }
            }
         } else {
            if (coverage.options.PO == InterfaceConstants.OPTION_PO.PO_W_COUPON_N) {
               if (!(coverage.catalog.call("isCoupon"))) {
                  return {
                     //Coupon is not supported.
                     code: "ERRC0022",
                     arguments: {"%PRODUCT_ID%" : productId}
                  };
               }
            }
         }
      }
   }
});

var POValidationRule = Class.define({
   ruleName: function(args) {
      return "POValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   getFundActivitySize: function(coverage) {
      if (!Utils.isNothing(coverage.proposal.fundActivities) && !Utils.isNothing(coverage.proposal.fundActivities.fundActivity)) {
         var fundActivityTable = Utils.iterator(coverage.proposal.fundActivities.fundActivity);
         return fundActivityTable.length;
      }
      return 0;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      if ((!Utils.isNothing(coverage.options)) && !(Utils.isNothing(coverage.options.PO)) && (coverage.options.PO == "Y")) {
         if (!coverage.schema.call("hasSupportOption", {
               option: SchemaConstants.SUPPORT_OPTION.PO
            })) {
            return {
               //"The Premium Offset is not supported."
               code: "ERRC0025",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         } else {
            if (coverage.catalog.call("isUVL")) {

               if (this.getFundActivitySize(coverage) > 0) {
                  return {
                     //"The Premium Offset is not available with Fund Activities."
                     code: "ERRC0026",
                     arguments: {"%PRODUCT_ID%" : productId}
                  };
               }
            } else {
               if (coverage.options.dividendOption != InterfaceConstants.OPTION_DIVIDEND.DVD_ACCUMULATED_DIVIDEND) {
                  return {
                     //"The Premium Offset supports Accumulated Dividend option only."
                     code: "ERRC0027",
                     arguments: {"%PRODUCT_ID%" : productId}
                  };
               }
            }
         }
      }
   }
});

var IPOValidationRule = Class.define({
   ruleName: function(args) {
      return "IPOValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      if ((!Utils.isNothing(coverage.options)) && !(Utils.isNothing(coverage.options.IPO)) && (coverage.options.IPO == "Y")) {
         if (!coverage.schema.call("hasSupportOption", {
               option: SchemaConstants.SUPPORT_OPTION.IPO
            })) {
            return {
               //"IPO is not supported."
               code: "ERRC0033",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         } else {
            var insureds = _PV( coverage, 'parties.party' );
            if (Utils.isNothing(insureds)) {
                return {
                   code: "ERRC0213",
                   arguments: {"%PRODUCT_ID%" : productId}
                };
            }
            var insuredList = Utils.iterator( insureds );
            for (insuredIdx in insuredList) {
               var insured = insuredList[insuredIdx];
               var insuredAge = insured.insuredAge;
               _DB("insuredAge("+ insuredIdx + ")", insuredAge);

               var variables = {"%PRODUCT_ID%" : productId};
               variables[PublicConstants.MESSAGE_FIELD.MAXIMUM_BASIC_IPO_ISSUE_AGE] = "" + Number(schema.ProductSchema.BasicParticular.MaxBasicIPOIssueAge.text);
               variables[PublicConstants.MESSAGE_FIELD.MINIMUM_BASIC_IPO_ISSUE_AGE] = "" + Number(schema.ProductSchema.BasicParticular.MinBasicIPOIssueAge.text);
               if (insuredAge > Number(schema.ProductSchema.BasicParticular.MaxBasicIPOIssueAge.text)) {
                  return {
                     // "IPO is not supported for age over " + psVO.getBasicParticular().getMaxBasicIPOIssueAge() + "."
                     code: "ERRC0034",
                     arguments: variables
                  };
               } else if (insuredAge < Number(schema.ProductSchema.BasicParticular.MinBasicIPOIssueAge.text)) {
                  return {
                     //"IPO is not supported for age below " + psVO.getBasicParticular().getMaxBasicIPOIssueAge() + ".
                     code: "ERRC0034",
                     arguments: variables
                  };
               }

            }
         }
         if (coverage.schema.call("hasExtraRating", {coverage: coverage})) {
            return {
               //IPO is not supported due to extra rating.
               code: "ERRC0036",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
      } else {
         if (coverage.schema.call("hasExtraRating", {
               coverage: coverage
            })) {
            if (!coverage.schema.call("hasSupportOption", {
                  option: SchemaConstants.SUPPORT_OPTION.EXTRA_RATING
               })) {
               return {
                  // Error: It does not support extra rating.
                  code: "ERRC0037",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            } else {
               //TO DO- check condition
               if (coverage.extraRating.tempFlatDuration > 0 && coverage.extraRating.tempFlat == "0.0") {
                  return {
                     // Error: The temp flat duration is not set.
                     code: "ERRC0038",
                     arguments: {"%PRODUCT_ID%" : productId}
                  };
               }
               if (coverage.extraRating.tempFlatDuration == 0 && coverage.extraRating.tempFlat > 0) {
                  return {
                     // Error: The temp flat rate is not set.
                     code: "ERRC0039",
                     arguments: {"%PRODUCT_ID%" : productId}
                  };
               }
               // compatible logic - clarification required
               if (coverage.extraRating.tempPercentageDuration > 0 && coverage.extraRating.tempPercentage < 1) {
                  return {
                     // Error: The temp percentage rate is not set.
                     code: "ERRC0040",
                     arguments: {"%PRODUCT_ID%" : productId}
                  };
               }
               if (coverage.extraRating.tempPercentageDuration == 0 && coverage.extraRating.tempPercentage > 1) {
                  return {
                     // Error: The temp percentage duration is not set.
                     code: "ERRC0041",
                     arguments: {"%PRODUCT_ID%" : productId}
                  };
               }

               // Error: The temp extra duration cannot over premium duration.
               var premiumPayingPeriod = coverage.schema.call("calculatePremiumPayingPeriod", {
                  coverage: coverage
               })
               if (coverage.extraRating.tempFlatDuration > premiumPayingPeriod || coverage.extraRating.tempPercentageDuration > premiumPayingPeriod) {
                  return {
                     code: "ERRC0042",
                     arguments: {"%PRODUCT_ID%" : productId}
                  };

               }
               // compatible logic - clarification required
               if (coverage.extraRating.percentageExtra < 1) {
                  var variables = {"%PRODUCT_ID%" : productId};
                  variables[PublicConstants.MESSAGE_FIELD.PRODUCT_ID] = coverage.product.productKey.primaryProduct.productPK.productId;
                  return {
                     // 259 Error: Percentage extra for $_ must be greater than 1 or equal to 1.
                     code: "ERRC0043",
                     arguments: variables
                  };
               }
            }

         }
      }
   }
});

var OccupationClassValidationRule = Class.define({
   ruleName: function(args) {
      return "OccupationClassValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE | PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      if (coverage.schema.call("hasSupportOption", {
            option: SchemaConstants.SUPPORT_OPTION.OCCUPATION
         })) {

         if (Utils.isNothing(coverage.occupation)) {
            return {
               // Error: No occupation class is set.
               code: "ERRC0048",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }

         var validOccupations = Utils.iterator( _V( _PV(schema, 'ProductSchema.BasicParticular.SupportOccupationClass.OccupationClass'), [] ) );
         if (!Utils.isNothing(coverage.occupation)) {
            var variables = {"%PRODUCT_ID%" : productId};
            variables[PublicConstants.MESSAGE_FIELD.OCCUPATION] = "" + coverage.occupation;

            var matched = false;
            for(i in validOccupations) {
               if (coverage.occupation == _V(validOccupations[i])) {
                   matched = true;
                   break;
               }
            }

            if (!matched) {
               //"Error: It does not support occupation class CLASS $_."
               return {
                  code: "ERRC0050",
                  arguments: variables
               };
            }
         }
      } else {
         if (!Utils.isNothing(coverage.occupation)) {
            return {
               //"Error: It does not support occupation class."
               code: "ERRC0049",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
      }
   }
});

var PremiumDiscountValidationRule = Class.define({
   ruleName: function(args) {
      return "PremiumDiscountValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE | PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      var variables = {"%PRODUCT_ID%" : productId};
      if (!Utils.isNothing(coverage.discountType)) {
         var errCode = "ERRC0052";
         variables[PublicConstants.MESSAGE_FIELD.PREMIUM_DISCOUNT] = coverage.discountType;
         if (coverage.discountType == SchemaConstants.PREMIUM_DISCOUNT.AGE_DISCOUNT) {
            errCode = "ERRC0052";
         } else if (coverage.discountType == SchemaConstants.PREMIUM_DISCOUNT.AMOUNT_DISCOUNT) {
            errCode = "ERRC0053";
         } else if (coverage.discountType == SchemaConstants.PREMIUM_DISCOUNT.RATE_DISCOUNT) {
            errCode = "ERRC0054";
         } else if (coverage.discountType == SchemaConstants.PREMIUM_DISCOUNT.PERCENTAGE_DISCOUNT) {
            errCode = "ERRC0055";
         }


         if (coverage.schema.call("hasSupportOption", {
               option: SchemaConstants.SUPPORT_OPTION.HAS_PREMIUM_DISCOUNT
            })) {
            // TO DO logic is not correct : same werror messages
            if (!(coverage.schema.call("isPremiumDiscountSupported", {
                  discountType: coverage.discountType
               }))) {
               return {
                  //"Error: Age Discount is not allowed for $_."
                  //"Error: Amount Discount is not allowed for $_."
                  //"Error: Rate Discount is not allowed for $_."
                  //"Error: Percentage Discount is not allowed for $_."
                  code: errCode,
                  arguments: variables
               };
            }
         } else {
            return {
               //"Error: Age Discount is not allowed for $_."
               //"Error: Amount Discount is not allowed for $_."
               //"Error: Rate Discount is not allowed for $_."
               //"Error: Percentage Discount is not allowed for $_."
               code: errCode,
               arguments: variables
            };
         }
      }

   }
});

var PayorAgeValidationRule = Class.define({
   ruleName: function(args) {
      return "PayorAgeValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   isAcceptableAgeGap: function(args) {
      var ageRelationType = args['ageRelationType'];
      var insuredAge = args['insuredAge'];
      var coverage = args['coverage'];
      var schema = args['schema'];

      var errorCode = "";
      var ageGap = coverage.payerAge - insuredAge;

      var dependencyTbl = null;
      if (schema.ProductSchema.Dependency) {
         if (schema.ProductSchema.Dependency.AgeRelationRecord) {
            dependencyTbl = schema.ProductSchema.Dependency.AgeRelationRecord;
         }
      }
      if (schema.ProductSchema.AgeDependency) {
         if (schema.ProductSchema.AgeDependency.AgeRelationRecord) {
            dependencyTbl = schema.ProductSchema.AgeDependency.AgeRelationRecord;
         }
      }
      if (Utils.isNothing(dependencyTbl)) {
         return errorCode;
      } else {
         var ageRelTable = Utils.iterator(dependencyTbl);

         if (ageRelTable.length == 0 && coverage.payerAge > 0) {
            return errorCode;
         }

         for (recKey in ageRelTable) {
            var rec = ageRelTable[recKey];
            if (ageRelationType.equalsIgnoreCase(rec.Type.text)) {
               if (coverage.payerAge < _V(rec.MinAge)) {
                  return "ERRC0151";
               }
               if (coverage.payerAge > _V(rec.MaxAge)) {
                  return "ERRC0152";
               }
               if (ageGap < _V(rec.MinAgeGap)) {
                  return "ERRC0153";
               }
               if (ageGap < _V(rec.MaxAgeGap)) {
                  return "ERRC0154";
               }
            }
         }
         return errorCode;
      }
   },

   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      if (!Utils.isNothing(coverage.payerAge) && Number(coverage.payerAge) == 0) {
         if (coverage.schema.call("hasSupportOption", {
               option: SchemaConstants.SUPPORT_OPTION.PAYOR_REQUIRED
            })) {
            return {
               //"The payor age must be entered and greater than zero
               code: "ERRC0157",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
      } else if (!Utils.isNothing(coverage.payerAge) && Number(coverage.payerAge) > 0) {
         if (!coverage.schema.call("hasSupportOption", {
               option: SchemaConstants.SUPPORT_OPTION.PAYOR_REQUIRED
            })) {
            return {
               //"TThe payor age is not required
               code: "ERRC0156",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         } else if (!Utils.isNothing(coverage.payerAge) && Number(coverage.payerAge) > 0) {
      var insureds = _PV( coverage, 'parties.party' );
           if (Utils.isNothing(insureds)) {
               return {
                  code: "ERRC0213",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
           }
           var insuredList = Utils.iterator( insureds );
           for (insuredIdx in insuredList) {
               var insured = insuredList[insuredIdx];
               var insuredAge = insured.insuredAge;
               _DB("insuredAge("+ insuredIdx + ")", insuredAge);

               var errorCode = this.self.isAcceptableAgeGap({
                    ageRelationType: SchemaConstants.AGE_RELATION_TYPE.PAYOR,
                    insuredAge: Number(insuredAge),
                    coverage: coverage,
                    schema: schema
               });
               if (errorCode != "") {
                  return {
                     code: errorCode,
                     arguments: {"%PRODUCT_ID%" : productId}
                  };
               }
            }
         }
      }
   }
});

var BillingMethodValidationRule = Class.define({
   ruleName: function(args) {
      return "BillingMethodValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      if ((!Utils.isNothing(coverage.options)) && !(Utils.isNothing(coverage.options.billingMethod))) {
         if (coverage.options.billingMethod.equalsIgnoreCase(SchemaConstants.BILLING_MODE.PDF)) {

            if (!coverage.schema.call("hasSupportOption", {
                  option: SchemaConstants.SUPPORT_OPTION.PDF
               })) {
               return {
                  //"PDF is not supported."
                  code: "ERRC0066",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            } else {
               //
               if (coverage.options.paymentMode != SchemaConstants.PAYMENT_MODE.ANNUAL) {
                  // Error: Only ANNUAL mode support PDF.
                  return {
                     code: "ERRC0067",
                     arguments: {"%PRODUCT_ID%" : productId}
                  };
               }

               if (coverage.noOfInstallmentYear > 0) {
                  var premiumPayingPeriod = (coverage.schema.call("calculatePremiumPayingPeriod", {
                     coverage: coverage
                  }))

                  if (coverage.noOfInstallmentYear >= premiumPayingPeriod) {
                     //Error: Installment Year cannot be equal or greater than premium paying period.
                     return {
                        code: "ERRC0068",
                        arguments: {"%PRODUCT_ID%" : productId}
                     };
                  } else {
                     if (coverage.prepayYear > 0) {
                        if (coverage.prepayYear < 5) {
                           // Error: Prepay Year must be 5 years or above.
                           return {
                              code: "ERRC0069",
                              arguments: {"%PRODUCT_ID%" : productId}
                           };
                        } else {
                           if (coverage.noOfInstallmentYear >= coverage.prepayYear) {
                              // Error: Installment Year cannot be equal or greater than Prepay Year.
                              return {
                                 code: "ERRC070",
                                 arguments: {"%PRODUCT_ID%" : productId}
                              };
                           }
                        }
                     }
                  }
               } else {
                  // Error: Installment Year must be 1 year or above.
                  return {
                     code: "ERRC0071",
                     arguments: {"%PRODUCT_ID%" : productId}
                  };
               }
            }
         }
      }

      //var billingMethod = coverage.options.billingMethod;
      _DB("coverage.options", coverage.options, "BillingMethodValidationRule->");
      if (Utils.isNothing( _PV( coverage, ['options', 'billingMethod'] ) )) {
         return {
            // Error: The billing method is not inputed.
            code: "ERRC0065",
            arguments: {"%PRODUCT_ID%" : productId}
         };
      } else {
         var billingMethod = coverage.options.billingMethod;
         if (!(Utils.isNothing(coverage)) && !(Utils.isNothing(coverage.currency)) && !(Utils.isNothing(coverage.currency.currencyPK))) {
            if (!(coverage.schema.call("isCurrencySupported", {
                  currencyId: coverage.currency.currencyPK.currencyId
               }))) {
               return {
                  // Error: The billing method cannot be checked due to invalid currency.
                  code: "ERRC0064",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            } else {
               if (!(coverage.schema.call("isBillingMethodPaymentModeSupported", {
                     billingMethod: coverage.options.billingMethod,
                     paymentMode: coverage.options.paymentMode
                  }))) {
                  //Autopay does not support for payment mode ANNUAL.
                  //Direct billing does not support for payment mode ANNUAL.
                  //Credit card does not support for payment mode ANNUAL.
                  //Autopay does not support for payment mode SEMI-ANNUAL.
                  //Direct billing does not support for payment mode SEMI-ANNUAL.
                  //Credit card does not support for payment mode SEMI-ANNUAL.
                  //Autopay does not support for payment mode QUARTERLY.
                  //Direct billing does not support for payment mode QUARTERLY.
                  //Credit card does not support for payment mode QUARTERLY.
                  //Autopay does not support for payment mode MONTHLY.
                  //Direct billing does not support for payment mode MONTHLY.
                  //Credit card does not support for payment mode MONTHLY.
                  //Single does not support for payment mode ANNUAL.
                  //Single does not support for payment mode SEMI-ANNUAL
                  //Single does not support for payment mode QUARTERLY.
                  //Single does not support for payment mode MONTHLY
                  var variables = {"%PRODUCT_ID%" : productId};
                  variables[PublicConstants.MESSAGE_FIELD.BILLING_METHOD] = coverage.options.billingMethod;
                  variables[PublicConstants.MESSAGE_FIELD.PAYMENT_MODE] = coverage.options.paymentMode;
                  return {
                     code: "ERRC0062",
                     arguments: variables
                  };
               }

               if (!(coverage.schema.call("isCurrencyBillingMethodSupported", {
                     billingMethod: coverage.options.billingMethod,
                     currencyId: coverage.currency.currencyPK.currencyId
                  }))) {
                  var variables = {"%PRODUCT_ID%" : productId};
                  variables[PublicConstants.MESSAGE_FIELD.BILLING_METHOD] = coverage.options.billingMethod;
                  variables[PublicConstants.MESSAGE_FIELD.CURRENCY] = coverage.currency.currencyPK.currencyId;
                  return {
                     code: "ERRC0063",
                     arguments: variables
                  };
               }
            }
         }
      }
   }
});

var DividendOptionValidationRule = Class.define({
   ruleName: function(args) {
      return "DividendOptionValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      var isInputtedValidDividendOption = false;
      if ((!Utils.isNothing(coverage.options)) && !(Utils.isNothing(coverage.options.dividendOption))) {
         if (coverage.options.dividendOption == SchemaConstants.DIVIDEND_OPTION.CASH_DIVIDEND || coverage.options.dividendOption == SchemaConstants.DIVIDEND_OPTION.ACCUMULATE || coverage.options.dividendOption == SchemaConstants.DIVIDEND_OPTION.PAID_UP_ADDITION || coverage.options.dividendOption == SchemaConstants.DIVIDEND_OPTION.BONUS_PROTECTION) {
            isInputtedValidDividendOption = true;
         }
      }
      if ((!Utils.isNothing(schema.ProductSchema.BasicParticular.SupportDividendOption)) && !(Utils.isNothing(schema.ProductSchema.BasicParticular.SupportDividendOption.DividendOption))) {
         var dividentOptionTable = Utils.iterator(schema.ProductSchema.BasicParticular.SupportDividendOption.DividendOption);
         if (dividentOptionTable.length > 0 && !isInputtedValidDividendOption) {
            return {
               // Error: The dividend option is not set
               code: "ERRC0077",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
      }
      if (isInputtedValidDividendOption) {
         if (coverage.options.billingMethod == SchemaConstants.BILLING_MODE.PDF && !(coverage.options.dividendOption == SchemaConstants.DIVIDEND_OPTION.ACCUMULATE)) {
            return {
               // Error: The dividend option must be Leave On Deposit because PDF is selected
               code: "ERRC0072",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }

         if (!coverage.schema.call("isDividendOptionSupported", {
               dividendOption: coverage.options.dividendOption
            })) {

            if (coverage.options.dividendOption == SchemaConstants.DIVIDEND_OPTION.CASH_DIVIDEND) {
               return {
                  //message = "Error: Cash Dividend option is not supported.";
                  code: "ERRC0073",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            } else if (coverage.options.dividendOption == SchemaConstants.DIVIDEND_OPTION.ACCUMULATE) {
               return {
                  //message = "Error: Accumulating Dividend option is not supported.";
                  code: "ERRC0074",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            } else if (coverage.options.dividendOption == SchemaConstants.DIVIDEND_OPTION.PAID_UP_ADDITION) {
               return {
                  //message = "Error: Paid-up Addition option is not supported.";
                  code: "ERRC0075",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            } else if (coverage.options.dividendOption == SchemaConstants.DIVIDEND_OPTION.BONUS_PROTECTION) {
               return {
                  //message = "Error: Bonus Protection option is not supported.";
                  code: "ERRC0076",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            }
         }
      }

   }
});

var UVDeathBenefitValidationRule = Class.define({
   ruleName: function(args) {
      return "UVDeathBenefitValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      var isValidDeathBenefitInputed = false;

      var dbLevel = _PV(coverage, ['options', 'dbLevel']);
      var dbOptions = _PV(schema, ['ProductSchema','BasicParticular','SupportDeathBenefit','DeathBenefit']);

      if (dbOptions && dbOptions.length > 0) {
         if (Utils.isNothing( _PV(coverage, ['options', 'dbLevel']) )) {
            // Error: The death benefit option is not set.
            return {
               code: "ERRC0078",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
         var dbLevel = _PV(coverage, 'options.dbLevel');
         if (coverage.schema.call("isDBOptionSupported", { dbOption: dbLevel })) {
             if (dbLevel == SchemaConstants.DB_OPTION.INCREASE) {
                var insureds = _PV( coverage, 'parties.party' );
                if (Utils.isNothing(insureds)) {
                   return {
                      code: "ERRC0213",
                      arguments: {"%PRODUCT_ID%" : productId}
                   };
                }
                var insuredList = Utils.iterator( insureds );
                for (insuredIdx in insuredList) {
                   var insured = insuredList[insuredIdx];
                   var insuredAge = insured.insuredAge;
                   _DB("insuredAge("+ insuredIdx + ")", insuredAge);

                   if (!Utils.isNothing(coverage.dbToLevelAtAge) && coverage.dbToLevelAtAge >= 0 && coverage.dbToLevelAtAge <= insuredAge) {
                      var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                      variables[PublicConstants.MESSAGE_FIELD.DB_LEVEL_AGE] = "" + coverage.dbToLevelAtAge;
                      // "Error: Change death benefit to Level at age ($_) must above issue age."
                      return {
                         code: "ERRC0079",
                         arguments: variables
                      };
                   }
                }
             } else {
                if (!Utils.isNothing(coverage.dbToLevelAtAge) && coverage.dbToLevelAtAge >= 0) {
                   // Error: Change death benefit to Level is not supported.
                   return {
                      code: "ERRC0080",
                      arguments: {"%PRODUCT_ID%" : productId}
                   };
                }
             }
         } else {
             if (coverage.options.dbLevel == SchemaConstants.DB_OPTION.LEVEL) {
                //message = "Error: The level death benefit option is not supported.";
                return {
                   code: "ERRC0081",
                   arguments: {"%PRODUCT_ID%" : productId}
                };
             } else if (coverage.options.dbLevel == SchemaConstants.DB_OPTION.INCREASE) {
                //message = "Error: The increase death benefit option is not supported.";
                return {
                   code: "ERRC0082",
                   arguments: {"%PRODUCT_ID%" : productId}
                };
             } else if (coverage.options.dbLevel == SchemaConstants.DB_OPTION.INDEXED) {
                //message = "Error: The indexed death benefit option is not supported.";
                return {
                   code: "ERRC0083",
                   arguments: {"%PRODUCT_ID%" : productId}
                };
             } else if (coverage.options.dbLevel == SchemaConstants.DB_OPTION.ROP) {
                //message = "Error: The ROP death benefit option is not supported.";
                return {
                   code: "ERRC0084",
                   arguments: {"%PRODUCT_ID%" : productId}
                };
             } else {
                //message = "Error: The death benefit option is not supported.";
                return {
                   code: "ERRC0214",
                   arguments: {"%PRODUCT_ID%" : productId}
                };
             }
         }
      } else {
         if (!Utils.isNothing( _PV(coverage, ['options', 'dbLevel']) )) {
            // Error: The death benefit option is not required.
            return {
               code: "ERRC0211",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
      }
   }
});

var IFLStartingAgeValidationRule = Class.define({
   ruleName: function(args) {
      return "IFLStartingAgeValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);

      var startIFLAge = schema.ProductSchema.BasicParticular.StartAgeIFL;
      var endIFLAge = schema.ProductSchema.BasicParticular.EndAgeIFL;

      if (coverage.schema.call("hasSupportOption", {
            option: SchemaConstants.SUPPORT_OPTION.INCOME_FOR_LIFE
         })) {
         if (!Utils.isNothing(coverage.iflAge)) {
            if (coverage.iflAge < startIFLAge) {
               var variables = {"%PRODUCT_ID%" : productId};
               variables[PublicConstants.MESSAGE_FIELD.IFL_AGE] = coverage.iflAge;
               return {
                  //Error: The starting age of IFL cannot be less than the age $_.
                  code: "ERRC0085",
                  arguments: variables
               };
            }
            if (coverage.iflAge > endIFLAge) {
               var variables = {"%PRODUCT_ID%" : productId};
               variables[PublicConstants.MESSAGE_FIELD.IFL_AGE] = coverage.iflAge;
               return {
                  //Error: The starting age of IFL cannot be over than the age $_.
                  code: "ERRC0086",
                  arguments: variables
               };
            }
         }
      } else {
         if (!Utils.isNothing(coverage.iflAge) || !Utils.isNothing(coverage.iflAmount)) {
            //Error: IFL is not allowed.
            return {
               code: "ERRC0087",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
      }
   }
});

var FundCodeValidationRule = Class.define({
   ruleName: function(args) {
      return "FundCodeValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      var catalog = coverage.catalog.call("getCatalog", null);
      if (!(coverage.catalog.call("isUVL"))) {
         return;
      }
      if (!Utils.isNothing(coverage.proposal.funds) && !Utils.isNothing(coverage.proposal.funds.fundRecord)) {
         var fundTable = Utils.iterator(coverage.proposal.funds.fundRecord);
         if (fundTable.length > 0) {
            var accumAllocation = 0;
            for (fundi in fundTable) {
               var fundRec = fundTable[fundi];
               accumAllocation += parseInt(fundRec.allocation);
               if (!(coverage.catalog.call("isFundCodeSupported", {
                     fund: fundRec._code
                  }))) {
                  var variables = {"%PRODUCT_ID%" : productId};
                  variables[PublicConstants.MESSAGE_FIELD.FUND_CODE] = fundRec._code;
                  return {
                     //message = "Error: Fund code (" + vo.getFundRecord(i).getFundCode() + ") is not supported.";
                     code: "ERRC0088",
                     arguments: variables
                  };
               }
            }
            if (accumAllocation != 100) {
               return {
                  //280", "Error: Total allocation of all funds must be 100%.
                  code: "ERRC0089",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            }
         } else {
            return {
               code: "ERRC0210",
               arguments: {"%PRODUCT_ID%" : productId}
            }
         }
      }
      if (!Utils.isNothing(coverage.proposal.topupFunds) && !Utils.isNothing(coverage.proposal.topupFunds.fundRecord)) {
         var topupFundTable = Utils.iterator(coverage.proposal.topupFunds.fundRecord);
         if (topupFundTable.length > 0) {
            var accumAllocation = 0;
            for (topupfundi in topupFundTable) {
               var topupfundRec = topupFundTable[fundi];
               accumAllocation += topupfundRec.allocation;
               if (!(coverage.catalog.call("isFundCodeSupported", {
                     fund: topupfundRec._code
                  }))) {
                  var variables = {"%PRODUCT_ID%" : productId};
                  variables[PublicConstants.MESSAGE_FIELD.FUND_CODE] = topupfundRec._code;
                  return {
                     //message = "Error: Fund code (" + vo.getFundRecord(i).getFundCode() + ") is not supported.";
                     code: "ERRC0090",
                     arguments: variables
                  };
               }
            }
            if (accumAllocation != 100) {
               return {
                  //280", "Error: Total allocation of all funds must be 100%.
                  code: "ERRC0209",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            }
         }
      }
   }
});

var FundInterestValidationRule = Class.define({
   ruleName: function(args) {
      return "FundInterestValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      // TO DO minRate and maxRate
      var minRate = 0;
      var maxRate = 99999;
      if (coverage.catalog.call("isUVL")) {
         var fundTable = Utils.iterator(coverage.funds.fundRecord);
         if (fundTable.length > 0) {
            var allocation = 0;
            for (fundi in fundTable) {
               var fundRec = fundTable[fundi];
               if (fundRec.returnRate < minRate || fundRec.returnRate > maxRate) {
                  var variables = {"%PRODUCT_ID%" : productId};
                  variables[PublicConstants.MESSAGE_FIELD.FUND_CODE] = fundRec._code;
                  variables[PublicConstants.MESSAGE_FIELD.RETURN_RATE] = fundRec.returnRate;
                  variables[PublicConstants.MESSAGE_FIELD.MIN_RETURN_RATE] = minRate;
                  variables[PublicConstants.MESSAGE_FIELD.MAX_RETURN_RATE] = maxRate;
                  return {
                     //"Error: The $_ return rate of $_ fund must be between $_ and $_."
                     code: "ERRC0091",
                     arguments: variables
                  };
               }
            }

         }

         var fundInt = coverage.FundInt;
         if (fundTable.length > 0) {
            var combineInt = 0;
            for (recKey in fundTable) {
               var fundRec = fundTable[recKey];
               combineInt += fundRec.returnRate * fundRec.allocation;
            }
            combineInt = combineInt / 100;

            if (combineInt > 0)
               fundInt = combineInt;
         }
         if (fundInt < 0) {
            // Error: Fund Growth Interest rate is not set.
            return {
               code: "ERRC0155",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
      }
   }
});

var SupportSOSByPolicyValidationRule = Class.define({
   ruleName: function(args) {
      return "SupportSOSByPolicyValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validateSupportSOSByCoverage: function(args) {
      var coverage = args['coverage'];
      var productKey = args['productKey'];
      var sosSupportedInCatalog = false;
      var sosSupportedInSchema = false;
      if ((coverage.catalog.call("isSOS"))) {
         sosSupportedInCatalog = true;
         if ((coverage.schema.call("hasSupportOption", {
               option: SchemaConstants.SUPPORT_OPTION.IS_SOS_SUPPORTED
            }))) {
            sosSupportedInSchema = true;
         }
      }
      if (sosSupportedInCatalog && sosSupportedInSchema) {
         return 1;
      } else
      if (sosSupportedInCatalog && !sosSupportedInSchema) {
         return 2;
      } else {
         return 3;
      }
   },

   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      var sosSupported = true;
      if (coverage.proposal.policyExcludeSOS == "N") {
         var validCode = this.validateSupportSOSByCoverage({
            coverage: coverage,
            productKey: coverage.productKey
         });
         if (validCode == 1) {
            sosSupported = true;
         } else
         if (validCode == 2) {
            sosSupported = false;
         }

         if (!Utils.isNothing(coverage.proposal.riders)) {
            var ridersTable = Utils.iterator(coverage.proposal.riders.coverageInfo);
            for (riderIndex in ridersTable) {
               var riderCov = coverage.proposal.riders.coverageInfo[riderIndex];
               var validCode = this.validateSupportSOSByCoverage({coverage: riderCov, productKey: riderCov.productKey});
               if (validCode == 1) {
                  sosSupported = true;
               } else
               if (validCode == 2) {
                  sosSupported = false;
               }
            }
         }

         if (!sosSupported) {
            var variables = {"%PRODUCT_ID%" : productId};
            variables[PublicConstants.MESSAGE_FIELD.LOCATION] = coverage.product.productKey.location;
            return {
               //"Error: The SOS is not supported for the location $_."
               code: "ERRC0092",
               arguments: variables
            };
         }
      }
   }
});

var AnnuityOptionValidationRule = Class.define({
   ruleName: function(args) {
      return "AnnuityOptionValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);

      var paymentPeriod = coverage.fixedPeriod;
      var bp = (coverage.schema.call("calculateBenefitPeriod", {
         coverage: coverage
      }));
      var giPayout = schema.ProductSchema.BasicParticular.PayoutPeriod;
      var rolling = coverage.rollingPeriod;

      if ((coverage.catalog.call("isIncome3"))) {
         if (coverage.startAnnuityAge > 0) {
            var premiumPayingPeriod = (coverage.schema.call("calculatePremiumPayingPeriod", {
               coverage: coverage
            }));

            if (!Utils.isNothing(coverage.proposal.riders)) {
               var ridersTable = Utils.iterator(coverage.proposal.riders.coverageInfo);
               if (ridersTable.length > 0) {
                  // 265 Error: Payout option is only available if no supplementary benefit is selected.
                  return {
                     code: "ERRC0206",
                     arguments: {"%PRODUCT_ID%" : productId}
                  };
               }
            }

            var insureds = _PV( coverage, 'parties.party' );
            if (Utils.isNothing(insureds)) {
                return {
                   code: "ERRC0213",
                   arguments: {"%PRODUCT_ID%" : productId}
                };
            }
            var insuredList = Utils.iterator( insureds );
            for (insuredIdx in insuredList) {
               var insured = insuredList[insuredIdx];
               var insuredAge = insured.insuredAge;
               _DB("insuredAge("+ insuredIdx + ")", insuredAge);

               if (coverage.startAnnuityAge < (insuredAge + premiumPayingPeriod)) {
                  // 260 Error: The dividend withdrawn start age must be after the premium paying period.
                  return {
                     code: "ERRC0093",
                     arguments: {"%INSURED%" : insured, "%PRODUCT_ID%" : productId}
                  };
               }
               if (paymentPeriod > 0) {
                  if ((coverage.startAnnuityAge + paymentPeriod) > (insuredAge + premiumPayingPeriod)) {
                     var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                     variables[PublicConstants.MESSAGE_FIELD.YEAR] = "" + (insuredAge + bp - startAnnuity);
                     // 262 Error: The fixed payment period cannot be greater than $_ year(s).
                     return {
                        code: "ERRC0094",
                        arguments: variables
                     };
                  }
               }

            }
         }
      } else {
         if (coverage.startAnnuityAge > 0) {
            return {
               // 261 Error: The payout option is not allowed.
               code: "ERRC0095",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }

         if (paymentPeriod > 0) {
            if ((rolling + paymentPeriod) < giPayout) {

               var variables = {"%PRODUCT_ID%" : productId};
               variables[PublicConstants.MESSAGE_FIELD.YEAR] = (giPayout);
               return {
                  // 263 Error: The total of rolling and fixed payment period cannot be less than the guarantee payout period ($_ years).
                  code: "ERRC0096",
                  arguments: variables
               };
            }

            if ((ppp + rolling + paymentPeriod) > bp) {
               var variables = {"%PRODUCT_ID%" : productId};
               variables[PublicConstants.MESSAGE_FIELD.YEAR] = (bp - ppp);
               return {
                  // 264, Error: The total of rolling and fixed payment period cannot be greater than $_ year(s).
                  code: "ERRC0097",
                  arguments: variables
               };
            }
         }
      }
   }
});

var IIOValidationRule = Class.define({
   ruleName: function(args) {
      return "IIOValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      var paymentMode = coverage.options.paymentMode;
      var isValidIIOInputed = false;
      if ((!Utils.isNothing(coverage.options)) && !(Utils.isNothing(coverage.options.IIO))) {
         if (coverage.options.IIO.equalsIgnoreCase(SchemaConstants.OPTION_IIO.OPTION_IIO_BASIC) || coverage.options.IIO.equalsIgnoreCase(SchemaConstants.OPTION_IIO.OPTION_IIO_TOPUP)) {
            isValidIIOInputed = true;
         }
         if (!coverage.schema.call("hasSupportOption", {
               option: SchemaConstants.SUPPORT_OPTION.IIO
            })) {
            if (isValidIIOInputed) {
               return {
                  // Error: IIO is not supported.
                  code: "ERRC0098",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            }
         } else {
            if (isValidIIOInputed) {
               if (coverage.options.IIO.equalsIgnoreCase(SchemaConstants.OPTION_IIO.OPTION_IIO_BASIC)) {
                  if (coverage.basicIncrePerc != 0) {
                     if (!coverage.schema.call("isValidIIO", {
                           basicIncrePerc: coverage.basicIncrePerc
                        })) {
                        // Error: Basic IIO% is not valid.
                        return {
                           code: "ERRC0099",
                           arguments: {"%PRODUCT_ID%" : productId}
                        };
                     }
                  } else {
                     // Error: Basic IIO% is not set.
                     return {
                        code: "ERRC0100",
                        arguments: {"%PRODUCT_ID%" : productId}
                     };
                  }
                  var premiumPayingPeriod = (coverage.schema.call("calculatePremiumPayingPeriod", {
                     coverage: coverage
                  }))
                  if (coverage.iioDuration > premiumPayingPeriod) {
                     // Error: Basic IIO Duration cannot over premium duration.
                     return {
                        code: "ERRC0101",
                        arguments: {"%PRODUCT_ID%" : productId}
                     };
                  }

               }

               if (coverage.options.IIO.equalsIgnoreCase(SchemaConstants.OPTION_IIO.OPTION_IIO_TOPUP)) {
                  if (coverage.topUpIncrePerc != 0) {
                     if (!coverage.schema.call("isValidIIO", {
                           basicIncrePerc: coverage.topUpIncrePerc
                        })) {
                        // Error: Top-up IIO% is not valid.
                        return {
                           code: "ERRC0102",
                           arguments: {"%PRODUCT_ID%" : productId}
                        };
                     }
                  } else {
                     // Error: Basic IIO% is not set.
                     return {
                        code: "ERRC0103",
                        arguments: {"%PRODUCT_ID%" : productId}
                     };
                  }
               } else {
                  if (coverage.topUpIncrePerc > 0) {
                     // Error: Top-up IIO% is not required.
                     return {
                        code: "ERRC0104",
                        arguments: {"%PRODUCT_ID%" : productId}
                     };
                  }
               }
            }
            if (coverage.topUpIncrePerc > 0 && coverage.topUpPremium <= 0) {
               // Error: Top-up Premium is not set.
               return {
                  code: "ERRC0105",
                  arguments: {"%PRODUCT_ID%" : productId}
               };

            }
            // TO DO some more logic

            var regularBasicPrem = coverage.plannedPremium;
            var minPremium = 0;
            if (regularBasicPrem > 0) {
               var fundChangeRec = getFundChangeRecord({
                  fundChange: SchemaConstants.FUND_CHANGE_TYPE.AUTOMATIC,
                  coverage: coverage,
                  duration: duration,
                  paymentMode: paymentMode,
                  schema: schema
               });
               if (!Utils.isNothing(fundChangeRec)) {
                  minPremium = Number(fundChangeRec.MinIncrease.text);
               }
               var fundChangeRec = getFundChangeRecord({
                  fundChange: SchemaConstants.FUND_CHANGE_TYPE.PREMIUM,
                  coverage: coverage,
                  duration: duration,
                  paymentMode: paymentMode,
                  schema: schema
               });
               if (!Utils.isNothing(fundChangeRec)) {
                  minPremium = Number(fundChangeRec.MinIncrease.text);
               }
               // TO DO : Clarification required on 100d
               var basicIncrePerc = Number(coverage.basicIncrePerc);
               var iioPremium = (basicIncrePerc * regularBasicPrem) / 100;
               if (coverage.options.IIO.equalsIgnoreCase(SchemaConstants.OPTION_IIO.OPTION_IIO_TOPUP)) {
                  var topUpIncrePerc = Number(coverage.topUpIncrePerc);
                  var topUpPremium = Number(coverage.topUpPremium);
                  iioPremium = (iioPremium + (topUpIncrePerc + topUpPremium)) / 100;
               }
               if (iioPremium > 0 && iioPremium < minPremium) {
                  // Error: Increase in Planned Premium is below minimum.
                  return {
                     code: "ERRC0197",
                     arguments: {"%PRODUCT_ID%" : productId}
                  };
               }
            }
         }
      }
   }
});

var CommencementYearValidationRule = Class.define({
   ruleName: function(args) {
      return "CommencementYearValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      if (!Utils.isNothing(coverage.proposal.commencementYear) && coverage.proposal.commencementYear > 0) {
         if (coverage.schema.call("hasSupportOption", {
               option: SchemaConstants.SUPPORT_OPTION.HAS_COMMENCEMENT_YEAR
            })) {
            var policyYearDate = new Date(coverage.proposal.policyYearDate);
            var policyYear = policyYearDate.getFullYear();
            if (coverage.proposal.commencementYear < schema.ProductSchema.BasicParticular.CommencementYear + policyYear) {
               var variables = {"%PRODUCT_ID%" : productId};
               variables[PublicConstants.MESSAGE_FIELD.YEAR] = policyYear;
               return {
                  //"Error: Commencement Year should be the year ??? or later.";
                  code: "ERRC0114",
                  arguments: variables
               };
            }
         } else {
            return {
               // Error: Commencement Year is not supported.
               code: "ERRC0115",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
      }
   }
});

var HookSubscriptionWithFAValidationRule = Class.define({
   ruleName: function(args) {
      return "HookSubscriptionWithFAValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE | PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);

      if (coverage.schema.call("hasSupportOption", {
            option: SchemaConstants.SUPPORT_OPTION.HOOK_SUBSCRIPTION_WITH_FA
         })) {
         amountFA = Utils.isNothing(coverage.faceAmount) ? 0 : Number(coverage.faceAmount);
         if (amountFA > 0) {
            return {
               // Error: No face amount is required..
               code: "ERRC0113",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
      }
   }
});

var DisallowRiderWithGuaranteedIssueValidationRule = Class.define({
   ruleName: function(args) {
      return "DisallowRiderWithGuaranteedIssueValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },

   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var hasNonGIrider = 0;
      var nonGIriderCode = "";

      var hasOverLimitGIrider = 0;
      var overLimitGIrider = "";
      var giRiderLimit = "";
      var schema = coverage.schema.call("getSchema", null);
      if ((!Utils.isNothing(coverage.options)) && !(Utils.isNothing(coverage.options.guaranteedIssue)) && coverage.options.guaranteedIssue == "Y") {
         if (!coverage.catalog.call("isGuaranteedIssue") && !coverage.catalog.call("isConditionalGuaranteedIssue")) {
            // Error: Guaranteed Issue is not allowed to this plan.
            return {
               code: "ERRC0124",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         } else if (!Utils.isNothing(coverage.proposal.riders)) {
            var ridersTable = Utils.iterator(coverage.proposal.riders.coverageInfo);
            for (riderIndex in ridersTable) {
               var riderCov = coverage.proposal.riders.coverageInfo[riderIndex];
               var riderCatalog = riderCov.catalog.call("getCatalog", null);
               var riderSchema = riderCov.schema.call("getSchema", null);
               if (!riderCov.catalog.call("isGuaranteedIssue") && !riderCov.catalog.call("isConditionalGuaranteedIssue")) {
                  hasNonGIrider = hasNonGIrider + 1;
                  if ("".equalsIgnoreCase(nonGIriderCode)) {
                     nonGIriderCode = nonGIriderCode + riderCov.product.productKey.primaryProduct.productPK.productId;
                  } else {
                     nonGIriderCode = nonGIriderCode + ", " + riderCov.product.productKey.primaryProduct.productPK.productId;
                  }
               } else {
                  if (riderCov.catalog.call("isGuaranteedIssue")) {
                     var hasGuarantedIssueRequirementSupport = (coverage.schema.call("hasSupportOption", {
                        option: SchemaConstants.SUPPORT_OPTION.GUARANTEED_ISSUE_REQUIREMENT
                     }));
                     if (hasGuarantedIssueRequirementSupport) {
                        var maxGIFaceamountRec = null;
                        var maxGIFaceamountRec = getMaxGIFaceamount({
                           coverage: riderCov,
                           schema: riderSchema
                        });
                        if (maxGIFaceamountRec != null) {
                           if (riderCov.faceAmount > Number(maxGIFaceamountRec.MaxGIFaceamount.text)) {
                              hasOverLimitGIrider = hasOverLimitGIrider + 1;
                              if ("".equalsIgnoreCase(hasOverLimitGIrider)) {
                                 overLimitGIrider = overLimitGIrider + riderCov.product.productKey.primaryProduct.productPK.productId;
                                 giRiderLimit = giRiderLimit + riderCov.currency.currencyPK.currencyId + (maxGIFaceamount);
                              } else {
                                 overLimitGIrider = overLimitGIrider + ", " + riderCov.product.productKey.primaryProduct.productPK.productId;
                                 giRiderLimit = giRiderLimit + ", " + riderCov.currency.currencyPK.currencyId + (maxGIFaceamount);
                              }
                           }
                        }

                     } else {
                        hasNonGIrider = hasNonGIrider + 1;
                        if ("".equalsIgnoreCase(nonGIriderCode)) {
                           nonGIriderCode = nonGIriderCode + riderCov.product.productKey.primaryProduct.productPK.productId;
                        } else {
                           nonGIriderCode = nonGIriderCode + ", " + riderCov.product.productKey.primaryProduct.productPK.productId;
                        }
                     }

                  }
               }

               if (hasNonGIrider > 0) {
                  // Error: Non-GI rider ($_) cannot be attached with Guaranteed Issue Option
                  var variables = {"%PRODUCT_ID%" : productId};
                  variables[PublicConstants.MESSAGE_FIELD.RIDER] = nonGIriderCode;
                  return {
                     code: "ERRC0207",
                     arguments: variables
                  };

               }
               if (hasOverLimitGIrider > 0) {
                  //Error: Guarantee Issue Option is not allowed since the face amount of riders (%RIDER%) is over the limit (%AMOUNT%)
                  var variables = {"%PRODUCT_ID%" : productId};
                  variables[PublicConstants.MESSAGE_FIELD.AMOUNT] = hasOverLimitGIrider;
                  variables[PublicConstants.MESSAGE_FIELD.RIDER] = nonGIriderCode;
                  return {
                     code: "ERRC0208",
                     arguments: variables
                  };

               }

            }
         }

      }

   }
});

var TopUpPremiumValidationRule = Class.define({
   ruleName: function(args) {
      return "TopUpPremiumValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE | PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      var catalog = coverage.catalog.call("getCatalog", null);
      var regularBasicPrem = 0;

      if (coverage.catalog.call("isRegularSaving")) {

         regularBasicPrem = _PV( coverage, ['plannedPremium'] );
         if (Utils.isNothing(regularBasicPrem)) regularBasicPrem = 0;

         if (coverage.schema.call("hasSupportOption", { option: SchemaConstants.SUPPORT_OPTION.TOPUPPREMIUM })) {
            if (!Utils.isNothing(coverage.topUpPremium) && (coverage.topUpPremium > 0)) {
               var minTopUpPremium = 999999999;
               var maxTopUpPremium = 0;

               var premiumRec = getPremiumRecord({
                  premiumType: SchemaConstants.PREMIUM_RANGE_TYPE.PREMIUMTYPE_TOPUPPREMIUM,
                  coverage: coverage,
                  schema: schema
               });

               if (!Utils.isNothing(premiumRec)) {
                  minTopUpPremium = Math.min(minTopUpPremium, Number( _PV( premiumRec, 'MinPremium' ) ));
                  maxTopUpPremium = Math.max(maxTopUpPremium, Number( _PV( premiumRec, 'MaxPremium' ) ));
               }

                _DB("minPremium (TopUp)", minTopUpPremium, "TopUpPremiumValidationRule.validate()->");
                _DB("maxPremium (TopUp)", maxTopUpPremium, "TopUpPremiumValidationRule.validate()->");

               /*
               for (premiumRangeKey in schema.ProductSchema.BasicParticular.PremiumRangeInformation.PremiumRange) {
                   var premiumRangeRec = schema.ProductSchema.BasicParticular.PremiumRangeInformation.PremiumRange[premiumRangeKey];
                   if ((coverage.currency.currencyPK.currencyId == premiumRangeRec.CurrencyPK.CurrencyId.text) && (premiumRangeRec.PaymentMode.text == coverage.options.paymentMode) && premiumRangeRec.MinAge > insuredAge && premiumRangeRec.MaxAge < insuredAge && premiumRangeRec.Type == SchemaConstants.PREMIUM_RANGE_TYPE.PREMIUMTYPE_TOPUPPREMIUM) {
                       minTopUpPremium = Math.min(minTopUpPremium, premiumRangeRec.MinPremium.text);
                       //maxTopUpPremium = Math.max(maxTopUpPremium, premiumRangeRec.MaxPremium.text);
                   }
               }*/

               /*
               if (regularBasicPrem > 0) {
                  maxTopUpPremium = coverage.topUpLimits * regularBasicPrem;
               } else {

                  var premiumRec = getPremiumRecord({
                     premiumType: SchemaConstants.PREMIUM_RANGE_TYPE.PREMIUMTYPE_BASICPREMIUM,
                     coverage: coverage,
                     schema: schema
                  });
                  if (!Utils.isNothing(premiumRec)) {
                     maxTopUpPremium = Math.max(maxTopUpPremium, Number(premiumRec.MaxPremium.text));
                  }
               }
               */
               if (coverage.topUpPremium < minTopUpPremium) {
                  var vars = {"%PRODUCT_ID%" : productId};
                  vars[PublicConstants.MESSAGE_FIELD.MINIMUM_INITIAL_TOP_UP] = minTopUpPremium;
                  // Error: Top Up Premium is below the minimum.
                  return {
                     code: "ERRC0119",
                     arguments: vars
                  };
               } else {
                  if (coverage.topUpPremium > maxTopUpPremium) {
                     var vars = {"%PRODUCT_ID%" : productId};
                     vars[PublicConstants.MESSAGE_FIELD.MAXIMUM_INITIAL_TOP_UP] = maxTopUpPremium;
                     // Error: Top Up Premium is above the maximum.
                     return {
                        code: "ERRC0120",
                        arguments: vars
                     };
                  } else {
                     //var maxBasicPremium = 0;
                     var premiumRec2 = getPremiumRecord({
                        premiumType: SchemaConstants.PREMIUM_RANGE_TYPE.PREMIUMTYPE_BASICPREMIUM,
                        coverage: coverage,
                        schema: schema
                     });
                     if (Utils.isNothing(premiumRec2)) {
                        premiumRec2 = getPremiumRecord({
                           premiumType: SchemaConstants.PREMIUM_RANGE_TYPE.PREMIUMTYPE_PLANNEDPREMIUM,
                           coverage: coverage,
                           schema: schema
                        });
                     }
                     var maxPremium = 0;
                     if (!Utils.isNothing(premiumRec2)) {
                        maxPremium = Math.max(maxPremium, Number( _PV( premiumRec, 'MaxPremium' ) ));
                     }
                     if ((regularBasicPrem + coverage.topUpPremium) > maxPremium) {
                        var vars = {"%PRODUCT_ID%" : productId};
                        vars[PublicConstants.MESSAGE_FIELD.MAXIMUM_PLANNEDPREMIUM] = maxPremium;
                        // Error: Premium is above the maximum.
                        return {
                           code: "ERRC0121",
                           arguments: vars
                        };
                     }
                  }
               }
            }
         } else {
            if (coverage.topUpPremium > 0) {
               return {
                  // Error: No Top Up Premium is required.
                  code: "ERRC0122",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            }
         }
      }
   }
});

var TotalPremiumValidationRule = Class.define({
   ruleName: function(args) {
      return "TotalPremiumValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.PROPOSAL;
   },
   getPremiumRange: function(args) {
      var proposal = args['proposal'];
      var coverage = proposal.coverageInfo;
      var schema = coverage.schema.call("getSchema");

      var premiumRec = getPremiumRecord({
         premiumType: SchemaConstants.PREMIUM_RANGE_TYPE.PREMIUMTYPE_BASICPREMIUM,
         coverage: coverage,
         schema: schema
      });
      if (Utils.isNothing(premiumRec)) {
         premiumRec = getPremiumRecord({
            premiumType: SchemaConstants.PREMIUM_RANGE_TYPE.PREMIUMTYPE_PLANNEDPREMIUM,
            coverage: coverage,
            schema: schema
         });
      }

      var minPP;
      var maxPP;
      if (!Utils.isNothing(premiumRec)) {
         minPP = Number( _PV( premiumRec, ['MinPremium'] ) );
         maxPP = Number( _PV( premiumRec, ['MaxPremium'] ) );
      }
      return {minLimit: minPP, maxLimit: maxPP};
   },
   getTotalPremium: function(args) {
      var proposal = args["proposal"];
      var coverage = proposal.coverageInfo;
      var totalPremium = 0;
      for (totalPremiumsKey in coverage._premiums.totalPremiums) {
         var totalPremiumsRec = coverage._premiums.totalPremiums[totalPremiumsKey];
         if (totalPremiumsRec.paymentMode == coverage.options.paymentMode) {
            totalPremium = totalPremiumsRec.totalPremium;
         }
      }
      return totalPremium;
   },
   validate: function(args) {
      var proposal = args["proposal"];
      var coverage = proposal.coverageInfo;
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');
      var schema = coverage.schema.call("getSchema", null);
      var catalog = coverage.catalog.call("getCatalog", null);

      var constantPaymentMode = "";
      var minError = "";
      var maxError = "";
      var issueAge = 0;
      var maxPP = 0;
      var minPP = 0;
      var totalPremium = this.self.getTotalPremium(args);

      //if (!coverage.catalog.call("isRegularSaving") && !coverage.catalog.call("isSinglePremium") && coverage.catalog.call("isUVL")) {
      //if ((!Utils.isNothing(coverage.options)) && !(Utils.isNothing(coverage.options.paymentMode))) {

      //for (totalPremiumsKey in coverage._premiums.totalPremiums) {
      //   var totalPremiumsRec = coverage._premiums.totalPremiums[totalPremiumsKey];
      //   if (totalPremiumsRec.paymentMode == coverage.options.paymentMode) {
      //      totalPremium = totalPremiumsRec.totalPremium;
      //   }
      //}

      //}
      //} else {
      //   for (totalPremiumsKey in coverage._premiums.totalPremiums) {
      //      var totalPremiumsRec = coverage._premiums.totalPremiums[totalPremiumsKey];
      //      if (totalPremiumsRec.paymentMode == coverage.options.paymentMode) {
      //         if (coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.ANNUAL || coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.MONTHLY) {
      //            totalPremium = totalPremiumsRec.totalPremium;
      //         }
      //      }
      //   }
      //}
      //_SS(proposal.coverageInfo, 10);
      _DB("totalPremium", totalPremium, "TotalPremiumValidationRule->");

      var variables = {"%PRODUCT_ID%" : productId};
      variables[PublicConstants.MESSAGE_FIELD.TOTAL_AMOUNT] = coverage.currency.currencyPK.currencyId + " " + totalPremium;

      if (!(Utils.isNothing(coverage.options)) && !(Utils.isNothing(coverage.options.paymentMode))) {
         if (coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.ANNUAL) {
            minError = "ERRC0158";
            maxError = "ERRC0159";
         } else if (coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.SEMIANNUAL) {
            minError = "ERRC0160";
            maxError = "ERRC0161";
         } else if (coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.QUARTERLY) {
            minError = "ERRC0162";
            maxError = "ERRC0163";
         } else if (coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.MONTHLY) {
            minError = "ERRC0164";
            maxError = "ERRC0165";
         }
      }

      /*
      var premiumRec = getPremiumRecord({
         premiumType: SchemaConstants.PREMIUM_RANGE_TYPE.PREMIUMTYPE_BASICPREMIUM,
         coverage: coverage,
         schema: schema
      });
      if (Utils.isNothing(premiumRec)) {
         premiumRec = getPremiumRecord({
            premiumType: SchemaConstants.PREMIUM_RANGE_TYPE.PREMIUMTYPE_PLANNEDPREMIUM,
            coverage: coverage,
            schema: schema
         });
      }
      if (!Utils.isNothing(premiumRec)) {
         minPP = Number( _PV( premiumRec, ['MinPremium'] ) );
         maxPP = Number( _PV( premiumRec, ['MaxPremium'] ) );
      }
      */
      var range = this.self.getPremiumRange({proposal: proposal});
      if (!Utils.isNothing(range)) {
          minPP = range.minLimit;
          maxPP = range.maxLimit;
      }
      _DB("minPP", minPP, "TotalPremiumValidationRule->");
      _DB("maxPP", maxPP, "TotalPremiumValidationRule->");


      if (totalPremium < minPP) {
         variables[PublicConstants.MESSAGE_FIELD.AMOUNT] = coverage.currency.currencyPK.currencyId + " " + minPP;
         if (minError != "") {
            return {
               code: minError,
               arguments: variables
            };
         }
      }
      if (totalPremium > maxPP) {
         variables[PublicConstants.MESSAGE_FIELD.AMOUNT] = coverage.currency.currencyPK.currencyId + " " + maxPP;
         if (maxError != "") {
            return {
               code: maxError,
               arguments: variables
            };
         }
      }
   }
});

var MPREPremiumValidationRule = Class.define({
   ruleName: function(args) {
      return "MPREPremiumValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.PROPOSAL;
   },
   validate: function(args) {
      var proposal = args["proposal"];
      var coverage = proposal.coverageInfo;
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');
      var schema = coverage.schema.call("getSchema", null);
      var catalog = coverage.catalog.call("getCatalog", null);

      var constantPaymentMode = "";
      var minError = "";
      var maxError = "";
      var issueAge = _PV( coverage, 'parties.party.insuredAge' ) ;
      var maxPremium = 0;
      var minPremium = 0;
      var totalPremium = 0;

      if (coverage.catalog.call("isUVL")) {
         if (!coverage.catalog.call("isRegularSaving")) {
            if (!coverage.catalog.call("isSinglePremium")) {

               var plannedPremium = _PV( coverage, ['plannedPremium'] );
               if ((Utils.isNothing(plannedPremium) || plannedPremium == 0) && !coverage.catalog.call("isSinglePremium")) {
                  return {
                     code: "ERRC0166",
                     arguments: {"%PRODUCT_ID%" : productId}
                  };
               } else {
                  var annualMPRE = 0;
                  var annualPP = 0;
                  if (coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.MONTHLY) {
                     annualPP = plannedPremium * 12;
                  }

                  for (totalPremiumsKey in coverage._premiums.totalPremiums) {
                     var totalPremiumsRec = coverage._premiums.totalPremiums[totalPremiumsKey];
                     if (totalPremiumsRec.paymentMode == coverage.options.paymentMode) {
                        if (coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.ANNUAL) {
                           annualMPRE = totalPremiumsRec.totalPremium;
                        }
                     }
                  }

                  if (!Utils.isNothing(coverage.proposal.riders)) {
                     var ridersTable = Utils.iterator(coverage.proposal.riders.coverageInfo);
                     for (riderIndex in ridersTable) {
                        var riderCov = coverage.proposal.riders.coverageInfo[riderIndex];
                        for (totalPremiumsKey in riderCov._premiums.totalPremiums) {
                           var totalPremiumsRec = riderCov._premiums.totalPremiums[totalPremiumsKey];
                           if (totalPremiumsRec.paymentMode == riderCov.options.paymentMode) {
                              if (riderCov.options.paymentMode == SchemaConstants.PAYMENT_MODE.ANNUAL) {
                                 annualMPRE += totalPremiumsRec.totalPremium;
                              }
                           }
                        }
                     }
                  }

                  var compropPremium = coverage.compropPremium;
                  if (plannedPremium < compropPremium || annualPP < annualMPRE) {
                     return {
                        code: "ERRC0167",
                        arguments: {"%PRODUCT_ID%" : productId}
                     };

                  }
               }

               var premiumRec = getPremiumRecord({
                  premiumType: SchemaConstants.PREMIUM_RANGE_TYPE.PREMIUMTYPE_PLANNEDPREMIUM,
                  coverage: coverage,
                  schema: schema
               });
               if (!Utils.isNothing(premiumRec)) {
                  maxPremium = Number( _PV( premiumRec , ['MaxPremium'] ) );
               }

               if (maxPremium > 0 && plannedPremium > maxPremium) {
                  return {
                     code: "ERRC0168",
                     arguments: {"%PRODUCT_ID%" : productId}
                  };

               }

            } else {
               //
               var billingMethod = coverage.options.billingMethod;
               if (billingMethod.equalsIgnoreCase(SchemaConstants.BILLING_MODE.SINGLE) && plannedPremium > 0) {
                  return {
                     code: "ERRC0169",
                     arguments: {"%PRODUCT_ID%" : productId}
                  };
               } else {
                  // HasValue Logic
                  var premiumRec = getPremiumRecord({
                     premiumType: SchemaConstants.PREMIUM_RANGE_TYPE.PREMIUMTYPE_PLANNEDPREMIUM,
                     coverage: coverage,
                     hasValue: "Y",
                     schema: schema
                  });
                  if (!Utils.isNothing(premiumRec)) {
                     minPremium = Number( _PV( premiumRec, ['MinPremium'] ) );
                     maxPremium = Number( _PV( premiumRec, ['MaxPremium'] ) );
                  }
                  /*
                  for (premiumRangeKey in schema.ProductSchema.BasicParticular.PremiumRangeInformation.PremiumRange) {
                      var premiumRangeRec = schema.ProductSchema.BasicParticular.PremiumRangeInformation.PremiumRange[premiumRangeKey];
                      if ((coverage.currency.currencyPK.currencyId == premiumRangeRec.CurrencyPK.CurrencyId.text) && premiumRangeRec.HasValue == "Y" && (premiumRangeRec.PaymentMode.text == coverage.options.paymentMode) && (premiumRangeRec.Type == SchemaConstants.PREMIUM_RANGE_TYPE.PREMIUMTYPE_PLANNEDPREMIUM)) {
                          minPremium = premiumRangeRec.MinPremium.text;
                          maxPremium = premiumRangeRec.MaxPremium.text;
                      }
                  }*/
                  var variables = {"%PRODUCT_ID%" : productId};
                  variables[PublicConstants.MESSAGE_FIELD.AGE] = issueAge;
                  if (plannedPremium > 0 && plannedPremium < minPremium) {
                     return {
                        code: "ERRC0170",
                        arguments: variables
                     };
                  }
                  if (plannedPremium > 0 && plannedPremium < maxPremium) {
                     return {
                        code: "ERRC0171",
                        arguments: variables
                     };
                  }
               }

            }
         }
      } else {
         if (maxPremium > 0) {
            return {
               code: "ERRC0169",
               arguments: {"%PRODUCT_ID%" : productId}
            };

         }
      }
   }
});

var FaceAmountLimitBySmokingStatusValidationRule = Class.define({
   ruleName: function(args) {
      return "FaceAmountLimitBySmokingStatusValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE | PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      var variables = {"%PRODUCT_ID%" : productId};

      var insureds = _PV( coverage, 'parties.party' );
      if (Utils.isNothing(insureds)) {
          return {
             code: "ERRC0213",
             arguments: {"%PRODUCT_ID%" : productId}
          };
      }
      var insuredList = Utils.iterator( insureds );
      for (insuredIdx in insuredList) {
         var insured = insuredList[insuredIdx];
         var insuredAge = insured.insuredAge;
         var smokingStatus = insured.smokingStatus;
         _DB("insuredAge("+ insuredIdx + ")", insuredAge);
         _DB("smokingStatus("+ insuredIdx + ")", smokingStatus);

         variables[PublicConstants.MESSAGE_FIELD.SMOKING_STATUS] = "" + smokingStatus;

         var currencyId = coverage.currency.currencyPK.currencyId;
         var basicFaceAmount = Utils.isNothing(coverage.faceAmount) ? 0 : Number(coverage.faceAmount);
         if (!Utils.isNothing(schema.ProductSchema.BasicParticular.SmokingStatusRequirement)) {
            var smokingStatusReqTable = Utils.iterator(schema.ProductSchema.BasicParticular.SmokingStatusRequirement);
            for (smokingStatusReqi in smokingStatusReqTable) {
               var smokingStatusReqRec = smokingStatusReqTable[smokingStatusReqi];
               if (smokingStatusReqRec.status.toLowerCase() == (smokingstatus).toLowerCase()) {
                  if (!Utils.isNothing(smokingStatusReqRec.Requirement)) {
                     var requirementTable = Utils.iterator(smokingStatusReqRec.Requirement);
                     for (requirementI in requirementTable) {
                        var requirementRec = requirementTable[requirementI];
                        if (requirementRec.status.toLowerCase() == (smokingstatus).toLowerCase()) {
                           if (!Utils.isNothing(requirementRec.MinFA)) {
                              if (requirementRec.MinFA.curr.toLowerCase() == (currencyId).toLowerCase()) {
                                 if (basicFaceAmount < Number(requirementRec.MinFA.text)) {
                                    return {
                                       code: "ERRC0172",
                                       arguments: {"%PRODUCT_ID%" : productId}
                                    };
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

   }
});


var JuvenileRiderTypeValidationRule = Class.define({
   ruleName: function(args) {
      return "JuvenileRiderTypeValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   getJuvenileAge: function(args) {
      var location = args['location'];
      if ((location == CatalogConstants.LOCATION.HONGKONG) || (location == CatalogConstants.LOCATION.MACAU) || (location == CatalogConstants.LOCATION.CHINA))
         return 15;
      else
      if (location == CatalogConstants.LOCATION.SINGAPORE)
         return 17;
      else
         return 15; // default
   },
   isEqualsParty: function(args) {
      var party1 = args['party1'];
      var party2 = args['party2'];

      //"type" : "BASIC",
      //"smokingStatus" : "NS",
      //"insuredSex" : "M",
      //"insuredId" : "PROJ, RIDER(ADD 800K)",
      //"insuredAge" : 29,
      //"birthDate" : "19890305070000"

      if (Utils.isNothing(party1) || Utils.isNothing(party2)) return false;
      var result = (party1.type == party2.type) &&
             (party1.smokingStatus == party2.smokingStatus) &&
             (party1.insuredSex == party2.insuredSex) &&
             (party1.insuredAge == party2.insuredAge) &&
             (party1.insuredId == party2.insuredId)
      return result;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var isJuvenile = true;
      var basicParty = null;
      /*var riderCatalog = coverage.catalog.call("getCatalog", null);
      var baseCatalog = coverage.catalog.call("getCatalog", null);*/

      var riderCatalog = coverage.catalog.call("getCatalog", null);
      var riderSchema = coverage.schema.call("getSchema", null);
      //console.log("Rider Product = " + riderCatalog.ProductCatalog.ProductPK.ProductId.text);

      var catalog = coverage.proposal.coverageInfo.catalog.call("getCatalog", null);
      var schema = coverage.proposal.coverageInfo.schema.call("getSchema", null);
      //console.log("Base Product = " + catalog.ProductCatalog.ProductPK.ProductId.text);

      var basePlan = coverage.proposal.coverageInfo;
      var location = _PV( basePlan, ['product','productKey','location'] );
      //var juvenileAge = getJuvenileAge({
      //   location: location
      //});
      var juvenileAge = this.self.getJuvenileAge({ location: location });

      //var partyTable = Utils.iterator(basePlan.parties.party);
      //for (partyi in partyTable) {
      //   basicParty = partyTable[partyi];
      //   if (basicParty.insuredAge <= juvenileAge) {
      //      isJuvenile = true;
      //   }
      //}

      var primary = getPrimaryInsured({coverage: basePlan});
      if (Utils.isNothing(primary)) {
         return {
            code: "ERRC0213",
            arguments: {"%PRODUCT_ID%" : productId}
         };
      }
      var primaryInsuredAge = primary.insuredAge;
      var primaryInsuredType = primary.type;
      var isJuvenile = (primaryInsuredAge <= juvenileAge);

      if (!coverage.catalog.call("isRiderProductType") && !coverage.catalog.call("isBenefit")) {
         // Error: It is not a rider.
         return {
            code: "ERRC0192",
            arguments: {"%PRODUCT_ID%" : productId}
         };
      }

      var insureds = _PV( coverage, 'parties.party' );
      if (Utils.isNothing(insureds)) {
          return {
             code: "ERRC0213",
             arguments: {"%PRODUCT_ID%" : productId}
          };
      }
      var insuredList = Utils.iterator( insureds );
      for (insuredIdx in insuredList) {
         var insured = insuredList[insuredIdx];
         var insuredAge = insured.insuredAge;
         var insuredType = insured.type;
         _DB("insuredAge("+ insuredIdx + ")", insuredAge);
         _DB("insuredType("+ insuredIdx + ")", insuredType);

         //To DO : Clarification on product type compare logic
         if (!isJuvenile && basePlan.catalog.call("isFamilyProductType")) {
            if (coverage.catalog.call("isSpouseProductType")) {
               if (insuredType != CatalogConstants.PRODUCT_TYPE.SPOUSE) {
                  // Error: This rider is for spouse insured.
                  return {
                     code: "ERRC0193",
                     arguments: {"%INSURED%" : insured, "%PRODUCT_ID%" : productId}
                  };
               }
            //} else {
               /*
               if (coverage.parties.party.type != CatalogConstants.PRODUCT_TYPE.SPOUSE) {
                  // Error: This rider is for spouse insured.
                  return {
                     code: "ERRC0193",
                     arguments: {}
                  };
               } else
               if (coverage.parties.party.type != CatalogConstants.PRODUCT_TYPE.CHILD) {
                  //message = "Error: This rider is for child insured.";
                  return {
                     code: "ERRC0194",
                     arguments: {}
                  };
               } else
               if (coverage.parties.party.type != CatalogConstants.PRODUCT_TYPE.BASIC) {
                  //message = "Error: This rider is for basic insured.";
                  return {
                     code: "ERRC0195",
                     arguments: {}
                  };
               }
               */
            }
         } else {
            if (!this.self.isEqualsParty( { party1: primary, party2: insured } )) {
               // Error: Rider insured must be same as basic insured.
               return {
                  code: "ERRC0196",
                  arguments: {"%INSURED%" : insured, "%PRODUCT_ID%" : productId}
               };
            }
         }
      }

   }
});

var IIOWithIPOValidationRule = Class.define({
   ruleName: function(args) {
      return "IIOWithIPOValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      if (coverage.catalog.call("isUVL")) {
         if (coverage.schema.call("hasSupportOption", {
               option: SchemaConstants.SUPPORT_OPTION.IPO
            })) {
            if ((!Utils.isNothing(coverage.options)) && !(Utils.isNothing(coverage.options.IPO)) && (coverage.options.IPO == "Y")) {
               if (!(Utils.isNothing(coverage.options.IIO)) && (coverage.options.IIO.equalsIgnoreCase(SchemaConstants.OPTION_IIO.OPTION_IIO_BASIC))) {
                  // Error: IPO is not supported due to Basic IIO selected.
                  return {
                     code: "ERRC0173",
                     arguments: {"%PRODUCT_ID%" : productId}
                  };
               }
            }
         }
      }
   }
});

var DBWithIPOValidationRule = Class.define({
   ruleName: function(args) {
      return "DBWithIPOValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      if (coverage.catalog.call("isUVL")) {
         if (coverage.schema.call("hasSupportOption", {
               option: SchemaConstants.SUPPORT_OPTION.IPO
            })) {
            if ((!Utils.isNothing(coverage.options)) && !(Utils.isNothing(coverage.options.IPO))) {
               var dbOption = coverage.options.dbLevel;
               if (coverage.options.IPO == "Y") {
                  if (!coverage.schema.call("isIpoDBOptionSupported", {
                        ipoDbOption: dbOption
                     })) {
                     // Error: IPO is not supported for DB option selected.
                     return {
                        code: "ERRC0174",
                        arguments: {"%PRODUCT_ID%" : productId}
                     };
                  }
               }
            }
         }
      }
   }
});


var RiderIPOValidationRule = Class.define({
   ruleName: function(args) {
      return "RiderIPOValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   getMinBasicLayerFA: function(args) {
      var coverage = args['coverage'];
      var schema = args['schema'];
      //Currency Filter - To Do Test
      if (Utils.isNothing(schema.ProductSchema.BasicParticular.MinInitialFAIPO)) {
         return 0;
      }
      var faIPOTable = Utils.iterator(schema.ProductSchema.BasicParticular.MinInitialFAIPO);
      for (recKey in faIPOTable) {
         var recInfo = faIPOTable[recKey];
         if (recInfo._currency != null && recInfo._currency == coverage.currency.currencyPK.currencyId) {
            return Number(recInfo.text);
         }
         return 0;
      }
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var catalog = coverage.catalog.call("getCatalog", null);
      var schema = coverage.schema.call("getSchema", null);

      if ((!Utils.isNothing(coverage.options)) && !(Utils.isNothing(coverage.options.IPO)) && (coverage.options.IPO == "Y")) {
         if (!coverage.schema.call("hasSupportOption", {
               option: SchemaConstants.SUPPORT_OPTION.IPO
            })) {
            return {
               //"IPO is not supported."
               code: "ERRC0033",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         } else {
            var insureds = _PV( coverage, 'parties.party' );
            if (Utils.isNothing(insureds)) {
                return {
                   code: "ERRC0213",
                   arguments: {"%PRODUCT_ID%" : productId}
                };
            }
            var insuredList = Utils.iterator( insureds );
            for (insuredIdx in insuredList) {
               var insured = insuredList[insuredIdx];
               var insuredAge = insured.insuredAge;
               _DB("insuredAge("+ insuredIdx + ")", insuredAge);

               var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
               variables[PublicConstants.MESSAGE_FIELD.MAXIMUM_BASIC_IPO_ISSUE_AGE] = "" + Number(schema.ProductSchema.BasicParticular.MaxBasicIPOIssueAge.text);
               variables[PublicConstants.MESSAGE_FIELD.MINIMUM_BASIC_IPO_ISSUE_AGE] = "" + Number(schema.ProductSchema.BasicParticular.MinBasicIPOIssueAge.text);
               if (insuredAge > Number(schema.ProductSchema.BasicParticular.MaxBasicIPOIssueAge.text)) {
                  // "IPO is not supported for age over " + psVO.getBasicParticular().getMaxBasicIPOIssueAge() + "."
                  return {
                     code: "ERRC0034",
                     arguments: variables
                  };
               } else if (insuredAge < Number(schema.ProductSchema.BasicParticular.MinBasicIPOIssueAge.text)) {
                  //"IPO is not supported for age below " + psVO.getBasicParticular().getMaxBasicIPOIssueAge() + ".
                  return {
                     code: "ERRC0034",
                     arguments: variables
                  };
               }
            }

            if (coverage.schema.call("hasExtraRating", {
                  coverage: coverage
               })) {
               return {
                  //IPO is not supported due to extra rating.
                  code: "ERRC0036",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            }

            var minBasicLayerFA = this.getMinBasicLayerFA({coverage: coverage, schema: schema});
            var basicFaceAmount = Utils.isNothing(coverage.faceAmount) ? 0 : Number(coverage.faceAmount);
            if (basicFaceAmount < minBasicLayerFA) {
               var variables = {"%PRODUCT_ID%" : productId};
               variables[PublicConstants.MESSAGE_FIELD.AMOUNT] = _V( _PV( coverage, 'currency.currencyPK.currencyId' ), "" ) + " " + minBasicLayerFA;
               //message = "Error: IPO cannot be selected if the initial face amount is less than $_"
               return {
                  code: "ERRC0175",
                  arguments: variables
               };
            }

         }
      }
   }
});

var IPOWithRidersValidationRule = Class.define({
   ruleName: function(args) {
      return "IPOWithRidersValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var catalog = coverage.catalog.call("getCatalog", null);
      var schema = coverage.schema.call("getSchema", null);

      if (!((!Utils.isNothing(coverage.options)) && !(Utils.isNothing(coverage.options.IPO)) && (coverage.options.IPO == "Y"))) {
         if (!Utils.isNothing(coverage.proposal.riders)) {
            var ridersTable = Utils.iterator(coverage.proposal.riders.coverageInfo);
            for (riderIndex in ridersTable) {
               var riderCov = coverage.proposal.riders.coverageInfo[riderIndex];
               var riderCatalog = riderCov.catalog.call("getCatalog", null);
               var riderSchema = riderCov.schema.call("getSchema", null);
               if ((!Utils.isNothing(riderCov.options)) && !(Utils.isNothing(riderCov.options.IPO)) && (riderCov.options.IPO == "Y")) {
                  if (!Utils.isNothing(riderSchema)) {
                     if (riderCov.schema.call("hasSupportOption", {
                           option: SchemaConstants.SUPPORT_OPTION.IPO
                        })) {

                        if (riderCov.schema.call("hasSupportOption", {
                              option: SchemaConstants.SUPPORT_OPTION.IPO_WITH_BASIC_IPO
                           })) {
                           var variables = {"%PRODUCT_ID%" : productId};
                           variables[PublicConstants.MESSAGE_FIELD.PRODUCT_ID] = riderCov.product.productKey.primaryProduct.productPK.productId;
                           return {
                              // 610 - Error: The basic plan IPO must be selected if $_ IPO is selected.
                              code: "ERRC0176",
                              arguments: variables
                           };
                        }
                     }

                  }
               }
            }
         }
      }
   }
});

var DeathBenefitValidationRule = Class.define({
   ruleName: function(args) {
      return "DeathBenefitValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE | PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);

      var dbLevel = _PV(coverage, ['options', 'dbLevel']);
      var dbOptions = _PV(schema, ['ProductSchema','BasicParticular','SupportDeathBenefit','DeathBenefit']);

      if (dbOptions && dbOptions.length > 0) {
         if (Utils.isNothing( _PV(coverage, ['options', 'dbLevel']) )) {
            return {
               // Error: The death benefit option is not set.
               code: "ERRC0078",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }

         if (!coverage.schema.call("isDBOptionSupported", {dbOption: coverage.options.dbLevel})) {
            if (coverage.options.dbLevel == SchemaConstants.DB_OPTION.LEVEL) {
               return {
                  //message = "Error: The level death benefit option is not supported.";
                  code: "ERRC0081",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            } else if (coverage.options.dbLevel == SchemaConstants.DB_OPTION.INCREASE) {
               return {
                  //message = "Error: The increase death benefit option is not supported.";
                  code: "ERRC0082",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            } else if (coverage.options.dbLevel == SchemaConstants.DB_OPTION.INDEXED) {
               return {
                  //message = "Error: The indexed death benefit option is not supported.";
                  code: "ERRC0083",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            } else if (coverage.options.dbLevel == SchemaConstants.DB_OPTION.ROP) {
               return {
                  //message = "Error: The ROP death benefit option is not supported.";
                  code: "ERRC0084",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            }
         }
      } else {
         if (!Utils.isNothing( _PV(coverage, ['options', 'dbLevel']) )) {
            return {
               code: "ERRC0211",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
      }
   }
});

var ExtraWithIPOValidationRule = Class.define({
   ruleName: function(args) {
      return "ExtraWithIPOValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      var basePlan = coverage.proposal.coverageInfo;
      //To DO Clarification of logic
      if ((!Utils.isNothing(basePlan.options)) && !(Utils.isNothing(basePlan.options.IPO)) && (basePlan.options.IPO == "Y")) {
         if (!coverage.schema.call("hasSupportOption", {
               option: SchemaConstants.SUPPORT_OPTION.EXTRA_RATING_WITH_IPO
            })) {
            if (coverage.schema.call("hasExtraRating", {
                  coverage: coverage
               })) {
               return {
                  //IPO is not supported due to extra rating.
                  code: "ERRC0036",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            }
         }
      }
   }
});

var SupportRCCValidationRule = Class.define({
   ruleName: function(args) {
      return "SupportRCCValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE | PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      var basePlan = coverage.proposal.coverageInfo;
      if (!coverage.schema.call("hasSupportOption", {
            option: SchemaConstants.SUPPORT_OPTION.RCC
         })) {

         _DB("rcc", coverage.rcc);

         if (!Utils.isNothing(coverage.rcc) && coverage.rcc == "Y") {
            return {
               //"Error: It does not support RCC."
               code: "ERRC0051",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
      }
   }
});

var NoOfPersonLimitsValidationRule = Class.define({
   ruleName: function(args) {
      return "NoOfPersonLimitsValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      var basePlan = coverage.proposal.coverageInfo;
      var personsLimit;
      var noOfPerson = 0;
      if (coverage.schema.call("hasSupportOption", {
            option: SchemaConstants.SUPPORT_OPTION.PERSONS_LIMIT
         })) {
         //if (!(Utils.isNothing(schema.ProductSchema.BasicParticular.SupportNoOfPersons))) {
         //   personsLimit = Number(schema.ProductSchema.BasicParticular.SupportNoOfPersons.PersonsLimit.text);
         //}
         personsLimit = Utils.iterator( _PV( schema, "ProductSchema.BasicParticular.SupportNoOfPersons.PersonsLimit" ) );
         if (personsLimit.length > 0) {
            //if ((!Utils.isNothing(coverage.otherOptions)) && !(Utils.isNothing(coverage.otherOptions.noOfPerson))) {
            //   noOfPerson = Number(coverage.otherOptions.noOfPerson);
            //}

            noOfPerson = _V( _PV( coverage, "otherOptions.noOfPerson" ), 0 );
            var maxPersonLimits = 0;
            for( personsLimitIdx in personsLimit ) {
               var personsLimitNo = _V( personsLimit[personsLimitIdx] );
               maxPersonLimits = Math.max( maxPersonLimits, Number(personsLimitNo) );
            }

            if ( noOfPerson > maxPersonLimits ) {
               var variables = {"%PRODUCT_ID%" : productId};
               variables[PublicConstants.MESSAGE_FIELD.COUNT] = "" + maxPersonLimits;
               return {
                  // Error: It does not support more than "+ limit +" person(s).
                  code: "ERRC0177",
                  arguments: variables
               };
            }
         }
      }
   }
});

var NoOfPersonValidationRule = Class.define({
   ruleName: function(args) {
      return "NoOfPersonValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      var basePlan = coverage.proposal.coverageInfo;
      var personsLimit = 0;
      var noOfPerson = 0;
      var found = false;
      var messageCode = null;
      // HP Series
      if (coverage.schema.call("hasSupportOption", {
            option: SchemaConstants.SUPPORT_OPTION.PERSONS_LIMIT
         })) {
         //personsLimit = Number( _V( _PV( schema, 'ProductSchema.BasicParticular.SupportNoOfPersons.PersonsLimit'), 0) );
         personsLimit = Utils.iterator( _PV( schema, 'ProductSchema.BasicParticular.SupportNoOfPersons.PersonsLimit') );
         if (personsLimit.length > 0) {
            //if ((!Utils.isNothing(coverage.otherOptions)) && !(Utils.isNothing(coverage.otherOptions.noOfPerson))) {
            //   noOfPerson = Number(coverage.otherOptions.noOfPerson);
            //}
            noOfPerson = _V( _PV(coverage, "otherOptions.noOfPerson") );
            var variables = {"%PRODUCT_ID%" : productId};
            //variables[PublicConstants.MESSAGE_FIELD.COUNT] = personsLimit;
            if (noOfPerson == Number(SchemaConstants.NO_OF_PERSONS.PERSONS_1)) {
               found = true;
               if (!coverage.schema.call("isNoOfPersonSupported", {
                     noOfPersonOption: SchemaConstants.NO_OF_PERSONS.PERSONS_1
                  })) {
                  //message = "Error: it does not support single person.";
                  messageCode = "ERRC0178";
               }
            }
            if (noOfPerson == Number(SchemaConstants.NO_OF_PERSONS.PERSONS_2)) {
               found = true;
               if (!coverage.schema.call("isNoOfPersonSupported", {
                     noOfPersonOption: SchemaConstants.NO_OF_PERSONS.PERSONS_2
                  })) {
                  //message = "Error: it does not support double person.";
                  messageCode = "ERRC0179";
               }
            }
            if (noOfPerson == Number(SchemaConstants.NO_OF_PERSONS.PERSONS_3)) {
               found = true;
               if (!coverage.schema.call("isNoOfPersonSupported", {
                     noOfPersonOption: SchemaConstants.NO_OF_PERSONS.PERSONS_3
                  })) {
                  //message = "Error: it does not support 3 person.";
                  messageCode = "ERRC0180";
               }
            }
            if (noOfPerson == Number(SchemaConstants.NO_OF_PERSONS.PERSONS_4)) {
               found = true;
               if (!coverage.schema.call("isNoOfPersonSupported", {
                     noOfPersonOption: SchemaConstants.NO_OF_PERSONS.PERSONS_4
                  })) {
                  //message = "Error: it does not support 4 person.";
                  messageCode = "ERRC0181";
               }
            }
            if (!found) {
               return {
                  // Error: No person number is entered.
                  code: "ERRC0182",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            } else {
               if (!(Utils.isNothing(messageCode))) {
                  return {
                     code: messageCode,
                     arguments: {"%PRODUCT_ID%" : productId}
                  };
               }
            }
         }

         /*
         if (found) {
            if (noOfPerson != Number(SchemaConstants.NO_OF_PERSONS.PERSONS_1) && basePlan.parties.party.insuredAge <= Number(InterfaceConstants.SINGLEPLAN_AGE)) {
               // Error: Only single plan (1 person) supports age 17 or below.
               return {

                  code: "ERRC0184",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            }
         } else {
            if ((noOfPerson == Number(SchemaConstants.NO_OF_PERSONS.PERSONS_1)) || (noOfPerson == Number(SchemaConstants.NO_OF_PERSONS.PERSONS_2)) || (noOfPerson == Number(SchemaConstants.NO_OF_PERSONS.PERSONS_3)) || (noOfPerson == Number(SchemaConstants.NO_OF_PERSONS.PERSONS_4))) {
               return {
                  //"Error: No. of person is not required."
                  code: "ERRC0183",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            }
         }
         */
      }
   }
});


var MajorMedicalValidationRule = Class.define({
   ruleName: function(args) {
      return "MajorMedicalValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      var basePlan = coverage.proposal.coverageInfo;
      var majorMedical = null;
      // HP Series
      if (coverage.schema.call("hasSupportOption", {
            option: SchemaConstants.SUPPORT_OPTION.PERSONS_LIMIT
         })) {
         //if ((!Utils.isNothing(coverage.otherOptions)) && !(Utils.isNothing(coverage.otherOptions.majorMedical))) {
         //   majorMedical = (coverage.otherOptions.majorMedical);
         //}
         majorMedical = _PV( coverage, "otherOptions.majorMedical" );
         var isMajorMedicalSupportedY = (coverage.schema.call("isMajorMedicalOptionSupported", {
            majorMedicalOption: SchemaConstants.MAJOR_MEDICAL.Y
         }));

         var isMajorMedicalSupportedN = (coverage.schema.call("isMajorMedicalOptionSupported", {
            majorMedicalOption: SchemaConstants.MAJOR_MEDICAL.N
         }));


         if (majorMedical == "Y" && !isMajorMedicalSupportedY) {
            return {
               //"Error: It does not support major medical."
               code: "ERRC0185",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
         if (majorMedical != "Y" && isMajorMedicalSupportedY) {
            if (!isMajorMedicalSupportedN) {
               return {
                  // Error: The major medical option is not set.
                  code: "ERRC0186",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            }
         }
      }
   }
});

var CoverageClassValidationRule = Class.define({
   ruleName: function(args) {
      return "CoverageClassValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      var basePlan = coverage.proposal.coverageInfo;
      var coverageClassSupported = null;
      var coverageClass = null;
      var found = false;
      var messageCode = null;
      // HP Series
      if (coverage.schema.call("hasSupportOption", {
            option: SchemaConstants.SUPPORT_OPTION.PERSONS_LIMIT
         })) {

         //if (!(Utils.isNothing(schema.ProductSchema.BasicParticular.SupportOtherOptions)) && !(Utils.isNothing(schema.ProductSchema.BasicParticular.CoverageClass))) {
         //   coverageClassSupported = (schema.ProductSchema.BasicParticular.SupportOtherOptions.CoverageClass.text);
         //}
         coverageClassSupported = Utils.iterator( _PV( schema, "ProductSchema.BasicParticular.SupportOtherOptions.CoverageClass" ) );
         if (!Utils.isNothing(coverageClassSupported)) {
            //if ((!Utils.isNothing(coverage.otherOptions)) && !(Utils.isNothing(coverage.otherOptions.coverageClass))) {
            //   coverageClass = (coverage.otherOptions.coverageClass);
            //}
            coverageClass = _V(_PV(coverage, "otherOptions.coverageClass"));
            if (coverageClass == (SchemaConstants.COVERAGE_CLASS.CLASS_A)) {
               found = true;
               if (!coverage.schema.call("isClassOtherOptionSupported", {
                     classOtherOption: SchemaConstants.COVERAGE_CLASS.CLASS_A
                  })) {
                  //message = "Error: Coverage Class A is not supported.";
                  messageCode = "ERRC0187";
               }
            }
            if (coverageClass == (SchemaConstants.COVERAGE_CLASS.CLASS_B)) {
               found = true;
               if (!coverage.schema.call("isClassOtherOptionSupported", {
                     classOtherOption: SchemaConstants.COVERAGE_CLASS.CLASS_B
                  })) {
                  //message = "Error: Coverage Class B is not supported.";
                  messageCode = "ERRC0188";
               }
            }
            if (coverageClass == (SchemaConstants.COVERAGE_CLASS.CLASS_C)) {
               found = true;
               if (!coverage.schema.call("isClassOtherOptionSupported", {
                     classOtherOption: SchemaConstants.COVERAGE_CLASS.CLASS_C
                  })) {
                  //message = "Error: Coverage Class C is not supported.";
                  messageCode = "ERRC0189";
               }
            }
            if (coverageClass == (SchemaConstants.COVERAGE_CLASS.CLASS_D)) {
               found = true;
               if (!coverage.schema.call("isClassOtherOptionSupported", {
                     classOtherOption: SchemaConstants.COVERAGE_CLASS.CLASS_D
                  })) {
                  //message = "Error: Coverage Class D is not supported.";
                  messageCode = "ERRC0190";
               }
            }

            if (!found) {
               return {
                  //"Error: No coverage class is set.";
                  code: "ERRC0191",
                  arguments: {"%PRODUCT_ID%" : productId}
               };
            } else {
               if (!(Utils.isNothing(messageCode))) {
                  return {
                     code: messageCode,
                     arguments: {"%PRODUCT_ID%" : productId}
                  };
               }
            }
         }
      }
   }
});


var NonHpSeriesValidationRule = Class.define({
   ruleName: function(args) {
      return "NonHpSeriesValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE | PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      var basePlan = coverage.proposal.coverageInfo;
      var coverageClassSupported = null;
      var coverageClass = null;
      var majorMedical = null;
      var found = false;
      var messageCode = null;
      var noOfPerson = 0;
      var partyLength = 0;
      // HP Series
      if (!coverage.schema.call("hasSupportOption", {
            option: SchemaConstants.SUPPORT_OPTION.PERSONS_LIMIT
         })) {
         if ((!Utils.isNothing(coverage.otherOptions)) && !(Utils.isNothing(coverage.otherOptions.coverageClass))) {
            coverageClass = (coverage.otherOptions.coverageClass);
         }
         if ((!Utils.isNothing(coverage.otherOptions)) && !(Utils.isNothing(coverage.otherOptions.noOfPerson))) {
            noOfPerson = Number(coverage.otherOptions.noOfPerson);
         }

         if (!Utils.isNothing(coverage.parties) && !Utils.isNothing(coverage.parties.party)) {
            var partyTable = Utils.iterator(coverage.parties.party);
            partyLength = partyTable.length;
         }

         if ((!Utils.isNothing(coverage.otherOptions)) && !(Utils.isNothing(coverage.otherOptions.majorMedical))) {
            majorMedical = (coverage.otherOptions.majorMedical);
         }

         if ((noOfPerson == Number(SchemaConstants.NO_OF_PERSONS.PERSONS_1)) || (noOfPerson == Number(SchemaConstants.NO_OF_PERSONS.PERSONS_2)) || (noOfPerson == Number(SchemaConstants.NO_OF_PERSONS.PERSONS_3)) || (noOfPerson == Number(SchemaConstants.NO_OF_PERSONS.PERSONS_4)) || partyLength > 1) {
            return {
               //"Error: It does not support multiple person."
               code: "ERRC0199",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }

         if (coverageClass == SchemaConstants.COVERAGE_CLASS.CLASS_A || coverageClass == SchemaConstants.COVERAGE_CLASS.CLASS_B || coverageClass == SchemaConstants.COVERAGE_CLASS.CLASS_C || coverageClass == SchemaConstants.COVERAGE_CLASS.CLASS_D) {
            return {
               //"Error: It does not support coverage class.";
               code: "ERRC0198",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
         if (majorMedical == SchemaConstants.MAJOR_MEDICAL.Y) {
            return {
               //"Error: It does not support major medical.";
               code: "ERRC0185",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
      }
   }
});

var BenefitOptionValidationRule = Class.define({
   ruleName: function(args) {
      return "BenefitOptionValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE | PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      var basePlan = coverage.proposal.coverageInfo;
      var baseSchema = coverage.proposal.coverageInfo.schema.call("getSchema", null);
      var baseCatalog = coverage.proposal.coverageInfo.catalog.call("getCatalog", null);
      var hasBenefitOptionSupport = coverage.schema.call("hasSupportOption", {
         option: SchemaConstants.SUPPORT_OPTION.HAS_BENEFIT_OPTION
      })

      if (Utils.isNothing(coverage.benefitOption)) {
         if (hasBenefitOptionSupport) {
            // Error: Benefit Option is missing.
            return {
               code: "ERRC0202",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
      } else {
         if (hasBenefitOptionSupport) {
            benefitOptionRec = getBenefitOptionRecord({
               coverage: coverage,
               schema: schema,
               benefitOptionType: coverage.benefitOption
            });
            if (benefitOptionRec != null) {
               var insureds = _PV( coverage, 'parties.party' );
               if (Utils.isNothing(insureds)) {
                   return {
                      code: "ERRC0213",
                      arguments: {"%PRODUCT_ID%" : productId}
                   };
               }
               var insuredList = Utils.iterator( insureds );
               for (insuredIdx in insuredList) {
                  var insured = insuredList[insuredIdx];
                  var age = insured.insuredAge;
                  _DB("insuredAge("+ insuredIdx + ")", age);

                  var basicFaceAmount = Utils.isNothing(coverage.faceAmount) ? 0 : Number(coverage.faceAmount);

                  var minFA = Number(benefitOption.minFaceAmount);
                  var maxFA = Number(benefitOption.maxFaceAmount);

                  var minAge = Number(benefitOption.minAge);
                  var maxAge = Number(benefitOption.maxAge);

                  if (!(age >= minAge && age <= maxAge)) {
                     // Error: Invalid insured age for ??. (name of Benefit Option)
                     var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                     // TO DO BenefitOption Name
                     variables[PublicConstants.MESSAGE_FIELD.BENEFIT_OPTION_NAME] = coverage.benefitOption;
                     return {
                        code: "ERRC0200",
                        arguments: variables
                     };
                  }

                  if (!(basicFaceAmount >= minFA && basicFaceAmount <= maxFA)) {
                     if (basicFaceAmount < minFA) {
                        //The face amount below the minimum for $_.
                        var msgCode = "ERRC0201"; //248
                        if (coverage.catalog.call("isRegularSaving") || coverage.catalog.call("isUVL")) {
                           //M325", "Error: The annualized Base Saving premium below the minimum for $_.
                           msgCode = "ERRC0203";
                        }
                        var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                        // TO DO BenefitOption Name
                        variables[PublicConstants.MESSAGE_FIELD.BENEFIT_OPTION_NAME] = coverage.benefitOption;
                        return {
                           code: msgCode,
                           arguments: variables
                        };
                     } else {
                        var msgCode = "ERRC0204"; //249
                        if (coverage.catalog.call("isRegularSaving") || coverage.catalog.call("isUVL")) {
                           //M326", "Error: The annualized Base Saving premium below the minimum for $_.
                           msgCode = "ERRC0205";
                        }
                        var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                        // TO DO BenefitOption Name
                        variables[PublicConstants.MESSAGE_FIELD.BENEFIT_OPTION_NAME] = coverage.benefitOption;
                        return {
                           code: msgCode,
                           arguments: variables
                        };
                     }
                  }
               }
            }
         } else {
            // Error: Benefit Option not matched with Base Plan.
            return {
               code: "ERRC0206",
               arguments: {"%PRODUCT_ID%" : productId}
            };
         }
      }
   }
});

var FundActivityValidationRule = Class.define({
   ruleName: function(args) {
      return "FundActivityValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   getMinSubscription: function(args) {
      var coverage = args['coverage'];

   },
   getMaxSubscription: function(args) {
      var coverage = args['coverage'];
      var currency = _PV( coverage, "currency.currencyPK.currencyId" );

      var maxSubcription = 0;
      var schema = coverage.schema.call("getSchema", {});
      var maxSubscriptionList = _PV(schema, "ProductSchema.BasicParticular.MaxSubscription");
      if (!Utils.isNothing(maxSubscriptionList)) {
         for(idx in maxSubscriptionList) {
            var maxSubcriptionItem = maxSubscriptionList[idx];
            var currencyCode = _PV(maxSubcriptionItem, "currency");
            if (currencyCode == currency) {
               var maxSubcript = _PV(maxSubcriptionItem, "text");
               if (!Utils.isNothing(maxSubcript)) {
                  maxSubcription = Math.max(Number(maxSubcript), maxSubcription);
                  break;
               }
            }
         }
      }

      /* new Max/Min Limits */
      var currencyBaseLimits = _PV(schema, "ProductSchema.CurrencyBaseLimits.CurrencyLimitRecord");
      if (!Utils.isNothing(currencyBaseLimits)) {
         for(idx in currencyBaseLimits) {
            var currencyBaseLimitItem = currencyBaseLimits[idx];
            var currencyCode = _PV(currencyBaseLimitItem, "CurrencyPK.CurrencyId");
            var nature = _PV(currencyBaseLimitItem, "LimitNature");
            if (currencyCode == currency && nature == "Max") {
               var maxSubcript = _PV(currencyBaseLimitItem, "TotalSubscription");
               if (!Utils.isNothing(maxSubcript)) {
                  maxSubcription = Math.max(Number(maxSubcript), maxSubcription);
                  break;
               }
            }
         }
      }
      return maxSubcription;
   },

   getLimits : function(args) {
      // duration is policy year.
      var coverage = args['coverage'];
      var duration = args['duration'];
      var changeType = args['changeType'];

      var schema = coverage.schema.call('getSchema');
      var fundChangeRec = getFundChangeRecord({
         fundChange: changeType,
         coverage: coverage,
         duration: duration,
         schema: schema
      });

      var minLimit = 0;
      var maxLimit = 999999999;
      if (!Utils.isNothing(fundChangeRec)) {
         minLimit = Number( _PV( fundChangeRec, "MinIncrease" ) );
         maxLimit = Number( _PV( fundChangeRec, "MaxIncrease" ) );
      }

      return {"minLimit":minLimit, "maxLimit":maxLimit};
   },

   getDumpinLimits : function(args) {
      return this.self.getLimits({
         coverage: args['coverage'],
         duration: args['duration'],
         changeType: SchemaConstants.FUND_CHANGE_TYPE.DUMPIN
      });
   },

   getWithdrawalLimits : function(args) {
      return this.self.getLimits({
         coverage: args['coverage'],
         duration: args['duration'],
         changeType: SchemaConstants.FUND_CHANGE_TYPE.WITHDRAWAL
      });
   },

   getPremiumLimits : function(args) {
      var coverage = args['coverage'];
      var premiumType = args['premiumType'];

      var schema = coverage.schema.call("getSchema");
      var premiumRec = getPremiumRecord({
         premiumType: premiumType,
         coverage: coverage,
         schema: schema
      });

      var minPremium = 0;
      var maxPremium = 999999999;
      if (!Utils.isNothing(premiumRec)) {
         minPremium = Number( _PV( premiumRec, 'MinPremium' ) );
         maxPremium = Number( _PV( premiumRec, 'MaxPremium' ) );
      }
      return {minPremium: minPremium, maxPremium: maxPremium};
   },

   getPlannedPremiumLimits : function(args) {
      return this.self.getPremiumLimits({
         premiumType: SchemaConstants.PREMIUM_RANGE_TYPE.PREMIUMTYPE_PLANNEDPREMIUM,
         coverage: args['coverage']
      });
   },

   getFaceAmountLimits : function(args) {
      var coverage = args['coverage'];
      var schema = coverage.schema.call("getSchema", null);
      var primary = getPrimaryInsured({coverage: coverage});
      if (Utils.isNothing(primary)) {
         return {
            code: "ERRC0213",
            arguments: {"%PRODUCT_ID%" : productId}
         };
      }
      var insuredAge = primary.insuredAge;

      var minFA = 99989898989898.999;
      var maxFA = -99989898989898.999;
      var result = {};
      for (bandKey in schema.ProductSchema.BandInformation.BandRecord) {
         var bandRec = schema.ProductSchema.BandInformation.BandRecord[bandKey];
         if (!Utils.isNothing(bandRec.CurrencyPK)) {
            //console.log("productRules.length = " + bandRec.CurrencyPK.CurrencyId.text);
            if (coverage.currency.currencyPK.currencyId == bandRec.CurrencyPK.CurrencyId.text) {
               if (insuredAge >= Number(bandRec.MinIssueAge.text) && insuredAge <= Number(bandRec.MaxIssueAge.text)) {
                  minFA = Math.min(minFA, Number(bandRec.MinFaceAmount.text));
                  maxFA = Math.max(maxFA, Number(bandRec.MaxFaceAmount.text));
               }
            }
         }
      }
      _DB("Min/Max", minFA+"/"+maxFA, "FundActivitiesValidationRule->");

      // Fund activity change limits
      var fundChangeLimit = getFundChangeRecord({
          coverage: coverage,
          fundChange: SchemaConstants.FUND_CHANGE_TYPE.FACEAMOUNT,
          duration: 0,
          schema: schema
      });
      if (!Utils.isNothing(fundChangeLimit)) {
          _DB("Fund change limt", fundChangeLimit, "FundActivitiesValidationRule->");
          if (!Utils.isNothing(fundChangeLimit.MinIncrease)) {
              minFA = Math.max( minFA, Number(_V(fundChangeLimit.MinIncrease)) );
          }
          if (!Utils.isNothing(fundChangeLimit.MaxIncrease)) {
              maxFA = Math.min( maxFA, Number(_V(fundChangeLimit.MaxIncrease)) );
          }
          _DB("Min/Max", minFA+"/"+maxFA, "FundActivitiesValidationRule->");
      }

      return {minLimit: minFA, maxLimit: maxFA};
   },

   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var schema = coverage.schema.call("getSchema", null);
      var productId = _PV( coverage, "product.productKey.primaryProduct.productPK.productId" );
      var catalog = coverage.catalog.call("getCatalog", null);
      if (!(coverage.catalog.call("isUVL"))) {
         return;
      }

      var isCurrencySupported = (coverage.schema.call("isCurrencySupported", {
         currencyId: _PV( coverage, "currency.currencyPK.currencyId" )
      }));

      var insured = getPrimaryInsured({coverage: coverage});
      if (Utils.isNothing(insured)) {
         return {
            code: "ERRC0213",
            arguments: {"%PRODUCT_ID%" : productId}
         };
      }
      var issueAge = insured.insuredAge;

      var maxSubscriptionHit = false;
      var totalSubscription = 0;
      var maxSubscription = this.self.getMaxSubscription({coverage : coverage});

      _DB("maxSubscription(" + productId + ")", maxSubscription);

      totalSubscription += coverage.initialDumpIn;
      if (totalSubscription > maxSubscription) {
         // 267 Error: The total subscription is over the maximum limit ($_ $_) at age $_.
         maxSubscriptionHit = true;
         var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
         variables[PublicConstants.MESSAGE_FIELD.AGE] = "" + issueAge;
         variables[PublicConstants.MESSAGE_FIELD.MAX_SUBSCRIPTION] = "" + maxSubscription;
         variables[PublicConstants.MESSAGE_FIELD.CURRENCY] = "" + coverage.currency.currencyPK.currencyId;
         return {
            code: "ERRC0125",
            arguments: variables
         };

      }

      var fundActivities = _PV( coverage, "proposal.fundActivities.fundActivity" );
      if (!Utils.isNothing(fundActivities)) {
         var fundActivityTable = Utils.iterator(fundActivities);
         if (fundActivityTable.length > 0 && isCurrencySupported == true) {
            var maxDumpInAge = schema.ProductSchema.BasicParticular.MaxDumpInAge;
            var maxDumpInDuration = schema.ProductSchema.BasicParticular.MaxDumpInDuration;
            if (issueAge + maxDumpInDuration < maxDumpInAge) {
               maxDumpInAge = issueAge + maxDumpInDuration;
            }
            var premiumPayingPeriod = (coverage.schema.call("calculatePremiumPayingPeriod", {
               coverage: coverage
            }))

            _DB("maxDumpInAge", maxDumpInAge);
            _DB("premiumPayingPeriod", premiumPayingPeriod);

            for (fundActivityi in fundActivityTable) {
               var fundActivityRec = fundActivityTable[fundActivityi];
               var duration = (fundActivityRec.attainAge) - issueAge;
               var maxPP = 0;
               var minPP = 99999999999;
               var minDumpin = 0;
               var maxDumpin = 0;
               var minFA = 0;
               var maxFA = 99999999999;
               var minWd = 0;
               var maxWd = 99999999999;

               var faLimits = this.self.getFaceAmountLimits({
                  coverage: coverage,
                  duration: duration,
               });
               minFA = faLimits.minLimit;
               maxFA = faLimits.maxLimit;

               /*
               var fundChangeRec = getFundChangeRecord({
                  fundChange: SchemaConstants.FUND_CHANGE_TYPE.DUMPIN,
                  coverage: coverage,
                  duration: duration,
                  schema: schema
               });
               if (!Utils.isNothing(fundChangeRec)) {
                  minDumpin = Number( _PV( fundChangeRec, "MinIncrease" ) );
                  maxDumpin = Number( _PV( fundChangeRec, "MaxIncrease" ) );
               }
               */
               var limits = this.self.getDumpinLimits({
                  coverage: coverage,
                  duration: duration,
               });
               minDumpin = limits.minLimit;
               maxDumpin = limits.maxLimit;

               _DB("minDumpin(" + productId + ")", minDumpin);
               _DB("maxDumpin(" + productId + ")", maxDumpin);

               /*
               if (coverage.catalog.call("isSinglePremium") && coverage.catalog.call("isUVL")) {
                  // HasValue Logic
                  var premiumRec = getPremiumRecord({
                     premiumType: SchemaConstants.PREMIUM_RANGE_TYPE.PREMIUMTYPE_PLANNEDPREMIUM,
                     coverage: coverage,
                     hasValue: "Y",
                     schema: schema
                  });
                  if (!Utils.isNothing(premiumRec)) {
                     minPP = Number(premiumRec.MinPremium.text);
                     maxPP = Number(premiumRec.MaxPremium.text);
                  }
               } else {
                  var premiumRec = getPremiumRecord({
                     premiumType: SchemaConstants.PREMIUM_RANGE_TYPE.PREMIUMTYPE_PLANNEDPREMIUM,
                     coverage: coverage,
                     schema: schema
                  });
                  if (!Utils.isNothing(premiumRec)) {
                     minPP = Number(premiumRec.MinPremium.text);
                     maxPP = Number(premiumRec.MaxPremium.text);
                  }
               }
               */

               /*
               var premiumRec = getPremiumRecord({
                  premiumType: SchemaConstants.PREMIUM_RANGE_TYPE.PREMIUMTYPE_PLANNEDPREMIUM,
                  coverage: coverage,
                  schema: schema
               });
               if (!Utils.isNothing(premiumRec)) {
                  minPP = Number( _PV( premiumRec, 'MinPremium' ) );
                  maxPP = Number( _PV( premiumRec, 'MaxPremium' ) );
               }
               */
               limits = this.self.getPlannedPremiumLimits({ coverage: coverage });
               minPP = limits.minPremium;
               maxPP = limits.maxPremium;

               _DB("minPP(" + productId + ")", minPP);
               _DB("maxPP(" + productId + ")", maxPP);

               /*
               for (bandKey in schema.ProductSchema.BandInformation.BandRecord) {
                  var bandRec = schema.ProductSchema.BandInformation.BandRecord[bandKey];
                  if (!Utils.isNothing(bandRec.CurrencyPK)) {
                     console.log("productRules.length = " + bandRec.CurrencyPK.CurrencyId.text);
                  }
               }
               */

               /*
               var fundChangeWithdrawalRec = getFundChangeRecord({
                  fundChange: SchemaConstants.FUND_CHANGE_TYPE.WITHDRAWAL,
                  coverage: coverage,
                  duration: duration,
                  schema: schema
               });
               if (!Utils.isNothing(fundChangeWithdrawalRec)) {
                  minWd = Number( _PV( fundChangeWithdrawalRec, "MinIncrease" ) );
                  maxWd = Number( _PV( fundChangeWithdrawalRec, "MaxIncrease" ) );
               }
               */

               limits = this.self.getWithdrawalLimits({
                  coverage: coverage,
                  duration: duration,
               });
               minWd = limits.minLimit;
               maxWd = limits.maxLimit;

               _DB("minWd(" + productId + ")", minWd);
               _DB("maxWd(" + productId + ")", maxWd);

               if (issueAge > fundActivityRec.attainAge) {
                  // Error: The attain age ($_) is below issue age.
                  var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                  variables[PublicConstants.MESSAGE_FIELD.AGE] = "" + fundActivityRec.attainAge;
                  return {
                     code: "ERRC0126",
                     arguments: variables
                  };
               } else {
                  if (fundActivityRec.plannedPremium >= 0) {
                     if (coverage.catalog.call("isSinglePremium") && coverage.catalog.call("isUVL")) {
                        if ((fundActivityRec.plannedPremium > 0) && (maxPP < fundActivityRec.plannedPremium)) {
                           //message = "Error: Regular Subscription (Planned Premium) is above the maximal requirement at age "+ fc.getAttainAge();
                           var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                           variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                           variables["%MAXIMUM_LIMIT%"] = "" + maxPP;
                           variables["%POLICY_YEAR%"] = "" + (fundActivityRec.attainAge - issueAge + 1);
                           return {
                              code: "ERRC0127",
                              arguments: variables
                           };
                        }
                        if ((fundActivityRec.plannedPremium > 0) && (minPP > fundActivityRec.plannedPremium)) {
                           //message = Error: Regular Subscription (Planned Premium) is below the minimal requirement at age "+ fc.getAttainAge();
                           var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                           variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                           variables["%MAXIMUM_LIMIT%"] = "" + maxPP;
                           variables["%POLICY_YEAR%"] = "" + (fundActivityRec.attainAge - issueAge + 1);
                           return {
                              code: "ERRC0128",
                              arguments: variables
                           };
                        }
                     } else {
                        if (maxPP < fundActivityRec.plannedPremium) {
                           if (coverage.catalog.call("isRegularSaving") || coverage.catalog.call("isUVL")) {
                              //M327", "Error: New Base Saving premium is above the maximum at age $_.
                              var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                              variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                              variables["%MAXIMUM_LIMIT%"] = "" + maxPP;
                              variables["%POLICY_YEAR%"] = "" + (fundActivityRec.attainAge - issueAge + 1);
                              return {
                                 code: "ERRC0129",
                                 arguments: variables
                              };
                           } else {
                              //message = "Error: New Planned Premium is above the maximum at age "+ fc.getAttainAge();
                              var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                              variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                              variables["%MAXIMUM_LIMIT%"] = "" + maxPP;
                              variables["%POLICY_YEAR%"] = "" + (fundActivityRec.attainAge - issueAge + 1);
                              return {
                                 code: "ERRC0130",
                                 arguments: variables
                              };
                           }
                        }
                        if (minPP > fundActivityRec.plannedPremium) {
                           if (coverage.catalog.call("isRegularSaving") || coverage.catalog.call("isUVL")) {
                              //M327", "Error: New Base Saving premium is above the maximum at age $_.
                              var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                              variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                              variables["%MINIMUM_LIMIT%"] = "" + minPP;
                              variables["%POLICY_YEAR%"] = "" + (fundActivityRec.attainAge - issueAge + 1);
                              return {
                                 code: "ERRC0215",
                                 arguments: variables
                              };
                           } else {
                              //message = "Error: New Planned Premium is above the maximum at age "+ fc.getAttainAge();
                              var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                              variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                              variables["%MINIMUM_LIMIT%"] = "" + minPP;
                              variables["%POLICY_YEAR%"] = "" + (fundActivityRec.attainAge - issueAge + 1);
                              return {
                                 code: "ERRC0216",
                                 arguments: variables
                              };
                           }
                        }
                     }
                  } else {
                     if (issueAge > fundActivityRec.attainAge) {
                        //message = "Error: The attain age (" + fc.getAttainAge()+") must be above issue age.";
                        var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                        variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                        return {
                           code: "ERRC0131",
                           arguments: variables
                        };
                     }
                  }

                  var dumpinAmount = 0;
                  if (fundActivityRec.dumpin) dumpinAmount += Number( fundActivityRec.dumpin );
                  if (fundActivityRec.topupPremium) dumpinAmount += Number( fundActivityRec.topupPremium);

                  var withdrawal = 0;
                  if (fundActivityRec.withdrawal) withdrawal = Number( fundActivityRec.withdrawal );

                  var doGWA = _PV( fundActivityRec, 'doGWA');

                  _DB("dumpinAmount",dumpinAmount)
                  _DB("withdrawal",withdrawal)
                  _DB("doGWA",doGWA)

                  if (coverage.iflAge) {
                      var startIFLAge = -1;
                      if (coverage.iflAge != null || coverage.iflAge != 0)
                         startIFLAge = coverage.iflAge;

                      if ((startIFLAge >= 0) && (startIFLAge <= fundActivityRec.attainAge)) {
                         //Error: Subsequent subscription cannot be done after IFL payment started.
                         return {
                            code: "ERRC0132",
                            arguments: {"%INSURED%" : insured, "%PRODUCT_ID%" : productId}
                         };
                      }
                  }

                  if (dumpinAmount > 0) {
                     if (minDumpin > dumpinAmount) {
                        if (coverage.catalog.call("isRegularSaving") || coverage.catalog.call("isUVL")) {
                           //M321", "Error: Top-up premium is below minimum at age $_.
                           var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                           variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                           variables["%MINIMUM_LIMIT%"] = "" + minDumpin;
                           variables["%POLICY_YEAR%"] = "" + (fundActivityRec.attainAge - issueAge + 1);
                           return {
                              code: "ERRC0133",
                              arguments: variables
                           };
                        } else {
                           //message 218, Top-up Premium is below minimum at age $_.
                           var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                           variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                           variables["%MINIMUM_LIMIT%"] = "" + minDumpin;
                           variables["%POLICY_YEAR%"] = "" + (fundActivityRec.attainAge - issueAge + 1);
                           return {
                              code: "ERRC0134",
                              arguments: variables
                           };
                        }

                     } else {
                        if (maxDumpin < dumpinAmount) {
                           if (coverage.catalog.call("isRegularSaving") || coverage.catalog.call("isUVL")) {
                              //M322", "Error: Top-up premium is above maximum at age $_.
                              var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                              variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                              variables["%MAXIMUM_LIMIT%"] = "" + maxDumpin;
                              variables["%POLICY_YEAR%"] = "" + (fundActivityRec.attainAge - issueAge + 1);
                              return {
                                 code: "ERRC0135",
                                 arguments: variables
                              };
                           } else {
                              //message 219,"Error: Top-up premium is above maximum at age $_.
                              var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                              variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                              variables["%MAXIMUM_LIMIT%"] = "" + maxDumpin;
                              variables["%POLICY_YEAR%"] = "" + (fundActivityRec.attainAge - issueAge + 1);
                              return {
                                 code: "ERRC0136",
                                 arguments: variables
                              };
                           }
                        }
                     }

                     if (issueAge < fundActivityRec.attainAge) {
                        if ((withdrawal > 0 && dumpinAmount > 0) || (withdrawal > 0 && doGWA)) {
                           var msgCode = "";
                           var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                           variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                           if (coverage.catalog.call("isRegularSaving") || coverage.catalog.call("isUVL")) {
                              //M323", "Error: Top-up premium and Withdrawal cannot be applied at the same time at age $_.
                              msgCode = "ERRC0137";
                           }
                           if (coverage.catalog.call("isSinglePremium") || coverage.catalog.call("isUVL")) {
                               if (((fundActivityRec.doGWA != null) && fundActivityRec.doGWA)) {
                                  //Error: Subsequent subscription and GWA payment cannot be applied at the same time at age $_.
                                  msgCode = "ERRC0138";
                               }
                               if (withdrawal > 0 && doGWA) {
                                  //Error: Subsequent subscription, GWA payment and withdrawal cannot be applied at the same time at age $_.
                                  msgCode = "ERRC0139";
                               }
                           }
                           if (msgCode == "") {
                              //"Error: Dump-in and Withdrawal cannot be applied at the same time at age " + fundActivityRec.gttainAge();
                              msgCode = "ERRC0140";
                           }
                           return {
                              code: msgCode,
                              arguments: variables
                           };
                        }
                     }

                     // 20061026 2007JanRelease
                     totalSubscription += dumpinAmount;
                     if (totalSubscription > maxSubscription) {
                        // 267 Error: The total subscription is over the maximum limit ($_ $_) at age $_.
                        maxSubscriptionHit = true;
                        var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                        variables[PublicConstants.MESSAGE_FIELD.AGE] = issueAge;
                        variables[PublicConstants.MESSAGE_FIELD.MAX_SUBSCRIPTION] = maxSubscription;
                        variables[PublicConstants.MESSAGE_FIELD.CURRENCY] = coverage.currency.currencyPK.currencyId;
                        return {
                           code: "ERRC0125",
                           arguments: variables
                        };

                     }

                     if (dumpinAmount > 0 && ((fundActivityRec.attainAge > maxDumpInAge) || (fundActivityRec.attainAge >= issueAge + premiumPayingPeriod))) {
                        // 271 Error: No subscription is allowed after the age $_.
                        var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                        var maxAge = 999;
                        if (!Utils.isNothing(maxDumpInAge)) {
                           maxAge = Math.min(maxAge, Number(maxDumpInAge));
                        }
                        if (!Utils.isNothing(premiumPayingPeriod)) {
                           maxAge = Math.min(maxAge, Number(issueAge + premiumPayingPeriod));
                        }
                        variables[PublicConstants.MESSAGE_FIELD.AGE] = "" + maxAge;
                        return {
                           code: "ERRC0142",
                           arguments: variables
                        };
                     }

                     // 20071210
                     if (coverage.stopPaymentAfterYear > 0) {
                        // <<20160919
                        var hasExtendPremiumPaidPeriod = (coverage.schema.call("hasSupportOption", {
                           option: SchemaConstants.SUPPORT_OPTION.EXTEND_PREMIUM_PAID_PERIOD
                        }));
                        if (hasExtendPremiumPaidPeriod) {
                           if (coverage.stopPaymentAfterYear <= premiumPayingPeriod) {
                              if (dumpinAmount > 0 && fundActivityRec.attainAge >= issueAge + coverage.stopPaymentAfterYear) {
                                 // 284 Warning: The top-up selected during premium deferment period won't be considered in the Premium Deferment Page
                                 var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                                 variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                                 return {
                                    code: "ERRC0143",
                                    arguments: variables
                                 };
                              }
                           }

                        }

                        /*
                        else if (fundActivityRec.attainAge >= issueAge + coverage.stopPaymentAfterYear) {
                           var msgCode = "ERRC0143";
                           if ("|LU105|LU205|LU110|LU210|105LU|205LU|110LU|210LU".indexOf("|" + coverage.product.productKey.primaryProduct.productPK.productId) >= 0) {
                              msgCode = "ERRC0144";
                           }
                           // 284 Warning: The top-up selected during premium deferment period won't be considered in the Premium Deferment Page
                           var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                           variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                           return {
                              code: msgCode,
                              arguments: variables
                           };
                        }
                        */
                     }

                  }

                  if (fundActivityRec.faceAmount > 0) {
                     if (issueAge == fundActivityRec.attainAge) {
                        // Error: New Face Amount is not allowed at issue age.
                        return {
                           code: "ERRC0145",
                           arguments: {"%INSURED%" : insured, "%PRODUCT_ID%" : productId}
                        };
                     } else {
                        if (minFA > fundActivityRec.faceAmount) {
                           //message = "Error: New Face Amount is below the minimum at age "+ fc.getAttainAge();
                           var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                           variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                           variables["%MINIMUM_LIMIT%"] = "" + minFA;
                           variables["%POLICY_YEAR%"] = "" + (fundActivityRec.attainAge - issueAge + 1);
                           return {
                              code: "ERRC0146",
                              arguments: variables
                           };
                        }
                        if (maxFA < fundActivityRec.faceAmount) {
                           //message = "Error: New Face Amount is over the maximum at age "+ fc.getAttainAge();
                           var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                           variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                           variables["%MAXIMUM_LIMIT%"] = "" + maxFA;
                           variables["%POLICY_YEAR%"] = "" + (fundActivityRec.attainAge - issueAge + 1);
                           return {
                              code: "ERRC0217",
                              arguments: variables
                           };
                        }
                     }
                  }

                  if (withdrawal > 0) {
                     if (!(coverage.catalog.call("isSinglePremium") || coverage.catalog.call("isUVL")) &&
                        issueAge == fundActivityRec.attainAge) {
                        // Error: Withdrawal is not allowed at issue age.
                        return {
                           code: "ERRC0147",
                           arguments: {"%INSURED%" : insured, "%PRODUCT_ID%" : productId}
                        };
                     } else {
                        if (schema.ProductSchema.BasicParticular.CommitmentPeriod != null &&
                           fundActivityRec.attainAge - issueAge < schema.ProductSchema.BasicParticular.CommitmentPeriod) {
                           // 276 Error: Withdrawal before the end of minimum Premium Payment Period ($_) is not illustrated.
                           var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                           variables[PublicConstants.MESSAGE_FIELD.YEAR] = schema.ProductSchema.BasicParticular.CommitmentPeriod + " years";
                           return {
                              code: "ERRC0148",
                              arguments: variables
                           };
                        } else {
                           if (minWd > withdrawal) {
                              /*
                              if ("|US799|US899|US999|US000".indexOf("|" + coverage.product.productKey.primaryProduct.productPK.productId) >= 0) {
                                 //message = "Error: Withdrawal is below the minimum PHP$_ at age $_;
                                 var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                                 variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                                 variables[PublicConstants.MESSAGE_FIELD.AMOUNT] = minWd;
                                 return {
                                    code: "ERRC0149",
                                    arguments: variables
                                 };
                              }
                              else {
                              */

                             //228
                             var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                             variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                             variables["%MINIMUM_LIMIT%"] = "" + minWd;
                             variables["%POLICY_YEAR%"] = "" + (fundActivityRec.attainAge - issueAge + 1);
                             return {
                                code: "ERRC0150",
                                arguments: variables
                             };

                              //}
                           }
                           if (maxWd < withdrawal) {
                              /*
                              if ("|US799|US899|US999|US000".indexOf("|" + coverage.product.productKey.primaryProduct.productPK.productId) >= 0) {
                                 //message = "Error: Withdrawal is below the minimum PHP$_ at age $_;
                                 var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                                 variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                                 variables[PublicConstants.MESSAGE_FIELD.AMOUNT] = minWd;
                                 return {
                                    code: "ERRC0149",
                                    arguments: variables
                                 };
                              }
                              else {
                              */

                             //228
                             var variables = {"%INSURED%" : insured, "%PRODUCT_ID%" : productId};
                             variables[PublicConstants.MESSAGE_FIELD.AGE] = fundActivityRec.attainAge;
                             variables["%MAXIMUM_LIMIT%"] = "" + maxWd;
                             variables["%POLICY_YEAR%"] = "" + (fundActivityRec.attainAge - issueAge + 1);
                             return {
                                code: "ERRC0150",
                                arguments: variables
                             };

                              //}
                           }
                        }
                     }
                     // 20061026 2007JanRelease
                     totalSubscription -= fundActivityRec.withdrawal;
                     //
                  }

               }
            }
         }
      }
   }
});

var RegularPremiumValidationRule = Class.define({
    ruleName: function(args) {
        return "RegularPremiumValidationRule";
    },
    shouldTest: function(args) {
        return true;
    },
    scope: function(args) {
        return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
    },
    validate: function(args) {
        var coverage = args["coverage"];
        var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

        var schema = coverage.schema.call("getSchema", null);
        var catalog = coverage.catalog.call("getCatalog", null);
        var regularBasicPrem = 0;
        var minPremium = 999999999;
        var maxPremium = 0;
        if (coverage.catalog.call("isRegularSaving")) {
            regularBasicPrem = coverage.plannedPremium;
            if (!Utils.isNothing(regularBasicPrem) && regularBasicPrem > 0) {
                var premiumRec = getPremiumRecord({
                    premiumType: SchemaConstants.PREMIUM_RANGE_TYPE.PREMIUMTYPE_PLANNEDPREMIUM,
                    coverage: coverage,
                    schema: schema
                });
                if (Utils.isNothing(premiumRec)) {
                    premiumRec = getPremiumRecord({
                        premiumType: SchemaConstants.PREMIUM_RANGE_TYPE.PREMIUMTYPE_BASICPREMIUM,
                        coverage: coverage,
                        schema: schema
                    });
                }

                _DB("premiumRec", premiumRec);

                if (!Utils.isNothing(premiumRec)) {
                    minPremium = Math.min(minPremium, Number(_PV(premiumRec, ['MinPremium'])));
                    maxPremium = Math.max(maxPremium, Number(_PV(premiumRec, ['MaxPremium'])));
                }

                _DB("minPremium (Regular)", minPremium, "RegularPremiumValidationRule.validate()->");
                _DB("maxPremium (Regular)", maxPremium, "RegularPremiumValidationRule.validate()->");

                if (regularBasicPrem < minPremium) {
                    // Error: Regular Basic Premium is below the minimum.
                    var vars = {"%PRODUCT_ID%" : productId};
                    vars[PublicConstants.MESSAGE_FIELD.MINIMUM_PLANNEDPREMIUM] = minPremium;
                    return {
                        code: "ERRC0116",
                        arguments: vars
                    };
                } else
                if (regularBasicPrem > maxPremium) {
                    // Error: Regular Basic Premium is above the maximum.
                    var vars = {"%PRODUCT_ID%" : productId};
                    vars[PublicConstants.MESSAGE_FIELD.MAXIMUM_PLANNEDPREMIUM] = minPremium;
                    return {
                        code: "ERRC0117",
                        arguments: vars
                    };
                }
            } else {
                // Error: Regular Basic Premium is not set.
                return {
                    code: "ERRC0118",
                    arguments: {"%PRODUCT_ID%" : productId}
                };
            }
        }
    }
});

var IsBasePlanValidationRule = Class.define({
   ruleName: function(args) {
      return "IsBasePlanValidationRule";
   },
   shouldTest: function(args) {
      return true;
   },
   scope: function(args) {
      return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
   },
   validate: function(args) {
      var coverage = args["coverage"];
      var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');

      var catalog = coverage.catalog.call("getCatalog", null);

      var isBasePlan = false;
      var productTypes = _PV( catalog, ['ProductCatalog', 'ProductType', 'ProductTypeCode'] );
      if (productTypes) {
          isBasePlan = Utils.matchInList(CatalogConstants.PRODUCT_TYPE.BASEPLAN, productTypes, false);
      }

      if (!isBasePlan) {
          var variables = {"%PRODUCT_ID%" : productId};
          var productId =  _PV( coverage, 'product.productKey.primaryProduct.productPK.productId' );
          variables[PublicConstants.MESSAGE_FIELD.PLAN_CODE] = productId;
          //Error: {%PLAN_CODE%} is not base plan.
          return {
            code: "ERRC0212",
            arguments: variables
          };
      }
   }
});

var ProductDependencyValidationRule = Class.define({
    ruleName: function(args) {
        return "ProductDependencyValidationRule";
    },
    shouldTest: function(args) {
        var coverage = args["coverage"];
        var deps = this.self.dependencyRules(args);
        return (deps.length > 0);
    },
    scope: function(args) {
        var s = 0;
        s = s + PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
        s = s + PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
        return s;
    },
    validate: function(args) {
        var coverage = args["coverage"];
        var riderList = Utils.iterator(_V(_PV(coverage, "proposal.riders.coverageInfo"), []));

        var insured = getPrimaryInsured({
            coverage: coverage
        });

        var variables = {};
        variables["%SUB_PRODUCT_ID%"] = _PV(coverage, "product.productKey.primaryProduct.productPK.productId");
        variables["%INSURED%"] = insured;

        var inclusions = this.self.inclusiveProductList(args);
        for (inclusionIdx in inclusions) {
            var inclusion = inclusions[inclusionIdx];
            variables["%OBJ_PRODUCT_ID%"] = _V(inclusion.ProductPK);

            var found = false;
            for (riderIdx in riderList) {
                var rider = riderList[riderIdx];

                found = this.self.matchDepRule({
                    rule: inclusion,
                    subject: coverage,
                    object: rider
                });
                if (found) break;
            }

            if (!found) {
                return {
                    code: "ERRC0220",
                    arguments: variables
                };
            }

        }

        var exclusions = this.self.exclusiveProductList(args);
        for (exclusionIdx in exclusions) {
            var exclusion = exclusions[exclusionIdx];
            variables["%OBJ_PRODUCT_ID%"] = _V(exclusion.ProductPK);

            var found = false;
            for (riderIdx in riderList) {
                var rider = riderList[riderIdx];

                found = this.self.matchDepRule({
                    rule: exclusion,
                    subject: coverage,
                    object: rider
                });
                if (found) break;
            }

            if (found) {
                return {
                    code: "ERRC0219",
                    arguments: variables
                };
            }

        }

        return;
    },
    isInclusiveDependencyRule: function(args) {
        var rule = args["rule"];
        var ruleType = _V(rule.Type);
        return ((ruleType == SchemaConstants.DEPENDENCY_RULE_TYPE.INCLUSIVE) || (ruleType == SchemaConstants.DEPENDENCY_RULE_TYPE.INCLUSION));
    },
    isEqualsParty: function(args) {
        var party1 = args['party1'];
        var party2 = args['party2'];

        if (Utils.isNothing(party1) || Utils.isNothing(party2)) return false;
        var result = (party1.type == party2.type) &&
            (party1.smokingStatus == party2.smokingStatus) &&
            (party1.insuredSex == party2.insuredSex) &&
            (party1.insuredAge == party2.insuredAge) &&
            (party1.insuredId == party2.insuredId)
        return result;
    },
    matchDepRule: function(args) {
        var rule = args['rule'];
        var subCov = args['subject'];
        var objCov = args['object'];

        // subject rider
        var subInsured = getPrimaryInsured({
            coverage: subCov
        });

        // object rider
        var objInsured = getPrimaryInsured({
            coverage: objCov
        });
        var objProductId = _PV(objCov, "product.productKey.primaryProduct.productPK.productId");

        // rule information
        var ruleProductId = _V(rule.ProductPK);
        var currency = _V(rule.DepCurrencyCode);
        var minAge = Number(_V(rule.MinAge, 0));
        var maxAge = Number(_V(rule.MaxAge, 99999));
        var minFA = Number(_V(_PV(rule, "FaceAmountRange.MinFaceAmount"), 0));
        var maxFA = Number(_V(_PV(rule, "FaceAmountRange.MaxFaceAmount"), 999999999));

        // rule checking
        var meet = true;
        meet = meet && (ruleProductId == objProductId);
        if (!meet) return false;
        meet = meet && this.self.isEqualsParty({
            party1: subInsured,
            party2: objInsured
        });
        if (!meet) return false;
        meet = meet && (currency == _PV(objCov, "currency.currencyPK.currencyId"));
        if (!meet) return false;
        meet = meet && (minFA <= _V(objCov.faceAmount));
        if (!meet) return false;
        meet = meet && (maxFA >= _V(objCov.faceAmount));
        if (!meet) return false;
        meet = meet && (minAge <= _V(objInsured.insuredAge));
        if (!meet) return false;
        meet = meet && (maxAge >= _V(objInsured.insuredAge));
        if (!meet) return false;
        return meet;
    },
    dependencyRules: function(args) {
        var coverage = args["coverage"];
        var schema = coverage.schema.call("getSchema");
        var dependencyList = Utils.iterator(_V(_PV(schema, "ProductSchema.Dependency.DependencyRuleRecord"), []));
        return dependencyList;
    },
    exclusiveProductList: function(args) {
        var dependencyList = this.self.dependencyRules(args);
        var exclusionList = [];
        for (depIdx in dependencyList) {
            var depRec = dependencyList[depIdx];
            var meet = this.self.isExclusiveDependencyRule({
                rule: depRec
            });
            if (meet) {
                exclusionList.push(depRec);
            }
        }
        return exclusionList;
    },
    inclusiveProductList: function(args) {
        var dependencyList = this.self.dependencyRules(args);
        var inclusionList = [];
        for (depIdx in dependencyList) {
            var depRec = dependencyList[depIdx];
            var meet = this.self.isInclusiveDependencyRule({
                rule: depRec
            });
            if (meet) {
                inclusionList.push(depRec);
            }
        }
        return inclusionList;
    },
    isExclusiveDependencyRule: function(args) {
        var rule = args["rule"];
        var ruleType = _V(rule.Type);
        return ((ruleType == SchemaConstants.DEPENDENCY_RULE_TYPE.EXCLUSIVE) || (ruleType == SchemaConstants.DEPENDENCY_RULE_TYPE.EXCLUSION));
    }
});var CORE_VALIDATION_RULES_BASE = Class.define({
   rules: function() {
      // default - empty rules
      return [];
   },
   create: function() {
      var rulesArray = [];
      var rules = this.self.rules();
      for (ruleIdx in rules) {
         var rule = rules[ruleIdx];
         rulesArray.push(IValidationRule.implementBy(rule));
      }
      return rulesArray;
   }
});

var CORE_DEFAULT_PRE_VALIDATION_RULES = CORE_VALIDATION_RULES_BASE.extend({
   rules: function() {
      return [
         IssueAgeValidationRule.create(),
         FaceAmountValidationRule.create(),
         CurrencyValidationRule.create(),
         PaymentModeValidationRule.create(),
         IssuedCountValidationRule.create(),
         InsuredSexValidationRule.create(),
         InsuredSmokingStatusValidationRule.create(),
         BillingMethodValidationRule.create(),
         BenefitPeriodValidationRule.create(),
         WaitingPeriodValidationRule.create(),
         POValidationRule.create(),
         IPOValidationRule.create(),
         SupportSOSByPolicyValidationRule.create(),
         CouponValidationRule.create(),
         OccupationClassValidationRule.create(),
         CommencementYearValidationRule.create(),
         PremiumDiscountValidationRule.create(),
         RegularPremiumValidationRule.create(),
         HookSubscriptionWithFAValidationRule.create(),
         FundCodeValidationRule.create(),
         DividendOptionValidationRule.create(),
         UVDeathBenefitValidationRule.create(),
         IFLStartingAgeValidationRule.create(),
         AnnuityOptionValidationRule.create(),
         FundActivityValidationRule.create(),
         PayorAgeValidationRule.create(),
         FaceAmountLimitBySmokingStatusValidationRule.create(),
         IIOWithIPOValidationRule.create(),
         DBWithIPOValidationRule.create(),
         RiderIPOValidationRule.create(),
         IPOWithRidersValidationRule.create(),
         DeathBenefitValidationRule.create(),
         ExtraWithIPOValidationRule.create(),
         SupportRCCValidationRule.create(),
         NoOfPersonLimitsValidationRule.create(),
         NoOfPersonValidationRule.create(),
         CoverageClassValidationRule.create(),
         MajorMedicalValidationRule.create(),
         NonHpSeriesValidationRule.create(),
         BenefitOptionValidationRule.create(),
         InitialDumpInValidationRule.create(),
         JuvenileRiderTypeValidationRule.create(),
         IsBasePlanValidationRule.create(),
         ProductDependencyValidationRule.create()
      ];
   }
});

var CORE_DEFAULT_POST_VALIDATION_RULES = CORE_VALIDATION_RULES_BASE.extend({
   rules: function() {
      return [
         TotalPremiumValidationRule.create(), MPREPremiumValidationRule.create()
      ];
   }
});

var CORE_DEFAULT_POST_PROJECTION_VALIDATION_RULES = CORE_VALIDATION_RULES_BASE.extend({
   rules: function() {
      return [];
   }
});

// Validation Builder / Director
var ValidatorBuilder = Class.define({

   init: function(args) {
      this.self.errors = [];
      this.self.errorJsons = [];
      this.self.proposalRules = [];
      this.self.basePlanRules = [];
      this.self.riderRules = [];
      this.self.exclusion = null;
      this.self.validationMapping = null;
      this.self.languageCode = null;
      //this.self.initialized = false;

      if (args) {
         var productRules = args["rules"];

         //console.log("productRules.length = " + productRules.length);

         //if (this.self.initialized == false) {
         for (ruleIdx in productRules) {
            var rule = productRules[ruleIdx];
            var scope = IValidationRule.appliedTo(rule.implementation).call("scope");

            //console.log("scope = " + scope);

            if (Utils.compareBits(scope, PublicConstants.TARGET_SCOPE.PROPOSAL)) {
               this.self.proposalRules.push(rule);
            }
            if (Utils.compareBits(scope, PublicConstants.TARGET_SCOPE.BASE_COVERAGE)) {
               this.self.basePlanRules.push(rule);
            }
            if (Utils.compareBits(scope, PublicConstants.TARGET_SCOPE.RIDER_COVERAGE)) {
               this.self.riderRules.push(rule);
            }
         }

         //console.log("this.self.proposalRules.length = " + this.self.proposalRules.length);
         //console.log("this.self.basePlanRules.length = " + this.self.basePlanRules.length);
         //console.log("this.self.riderRules.length = " + this.self.riderRules.length);
         //this.self.initialized = true;
         //}

         var validationMapping = args["validationMapping"];
         if (validationMapping) {
            this.self.validationMapping = validationMapping;
         }

         var exclusion = args["exclusion"];
         if (exclusion) {
            this.self.exclusion = exclusion;
         }
      }
   },

   getErrors: function(args) {
      var mm = Repository.getManager(SystemComponents.MANAGER.MESSAGE_MANAGER);

      var language = args["language"];
      var errors = []
      for (i in this.self.errors) {
         var message = mm.call("getMessage", {
            error: this.self.errors[i],
            language: language
         });
         if (message != undefined && message != null) {
            errors.push(message);
         }
      }
      return errors;
   },

   proceedValidation: function(productId, debugMode, testData, validationRules, globalProductMapping, productSpecificMapping) {
      //console.log("validationBuilder.proceedValidation.validationRules.length = " + validationRules.length);

      for (ruleIndex in validationRules) {
         var rule = validationRules[ruleIndex];
         var ruleName = rule.call("ruleName");
        //console.log("rule name = " + rule.call("ruleName", {}));

         var rulesIsExcluded = false;
         var rulesIsApplied = false;
         if (Utils.isNothing(globalProductMapping) && Utils.isNothing(productSpecificMapping)) {
            // not applied in country, assume all rules are applied
            rulesIsApplied = true;
         } else {
            var found = false;
            if (globalProductMapping) {
               found = (Utils.contains(ruleName, globalProductMapping));
            }
            if (!found) {
               if (productSpecificMapping) {
                  found = (Utils.contains(ruleName, productSpecificMapping));
               }
            }
            if (found) {
               if (!Utils.isNothing( this.self.exclusion ) && !Utils.isNothing(productId) ) {
                  var exclusionItems = this.self.exclusion[productId];
                  if (exclusionItems) {
                     var excludeFound = (exclusionItems.indexOf(ruleName) >= 0);
                     if (excludeFound) {
                        rulesIsExcluded = true;
                        found = false;
                     }
                  }
               }
            }
            rulesIsApplied = found;
         }
         if (rulesIsApplied) {
            if (debugMode) {
               if (rule.call("shouldTest", testData)) {
                  console.log("Rule (" + ruleName + ") --> START VALIDATE ... ");
                  var result = rule.call("validate", testData);
                  //console.log("result = " + result);
                  if (result) {
                      console.log(">>>>> --> " + JSON.stringify(result));
                  } else {
                      console.log(">>>>> --> PASS");
                  }
                  if (!Utils.isNothing(result)) {
                     var ii = this.self.errorJsons.indexOf( JSON.stringify(result) );
                     if (ii < 0) {
                         this.self.errors.push(result);
                         this.self.errorJsons.push( JSON.stringify(result) );
                     }
                  }
               } else {
                  console.log("Rule (" + ruleName + ") SKIPPED");
               }
            } else {
               try {
                  if (rule.call("shouldTest", testData)) {
                     console.log("Rule (" + ruleName + ") --> START VALIDATE ... ");
                     var result = rule.call("validate", testData);
                     //console.log("result = " + result);
                     if (result) {
                         console.log(">>>>> --> " + JSON.stringify(result));
                     } else {
                         console.log(">>>>> --> PASS");
                     }
                     if (!Utils.isNothing(result)) {
                        var ii = this.self.errorJsons.indexOf( JSON.stringify(result) );
                        if (ii < 0) {
                            this.self.errors.push(result);
                            this.self.errorJsons.push( JSON.stringify(result) );
                        }
                     }
                  } else {
                     console.log("Rule (" + ruleName + ") SKIPPED");
                  }
               } catch (e) {
                  _D(">>>>> Exception", e);
                  console.log("Rule (" + ruleName + ") EXCEPTION OCCURRED - SKIPPED");
               }
            }
         } else {
            if (rulesIsExcluded) {
               console.log("Rule (" + ruleName + ") EXCLUDED");
            } else {
               console.log("Rule (" + ruleName + ") NOT APPLICABLE");
            }
         }
      }
   },

   validateProposal: function(args) {
      var proposal = args['proposal'];
      var context = args['context'];
      var projectionResult = args['projectionResult'];
      var productId = _PV( proposal, 'coverageInfo.product.productKey.primaryProduct.productPK.productId' );
      var testData = {
         proposal: proposal,
         context: context,
         projectionResult: projectionResult
      };
      var globalProductMapping = null;
      if (this.self.validationMapping) {
         globalProductMapping = this.self.validationMapping[PublicConstants.VALIDATION_RULES_MAPPING.GLOBAL_RULES];
      }
      var productSpecificMapping = null;
      if (this.self.validationMapping) {
         if (!Utils.isNothing(productId)) {
            productSpecificMapping = this.self.validationMapping[productId];
         }
      }
      this.self.proceedValidation(productId, proposal.enableDebug, testData, this.self.proposalRules, globalProductMapping, productSpecificMapping);
   },

   validateBasePlan: function(args) {
      var basePlan = args['basePlan'];
      var context = args['context'];
      var projectionResult = args['projectionResult'];
      var productId = _PV( basePlan, 'product.productKey.primaryProduct.productPK.productId' );
      var testData = {
         coverage: basePlan,
         context: context,
         projectionResult: projectionResult
      };
      var globalProductMapping = null;
      if (this.self.validationMapping) {
         globalProductMapping = this.self.validationMapping[PublicConstants.VALIDATION_RULES_MAPPING.GLOBAL_RULES];
      }
      var productSpecificMapping = null;
      if (this.self.validationMapping) {
         if (!Utils.isNothing(productId)) {
            productSpecificMapping = this.self.validationMapping[productId];
         }
      }
      this.self.proceedValidation(productId, basePlan.proposal.enableDebug, testData, this.self.basePlanRules, globalProductMapping, productSpecificMapping);
   },

   validateRider: function(args) {
      var rider = args['rider'];
      var context = args['context'];
      var projectionResult = args['projectionResult'];
      var productId = _PV( rider, 'product.productKey.primaryProduct.productPK.productId' );
      var testData = {
         coverage: rider,
         context: context,
         projectionResult: projectionResult
      };
      var globalProductMapping = null;
      if (this.self.validationMapping) {
         globalProductMapping = this.self.validationMapping[PublicConstants.VALIDATION_RULES_MAPPING.GLOBAL_RULES];
      }
      var productSpecificMapping = null;
      if (this.self.validationMapping) {
         if (!Utils.isNothing(productId)) {
            productSpecificMapping = this.self.validationMapping[productId];
         }
      }
      this.self.proceedValidation(productId, rider.proposal.enableDebug, testData, this.self.riderRules, globalProductMapping, productSpecificMapping);
   }
});

var ValidationDirector = Class.define({
   execute: function(args) {
      var proposal = args["proposal"];
      var validateBuilder = args["builder"];
      var context = args["context"];
      var projectionResult = args["projectionResult"];

      //console.log("validateBuilder.proposalRules.length = " + validateBuilder.proposalRules.length);
      //console.log("validateBuilder.basePlanRules.length = " + validateBuilder.basePlanRules.length);
      //console.log("validateBuilder.riderRules.length = " + validateBuilder.riderRules.length);

      var builder = IValidationBuilder.appliedTo(validateBuilder);

      //builder.call("init");
      builder.call("validateProposal", {
         proposal: proposal, context: context, projectionResult: projectionResult
      });
      builder.call("validateBasePlan", {
         basePlan: proposal.coverageInfo, context: context, projectionResult: projectionResult
      });
      if (!Utils.isNothing(proposal.riders)) {
        var ridersTable = Utils.iterator(proposal.riders.coverageInfo);
        for (riderIndex in ridersTable) {
            builder.call("validateRider", {
               rider: proposal.riders.coverageInfo[riderIndex], context: context, projectionResult: projectionResult
            });
         }
      }
      return builder.call("getErrors", {
         language: proposal.language
      });
   }
});

var ValidatorImpl = Class.define({

   init: function(args) {
      this.self.preValidationRules = [];
      this.self.postValidationRules = [];
      this.self.postProjectionValidationRules = [];
      this.self.productValidationMapping = [];

      if (args == undefined) {
         args = {};
      }
      var preValidationRules = args['preValidationRules'];
      var postValidationRules = args['postValidationRules'];
      var postProjectionValidationRules = args['postProjectionValidationRules']
      var productValidationMapping = args['productValidationMapping'];

      //console.log("(parameter) preValidationRules = " + JSON.stringify(preValidationRules));
      //console.log("(parameter) postValidationRules = " + JSON.stringify(postValidationRules));
      //console.log("(parameter) postProjectionValidationRules = " + JSON.stringify(postProjectionValidationRules));
      //console.log("(parameter) productValidationMapping = " + JSON.stringify(productValidationMapping));

      var preRules = (ICoreRules.implementBy(CORE_DEFAULT_PRE_VALIDATION_RULES.create())).call("create");
      if (Utils.isNothing(preValidationRules) == false) {
         for (key in preValidationRules) {
            preRules.push(preValidationRules[key]);
         }
      }
      this.self.preValidationRules = preRules;

      var postRules = (ICoreRules.implementBy(CORE_DEFAULT_POST_VALIDATION_RULES.create())).call("create");
      if (Utils.isNothing(preValidationRules) == false) {
         for (key in postValidationRules) {
            postRules.push(postValidationRules[key]);
         }
      }
      this.self.postValidationRules = postRules;

      var preProjRules = (ICoreRules.implementBy(CORE_DEFAULT_POST_PROJECTION_VALIDATION_RULES.create())).call("create");
      if (Utils.isNothing(postProjectionValidationRules) == false) {
         for (key in postProjectionValidationRules) {
            preProjRules.push(postProjectionValidationRules[key]);
         }
      }
      this.self.postProjectionValidationRules = preProjRules;

      //console.log("# of this.self.preValidationRules = " + this.self.preValidationRules.length);
      //console.log("# of this.self.postValidationRules = " + this.self.postValidationRules.length);
      //console.log("# of this.self.postProjectionValidationRules = " + this.self.postProjectionValidationRules.length);

      _D("validation mapping", productValidationMapping);
      if (!(Utils.isNothing(productValidationMapping))) {
         this.self.productValidationMapping = productValidationMapping;
         //if (Utils.isNothing(this.self.productValidationMapping.exclusion)) {
         //   this.self.productValidationMapping['exclusion'] = {};
         //}
      }
   },

   addValidationRule: function(args) {
      var rule = args['rule'];
      var ruleElement = IValidationRule.implementBy(rule);
      this.self.preValidationRules.push(ruleElement);
      this.self.postValidationRules.push(ruleElement);
      this.self.postProjectionValidationRules.push(ruleElement);
   },

   showRuleNames: function(args) {
      _D("", "[ Validation Map - PRIMARY ]", "");
      _D("", JSON.stringify(this.self.productValidationMapping, null, 2), "");
      _D("", "", "");
      _D("", "[ preValidationRules ]", "");
      for(ri in this.self.preValidationRules) {
         var r = this.self.preValidationRules[ri]
         _D("", "rule: " + r.call("ruleName", {}), "");
      }
      _D("", "[ postValidationRules ]", "");
      for(ri in this.self.postValidationRules) {
         var r = this.self.postValidationRules[ri]
         _D("", "rule: " + r.call("ruleName", {}), "");
      }
      _D("", "[ postProjectionValidationRules ]", "");
      for(ri in this.self.postProjectionValidationRules) {
         var r = this.self.postProjectionValidationRules[ri]
         _D("", "rule: " + r.call("ruleName", {}), "");
      }
   },

   validate: function(args) {
      var proposal = args["proposal"];

      var errors = [];
      var canContinue = true;

      // pre-calculation validation
      var preErrors = this.self.preCalculationValidate({
         proposal: proposal
      });
      if (!Utils.isNothing(preErrors)) {
         for (i in preErrors) {
            errors.push(preErrors[i]);
            if (!preErrors[i].warning) canContinue = false;
         }
      }
      //if (errors.length == 0) {
      if (canContinue) {
         // premium calculation
         var _pe = Repository.getManager(SystemComponents.MANAGER.PRODUCT_ENGINE_INSTANCE);
         _pe.call("calculatePremiums");
         // post-calculation validation
         var postErrors = this.self.postCalculationValidate({
            proposal: proposal
         });
         if (!Utils.isNothing(postErrors)) {
            for (i in postErrors) {
               errors.push(postErrors[i]);
            }
         }
      }
      return errors;
   },

   preCalculationValidate: function(args) {
      var proposal = args["proposal"];

      var validationBuilder = ValidatorBuilder.create({
         rules: this.self.preValidationRules,
         validationMapping: this.self.productValidationMapping.preCalculationValidationRules,
         exclusion: this.self.productValidationMapping.exclusion
      });
      var validationDirector = IExecutable.implementBy(ValidationDirector.create());
      return validationDirector.call("execute", {
         proposal: proposal,
         builder: validationBuilder
      });
   },

   postCalculationValidate: function(args) {
      var proposal = args["proposal"];
      var validationBuilder = ValidatorBuilder.create({
         rules: this.self.postValidationRules,
         validationMapping: this.self.productValidationMapping.postCalculationValidationRules,
         exclusion: this.self.productValidationMapping.exclusion
      });
      var validationDirector = IExecutable.implementBy(ValidationDirector.create());
      return validationDirector.call("execute", {
         proposal: proposal,
         builder: validationBuilder
      });
   },

   postProjectionValidate: function(args) {
      var proposal = args["proposal"];
      var context = args["context"];
      var projectionResult = args["projectionResult"];

      var validationBuilder = ValidatorBuilder.create({
         rules: this.self.postProjectionValidationRules,
         validationMapping: this.self.productValidationMapping.postProjectionValidationRules,
         exclusion: this.self.productValidationMapping.exclusion
      });
      var validationDirector = IExecutable.implementBy(ValidationDirector.create());
      return validationDirector.call("execute", {
         proposal: proposal,
         context: context,
         projectionResult: projectionResult,
         builder: validationBuilder
      });
   }

});
var ProductEngineImpl = Class.define({

    init: function(args) {
        this.self.context = {};
        this.self.context.proposal = null;

        var managers = args['managers'];

        var validator = managers[SystemComponents.MANAGER.VALIDATOR];
        var premiumCalculator = managers[SystemComponents.MANAGER.PREMIUM_CALCULATOR];
        var policyProjector = managers[SystemComponents.MANAGER.POLICY_VALUE_PROJECTOR];
        var catalogManager = managers[SystemComponents.MANAGER.CATALOG_HELPER];
        var catalogFactory = managers[SystemComponents.MANAGER.CATALOG_FACTORY];
        var schemaManager = managers[SystemComponents.MANAGER.SCHEMA_HELPER];
        var schemaFactory = managers[SystemComponents.MANAGER.SCHEMA_FACTORY];
        var messageManager = managers[SystemComponents.MANAGER.MESSAGE_MANAGER];
        var dataService = managers[SystemComponents.MANAGER.DATA_SERVICE];
        var rateManager = managers[SystemComponents.MANAGER.RATE_MANAGER];
        var inputTransformer = managers[SystemComponents.MANAGER.INPUT_TRANSFORMER];
        var eventsManager = managers[SystemComponents.MANAGER.EVENTS_MANAGER];
        try {
            if (Utils.isNothing(validator)) {
                console.log("create a new validator");
                validator = Repository.getManager(SystemComponents.VALIDATOR);
                if (!validator) {
                    validator = IValidator.implementBy(ValidatorImpl.create( { productValidationMapping: mapping } ));
                }
            }
            if (Utils.isNothing(premiumCalculator)) {
                premiumCalculator = Repository.getManager(SystemComponents.PREMIUM_CALCULATOR);
                if (!premiumCalculator) {
                    methods = [
                        CommonBpm.create({formula: BPM1.create()}),
                        CommonBpm.create({formula: SPSM.create()})
                    ];
                    premiumCalculator = IPremiumCalculator.implementBy(PremiumCalculatorImpl.create({
                        methods: methods
                    }));
                }
            }
            if (Utils.isNothing(policyProjector)) {
               policyProjector = Repository.getManager(SystemComponents.POLICY_VALUE_PROJECTOR);
               if (!policyProjector) {
                   policyProjector = IProjectionManager.implementBy(ProjectionManagerImpl.create());
               }
            }
            if (Utils.isNothing(catalogManager)) {
               catalogManager = Repository.getManager(SystemComponents.CATALOG_HELPER);
               if (!catalogManager) {
                  catalogManager = ICatalogManager.implementBy(CatalogManagerImpl.create());
               }
            }
            if (Utils.isNothing(catalogFactory)) {
               catalogFactory = Repository.getManager(SystemComponents.CATALOG_FACTORY);
               if (!catalogFactory) {
                  catalogFactory = Factory.define(ICatalog, CatalogImpl);
               }
            }
            if (Utils.isNothing(schemaManager)) {
                schemaManager = Repository.getManager(SystemComponents.SCHEMA_HELPER);
                if (!schemaManager) {
                   schemaManager = ISchemaManager.implementBy(SchemaManagerImpl.create());
                }
            }
            if (Utils.isNothing(schemaFactory)) {
                schemaFactory = Repository.getManager(SystemComponents.SCHEMA_FACTORY);
                if (!schemaFactory) {
                   schemaFactory = Factory.define(ISchema, SchemaImpl);
                }
            }
            if (Utils.isNothing(messageManager)) {
                // no message by default
                messageManager = Repository.getManager(SystemComponents.MESSAGE_MANAGER);
                if (!messageManager) {
                   messageManager = IMessageManager.implementBy(MessageManagerImpl.create([]));
                }
            }
            if (!Utils.isNothing(dataService)) {
                peServiceLocator.register(SystemComponents.DATA_SERVICE.CUSTOM_DATA_SERVICE, dataService);
                PEservice.dataSource = SystemComponents.DATA_SERVICE.CUSTOM_DATA_SERVICE;
            }
            if (Utils.isNothing(rateManager)) {
                rateManager = Repository.getManager(SystemComponents.RATE_MANAGER);
                if (!rateManager) {
                   rateManager = PERateManager.create();
                }
            }
            if (Utils.isNothing(inputTransformer)) {
                inputTransformer = Repository.getManager(SystemComponents.INPUT_TRANSFORMER);
                if (!inputTransformer) {
                   inputTransformer = IInputTransformer.implementBy(InputTransformerImpl.create());
                }
            }
            if (Utils.isNothing(eventsManager)) {
                eventsManager = Repository.getManager(SystemComponents.EVENTS_MANAGER);
                if (!eventsManager) {
                   eventsManager = IEventsManager.implementBy(EventsManagerImpl.create());
                }
            }

            Repository.addManager(SystemComponents.MANAGER.VALIDATOR, validator);
            Repository.addManager(SystemComponents.MANAGER.PREMIUM_CALCULATOR, premiumCalculator);
            Repository.addManager(SystemComponents.MANAGER.POLICY_VALUE_PROJECTOR, policyProjector);
            Repository.addManager(SystemComponents.MANAGER.CATALOG_HELPER, catalogManager);
            Repository.addManager(SystemComponents.MANAGER.CATALOG_FACTORY, catalogFactory);
            Repository.addManager(SystemComponents.MANAGER.SCHEMA_HELPER, schemaManager);
            Repository.addManager(SystemComponents.MANAGER.SCHEMA_FACTORY, schemaFactory);
            Repository.addManager(SystemComponents.MANAGER.MESSAGE_MANAGER, messageManager);
            Repository.addManager(SystemComponents.MANAGER.PRODUCT_ENGINE_INSTANCE, IProductEngine.appliedTo(this.self));
            Repository.addManager(SystemComponents.MANAGER.RATE_MANAGER, rateManager);
            Repository.addManager(SystemComponents.MANAGER.INPUT_TRANSFORMER, inputTransformer);
            Repository.addManager(SystemComponents.MANAGER.EVENTS_MANAGER, eventsManager);
            Repository.addManager(SystemComponents.MANAGER.POLICY_VALUE_PROJECTOR, policyProjector);
        } catch (e) {
            console.log(e);
            console.trace();
        }
    },

    setProposal: function(args) {
        // this is a function to load all necessary configurations
        // into the current context of engine. Actually, it must be called before
        // other functions called.
        var proposal = args['proposal'];

        console.log("");
        console.log("**************************************");
        _D("proposal (input)", proposal, "PE_core.setProposal()");
        console.log("");

        // backward compatible with Comprop and New PE on _code reserve word in fundRecord
        // proposal.funds.fundRecord & .proposal.topupFunds.fundRecord
        var fundSectionIdx = 0;
        var fundSections = [ ['funds','fundRecord'], ['topupFunds','fundRecord'] ];
        for(fundSectionI in fundSections) {
            var fs = fundSections[ fundSectionI ];
            var fundRecords = _PV(proposal, fs);

            console.log("*** Proposal Funds " + ( (fundSectionIdx==0) ? "(Funds)" : "(Topup Funds)" ) + " ***" );
            _D("fs", fs, "PE_core->setProposal->");
            _D("fundRecords (BEFORE)", fundRecords, "PE_core->setProposal->");

            if (!Utils.isNothing(fundRecords)) {
                var fundRecordsIter = Utils.iterator(fundRecords);

                for(fri in fundRecordsIter) {
                    var fundRec = fundRecordsIter[fri];
                    if (Utils.isNothing(fundRec._code) && !Utils.isNothing(fundRec.code)) {
                        fundRec['_code'] = fundRec.code;
                    } else
                    if (!Utils.isNothing(fundRec._code) && Utils.isNothing(fundRec.code)) {
                        fundRec['code'] = fundRec._code;
                    }
                }
            }

            _D("fundRecords (AFTER)", fundRecords, "PE_core->setProposal->");
            fundSectionIdx ++;
        }
        console.log("");
        console.log("**************************************");

        var cm = Repository.getManager(SystemComponents.MANAGER.CATALOG_HELPER);
        var sm = Repository.getManager(SystemComponents.MANAGER.SCHEMA_HELPER);
        if (Utils.isNothing(proposal.language)) {
            // default language is EN
            proposal.language = "en";
        }

        // the transformation can be replaced by builder pattern in later stage
        var translator = Repository.getManager(SystemComponents.MANAGER.INPUT_TRANSFORMER);
        proposal.coverageInfo = translator.call("transform", {
            coverage: proposal.coverageInfo
        });
        // translator.call("setReturnRatesByCurrency", {proposal: proposal});
        // ---

        proposal.coverageInfo = Utils.extend(proposal.coverageInfo, {
            proposal: proposal
        });

        // ****** BASE PLAN CATALOG ******
        var catalog = cm.call("lookup", {
            productCode: proposal.coverageInfo.product.productKey.primaryProduct.productPK.productId
        });

        // ****** BASE PLAN SCHEMA ******
        var schema = sm.call("lookup", {
            coverage: proposal.coverageInfo
        });

        //if (proposal.enableDebug) {
        //    _D("", "", "");
        //    _D("", "[ Schema Content ]", "");
        //    var sch = schema.call("getSchema");
        //    _SS( sch, 10 );
        //    _D("", "", "");
        //}

        // ****** CALCULATE AND UPDATE BANDING ******
        var banding = schema.call("calculateBanding", {
            coverage: proposal.coverageInfo
        });

        _DB("after calculateBanding - banding = ", banding);
        console.log("");

        if (banding) {
            proposal.band = banding.Band.text;
        }

        // ****** Update indicators for calculation use ****
        proposal.indicators = {};
        proposal.indicators.hasDividendWithdrawal = false;
        if (!Utils.isNothing(proposal.coverageInfo.dividendWithdrawnStartYear)) {
           if (Number(proposal.coverageInfo.dividendWithdrawnStartYear) > 0) {
              proposal.indicators.hasDividendWithdrawal = true;
           }
        }
        proposal.indicators.hasFundActivities = false;
        if (!Utils.isNothing(proposal.fundActivities) && !Utils.isNothing(proposal.fundActivities.fundActivity)) {
           var activities = Utils.iterator( proposal.fundActivities.fundActivity );
           if (activities.length > 0) {
              proposal.indicators.hasFundActivities = true;
           }
        }

        // Update coverage record with catalog, schema and banding information
        proposal.coverageInfo = Utils.extend(proposal.coverageInfo, {
            catalog: catalog,
            schema: schema,
            banding: banding
        });

        // ****** REQUEST ******
        this.self.context.proposal = proposal;

        if (!Utils.isNothing(proposal.riders)) {
           for (riderIndex in proposal.riders.coverageInfo) {

             proposal.riders.coverageInfo[riderIndex] = translator.call("transform", {
                 coverage: proposal.riders.coverageInfo[riderIndex]
             });

             proposal.riders.coverageInfo[riderIndex] = Utils.extend(proposal.riders.coverageInfo[riderIndex], {
                 proposal: proposal
             });

             if (Utils.isNothing(proposal.riders.coverageInfo[riderIndex].options)) {
                proposal.riders.coverageInfo[riderIndex]['options'] = {};
             }
             proposal.riders.coverageInfo[riderIndex]['options']['paymentMode'] = proposal.coverageInfo['options']['paymentMode']

             // ****** Rider CATALOG ******
             var catalog = cm.call("lookup", {
                 productCode: proposal.riders.coverageInfo[riderIndex].product.productKey.primaryProduct.productPK.productId
             });

             // ****** Rider SCHEMA ******
             var schema = sm.call("lookup", {
                 coverage: proposal.riders.coverageInfo[riderIndex]
             });

             var banding = schema.call("calculateBanding", {
                 coverage: proposal.riders.coverageInfo[riderIndex]
             });

             proposal.riders.coverageInfo[riderIndex] = Utils.extend(proposal.riders.coverageInfo[riderIndex], {
                 catalog: catalog,
                 schema: schema,
                 banding: banding,
                 proposal: proposal
             });
           }
        }

        //if (proposal.enableDebug) {
        //   // ****** show context ******
        //   _DB("Product Engine Context: ", this.self.context);
        //}

    },

    getContext: function() {
        return this.self.context;
    },

    validate: function() {
        // this is a function to check if the calcRequest is valid
        // based on the loaded configuration. error will be returned if it is
        // not correct or null if no problem
        var vm = Repository.getManager(SystemComponents.MANAGER.VALIDATOR);
        return vm.call("validate", {
            proposal: this.self.context.proposal
        });
    },

    calculatePremiums: function() {
        // this is a function to calculate all premiums in all available payment modes.
        // null will be returned if error found.
        var pc = Repository.getManager(SystemComponents.MANAGER.PREMIUM_CALCULATOR);
        return pc.call("calculate", {
            proposal: this.self.context.proposal
        });
    },

    listBaseProducts: function(args) {
        var planCodes = args["planCodes"];
        var language = args["language"];
        var effectiveDate = args["effectiveDate"];
        var location = args["location"];
        return PEservice.listBaseProducts(planCodes, language, effectiveDate, location);
    },

    calculateInsuredAge: function(args) {
        var dateOfBirth = args["dateOfBirth"];
        var policyYearDate = args["policyYearDate"];
        var language = args["language"];

        var dob = null;
        if (typeof dateOfBirth === 'string' || dateOfBirth instanceof String) {
            console.log("dob string is received - " + dateOfBirth);
            dob = dateOfBirth.date();
        } else
        if (typeof dateOfBirth === 'date' || dateOfBirth instanceof Date) {
            console.log("dob date is received - " + dateOfBirth);
            dob = dateOfBirth;
        }

        var pyd = null;
        if (typeof policyYearDate === 'string' || policyYearDate instanceof String) {
            console.log("pyd string is received - " + policyYearDate);
            pyd = policyYearDate.date();
        } else
        if (typeof policyYearDate === 'date' || policyYearDate instanceof Date) {
            console.log("pyd date is received - " + policyYearDate);
            pyd = policyYearDate;
        }

        if (Utils.isNothing(language)) {
            language = "en";
        }

        console.log("calculateInsuredAge - begin")
        console.log("dateOfBirth = " + dateOfBirth);
        console.log("policyYearDate = " + policyYearDate);
        console.log("dob = " + dob);
        console.log("pyd = " + pyd);

        var result = {};
        if (dob == null || pyd == null) {
            result["error"] = this.getErrors({
                language: "en",
                errorCode: "ERRC0000",
                variables: []
            });
            result["age"] = -1;
        } else {
            try {
                if (Utils.isNothing(pyd)) {
                    pyd = Utils.now();
                }

                /**
                 * LAST calculation
                 */
                var age = pyd.getFullYear() - dob.getFullYear();
                var dtLstBirth = new Date(dob.getFullYear() + age, dob.getMonth(), dob.getDate());
                /**
                 * If it is a leap year, then do not shift to next day (AddDate function of VB.NET) 
                 * for example if DOB=29/2/2016, then DOB + 1YEAR is expected as 28/02/2017
                 */
                var lstDateOfMonth = new Date(dob.getFullYear() + age, dob.getMonth() + 1, 0);
                if( dtLstBirth > lstDateOfMonth){
                  dtLstBirth = lstDateOfMonth;
                }

                if( pyd < dtLstBirth){
                  age = age - 1;
                }

                /*
                 * NEAREST calculation
                var julianDayDob = dob.julianDay();
                var julianDayPyd = pyd.julianDay();

                if (dob.daysInYear() > 365 && julianDayDob > 59) {
                    julianDayDob--;
                }

                if (pyd.daysInYear() > 365 && julianDayPyd > 59) {
                    julianDayPyd--;
                }

                var age = pyd.getFullYear() - dob.getFullYear();
                if (julianDayDob > julianDayPyd) {
                    age--;
                    julianDayPyd += 365;
                }

                if ((julianDayPyd - julianDayDob) > 183) {
                    age++;
                }
                */

                result["age"] = age;
                result["error"] = "";
            } catch (e) {
                result["age"] = null;
                result["error"] = e;
            }
        }
        return result;
    },

    getErrors: function(args) {
        var mm = Repository.getManager(SystemComponents.MANAGER.MESSAGE_MANAGER);

        var language = args["language"];
        var errorCode = args["errorCode"];
        var variables = args["variables"];
        var message = mm.call("getMessage", {
            error: {
                code: errorCode,
                arguments: variables
            },
            language: language
        });
        return message;
    },

    listRiders: function(args) {
        var planCodes = args["planCodes"];
        var language = args["language"];
        var effectiveDate = args["effectiveDate"];
        var location = args["location"];

        console.log("step 2 - begin");
        console.log(planCodes);
        console.log("step 2 - end");
        return PEservice.listRiders(planCodes, language, effectiveDate, location);
    },

    runProjection: function(args) {
        // it accept the options of assumption and run a projection and return the
        // result to calling application. null will be returned if error happen.
        var proposal = this.self.context.proposal;

        var schemaObj =  this.self.context.proposal.coverageInfo.schema.call('getSchema', {});
        var policyOptionsX = _V( _PV( schemaObj, "ProductSchema.CompropOption.PolOpt" ), { } );
        var projectionOptionsX = _V( _PV( schemaObj, "ProductSchema.CompropOption.ProjOpt" ), { BasePlanWithRiders: true } );
        _DB("", "", "");
        _DB("", "[ Policy Options - Schema ]", "");
        _SS(policyOptionsX, 2);
        _DB("", "", "");
        _DB("", "[ Projection Options - Schema ]", "");
        _SS(projectionOptionsX, 2);

        var projectionOptionsXX = [];
        for(projOptKey in projectionOptionsX) {
            var projOptVal = projectionOptionsX[projOptKey];
            if (projOptVal) {
                projectionOptionsXX.push(projOptKey);
            }
        }

        var policyOptionsXX = [];
        for(polOptKey1 in policyOptionsX) {
            var newDimension = [];
            var polOptObj = policyOptionsX[polOptKey1]; // Y, N or YN
            var polOptVal = _V( polOptObj )
            if (polOptVal == 'Y' || polOptVal == 'YN') {
                newDimension.push( polOptKey1 );
            }
            if (polOptVal == 'N' || polOptVal == 'YN') {
                newDimension.push( '**EMPTY**' );
            }
            policyOptionsXX = Utils.expand(policyOptionsXX, newDimension);
        }

        var policyOptionsXXX = [];
        for (polOptsIdx in policyOptionsXX) {
            var options = [];
            for (optIdx in policyOptionsXX[polOptsIdx]) {
                var opt = policyOptionsXX[polOptsIdx][optIdx];
                if (opt != '**EMPTY**') {
                    options.push(opt);
                }
            }
            policyOptionsXXX.push(options);

        }

        _DB("", "", "");
        _DB("", "*** Policy Options - Result ***", "");
        _DB("", JSON.stringify(policyOptionsXXX, null, 2), "");
        _DB("", "", "");
        _DB("", "*** Projection Options - Result ***", "");
        _DB("", JSON.stringify(projectionOptionsXX, null, 2), "");

        var data = [];
        if (policyOptionsXXX.length > 0) {
            for(projIdx in projectionOptionsXX) {
                for(polIdx in policyOptionsXXX) {
                    data.push( {
                       proposal: proposal,
                       projectionOptions: [ projectionOptionsXX[projIdx] ],
                       policyOptions: policyOptionsXXX[polIdx]
                    } );
                }
            }
        } else {
            for(projIdx in projectionOptionsXX) {
                data.push( {
                   proposal: proposal,
                   projectionOptions: [ projectionOptionsXX[projIdx] ],
                   policyOptions: []
                } );
            }
        }

        _DB("", "", "");
        _DB("", "*** Scenarios ***", "");
        for(datai in data) {
            var dataVal = data[datai];
            _DB("", "ProjOpt:" + JSON.stringify(dataVal.projectionOptions) + " / PolOpt:" + JSON.stringify(dataVal.policyOptions), "");
        }

        /*
        //console.log("- BasePlanOnly");
        var data = [
           {
              proposal: proposal,
              projectionOptions: [InterfaceConstants.PROJECTION_OPTIONS.BASE_PLAN_ONLY],
              policyOptions: []
           }
        ];
        if (proposal.indicators.hasFundActivities) {
           //console.log("- BasePlanOnly/fundActivity");
           data.push(
              {
                 proposal: proposal,
                 projectionOptions: [InterfaceConstants.PROJECTION_OPTIONS.BASE_PLAN_ONLY],
                 policyOptions: [InterfaceConstants.POLICY_OPTIONS.FUND_ACTIVITIES_OPTION]
              }
           );
        }
        if (proposal.indicators.hasDividendWithdrawal) {
           //console.log("- BasePlanOnly/cashOutOption");
           data.push(
             {
                 proposal: proposal,
                 projectionOptions: [InterfaceConstants.PROJECTION_OPTIONS.BASE_PLAN_ONLY],
                 policyOptions: [InterfaceConstants.POLICY_OPTIONS.CASH_OUT_OPTION]
             }
           );
           if (proposal.indicators.hasFundActivities) {
              //console.log("- BasePlanOnly/cashOutOption+fundActivity");
              data.push(
                 {
                    proposal: proposal,
                    projectionOptions: [InterfaceConstants.PROJECTION_OPTIONS.BASE_PLAN_ONLY],
                    policyOptions: [InterfaceConstants.POLICY_OPTIONS.FUND_ACTIVITIES_OPTION, InterfaceConstants.POLICY_OPTIONS.CASH_OUT_OPTION]
                 }
              );
           }
        }
        */

        var product = {
           name: this.self.context.proposal.coverageInfo.catalog.call("getCatalog", {}).ProductCatalog.ProductName.Name.text,
           code: this.self.context.proposal.coverageInfo.product.productKey.primaryProduct.productPK.productId
        };
        var result = {
           product: product,
           projections: []
        }

        //for(d in data) {
        //  console.log("projection options: " + JSON.stringify(data[d].projectionOptions) + " / " + JSON.stringify(data[d].policyOptions));
        //}

        var projector = Repository.getManager(SystemComponents.MANAGER.POLICY_VALUE_PROJECTOR);
        for(d in data) {
           var r = projector.call("runProjection", data[d]);
           result.projections.push(r);
        }
        // console.log(JSON.stringify(results));
        return result;
    }

});
var InputTransformerImpl = Class.define({

   map: function(v, valueMap) {
      var o = v;
      if (v) {
         for (k in valueMap) {
            var vp = valueMap[k];
            if (v.equalsIgnoreCase(vp.key)) {
               o = vp.value;
               break;
            }
         }
      }
      return o;
   },

   transformLocation: function(location) {
      //var location = args['location'];
      var _location = this.map(location, [{
            key: InterfaceConstants.LOCATION.HONGKONG,
            value: CatalogConstants.LOCATION.HONGKONG
         },
         {
            key: InterfaceConstants.LOCATION.MACAU,
            value: CatalogConstants.LOCATION.MACAU
         },
         {
            key: InterfaceConstants.LOCATION.SINGAPORE,
            value: CatalogConstants.LOCATION.SINGAPORE
         },
         {
            key: InterfaceConstants.LOCATION.CHINA,
            value: CatalogConstants.LOCATION.CHINA
         },
         {
            key: InterfaceConstants.LOCATION.PHILIPPINE,
            value: CatalogConstants.LOCATION.PHILIPPINE
         },
         {
            key: InterfaceConstants.LOCATION.VIETNAM,
            value: CatalogConstants.LOCATION.VIETNAM
         }      ]);
      return _location;
   },

   transformPaymentMode: function(paymentMode) {
      //var paymentMode = args['paymentMode'];
      var _paymentMode = this.map(paymentMode, [{
            key: InterfaceConstants.PAYMENT_MODE.ANNUAL,
            value: SchemaConstants.PAYMENT_MODE.ANNUAL
         },
         {
            key: InterfaceConstants.PAYMENT_MODE.SEMIANNUAL,
            value: SchemaConstants.PAYMENT_MODE.SEMIANNUAL
         },
         {
            key: InterfaceConstants.PAYMENT_MODE.QUARTERLY,
            value: SchemaConstants.PAYMENT_MODE.QUARTERLY
         },
         {
            key: InterfaceConstants.PAYMENT_MODE.MONTHLY,
            value: SchemaConstants.PAYMENT_MODE.MONTHLY
         }
      ]);
      return _paymentMode;
   },

   transformGender: function(gender) {
      //var gender = args['gender'];
      var _gender = this.map(gender, [{
            key: SchemaConstants.GENDER.MALE,
            value: SchemaConstants.GENDER_FULL.MALE
         },
         {
            key: SchemaConstants.GENDER.FEMALE,
            value: SchemaConstants.GENDER_FULL.FEMALE
         }
      ]);
      return _gender;
   },

   transformSmokingStatus: function(smokingStatus) {
      //var smokingStatus = args['smokingStatus'];
      var _smokingStatus = this.map(smokingStatus, [{
            key: InterfaceConstants.SMOKING_STATUS.SMOKER,
            value: SchemaConstants.SMOKING_STATUS.SMOKER
         },
         {
            key: InterfaceConstants.SMOKING_STATUS.NONSMOKER,
            value: SchemaConstants.SMOKING_STATUS.NONSMOKER
         },
         {
            key: InterfaceConstants.SMOKING_STATUS.AGGREGATE,
            value: SchemaConstants.SMOKING_STATUS.AGGREGATE
         },
         {
            key: InterfaceConstants.SMOKING_STATUS.PREFERRED_LIVES,
            value: SchemaConstants.SMOKING_STATUS.PREFERRED_LIVES
         }
      ]);
      return _smokingStatus;
   },
   transformBillingMode: function(billingMode) {
      //var smokingStatus = args['smokingStatus'];
      var _billingMode = this.map(billingMode, [{
            key: InterfaceConstants.BILLING_MODE.DIRECTBILLING,
            value: SchemaConstants.BILLING_MODE.DIRECTBILLING
         },
         {
            key: InterfaceConstants.BILLING_MODE.AUTOPAY,
            value: SchemaConstants.BILLING_MODE.AUTOPAY
         },
         {
            key: InterfaceConstants.BILLING_MODE.PDF,
            value: SchemaConstants.BILLING_MODE.PDF
         },
         {
            key: InterfaceConstants.BILLING_MODE.CREDIT_CARD,
            value: SchemaConstants.BILLING_MODE.CREDIT_CARD
         },
         {
            key: InterfaceConstants.BILLING_MODE.MANUCARD,
            value: SchemaConstants.BILLING_MODE.MANUCARD
         },
         {
            key: InterfaceConstants.BILLING_MODE.SINGLE,
            value: SchemaConstants.BILLING_MODE.SINGLE
         }
      ]);
      return _billingMode;
   },

   transformDividentOption: function(dividentOption) {
      //var smokingStatus = args['smokingStatus'];
      var _dividentOption = this.map(dividentOption, [{
            key: InterfaceConstants.OPTION_DIVIDEND.DVD_CASH_DIVIDEND,
            value: SchemaConstants.DIVIDEND_OPTION.CASH_DIVIDEND
         },
         {
            key: InterfaceConstants.OPTION_DIVIDEND.DVD_ACCUMULATED_DIVIDEND,
            value: SchemaConstants.DIVIDEND_OPTION.ACCUMULATE
         },
         {
            key: InterfaceConstants.OPTION_DIVIDEND.DVD_PAID_UP_ADDITION,
            value: SchemaConstants.DIVIDEND_OPTION.PAID_UP_ADDITION
         },
         {
            key: InterfaceConstants.OPTION_DIVIDEND.DVD_BONUS_PROTECTION,
            value: SchemaConstants.DIVIDEND_OPTION.BONUS_PROTECTION
         }
      ]);
      return _dividentOption;
   },

   transformBenefitOption: function(benefitOption) {
      var _benefitOption = this.map(benefitOption, [{
            key: InterfaceConstants.BENEFIT_OPTION_TYPE.HEALTH_MAX_PROGRAM,
            value: SchemaConstants.BENEFIT_OPTION_TYPE.HEALTH_MAX_PROGRAM
         },
         {
            key: InterfaceConstants.BENEFIT_OPTION_TYPE.ADDITIONAL_LIFE_COVERAGE,
            value: SchemaConstants.BENEFIT_OPTION_TYPE.ADDITIONAL_LIFE_COVERAGE
         },
         {
            key: InterfaceConstants.BENEFIT_OPTION_TYPE.NO_VALUE,
            value: SchemaConstants.BENEFIT_OPTION_TYPE.NO_VALUE
         }
      ]);
      return _benefitOption;
   },

   setReturnRatesByCurrency: function(args) {
       var proposal = args['proposal'];
       //console.log(JSON.stringify(proposal));
       if (!Utils.isNothing(proposal) && !Utils.isNothing(proposal.funds)) {
          var policyCurrency = proposal.coverageInfo.currency.currencyPK.currencyId;
          var fundRecords = proposal.funds.fundRecord;
          //console.log(JSON.stringify(fundRecords));
          if (!Utils.isNothing(fundRecords)) {
             var funds = Utils.iterator(fundRecords);
             for(idx in funds) {
                var fundRecord = funds[idx];
                if (policyCurrency == CURRENCY.USD) {
                   fundRecord.returnRate = 3;
                   fundRecord.returnRateMedium = 6;
                   fundRecord.returnRateHigh = 8;
                   if (Number(fundRecord.targetPayoutRate) > Number(0)) {
                      fundRecord.targetPayoutRate = 0.3958333333333334;
                   }
                }
                if (policyCurrency == CURRENCY.PHP) {
                   fundRecord.returnRate = 4;
                   fundRecord.returnRateMedium = 8;
                   fundRecord.returnRateHigh = 10;
                   if (Number(fundRecord.targetPayoutRate) > Number(0)) {
                      fundRecord.targetPayoutRate = 0.250;
                   }
                }
             }
          }
       }
   },
   transform: function(args) {
      var _coverage = args['coverage'];
      _coverage.product.productKey.location_original = _coverage.product.productKey.location;
      _coverage.product.productKey.location = this.self.transformLocation(_coverage.product.productKey.location);
      /*var partyTable = Utils.iterator(_coverage.parties.party);
      for (partyi in partyTable) {
          var partyRec = partyTable[partyi];
          partyRec.insuredSex_original = partyRec.insuredSex;
          partyRec.insuredSex = this.transformGender(partyRec.insuredSex);
          partyRec.smokingStatus_original = partyRec.smokingStatus;
          partyRec.smokingStatus = this.transformSmokingStatus(partyRec.smokingStatus);
      }
      _coverage.parties.party=partyTable;*/

      var insured = getPrimaryInsured({coverage: _coverage});
      insured["insuredSex_original"] = insured.insuredSex;
      insured.insuredSex = this.self.transformGender(insured.insuredSex);
      insured["smokingStatus_original"] = insured.smokingStatus;
      insured.smokingStatus = this.self.transformSmokingStatus(insured.smokingStatus);

      _coverage.benefitOption_original = _coverage.benefitOption;
      _coverage.benefitOption = this.self.transformBenefitOption(_coverage.benefitOption);
      if (!Utils.isNothing(_coverage.options)) {
          _coverage.options.paymentMode_original = _coverage.options.paymentMode;
          _coverage.options.paymentMode = this.self.transformPaymentMode(_coverage.options.paymentMode);
          _coverage.options.billingMethod_original = _coverage.options.billingMethod;
          _coverage.options.billingMethod = this.self.transformBillingMode(_coverage.options.billingMethod);
          _coverage.options.dividendOption_original = _coverage.options.dividendOption;
          _coverage.options.dividendOption = this.self.transformDividentOption(_coverage.options.dividendOption);
      }
      return _coverage;
   },
});
/*

	ProductEngine (PE) - Primary JS class

	attributes:
	-----------
	context :          it is the memory space to store all working variables and reference
					   data. Inside context, the following objects can be referred.

					   calcRequest :      it is the request object from call application for validation,
				   						  premium calculation and/or projection
					   catalogs :         catalog of coverage
					   schema :           schema of coverage
					   error:			  last error among function call

	methods:
	--------
	init:              function to load all necessary configurations of products and plans
	validate:          perform validation and return error
	calculatePremiums: perform premium calculations for base plan and all riders
	runProjection:     perform projection for calcRequest

*/

// Default core settings
// PE core module
var PE = function( engine ) {
   return {
      _PE: engine,
      setProposal: function( proposal ) {
         var _proposal = JSON.parse(JSON.stringify(proposal));
         //if (proposal.enableDebug) {
         //    console.log("proposal (input): \n");
         //    console.log(JSON.stringify(_proposal, null, 2));
         //    console.log("---------------");
         //}
         this._PE.call("setProposal", {proposal: _proposal});
      },
      context: function() {
         return this._PE.call("getContext");
      },
      validate: function() {
         return this._PE.call("validate");
      },
      listBaseProducts: function(planCodes, language, effectiveDate, location) {
         return this._PE.call("listBaseProducts", {planCodes: planCodes, language: language, effectiveDate: effectiveDate, location: location});
      },
      listRiders: function(planCodes, language, effectiveDate, location) {
         console.log('Plan Codes: ' + planCodes);
         return this._PE.call("listRiders", {planCodes: planCodes, language: language, effectiveDate: effectiveDate, location: location});
      },
      calculateInsuredAge: function(dateOfBirth, policyYearDate) {
         return this._PE.call("calculateInsuredAge", dateOfBirth, policyYearDate);
      },
      calculatePremiums: function() {
         return this._PE.call("calculatePremiums");
      },
      runProjection: function( projectionOptions ) {
         return this._PE.call("runProjection", {projectionOptions: projectionOptions});
      }
   };
}

var eventAccumulatePolicyValues = Class.define({
   eventName: function(args) {
    return "Accumulate Policy Values (Local)";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    return true;
},
   run: function(args) {
    var context = args["context"];
    var runningPolicyValues = context.runningPolicyValues;
    var basePlan = context.proposal.coverageInfo;
    var months = runningPolicyValues.months;
    var year = runningPolicyValues.year;
    var month = runningPolicyValues.months % 12;

    var accFunds = DataDrivenCalculatorImpl.create({
        drivenKeys: PublicConstants.FUND_RETURN,
        drivenDatas: [runningPolicyValues["annualTotalFundBalances"], IFund.appliedTo(runningPolicyValues.fundPool).call("fundBalances", {}), IFund.appliedTo(runningPolicyValues.cashoutAccount).call("fundBalances", {})]
    });
    var accBalances = IDataDrivenCalculator.appliedTo(accFunds).call("calculate", {
        calculateBlock: function(funds) {
            var accumFund = funds[0];
            var poolFund = funds[1];
            var cashFund = funds[2];
            //var balance = Number(Number(Number(accumFund) + Number(poolFund) + Number(cashFund)).toFixed(context.precision)).valueOf();
   
            var balance = _R(Number(accumFund) + Number(poolFund) + Number(cashFund), context.precision);

            _DBR("balance = accumFund(" + accumFund + ") + poolFund(" + poolFund + ") + cashFund(" + cashFund + ")", balance, "eventAccumulateFundBalances->", runningPolicyValues.months);
            return balance;
        }
    });

    runningPolicyValues["annualTotalFundBalances"] = accBalances;

    if (month==0 && basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.ANNUAL) {
        runningPolicyValues.totalPremiumRequested += + basePlan.plannedPremium;
    } else
    if ((month==0 || month==6) && basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.SEMIANNUAL) {
        runningPolicyValues.totalPremiumRequested += + basePlan.plannedPremium;
    } else
    if ((month==0 || month==3 || month==6 || month==9) && basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.QUARTERLY) {
        runningPolicyValues.totalPremiumRequested += + basePlan.plannedPremium;
    } else
    if (basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.MONTHLY) {
        runningPolicyValues.totalPremiumRequested += + basePlan.plannedPremium;
    }
    _DBR("annualTotalFundBalances", runningPolicyValues['annualTotalFundBalances'], "eventAccumulatePolicyValues->", months);
    _DBR("totalPremiumRequested", runningPolicyValues['totalPremiumRequested'], "eventAccumulatePolicyValues->", months);
}
});
var eventApplyPolicyChangePlannedPremium = Class.define({
   eventName: function(args) {
    return "Apply New Planned Premium";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    return true;
},
   run: function(args) {
    var context = args["context"];
    var running = context.runningPolicyValues;
    var year = running.year;
    var months = running.months;
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    var primary = getPrimaryInsured( {coverage: basePlan} );
    var activities = _V( _PV( proposal, "fundActivities.fundActivity" ), [ ] );
    for(var actIdx in activities) {
        var activity = activities[actIdx];
        if (activity.attainAge == (primary.insuredAge + year)) {
            this.self.applyPolicyChange( {args:args, activity:activity} );
        }
    }
    return;
},
   applyPolicyChange: function(args) {
    var vargs = args['args'];
    var activity = args['activity'];
    
    var context = vargs["context"];
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    
    if (!Utils.isNothing(activity.plannedPremium)) {
       basePlan.plannedPremium = activity.plannedPremium;
    }
}
});
var eventAvyInitializationLocal = Class.define({
   eventName: function(args) {
    return "AVY Initialization (Local)";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    return true;
},
   run: function(args) {
    var context = args['context'];
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    var metadata = context.events.metadata;
    var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
    var runningPolicyValues = context.runningPolicyValues;
    var regularPremium = basePlan.plannedPremium;

    context['precision'] = 2;
    context['interestRatePrecision'] = 7;

    var year = runningPolicyValues.year;
    var months = runningPolicyValues.months;
    var month = months % 12;
    var fundConfig;
    fundConfig = {
        fundId: "Cash"
    };
    fundConfig['annualInterestRate'] = {};
    fundConfig[PublicConstants.FUND_RETURN.LOW] = Number(0);
    fundConfig[PublicConstants.FUND_RETURN.MEDIUM] = Number(0);
    fundConfig[PublicConstants.FUND_RETURN.HIGH] = Number(0);
    fundConfig.annualInterestRate[PublicConstants.FUND_RETURN.LOW] = Number(0);
    fundConfig.annualInterestRate[PublicConstants.FUND_RETURN.MEDIUM] = Number(0);
    fundConfig.annualInterestRate[PublicConstants.FUND_RETURN.HIGH] = Number(0);
    fundConfig['policyOptions'] = context.policyOptions;

    runningPolicyValues['cors'] = {};
    for (returnTypeKey in PublicConstants.FUND_RETURN) {
        runningPolicyValues['cors'][returnTypeKey] = Number(0);
    }

    if (months == 0) {
        runningPolicyValues['lapse'] = {};
        for (returnTypeKey in PublicConstants.FUND_RETURN) {
            runningPolicyValues['lapse'][returnTypeKey] = "N";
        }

        /* message manager -> */
        //var mm = Repository.getManager(SystemComponents.MANAGER.MESSAGE_MANAGER);
        //_SS(mm.implementation.messages[0],2);

        /* create fund accounts */
        if (!Utils.isNothing(proposal.funds.fundRecord)) {
            runningPolicyValues['fundPool'] = FundPoolImpl.create({
                context: context
            });

            //_SS(runningPolicyValues['fundPool'], 10)
            // unremark if profolio is used.
            // this.self.context.runningPolicyValues['fundPool'] = PortfolioImpl.create({
            //    context: context
            // });
        }

        runningPolicyValues['cashoutAccount'] = FundImpl.create({
            fundConfig: fundConfig,
            context: context
        });

        /* list available rates */
        _DB("rates", context.rates, "@@@@@@@@");

        /* initialize accumulated policy values */
        runningPolicyValues['totalPremiums'] = 0;
        runningPolicyValues['totalWithdrawal'] = 0;
        runningPolicyValues['totalTopup'] = 0;
        runningPolicyValues['totalPremiumRequested'] = 0;
        runningPolicyValues['wocTargetPremium'] = 0;
        /* initialize product type factors */
        var isRegularSaving = Utils.matchInList(CatalogConstants.PRODUCT_TYPE.REGULAR_SAVING, basePlan.catalog.call("getCatalog", {}).ProductCatalog.ProductType.ProductTypeCode, false);
        var isSinglePremium = Utils.matchInList(CatalogConstants.PRODUCT_TYPE.SINGLE_PREMIUM, basePlan.catalog.call("getCatalog", {}).ProductCatalog.ProductType.ProductTypeCode, false);
        var isUVL = Utils.matchInList(CatalogConstants.PRODUCT_TYPE.UVLIFE, basePlan.catalog.call("getCatalog", {}).ProductCatalog.ProductType.ProductTypeCode, false);

        if (isUVL) {
            if (isRegularSaving) {
                runningPolicyValues['initialFaceAmountRatio'] = Number(basePlan.faceAmount / basePlan.plannedPremium).toFixed(2);
                console.log("initialFaceAmountRatio (RS) = " + runningPolicyValues.initialFaceAmountRatio);
            }
            if (isSinglePremium) {
                runningPolicyValues['initialFaceAmountRatio'] = Number(basePlan.faceAmount / basePlan.initialDumpIn).toFixed(2);
                console.log("initialFaceAmountRatio (SP) = " + runningPolicyValues.initialFaceAmountRatio);
            }
        }

        if (isUVL) {
            IFund.appliedTo(runningPolicyValues.fundPool).call("dumpin", {
                amount: 0
            });
            IFund.appliedTo(runningPolicyValues.cashoutAccount).call("dumpin", {
                amount: 0
            });
        }

        /* calculate planned premiums */
        if (basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.ANNUAL) {
            regularPremium = Number(basePlan.plannedPremium);
            _DBR("regularPremium", regularPremium, "eventReceivePremiumsLocal->", months);
        } else
        if (basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.SEMIANNUAL) {
            regularPremium = Number(basePlan.plannedPremium) * 2;
            _DBR("regularPremium", regularPremium, "eventReceivePremiumsLocal->", months);
        } else
        if (basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.QUARTERLY) {
            regularPremium = Number(basePlan.plannedPremium) * 4;
            _DBR("regularPremium", regularPremium, "eventReceivePremiumsLocal->", months);
        } else
        if (basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.MONTHLY) {
            regularPremium = Number(basePlan.plannedPremium) * 12;
            _DBR("regularPremium", regularPremium, "eventReceivePremiumsLocal->", months);
        }
        runningPolicyValues["regularPremium"] = regularPremium;
    }

    if (month == 0) {
        runningPolicyValues['initialDumpIn'] = 0;
        runningPolicyValues['newPremium'] = 0;
        runningPolicyValues['netPremium'] = 0;
        runningPolicyValues['withdrawal'] = 0;
        runningPolicyValues['firstMonthCoi'] = Number(0);
        runningPolicyValues['cashOutPayments'] = {};
        runningPolicyValues['cois'] = {};
        runningPolicyValues['annualCois'] = {};
        runningPolicyValues['accumulatedcashOutPayments'] = {};
        runningPolicyValues["annualTotalFundBalances"] = {};
        runningPolicyValues['topup'] = 0;
        runningPolicyValues["surrenderValues"] = {};
        runningPolicyValues['deathBenefits'] = {};

        // ATP-500
        // Set default policyFee
        runningPolicyValues['policyFee'] = 0;

        for (returnTypeKey in PublicConstants.FUND_RETURN) {
            runningPolicyValues["annualTotalFundBalances"][returnTypeKey] = Number(0);
            runningPolicyValues['cashOutPayments'][returnTypeKey] = Number(0);
            runningPolicyValues['cois'][returnTypeKey] = Number(0);
            runningPolicyValues['annualCois'][returnTypeKey] = Number(0);
            runningPolicyValues['accumulatedcashOutPayments'][returnTypeKey] = Number(0);
            runningPolicyValues['surrenderValues'][returnTypeKey] = Number(0);
            runningPolicyValues['deathBenefits'][returnTypeKey] = Number(0);
        }
    }

    if (months == 0) {
        var pc = Repository.getManager(SystemComponents.MANAGER.PREMIUM_CALCULATOR);
        var premiums = pc.call("calculate", {
            proposal: proposal
        });
        runningPolicyValues['premiums'] = premiums;

        var targetPremiums = 0;
        for (ii in premiums.basePlan.totalPremiums) {
            var premiumRec = premiums.basePlan.totalPremiums[ii];
            if (premiumRec.paymentMode == basePlan.options.paymentMode) {
                targetPremiums = targetPremiums + premiumRec.totalPremium;
            }
        }
        for (jj in premiums.riders) {
            var riderRec = premiums.riders[jj];
            //_SS(riderRec, 10);

            for (ii in riderRec.premiums.totalPremiums) {
                var premiumRec = riderRec.premiums.totalPremiums[ii];
                if (premiumRec.paymentMode == basePlan.options.paymentMode) {
                    targetPremiums = targetPremiums + premiumRec.totalPremium;
                }
            }
        }
        runningPolicyValues['targetPremiums'] = targetPremiums;
        
        //_SS(premiums, 10);
        _DBR("targetPremiums (WOC)", targetPremiums, "eventAvyInitializationLocal.", runningPolicyValues.months);
    }
}
});
var eventCalculateCOR_XXXXX = Class.define({
   eventName: function(args) {
    return "Calculate COR of Riders (BPM41)";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    var projectionOptions = context.projectionOptions;
    return (projectionOptions.indexOf("BasePlanWithRiders") >= 0);
},
   run: function(args) {
    var context = args["context"];
    var running = context.runningPolicyValues;
    var month = running.months % 12;
    var months = running.months;
    var year = running.year;
    var proposal = context.proposal;

    var cor = {};
    for (returnType in PublicConstants.FUND_RETURN) {
        cor[returnType] = {};
        for (run = 0; run < 2; run++) {
            for (riderIdx in proposal.riders.coverageInfo) {
                var rider = proposal.riders.coverageInfo[riderIdx];
                var schema = rider.schema.call("getSchema", {});

                var isBenefit = rider.catalog.call("isBenefit", {});
                if ((run == 0 && !isBenefit) || (run == 1 && isBenefit)) {
                    // when run == 0, all riders will be calculated first
                    // when run == 1, only benefit riders will be calculated
                    for (idx in schema.ProductSchema.PremiumInfo.PremiumTable) {
                        var premInfo = schema.ProductSchema.PremiumInfo.PremiumTable[idx];
                        if (premInfo.PremiumType.text == SchemaConstants.PREMIUM_TYPE.COSTOFINSURANCE) {
                            //if (months == 0 && returnType == PublicConstants.FUND_RETURN.LOW) {
                            //    _SS(premInfo, 5);
                            //}
                            var thisMethod = this.self.calculateMethod({
                                    context: context,
                                    rider: rider
                                });

                            //_DBR("premiumInfo", premInfo.Method.text, "", months);
                            //_DBR("thisMethod", thisMethod, "", months);
                            if (premInfo.Method.text == thisMethod) {
                                var shouldPay = this.self.shouldPayCOR({
                                    context: context,
                                    rider: rider
                                }); 
                                var corValue = 0;
                                if (shouldPay) {
                                    var corValue = this.self.calculateMonthlyCOR({
                                        context: context,
                                        rider: rider,
                                        returnType: returnType
                                    });
                                }
                                cor[returnType][schema.ProductSchema.ProductSchemaPK.ProductPK.ProductId.text] = corValue;
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

    for(returnType in PublicConstants.FUND_RETURN) {
        for(riderProductId in cor[returnType]) {
            running['cors'][returnType] += cor[returnType][riderProductId];
        }
    }

    running['cors']
    running['corsDetails'] = cor;
 
    var showValue = proposal.enableDebug;
    if ( showValue && year < proposal.startDebugYear ) {
        showValue = false;
    }
    if ( showValue && year > proposal.stopDebugYear ) {
        showValue = false;
    }
    if (showValue) {
        console.log("[  CORs by Product  ]");  
        _SS(cor, 5);
        console.log("[  Total CORs  ]");  
        _SS(running['cors'], 5);
    }
    return;
},
   calculateMonthlyCOR: function(args) {
   // to be overrided by child class
   //
   var context = args['context'];
   var running = context.runningPolicyValues;
   var rider = args['rider'];
   var rates = context['rates'];
   var riderProductId = rider.product.productKey.primaryProduct.productPK.productId;
   var year = running.year;

   return 0.0;
},
   calculateMethod: function(args) {
    // calculation method e.g. COR01
    return "XXXXX"
},
   shouldPayCOR: function(args) {
    var context = args['context'];
    var rider = args['rider'];
    var year = context.runningPolicyValues.year;
    var months = context.runningPolicyValues.months;

    var ppp = rider.schema.call('calculatePremiumPayingPeriod', { coverage: rider });

    _DBR("ppp", ppp, "eventCalculateCOR_XXXXX.shouldPayCOR", months);
    _DBR("year", year, "eventCalculateCOR_XXXXX.shouldPayCOR", months);
    _DBR("year <= ppp",(year <= ppp), "eventCalculateCOR_XXXXX.shouldPayCOR", months);
    return (year <= ppp);
}
});
var eventCalculateDeathBenefitUVL = Class.define({
   eventName: function(args) {
    return "Calculate Death Benefit UVL";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    return true;
},
   run: function(args) {
    var context = args["context"];
    var metadata = context.events.metadata;
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    var runningPolicyValues = context.runningPolicyValues;

    for (returnTypeIdx in PublicConstants.FUND_RETURN) {
        var returnType = PublicConstants.FUND_RETURN[returnTypeIdx];
        var db = basePlan.faceAmount;
        var isLapse = runningPolicyValues.lapse[returnTypeIdx];
        if (isLapse == 'Y') {
            runningPolicyValues.deathBenefits[returnType] = 0;
        } else {
            runningPolicyValues.deathBenefits[returnType] = db;
        }
    }
}
});
var eventCalculateFundBonusUL007 = Class.define({
   eventName: function(args) {
    return "Calculate Fund Bonus (UL007)";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    var runningPolicyValues = context.runningPolicyValues;
    var year = runningPolicyValues.year;
    var month = runningPolicyValues.months % 12;
    
    return (year >= 4 && month == 11);
},
   run: function(args) {
    var context = args["context"];
    var running = context.runningPolicyValues;
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;

    /* accumulated values */
    var totalPremiums = running.totalPremiums;
    var totalWithdrawal = running.totalWithdrawal;
    var totalTopup = running.totalTopup;

    /* annualised values */
    var newPremium = running.newPremium;
    var withdrawal = running.withdrawal;
    var topup = running.topup;

    // test eligibility of bonus
    var totalPremiumPaid = newPremium;
    var withdrawalAmount = withdrawal;
    var requestPremiumPaid = running.regularPremium;
    if (running.months == 59) {
        totalPremiumPaid = totalPremiums;
        withdrawalAmount  = totalWithdrawal;
        requestPremiumPaid = running.totalPremiumRequested;
    }
    var fundBonusCalc = DataDrivenCalculatorImpl.create({
        drivenKeys: PublicConstants.FUND_RETURN,
        drivenDatas: [ running.annualTotalFundBalances, running.lapse ]
    });
    var eligible = (totalPremiumPaid >= requestPremiumPaid && withdrawalAmount == 0);
    _DBR("totalPremiumPaid(" + totalPremiumPaid + ") >= requestPremiumPaid(" + requestPremiumPaid + ") && withdrawalAmount(" + withdrawalAmount + ") == 0", eligible, "eventCalculateFundBonusUL007->", running.months);
    var bonus = IDataDrivenCalculator.appliedTo(fundBonusCalc).call("calculate", {
        calculateBlock: function(args) {
            var fundBalance = args[0];
            var lapse = args[1];

            if (lapse == "Y") return 0;

            if (eligible) {
                var annualBalance = fundBalance;
                fundBonus = _R2(Number(0.005) * Number(annualBalance) / 12);
                _DBR("fundBonus = R2(0.5% * annualBalance(" + annualBalance + ")/12)", fundBonus, "eventCalculateFundBonusUL007->", running.months);
                return fundBonus;
            } else {
                return 0;
            }
        }
    });
    running['fundBonus'] = bonus;
}
});
var eventCalculateNARLocal = Class.define({
   eventName: function(args) {
    return "eventCalculateNARLocal";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    return true;
},
   run: function(args) {
    var context = args["context"];

    var metadata = context.events.metadata;
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    var runningPolicyValues = context.runningPolicyValues;

    // ---------- Caculate lienForJuveniles -------------------

    // Set lienForJuveniles rates
    var lienForJuvenilesRate = {
        "rates": [.2, .4, .6, .8, 1]
    };

    var age = Number(context.insuredAge) + Number(context.runningPolicyValues.year);

    // Calculate lienForJuveniles value base on age
    var lienForJuvenilesValue = lienForJuvenilesRate.rates[age] || 1;
    // --------------------------------------------------------

    var narCalc = DataDrivenCalculatorImpl.create({
        drivenKeys: PublicConstants.FUND_RETURN,
        drivenDatas: [IFund.appliedTo(runningPolicyValues.fundPool).call("fundBalances", {}),
            IFund.appliedTo(runningPolicyValues.cashoutAccount).call("fundBalances", {}),
            runningPolicyValues.lapse
        ]
    });
    var nars = IDataDrivenCalculator.appliedTo(narCalc).call("calculate", {
        calculateBlock: function(args) {
            var isLapse = args[2];

            _DBR("dbLevel", basePlan.options.dbLevel, "eventCalculateNAR->", context.runningPolicyValues.months);
            _DBR("lapse", args[2], "eventCalculateNAR->", context.runningPolicyValues.months);

            if (isLapse == 'Y') return 0;

            if (basePlan.options.dbLevel == SchemaConstants.DB_OPTION.INCREASE) {
                if (basePlan.catalog.call("isSinglePremium", {})) {
                    var nar = _R(runningPolicyValues.totalPremiums * runningPolicyValues.initialFaceAmountRatio, 15);
                    _DBR("fa = totalPremiums(" + runningPolicyValues.totalPremiums + ") * initialFaceAmountRatio(" + runningPolicyValues.initialFaceAmountRatio + ")", nar, "eventCalculateNAR->", context.runningPolicyValues.months);
                    return nar;
                } else {

                    // Comment out this code for fixing issue age < 4
                    // var nar = Number(basePlan.faceAmount);

                    // Calculate nar with lienForJuvenilesValue
                    var nar = Number(basePlan.faceAmount) * lienForJuvenilesValue;

                    _DBR("fa = faceAmount(" + basePlan.faceAmount + ")", nar, "eventCalculateNAR->", context.runningPolicyValues.months);
                    return nar;
                }
                _DBR("NAR", narValue, "eventCalculateNAR->", context.runningPolicyValues.months);
                return narValue
            }
            if (basePlan.options.dbLevel == SchemaConstants.DB_OPTION.LEVEL) {
                var fundBalance = args[0];
                var cashOutAccountBalance = args[1];
                var totalFundBalance = Number(fundBalance) + Number(cashOutAccountBalance);
                var narValue = 0;
                _DBR("fundBalance", fundBalance, "eventCalculateNAR->", context.runningPolicyValues.months);
                _DBR("cashOutAccountBalance", cashOutAccountBalance, "eventCalculateNAR->", context.runningPolicyValues.months);
                _DBR("totalFundBalance", totalFundBalance, "eventCalculateNAR->", context.runningPolicyValues.months);
                if (basePlan.catalog.call("isSinglePremium", {})) {
                    var fa = _R(runningPolicyValues.totalPremiums * runningPolicyValues.initialFaceAmountRatio, 15);
                    var nar = Math.max(Number(0), _R(Number(fa) - Number(totalFundBalance), 15));
                    //console.log("months :" + runningPolicyValues.months + ", fundBalance :" + fundBalance + " ,cahoutAccountBalance :" + cahoutAccountBalance + " ,fa :" + fa + ", nar :" + nar);
                    _DBR("fa = Max(0, totalPremiums(" + runningPolicyValues.totalPremiums + ") * initialFaceAmountRatio(" + runningPolicyValues.initialFaceAmountRatio + ") - totalFundBalance(" + totalFundBalance + "))", nar, "eventCalculateNAR->", context.runningPolicyValues.months);
                    return nar;
                } else {
                    // Comment out this code for fixing issue age < 4
                    // var nar = _R(Math.max(Number(0), Number(basePlan.faceAmount) - Number(totalFundBalance)), 15);

                    // Calculate nar with lienForJuvenilesValue
                    var nar = _R(Math.max(Number(0), Number(basePlan.faceAmount) * lienForJuvenilesValue - Number(totalFundBalance)), 15);

                    _DBR("fa = Max(0, faceAmount(" + basePlan.faceAmount + ") - totalFundBalance(" + totalFundBalance + "))", nar, "eventCalculateNAR->", context.runningPolicyValues.months);
                    return nar;
                }
            }
        }
    });
    runningPolicyValues['nars'] = nars;
}
});
var eventCalculatePolicyFeeUL007 = Class.define({
   eventName: function(args) {
    return "Calculate Monthly Policy Fee (UL007)";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    return true;
},
   run: function(args) {
    var context = args["context"];
    var metadata = context.events.metadata;
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    var year = context.runningPolicyValues.year;
    var months = context.runningPolicyValues.months;
    var runningPolicyValues = context.runningPolicyValues;

    var policyFees = context.rates[basePlan.product.productKey.primaryProduct.productPK.productId][SchemaConstants.CHARGE_TYPE.POLICYFEE];
    var effectiveDate = policyFees.header.effectiveDate;
    var issueDate = proposal.policyYearDate.date();
    
    var effectDate = new Date(effectiveDate);
    
    var orgYear = 2016;
    var currDate = issueDate; 
    currDate.setMonth(issueDate.getMonth() + months);
    var currYear  = currDate.getFullYear();

    // since the rate is from 20160101 and the rate should be increased by $2 for each year after 2016 and cap at $60
    var fee = Math.min(60, policyFees.rates[0] + (currYear - orgYear) * 2);
    _DBR("Policy Fee = Math.min(60, policyFees.rates[0] (" + policyFees.rates[0] +  ") + (currYear (" + currYear + ") - orgYear (" + orgYear + ")) * 2)", fee, "eventDeductMonthlyPolicyFeeUL007->", months);

    var fees = {};
    for(fundIdx in PublicConstants.FUND_RETURN) {
        if (runningPolicyValues.lapse[fundIdx] == 'Y') {
            fees[fundIdx] = 0;
        } else {
            fees[fundIdx] = fee;
        }
    }

    runningPolicyValues['policyFees'] = fees;
}
});
var eventCalculateSurrenderValuesUL007 = Class.define({
   eventName: function(args) {
    return "Calculate Surrender Value (UL007)";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    return true;
},
   run: function(args) {
    var context = args["context"];
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
    var running = context.runningPolicyValues;
    var year = running.year;

    var surrenderRates = context.rates[productId][SchemaConstants.CHARGE_TYPE.SURRENDER];
    //_D("surrenderRates", surrenderRates, "");

    var surrenderCharges = _R(Number(running.regularPremium) * Number(surrenderRates.rates[year]), context.precision);
    _DBR("surrenderCharges = regularPremium(" + running.regularPremium + ") * surrenderRate(" + surrenderRates.rates[year] + ")", surrenderCharges, "eventCalculateSurrenderChargesUL007->", running.months);

    var fundBalances = IFund.appliedTo(running.fundPool).call("fundBalances", {});
    running["surrenderCharges"] = surrenderCharges;
    for(returnTypeIdx in PublicConstants.FUND_RETURN) {
        var returnType = PublicConstants.FUND_RETURN[returnTypeIdx];

        var surrenderValue = fundBalances[returnType] - surrenderCharges;
        running["surrenderValues"][returnType] = ((surrenderValue < 0) ? 0 : surrenderValue);
    }
    _DBR("surrenderValues", running["surrenderValues"], "eventCalculateSurrenderChargesUL007->", running.months);
}
});
var eventCalculateTotalDeathBenefitLocal = Class.define({
   eventName: function(args) {
    return "Calculate Total Death Benefit Local";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    return true;
},
   run: function(args) {
    var context = args["context"];

    var metadata = context.events.metadata;
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    var runningPolicyValues = context.runningPolicyValues;

    var narCalc = DataDrivenCalculatorImpl.create({
      drivenKeys: PublicConstants.FUND_RETURN,
      drivenDatas: [runningPolicyValues.deathBenefits, IFund.appliedTo(runningPolicyValues.fundPool).call("fundBalances", {}), IFund.appliedTo(runningPolicyValues.cashoutAccount).call("fundBalances", {})]
    });

    // Set lienForJuveniles rates
    var lienForJuvenilesRate = {
      "rates": [.2, .4, .6, .8, 1]
    };

    var age = Number(context.insuredAge) + Number(context.runningPolicyValues.year);

    // Calculate lienForJuveniles value base on age
    var lienForJuvenilesValue = lienForJuvenilesRate.rates[age] || 1;

    var totalDeathBenefits = IDataDrivenCalculator.appliedTo(narCalc).call("calculate", {
      calculateBlock: function(data) {
        var db = data[0];
        var fundBalance = data[1];
        var cashOutAccountBalance = data[2];
        var totalFundBalance = Number(fundBalance) + Number(cashOutAccountBalance);
        var tdb = 0;
        if (basePlan.options.dbLevel == SchemaConstants.DB_OPTION.LEVEL) {
          // Caculate deathBenefits with lienForJuveniles
          tdb = Math.max(_R2(db), _R2(totalFundBalance)) * lienForJuvenilesValue;

          _DBR("TDB = Max(R2(DB(" + db + ")), R2(totalFundBalance(" + totalFundBalance + ")))", tdb, "eventCalculateDeathBenefit->", runningPolicyValues.months);
        } else {
          // Caculate deathBenefits with lienForJuveniles
          tdb = _R2(Number(db) * lienForJuvenilesValue + Number(totalFundBalance));

          _DBR("TDB = DB(" + db + ") + totalFundBalance(" + totalFundBalance + ")", tdb, "eventCalculateDeathBenefit->", runningPolicyValues.months);
        }
        return tdb;
      }
    });
    runningPolicyValues['totalDeathBenefits'] = totalDeathBenefits;
}
});
var eventCalculateTradPlanCashValue = Class.define({
   eventName: function(args) {
    return "Calculate Trad Plan Cash Value";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    return true;
},
   run: function(args) {
    var context = args["context"];
    var running = context.runningPolicyValues;
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
    var csvRates = context.rates[productId][SchemaConstants.POLICY_VALUE.CASHVALUE];
    //_DBR("csvRates", csvRates, "eventCalculateTradPlanCashValue->", running.months)
    var csvRate = csvRates.rates[running.year];
    var csv = basePlan.faceAmount * csvRate / 1000;
    
    running.cashValue = csv;
    return;
}
});
var eventCalculateTradPlanCIBenefitENCXXX = Class.define({
   eventName: function(args) {
    return "Calculate Trad Plan Death Benefit (ENCXXX)";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    return true;
},
   run: function(args) {
    var context = args["context"];
    var running = context.runningPolicyValues;
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    var year = running.year;
    var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
    
    var lienRates = context.rates[productId][SchemaConstants.POLICY_VALUE.PROTECTION];
    var lienRate = lienRates.rates[year];

    var ciBenefit = basePlan.faceAmount * 1.25 * lienRate;
    running.ciBenefit = ciBenefit;
    return;
}
});
var eventCalculateTradPlanCouponENCXXX = Class.define({
   eventName: function(args) {
    return "Calculate Trad Plan Coupon (ENCXXX)";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    var running = context.runningPolicyValues;
    var insured = getPrimaryInsured( {coverage: context.proposal.coverageInfo} );
    return (( insured.issueAge + running.year ) == 75);
},
   run: function(args) {
    var context = args["context"];
    var running = context.runningPolicyValues;
    running.coupon = context.proposal.coverageInfo.faceAmount;
    return;
}
});
var eventCalculateTradPlanDeathBenefitENCXXX = Class.define({
   eventName: function(args) {
    return "Calculate Trad Plan Death Benefit (ENCXXX)";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    return true;
},
   run: function(args) {
    var context = args["context"];
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    var running = context.runningPolicyValues;
    var year = running.year;
    var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
    
    var lienRates = context.rates[productId][SchemaConstants.POLICY_VALUE.PROTECTION];
    var lienRate = lienRates.rates[year];
    
    //_DBR("lienRates", lienRates.rates, "eventCalculateTradPlanDeathBenefitENCXXX", running.months);
    
    running.deathBenefit = basePlan.faceAmount * lienRate;
    return;
}
});
var eventCalculateTradPlanDividends = Class.define({
   eventName: function(args) {
    return "Calculate Trad Plan Dividends";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    return true;
},
   run: function(args) {
    var context = args["context"];
    var running = context.runningPolicyValues;
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
    var dividendRates = context.rates[productId][SchemaConstants.POLICY_VALUE.DIVIDEND];
    //_DBR("csvRates", csvRates, "eventCalculateTradPlanCashValue->", running.months)
    var dividendRate = dividendRates.rates[running.year];
    var dividend = basePlan.faceAmount * dividendRate / 1000;
    
    running.dividends = dividend;
    return;
}
});
var eventDeductCOR = Class.define({
   eventName: function(args) {
    return "Deduct CORs from fund";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    return true;
},
   run: function(args) {
    var context = args["context"];
    var proposal = context.proposal;
    var runningPolicyValues = context.runningPolicyValues;

    var fundBalances = IFund.appliedTo(runningPolicyValues.fundPool).call("fundBalances", {});
    var cors = {};
    for( corIdx in runningPolicyValues.cors ) {
        if (runningPolicyValues.lapse[corIdx] == 'Y') {
            cors[corIdx] = 0;
        } else {
            cors[corIdx] = Math.min(fundBalances[corIdx], runningPolicyValues.cors[corIdx]);
        }
    }

    IFund.appliedTo(runningPolicyValues.fundPool).call("withdraw", {
        amounts: cors
    });

}
});
var eventDeductMonthlyPolicyFeeUL007 = Class.define({
   eventName: function(args) {
    return "Deduct Monthly Policy Fee (UL007)";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    return true;
},
   run: function(args) {
    var context = args["context"];
    var runningPolicyValues = context.runningPolicyValues;

    var policyFees = runningPolicyValues['policyFees'];

    // ATP-488
    // #1 - comment out this code - it seem raise issue gap data
    // var fee = policyFees[ PublicConstants.FUND_RETURN.HIGH ];

    // ATP-488  Replace this code for #1 for fixing issue gap data
    var fee = policyFees[PublicConstants.FUND_RETURN.LOW];

    var fees = {};
    var fundBalances = IFund.appliedTo(runningPolicyValues.fundPool).call("fundBalances", {});
    var cashOutBalances = IFund.appliedTo(runningPolicyValues.cashoutAccount).call("fundBalances", {});
    for(fundIdx in fundBalances) {
        var balance = fundBalances[fundIdx] + cashOutBalances[fundIdx];
        if (runningPolicyValues.lapse[fundIdx] == 'Y') {
            fees[fundIdx] = 0;
        } else {
            fees[fundIdx] = Math.min( policyFees[fundIdx], balance );
        }
    }

    IFund.appliedTo(runningPolicyValues.fundPool).call("withdraw", {
         amounts: fees
    });

    /*
    * ATP-500
    * Calculate policy fee for each year.
    * Summary 12 months of policy fee and reset policy fee when month is 0
    */
    runningPolicyValues['policyFee'] += fee;
}
});
var eventDumpinFundBonusUL007 = Class.define({
   eventName: function(args) {
    return "Dump in Fund Bonus (UL007)";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    var runningPolicyValues = context.runningPolicyValues;
    var year = runningPolicyValues.year;
    var month = runningPolicyValues.months % 12;
    
    return (year >= 4 && month == 11);
},
   run: function(args) {
    var context = args["context"];
    var running = context.runningPolicyValues;
    var fundBonus = running.fundBonus;
    IFund.appliedTo(running.fundPool).call("dumpin", {
        amounts: fundBonus
    });
}
});
var eventReceiveInitialPremiumLocal = Class.define({
   eventName: function(args) {
    return "Receive Initial Premium (Local)";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    return true;
},
   run: function(args) {
    var context = args["context"];

    var metadata = context.events.metadata;
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
    var runningPolicyValues = context.runningPolicyValues;

    var year = runningPolicyValues.year;
    var months = runningPolicyValues.months;
    var month = months % 12;

    var newPremium = this.self.newPremiumReceived(args);
    var netNewPremium = this.self.netPremiumReceived({ args: args, newPremium: newPremium });
    runningPolicyValues.newPremium = Number(runningPolicyValues.newPremium) + Number(newPremium);
    runningPolicyValues.totalPremiums = Number(runningPolicyValues.totalPremiums) + Number(newPremium);

    if (Number(netNewPremium) > 0) {
        var netNewPremiums = {};
        for(returnTypeIdx in PublicConstants.FUND_RETURN) {
            var returnType = PublicConstants.FUND_RETURN[returnTypeIdx];

            var isLapse = runningPolicyValues.lapse[returnType];
            if (isLapse == 'Y') {
                netNewPremiums[returnType] = 0;
            } else {
                netNewPremiums[returnType] = Number(netNewPremium);
            }
        }

        _DBR("netNewPremiums", netNewPremiums, "$$$$$ eventReceiveXXXXXXPremiumLocal->");

        IFund.appliedTo(runningPolicyValues.fundPool).call("dumpins", {
            amounts: netNewPremiums
        });
        _DBR("totalPremiums", runningPolicyValues['totalPremiums'], "eventReceiveXXXXXPremiumLocal->", months);
    }
},
   newPremiumReceived: function(args) {
    var context = args["context"];

    var metadata = context.events.metadata;
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    var runningPolicyValues = context.runningPolicyValues;
    var months = runningPolicyValues.months;
    var newPremium = 0;

    if (months == 0) {
        if (!Utils.isNothing(basePlan.initialDumpIn)) {
            newPremium = Number(basePlan.initialDumpIn);
        }
        _DBR("initialPremium", newPremium, "eventReceiveXXXXXPremiumLocal->", months);
    }
    
    return newPremium;
},
   netPremiumReceived: function(args) {
    var vargs = args['args'];
    var newPremium = args['newPremium'];

    var context = vargs["context"];

    var metadata = context.events.metadata;
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
    var runningPolicyValues = context.runningPolicyValues;

    var year = runningPolicyValues.year;
    var months = runningPolicyValues.months;
    //var month = months % 12;

    //var newPremium = this.self.newPremiumReceived(vargs);

    var allocationRate = this.self.lookupAllocationRate(vargs);
    var newPremiumRounding = proposal.coverageInfo.catalog.call("rounding", {
         coverage: basePlan
    });

    //var netNewPremium = Number(Number(newPremium * (1 - allocationRate).toFixed(4)).toFixed(newPremiumRounding));
    var netNewPremium = _R(newPremium * _R4(1 - allocationRate), newPremiumRounding);
    if (netNewPremium > 0) {
        _DBR("netNewPremium", netNewPremium, "eventReceiveXXXXXPremiumLocal->", months);
    }
    runningPolicyValues['netPremium'] += netNewPremium;
    return netNewPremium;
},
   lookupAllocationRate: function(args) {
    var context = args["context"];

    var metadata = context.events.metadata;
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
    var runningPolicyValues = context.runningPolicyValues;

    var year = runningPolicyValues.year;
    var months = runningPolicyValues.months;
    var allocationRate = 1;

    var allocationRates = context.rates[productId][SchemaConstants.PREMIUM_TYPE.ALLOCATION];
    if (!Utils.isNothing(allocationRates)) {
        allocationRate = allocationRates.rates[year];
    }
    _DBR("allocationRate", allocationRate, "eventReceiveXXXXXPremiumLocal->", months);

    return allocationRate;
}
});
var eventReceivePremiumsLocal = Class.define({
   eventName: function(args) {
    return "Receive All Premiums (Local)";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    var running = context.runningPolicyValues;

    var shouldRun = false;
    for(idx in PublicConstants.FUND_RETURN) {
        shouldRun = (shouldRun || running.lapse[idx] == "N");
    }

    return shouldRun;
},
   run: function(args) {
    var context = args["context"];

    var metadata = context.events.metadata;
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
    var runningPolicyValues = context.runningPolicyValues;
    var newPremium = 0;
    var topupPremium = 0;

    var year = runningPolicyValues.year;
    var months = runningPolicyValues.months;
    var month = months % 12;

    if (month == 0) {
        if (months == 0) {
            if (!Utils.isNothing(basePlan.initialDumpIn)) {
                newPremium = Number(newPremium) + Number(basePlan.initialDumpIn);
            }
        }

        if (!Utils.isNothing(proposal.fundActivities) && !Utils.isNothing(proposal.fundActivities.fundActivity)) {
            var activities = Utils.iterator(proposal.fundActivities.fundActivity);
            var parties = Utils.iterator(basePlan.parties.party);
            for (idx in activities) {
                var activity = activities[idx];
                if (Number(activity.topupPremium) > 0 && Number(activity.attainAge) == (Number(parties[0].insuredAge) + Number(year))) {
                    if (!Utils.isNothing(activity.topupPremium)) {
                        topupPremium = Number(topupPremium) + Number(activity.topupPremium);
                    }
                    break;
                }
            }
        }
        //_DBR("new fund received (topup / initial dumpIn)", newPremium, "eventReceivePremiumsLocal->", months);
    }
    if (month == 0 && basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.ANNUAL) {
        newPremium = Number(newPremium) + Number(basePlan.plannedPremium);
        _DBR("total fund received (include plannedPremium)", newPremium, "eventReceivePremiumsLocal->", months);
    } else
    if ((month % 6) == 0 && basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.SEMIANNUAL) {
        newPremium = Number(newPremium) + Number(basePlan.plannedPremium);
        _DBR("total fund received (include plannedPremium)", newPremium, "eventReceivePremiumsLocal->", months);
    } else
    if ((months % 3) == 0 && basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.QUARTERLY) {
        newPremium = Number(newPremium) + Number(basePlan.plannedPremium);
        _DBR("total fund received (include plannedPremium)", newPremium, "eventReceivePremiumsLocal->", months);
    } else
    if (basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.MONTHLY) {
        newPremium = Number(newPremium) + Number(basePlan.plannedPremium);
        _DBR("total fund received (include plannedPremium)", newPremium, "eventReceivePremiumsLocal->", months);
    }
    if (Number(newPremium) > 0) {
        var allocationRates = context.rates[productId][SchemaConstants.PREMIUM_TYPE.ALLOCATION];

        var netNewPremium = Number(newPremium);
        var newPremiumRounding = proposal.coverageInfo.catalog.call("rounding", {
            coverage: basePlan
        });
        if (!Utils.isNothing(allocationRates)) {
            netNewPremium = Number(Number(newPremium * (1 - allocationRates.rates[year]).toFixed(4)).toFixed(newPremiumRounding));
        }
        _DBR("netNewPremium", netNewPremium, "eventReceivePremiumsLocal->", months);

        if (Number(netNewPremium) > 0) {
            if (months == 0) {
                runningPolicyValues.initialDumpIn = Number(runningPolicyValues.initialDumpIn) + Number(basePlan.initialDumpIn);
            }
            runningPolicyValues.newPremium = Number(runningPolicyValues.newPremium) + Number(newPremium);
            runningPolicyValues.totalPremiums = Number(runningPolicyValues.totalPremiums) + Number(newPremium);
            IFund.appliedTo(runningPolicyValues.fundPool).call("dumpin", {
                amount: netNewPremium
            });
            _DBR("totalPremiums", runningPolicyValues['totalPremiums'], "eventAccumulatePolicyValues->", months);
        }
    }
    if (Number(topupPremium) > 0) {
        var topupAllocationRates = context.rates[productId][SchemaConstants.PREMIUM_TYPE.TOPUP_ALLOCATION];

        var netNewTopupPremium = Number(topupPremium);
        var newPremiumRounding = proposal.coverageInfo.catalog.call("rounding", {
            coverage: basePlan
        });
        if (!Utils.isNothing(topupAllocationRates)) {
            netNewTopupPremium = Number(Number(topupPremium * (1 - topupAllocationRates.rates[year]).toFixed(4)).toFixed(newPremiumRounding));
        }
        _DBR("netNewTopupPremium", netNewTopupPremium, "eventReceivePremiumsLocal->", months);

        if (Number(netNewTopupPremium) > 0) {
            if (months == 0) {
                runningPolicyValues.initialDumpIn = Number(runningPolicyValues.initialDumpIn) + Number(topupPremium);
            }
            runningPolicyValues.topup = Number(runningPolicyValues.topup) + Number(topupPremium);
            runningPolicyValues.totalTopup = Number(runningPolicyValues.totalTopup) + Number(topupPremium);
            IFund.appliedTo(runningPolicyValues.fundPool).call("dumpin", {
                amount: netNewTopupPremium
            });
            _DBR("totalTopup", runningPolicyValues['totalTopup'], "eventAccumulatePolicyValues->", months);
        }
    }
}
});
var eventTestLapsation = Class.define({
   eventName: function(args) {
    return "Test Policy Lapse (UVL & UL)";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    return true;
},
   run: function(args) {
    var context = args["context"];
    var running = context.runningPolicyValues;

    var fundBalances = IFund.appliedTo(running.fundPool).call("fundBalances", {});
    var lapseTester = {};
    for(returnTypeIdx in PublicConstants.FUND_RETURN) {
         var returnType = PublicConstants.FUND_RETURN[returnTypeIdx];
         var fb = fundBalances[returnType];
         if (fb <= 0 ||  lapseTester[returnType] == "Y") {
             lapseTester[returnType] = "Y";
         } else {
             lapseTester[returnType] = "N";
         }
    }

    running["lapse"] = lapseTester;
}
});
var eventUpdateGuaranteeReturnInFunds = Class.define({
   eventName: function(args) {
    return "Update Guarantee Return Rates in funds by rate defined";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    var month = context.runningPolicyValues.months % 12;
    return (month == 0);
},
   run: function(args) {
    var context = args["context"];
    var running = context.runningPolicyValues;
    var year = running.year;    
    var proposal = context['proposal'];
    var basePlan = proposal.coverageInfo;
    var month = running.months % 12;

    var productId = proposal.coverageInfo.schema.implementation.schema.ProductSchema.ProductSchemaPK.ProductPK.ProductId.text;

    //if (running.months == 0) {
    //    _SS(running.fundPool.context.fundPools[productId], 10);
    //}

    var rates = context['rates'];
    var guarInterestRates = rates[productId][SchemaConstants.POLICY_VALUE.IRR].rates;
    var guarInterestRate = guarInterestRates[year];

    for(fundId in running.fundPool.context.fundPools) {
        running.fundPool.context.fundPools[fundId].context.annualInterestRate[PublicConstants.FUND_RETURN.LOW] =  _R(guarInterestRate * 100, 2); 
        running.fundPool.context.fundPools[fundId].context.interestRate[PublicConstants.FUND_RETURN.LOW] =  _R(Math.pow(1 + guarInterestRate, 1/12) - 1, context.interestRatePrecision); 
    }

    //_DB("LOW Pool Monthly Interest Rate", _R(Math.pow(1 + guarInterestRate, 1/12), context.interestRatePrecision) - 1);

    //if (month == 0) {
    //_DBR("year", year, "", running.months);
    //_DBR("guarInterestRate",guarInterestRate, "", running.months);
    //}

    return;
}
});
var showMonthlyResults = Class.define({
   eventName: function(args) {
    return "Show Monthly Result for Verification";
},
   shouldBeRun: function(args) {
    var context = args["context"];
    var proposal = context.proposal;

    if (proposal.enableDebug) return proposal.enableDebug;
    return false;
},
   run: function(args) {
    var context = args["context"];
    var running = context.runningPolicyValues;
    var proposal = context.proposal;
    var year = running.year;
    var month = running.months % 12;

    var cors = running['cors']
    var cord = running['corsDetails'];
    var balances = IFund.appliedTo(running.fundPool).call("fundBalances", {});

    var showValue = proposal.enableDebug;
    if ( showValue && year < proposal.startDebugYear ) {
        showValue = false;
    }
    if ( showValue && year > proposal.stopDebugYear ) {
        showValue = false;
    }
    if (showValue) {
        console.log("***** Y E A R " + year + " / M O N T H " + month + " *****");
        console.log("[  CORs by Product  ]");
        _SS(cord, 5);
        console.log("[  Total CORs  ]");
        _SS(cors, 5);
        console.log("[  Fund Balances  ]");
        _SS(balances, 5);
    }
}
});
var eventApplyPolicyChangeFaceAmount = eventApplyPolicyChangePlannedPremium.extend({
   eventName: function(args) {
    return "Apply New Face Amount";
},
   applyPolicyChange: function(args) {
    var vargs = args['args'];
    var activity = args['activity'];
    
    var context = vargs["context"];
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    
    if (!Utils.isNothing(activity.faceAmount)) {
        basePlan.faceAmount = activity.faceAmount;
    }
}
});
var eventApplyPolicyChangeRegularPremiumUL007 = eventApplyPolicyChangePlannedPremium.extend({
   eventName: function(args) {
    return "Apply New Regular Premium";
},
   applyPolicyChange: function(args) {
    var vargs = args['args'];
    var activity = args['activity'];
    
    var context = vargs["context"];
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    
    if (!Utils.isNothing(activity.regularPayment)) {
       basePlan.regularPayment = activity.regularPayment;
    }
}
});
var eventCalculateCOILocal = eventCalculateCOI.extend({
   eventName: function(args) {
    return "Calculate COI and Round to 2 digits (Local)";
},
   run: function(args) {
    this.self.parent.run(args);
    var context = args['context'];
    var runningPolicyValues = context.runningPolicyValues;

    //var fundBalances = IFund.appliedTo(runningPolicyValues.fundPool).call("fundBalances", {});
    //var cashOutBalances = IFund.appliedTo(runningPolicyValues.cashoutAccount).call("fundBalances", {});

    for (coiIdx in runningPolicyValues.cois) {
        //var balance = Number( fundBalances[coiIdx] ) + Number( cashOutBalances[coiIdx] );

        var isLapse = runningPolicyValues.lapse[coiIdx];
        var coi = runningPolicyValues.cois[coiIdx];

        if (isLapse == 'Y') {
            coi = 0;
        } else {
            coi = _R2( coi );
        }

        //coi = _R2(Math.min( coi, balance ) );
        //_DBR("coi[" + coiIdx + "] = R2( min(" + balance + ", " + runningPolicyValues.cois[coiIdx] + "))", coi, "eventCalculateCOILocal->", runningPolicyValues.months);    
    
        runningPolicyValues.cois[coiIdx] = coi;
    }

    _DBR("all COIs is rounded to 2 digits", runningPolicyValues.cois, "eventCalculateCOILocal->", context.runningPolicyValues.months);
},
   shouldBeRun: function(args) {
    return this.self.parent.shouldBeRun(args);
}
});
var eventCalculateCOR_COR01Local = eventCalculateCOR_XXXXX.extend({
   eventName: function(args) {
   return "Calculate COR of Riders (COR01)";
},
   calculateMonthlyCOR: function(args) {
   // to be overrided by child class
   //
   var context = args['context'];
   var running = context.runningPolicyValues;
   var proposal = context.proposal;
   var basePlan = proposal.coverageInfo;
   var rider = args['rider'];
   var rates = context['rates'];
   var baseProductId = basePlan.product.productKey.primaryProduct.productPK.productId;
   var riderProductId = rider.product.productKey.primaryProduct.productPK.productId;
   var year = running.year;
   var extraRating = rider.extraRating;
   var returnType = args['returnType'];
   var currentCors = running.cors[returnType];
   var premiums = running.premiums;
   
   var modalFactorForCoi = 1;
   if (basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.SEMIANNUAL) {
       modalFactorForCoi = 1/2;
   }
   if (basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.QUARTERLY) {
       modalFactorForCoi = 1/4;
   }
   if (basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.MONTHLY) {
       modalFactorForCoi = 1/12;
   }

   // Target Premium rate
   var targetPremiumRate = rates[riderProductId][SchemaConstants.PREMIUM_TYPE.BASICPREMIUM].rates[0];
   // COR rate
   var coiRate = rates[riderProductId][SchemaConstants.PREMIUM_TYPE.COSTOFINSURANCE].rates[year];
   // WOC Factor for Base
   var wocFactor = rates[riderProductId][SchemaConstants.PREMIUM_TYPE.MINIMUMPREMIUM].rates[year];
   // WOC Factor for Policy Fee
   var wocPolicyFeeFactor = rates[riderProductId][SchemaConstants.PREMIUM_TYPE.MIN_PLANNED_PREMUM].rates[year];
   var monthlyCoiRate = coiRate / 12 * extraRating.percentageExtra;

   // Basic information for calculation
   var narAmount = Number(running['nars'][returnType]);
   var extraRating = rider.extraRating;
   // WAIVER TRENDING FACTOR
   var waiverTrendingFactor = -3.6/100;   
   var policyDate = proposal.policyYearDate.date();
   policyDate.setMonth( policyDate.getMonth() + running.months );   
   var yearOfPolicyFee = Math.max(Math.min(2033, policyDate.getFullYear()) - 2015, 0);

   if (running.year < 2) {      
      _DBR("", "", "", running.year);
      _DBR("", "[ Rates for Reference (" + returnType + ") ]", "", running.year);
      _DBR("yearOfPolicyFee", yearOfPolicyFee, "", running.year);
      _DBR("narAmount", narAmount, "", running.year);
      _DBR("targetPremiumRate", targetPremiumRate, "", running.year);
      _DBR("coiRate", coiRate, "", running.year);
      _DBR("wocFactor", wocFactor, "", running.year);
      _DBR("wocPolicyFeeFactor", wocPolicyFeeFactor, "", running.year);
      _DBR("extraRating", extraRating, "", running.year);
      _DBR("monthlyCoiRate", monthlyCoiRate, "", running.year);
      _DBR("currentCors", currentCors, "", running.year);
      _DBR("policyDate", policyDate.formatString(), "", running.year);
   }

   /**  calculate NAAR model (initial) **/
   // 1. WOC POLICY FEE
   var trendingFactor = Math.pow(1 + waiverTrendingFactor, yearOfPolicyFee);
   var wocPolicyFee = _R(_R(wocPolicyFeeFactor * trendingFactor, 1) * running.policyFees[returnType] * 12, 3);

   /**  calculate NAAR model (regular) **/
   var wocNaarBase = _R((wocFactor * Number(extraRating.percentageExtra) + Number( _V( extraRating.flatExtra, 0) ) / 1000) * narAmount / 1000, 3);
   //wocPremiums = (currentCors + Number(basePlan.plannedPremium) + running['wocTargetPremium']) * 3;
   var wocPremiums = running['targetPremiums'] * 3;
   var totalWocFA = _R2(wocNaarBase + wocPolicyFee + wocPremiums);
   //var targetPremium = calculateTargetPremium(totalWocFA);
   if (year == 0) {
       wocCor = _R2(_R2(totalWocFA * monthlyCoiRate / 1000) / 2);
   } else {
       wocCor = _R2(totalWocFA * monthlyCoiRate / 1000);
   }

   var showValue = true;
   if (running.year < proposal.startDebugYear) {
      showValue = false;
   }
   if (running.year > proposal.stopDebugYear) {
      showValue = false;
   }
   if (showValue) {
       _DBR("", "", "", running.year);
       _DBR("", "[ Regular NAAR model for WOC (" + returnType + ") ]", "", running.year);
       _DBR("wocNaarBase", wocNaarBase, "", running.year);
       _DBR("wocPolicyFee", wocPolicyFee, "", running.year);
       _DBR("wocPremiums", wocPremiums, "", running.year);
       _DBR("totalWocFA", totalWocFA, "", running.year);
       _DBR("wocCor", wocCor, "", running.year);
   }

   return wocCor;
},
   calculateMethod: function(args) {
   return "COR01";
}
});
var eventCalculateCOR_COR02Local = eventCalculateCOR_XXXXX.extend({
   eventName: function(args) {
return "Calculate COR of Riders (COR02)";
},
   calculateMonthlyCOR: function(args) {
   // to be overrided by child class
   //
   var context = args['context'];
   var running = context.runningPolicyValues;
   var proposal = context.proposal;
   var basePlan = proposal.coverageInfo;
   var rider = args['rider'];
   var rates = context['rates'];
   var baseProductId = basePlan.product.productKey.primaryProduct.productPK.productId;
   var riderProductId = rider.product.productKey.primaryProduct.productPK.productId;
   var year = running.year;
   var months = running.months;
   var extraRating = rider.extraRating;
   var returnType = args['returnType'];

   var modalFactorForCoi = 1;
   if (basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.SEMIANNUAL) {
       modalFactorForCoi = 1/2;
   }
   if (basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.QUARTERLY) {
       modalFactorForCoi = 1/4;
   }
   if (basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.MONTHLY) {
       modalFactorForCoi = 1/12;
   }

   //if (running.months == 0 && returnType == PublicConstants.FUND_RETURN.LOW) {
   //   _SS(running, 5);
   //   _SS(rates, 5);
   //}
   
   var corRate = rates[riderProductId][SchemaConstants.PREMIUM_TYPE.COSTOFINSURANCE].rates[year];
   
   _DBR("corRate (" + riderProductId + ")", corRate, "eventCalculateCOR_COR02Local->", months);
   _DBR("modal corRate (" + riderProductId + ")", _R13(corRate * _R13(1/12) * Number(extraRating.percentageExtra)), "eventCalculateCOR_COR02Local->", months);

   var cor = _R2(rider.faceAmount * _R13(corRate * _R13(1/12) * Number(extraRating.percentageExtra)) / 1000);
   if (!Utils.isNothing(extraRating) && !Utils.isNothing(extraRating.flatExtra)) {
       cor  +=  _R2(rider.faceAmount * _R13(Number(extraRating.flatExtra) / 12) / 1000) ;
   }
   if (!Utils.isNothing(extraRating) && !Utils.isNothing(extraRating.tempFlat) && !Utils.isNothing(extraRating.tempFlatDuration)) {
       if (year < Number(extraRating.tempFlatDuration)) {
           cor  +=  _R2(rider.faceAmount * _R13(Number(extraRating.tempFlat) / 12) / 1000) ;
       }
   }

   if (year == 0) {
        cor = _R2(cor / 2);
   }

   //_DBR("COR (" + riderProductId + ") @ " + year + "[COR rate: " + corRate + "]", cor, "eventCalculateCOR_COR02Local->", running.months);
   return cor;
},
   calculateMethod: function(args) {
return "COR02";
}
});
var eventCalculateCOR_COR03Local = eventCalculateCOR_XXXXX.extend({
   eventName: function(args) {
return "Calculate COR of Riders (COR03)";
},
   calculateMonthlyCOR: function(args) {
   // to be overrided by child class
   //
   var context = args['context'];
   var running = context.runningPolicyValues;
   var proposal = context.proposal;
   var basePlan = proposal.coverageInfo;
   var rider = args['rider'];
   var rates = context['rates'];
   var baseProductId = basePlan.product.productKey.primaryProduct.productPK.productId;
   var riderProductId = rider.product.productKey.primaryProduct.productPK.productId;
   var year = running.year;
   var extraRating = rider.extraRating;
   var returnType = args['returnType'];

   var modalFactorForCoi = 1;
   if (basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.SEMIANNUAL) {
       modalFactorForCoi = 1/2;
   }
   if (basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.QUARTERLY) {
       modalFactorForCoi = 1/4;
   }
   if (basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.MONTHLY) {
       modalFactorForCoi = 1/12;
   }

   //if (running.months == 0 && returnType == PublicConstants.FUND_RETURN.LOW) {
   //   _SS(running, 5);
   //   _SS(rates, 5);
   //}
   
   _DBR("", "", "", running.months);
   _DBR("rider code", riderProductId, "eventCalculateCOR_COR03Local->", running.months);

   var pyd = proposal.policyYearDate.date();
   pyd.setMonth( pyd.getMonth() + running.months );
   var noOfInflationYears = pyd.getFullYear() - 2016;
   var inflationMultiplier = Math.pow(1.08, noOfInflationYears);
   
   _DBR("noOfInflationYears", noOfInflationYears, "eventCalculateCOR_COR03Local->",running.months);
   _DBR("inflationMultiplier", inflationMultiplier, "eventCalculateCOR_COR03Local->",running.months);

   var corRate = rates[riderProductId][SchemaConstants.PREMIUM_TYPE.COSTOFINSURANCE].rates[year];
   _DBR("corRate (no inflation)", corRate, "eventCalculateCOR_COR03Local->",running.months);

   var ciRate = 0;
   if (!Utils.isNothing(rates[riderProductId][SchemaConstants.PREMIUM_TYPE.MINIMUMPREMIUM])) {
       ciRate = rates[riderProductId][SchemaConstants.PREMIUM_TYPE.MINIMUMPREMIUM].rates[year];
   }
   _DBR("CI Rate (no inflation)", ciRate, "eventCalculateCOR_COR03Local->",running.months);

   var maxCorRate = rates[riderProductId][SchemaConstants.PREMIUM_TYPE.MIN_PLANNED_PREMUM].rates[year];
   _DBR("Max COR Rate", maxCorRate, "eventCalculateCOR_COR03Local->",running.months);

   corRate = _R( corRate * inflationMultiplier, 0);
   _DBR("corRate (with inflation)", corRate, "eventCalculateCOR_COR03Local->",running.months);   

   var cor = _R2( _R13( Math.min(corRate + ciRate, maxCorRate + ciRate) / 12 * Number(extraRating.percentageExtra) ) );
   if (!Utils.isNothing(extraRating) && !Utils.isNothing(extraRating.flatExtra)) {
       cor  +=  _R2(_R13(Number(extraRating.flatExtra) / 12)) ;
   }
   if (!Utils.isNothing(extraRating) && !Utils.isNothing(extraRating.tempFlat) && !Utils.isNothing(extraRating.tempFlatDuration)) {
       if (year < Number(extraRating.tempFlatDuration)) {
           cor  +=  _R2(_R13(Number(extraRating.tempFlat) / 12)) ;
       }
   }

   if (year == 0) {
        cor = _R2(cor / 2);
   }

   _DBR("COR (" + riderProductId + ") @ " + year + "[COR rate: " + corRate + "]", cor, "eventCalculateCOR_COR02Local->", running.months);
   return cor;
},
   calculateMethod: function(args) {
return "COR03";
},
   shouldPayCOR: function(args) {
    var context = args['context'];
    var rider = args['rider'];
    var year = context.runningPolicyValues.year;
    var months = context.runningPolicyValues.months;
    var oppp = rider.schema.call('calculatePremiumPayingPeriod', { coverage: rider });
    var ppp = Math.min(11, oppp );

    _DBR("Min(ppp[" + ppp + "], 12)", ppp, "eventCalculateCOR_XXXXX.shouldPayCOR", months);
    _DBR("year", year, "eventCalculateCOR_XXXXX.shouldPayCOR", months);
    _DBR("year <= ppp",(year <= ppp), "eventCalculateCOR_XXXXX.shouldPayCOR", months);
    return (year <= ppp);
}
});
var eventCoreCalculatePremiumBPM44 = eventCoreCalculatePremiumXXXXX.extend({
   eventName: function(args) {
return "Calculate Base Plan Premium with method (" + this.self.premiumMethod(args) + ")";
},
   premiumMethod: function(args) {
    return "BPM44"
},
   calculatePremium: function(args) {
    var coverage = args["coverage"];
    var context = args["context"];
    var methodId = this.self.premiumMethod(args);

    var rates = context.rates;
    var running = context.runningPolicyValues;
    var months = running.months;
    var productId = _PV(coverage, "product.productKey.primaryProduct.productPK.productId");
    var year = running.year;

    // annual mode rate
    var premRate = this.self.getNetPremiumRate( args );

    _DBR("year", year, "eventCalculatePremium" + methodId + "->", months);
    _DBR("premRate", premRate, "eventCalculatePremium" + methodId + "->", months);
    _DBR("coverage.faceAmount", coverage.faceAmount, "eventCalculatePremium" + methodId + "->", months);

    var annualPremium = _R2(coverage.faceAmount * premRate * coverage.extraRating.percentageExtra / 1000);
    if (!Utils.isNothing(coverage.extraRating.flatExtra)) {
        annualPremium = annualPremium + _R2(coverage.faceAmount * coverage.extraRating.flatExtra / 1000);
    }
    if (!Utils.isNothing(coverage.extraRating.tempFlat) && !Utils.isNothing(coverage.extraRating.tempFlatDuration)) {
        if (year < coverage.extraRating.tempFlatDuration) {
            annualPremium = annualPremium + _R2(coverage.faceAmount * coverage.extraRating.tempFlat / 1000);
        }
    }
    _DBR("annualPremium", annualPremium, "eventCalculatePremium" + methodId + "->", months);
    return annualPremium;
},
   getNetPremiumRate: function(args) {
    var coverage = args["coverage"];
    var primary = getPrimaryInsured( { coverage: coverage } );
    var context = args["context"];
    var rates = context.rates;
    var running = context.runningPolicyValues;
    var productId = _PV(coverage, "product.productKey.primaryProduct.productPK.productId");
    var year = running.year;
    var ppp = coverage.schema.call("calculatePremiumPayingPeriod", {coverage: coverage, issueAge: primary.insuredAge});

    // level premium rate;
    var premRate = rates[productId][SchemaConstants.PREMIUM_TYPE.BASICPREMIUM].rates[0];
    if (ppp < year) premRate = 0;

    var band = coverage.proposal.band;
    var discountRate = 0;
    if (band == 3) discountRate = 0.06;
    if (band == 2) discountRate = 0.03;
    return _R2( premRate * (1 - discountRate) );
}
});
var eventDeductCOILocal = eventDeductCOI.extend({
   eventName: function(args) {
    return "Deduct COI (Local)";
},
   run: function(args) {
      var context = args["context"];
      var runningPolicyValues = context.runningPolicyValues;
      
      var fundBalances = IFund.appliedTo(runningPolicyValues.fundPool).call("fundBalances", {});
      var cashOutBalances = IFund.appliedTo(runningPolicyValues.cashoutAccount).call("fundBalances", {});

      _DBR("", "", "", runningPolicyValues.months);    
      _DBR("", "Net COI for Deduction", "", runningPolicyValues.months);    

      var ncois = runningPolicyValues.cois;
      for(coiIdx in ncois) {
           var balance = fundBalances[coiIdx] + cashOutBalances[coiIdx];
           var coi = ncois[coiIdx];
           ncois[coiIdx] = Math.min( coi, balance );

           _DBR("coi[" + coiIdx + "] = R2( min(" + balance + ", " + coi + "))", ncois[coiIdx], "eventDeductCOILocal->", runningPolicyValues.months);    
      }

      IFund.appliedTo(runningPolicyValues.fundPool).call("withdraw", {
         amounts: ncois
      });
}
});
var eventDeductWithdrawalLocal = eventDeductWithdrawal.extend({
   eventName: function(args) {
    return "Deduct withdrawal from accounts (Local)";
},
   shouldBeRun: function(args) {
    var context = args['context'];
    var shouldRun = (context.policyOptions.indexOf("FundActivitiesOption") >= 0);
    var running = context.runningPolicyValues;

    for(idx in PublicConstants.FUND_RETURN) {
        shouldRun = (shouldRun || running.lapse[idx] == "N");
    }

    return shouldRun;
}
});
var eventReceiveRegularPremiumLocal = eventReceiveInitialPremiumLocal.extend({
   eventName: function(args) {
    return "Receive Planned Premium (Local)";
},
   newPremiumReceived: function(args) {
    var context = args["context"];

    var metadata = context.events.metadata;
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    var runningPolicyValues = context.runningPolicyValues;
    var months = runningPolicyValues.months;
    var newPremium = 0;

    var year = runningPolicyValues.year;
    var months = runningPolicyValues.months;
    var month = months % 12;
    var premiumAmount = Number(this.self.newPremium(args));

    if (month == 0 && basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.ANNUAL) {
        newPremium = premiumAmount;
        _DBR("plannedPremium", newPremium, "eventReceiveRegularPremiumLocal->", months);
    } else
    if ((month % 6) == 0 && basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.SEMIANNUAL) {
        newPremium = premiumAmount;
        _DBR("plannedPremium", newPremium, "eventReceiveRegularPremiumLocal->", months);
    } else
    if ((months % 3) == 0 && basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.QUARTERLY) {
        newPremium = premiumAmount;
        _DBR("plannedPremium", newPremium, "eventReceiveRegularPremiumLocal->", months);
    } else
    if (basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.MONTHLY) {
        newPremium = premiumAmount;
        _DBR("plannedPremium", newPremium, "eventReceiveRegularPremiumLocal->", months);
    }
    return newPremium;
},
   shouldBeRun: function(args) {
    var context = args["context"];
    var running = context.runningPolicyValues;

    var shouldRun = false;
    for(idx in PublicConstants.FUND_RETURN) {
        shouldRun = (shouldRun || running.lapse[idx] == "N");
    }

    return shouldRun;
},
   newPremium: function(args) {
    var context = args["context"];
    var running = context.runningPolicyValues;
    var metadata = context.events.metadata;
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;

    if (!Utils.isNothing(basePlan.regularPayment)) {
        var p = Math.min(basePlan.regularPayment, basePlan.plannedPremium);
        _DBR("newPremium = Math.min( " + basePlan.regularPayment + ", " + basePlan.plannedPremium + " )", p, "eventReceiveRegularPremiumLocal->", running.months);
        return p
    } else {
        return basePlan.plannedPremium;
    }
}
});
var eventReceiveTopupPremiumLocal = eventReceiveInitialPremiumLocal.extend({
   eventName: function(args) {
    return "Receive Topup Premium (Local)";
},
   shouldBeRun: function(args) {
    var context = args['context'];
    var shouldRun = (context.policyOptions.indexOf("FundActivitiesOption") >= 0);
    var running = context.runningPolicyValues;

    for(idx in PublicConstants.FUND_RETURN) {
        shouldRun = (shouldRun || running.lapse[idx] == "N");
    }

    return shouldRun;
},
   newPremiumReceived: function(args) {
    var context = args["context"];

    var metadata = context.events.metadata;
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    var runningPolicyValues = context.runningPolicyValues;
    var months = runningPolicyValues.months;
    var newPremium = this.self.newRegularTopup(args);

    var year = runningPolicyValues.year;
    var months = runningPolicyValues.months;
    var month = months % 12;

    if (month == 0) {
        if (!Utils.isNothing(proposal.fundActivities) && !Utils.isNothing(proposal.fundActivities.fundActivity)) {
            var activities = Utils.iterator(proposal.fundActivities.fundActivity);
            var parties = Utils.iterator(basePlan.parties.party);
            for (idx in activities) {
                var activity = activities[idx];
                if (Number(activity.topupPremium) > 0 && Number(activity.attainAge) == (Number(parties[0].insuredAge) + Number(year))) {
                    if (!Utils.isNothing(activity.topupPremium)) {
                        newPremium = Number(newPremium) + Number(activity.topupPremium);
                    }
                    break;
                }
            }
        }
    }
    
    if (newPremium > 0) {
        // save top-up premium in context to show in result column
        runningPolicyValues['topup'] = runningPolicyValues['topup'] + Number(newPremium);

        _DBR("", "", "", months);
        _DBR("newTopupPremium", newPremium, "eventReceiveTopupPremiumLocal->", months);
        _DBR("topupPremium", runningPolicyValues['topup'], "eventReceiveTopupPremiumLocal->", months);
    }

    return newPremium;
},
   lookupAllocationRate: function(args) {
   var context = args["context"];

   var metadata = context.events.metadata;
   var proposal = context.proposal;
   var basePlan = proposal.coverageInfo;
   var productId = basePlan.product.productKey.primaryProduct.productPK.productId;
   var runningPolicyValues = context.runningPolicyValues;

   var year = runningPolicyValues.year;
   var months = runningPolicyValues.months;
   var allocationRate = 1;

   var allocationRates = context.rates[productId][SchemaConstants.PREMIUM_TYPE.TOPUP_ALLOCATION];
   if (!Utils.isNothing(allocationRates)) {
       allocationRate = allocationRates.rates[year];
   }
   _DBR("topupAllocationRate", allocationRate, "eventReceiveXXXXXPremiumLocal->", months);

   return allocationRate;
},
   newRegularTopup: function(args) {
    var context = args["context"];

    var metadata = context.events.metadata;
    var proposal = context.proposal;
    var basePlan = proposal.coverageInfo;
    var runningPolicyValues = context.runningPolicyValues;

    var regularTopupAmount = 0;
    if (!Utils.isNothing(basePlan.regularPayment) && !Utils.isNothing(basePlan.regularPayment)) {
        regularTopupAmount = Math.max(0, basePlan.regularPayment - basePlan.plannedPremium);
    }

    var regularTopup = 0;
    var months = runningPolicyValues.months;
    var month = months % 12;
    if (month == 0 && basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.ANNUAL) {
        regularTopup = regularTopupAmount;
        _DBR("regularTopup", regularTopup, "eventReceiveTopupPremiumLocal->", months);
    } else
    if ((month % 6) == 0 && basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.SEMIANNUAL) {
        regularTopup = regularTopupAmount;
        _DBR("regularTopup", regularTopup, "eventReceiveTopupPremiumLocal->", months);
    } else
    if ((months % 3) == 0 && basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.QUARTERLY) {
        regularTopup = regularTopupAmount;
        _DBR("regularTopup", regularTopup, "eventReceiveTopupPremiumLocal->", months);
    } else
    if (basePlan.options.paymentMode == SchemaConstants.PAYMENT_MODE.MONTHLY) {
        regularTopup = regularTopupAmount;
        _DBR("regularTopup", regularTopup, "eventReceiveTopupPremiumLocal->", months);
    }

    return regularTopup;
}
});
var eventTradParPlanAvyInitialization = eventCoreMvyAvyInitialization.extend({
   eventName: function(args) {
     return "Initialise MVY / AVY";
},
   shouldBeRun: function(args) {
     return this.self.parent.shouldBeRun(args);
},
   MVY: function(args) {
     var context = args["context"];
     var running = context.runningPolicyValues;
     // nothing
},
   AVY: function(args) {
     var context = args["context"];
     var running = context.runningPolicyValues;
     var months = running.months;
     running['dividends'] = 0;
     running['cashValue'] = 0;
     running['rpu'] = 0;     
     running['basePlanPremiums'] = 0;
     running['riderPremiums'] = 0;
     running['totalPremiums'] = 0;
     running['deathBenefit'] = 0;
     running['ciBenefit'] = 0;
     running['cashValue'] = 0;
     running['dividends'] = 0;
     running['higherDividends'] = 0;
     running['lowerDividends'] = 0;
},
   initialSetup: function(args) {
     var context = args["context"];
     var running = context.runningPolicyValues;
     var months = running.months;
     running['accPremiums'] = 0;
     running['accDividends'] = 0;
}
});

    var colLoyaltyBonusHigh = Class.define({
            columnName: function(args) {
    return "Loyalty Bonus (HIGH)";
},
            shouldBeShown: function(args) {
    var context = args["context"];
    return true;
},
            calculatedValue: function(args) {
   var record = args["record"];
   var calValue = Math.max(0, Number(record.fundBonus[PublicConstants.FUND_RETURN.HIGH]));
   return calValue;
}
    });
    var colLoyaltyBonusLow = Class.define({
            columnName: function(args) {
    return "Loyalty Bonus (LOW)";
},
            shouldBeShown: function(args) {
    var context = args["context"];
    return true;
},
            calculatedValue: function(args) {
   var record = args["record"];
   var calValue = Math.max(0, Number(record.fundBonus[PublicConstants.FUND_RETURN.LOW]));
   return calValue;
}
    });
    var colLoyaltyBonusMedium = Class.define({
            columnName: function(args) {
    return "Loyalty Bonus (MEDIUM)";
},
            shouldBeShown: function(args) {
    var context = args["context"];
    return true;
},
            calculatedValue: function(args) {
   var record = args["record"];
   var calValue = Math.max(0, Number(record.fundBonus[PublicConstants.FUND_RETURN.MEDIUM]));
   return calValue;
}
    });
    var colPolicyFee = Class.define({
            columnName: function(args) {
    return "Policy Fee";
},
            shouldBeShown: function(args) {
    var context = args["context"];
    return true;
},
            calculatedValue: function(args) {
    var record = args["record"];
    return record.policyFee;
}
    });
    var colPolicyLapseHigh = Class.define({
            columnName: function(args) {
    return "Lapse (HIGH)";
},
            shouldBeShown: function(args) {
    var context = args["context"];
    return true;
},
            calculatedValue: function(args) {
    var record = args["record"];
    var policyLapse = record.policyLapse;
    var lapseHigh = policyLapse[PublicConstants.FUND_RETURN.HIGH];
    return lapseHigh;
}
    });
    var colPolicyLapseLow = Class.define({
            columnName: function(args) {
    return "Lapse (LOW)";
},
            shouldBeShown: function(args) {
    var context = args["context"];
    return true;
},
            calculatedValue: function(args) {
    var record = args["record"];
    var policyLapse = record.policyLapse;
    var lapseLow = policyLapse[PublicConstants.FUND_RETURN.LOW];
    return lapseLow;
}
    });
    var colPolicyLapseMedium = Class.define({
            columnName: function(args) {
    return "Lapse (MEDIUM)";
},
            shouldBeShown: function(args) {
    var context = args["context"];
    return true;
},
            calculatedValue: function(args) {
    var record = args["record"];
    var policyLapse = record.policyLapse;
    var lapseMedium = policyLapse[PublicConstants.FUND_RETURN.MEDIUM];
    return lapseMedium;
}
    });
    var colPremiumLoad = Class.define({
            columnName: function(args) {
    return "Premium Load";
},
            shouldBeShown: function(args) {
    var context = args["context"];
    return true;
},
            calculatedValue: function(args) {
    var record = args["record"];

    // ATP-488  #1 comment out this code - it seem raise issue gap data
    // var calValue = record.newPremium - record.netPremium;

    // ATP-488 Replace this code for #1 for fixing issue gap data
    var policyLapse = record.policyLapse;
    var lapseLow = policyLapse[PublicConstants.FUND_RETURN.LOW];
    var calValue = lapseLow === 'N' ? record.newPremium - record.netPremium : 0;
    // ATP-488 End #1

    return calValue;
}
    });
    var colPremiumLoadAndOtherFees = Class.define({
            columnName: function(args) {
    return "Premium Load & other fees";
},
            shouldBeShown: function(args) {
    var context = args["context"];
    return true;
},
            calculatedValue: function(args) {
    var record = args["record"];
     var calValue = record.newPremium - record.netPremium + record.policyFee;
     return calValue;
}
    });
    var colTopupPremium = Class.define({
            columnName: function(args) {
    return "Top-up Premium";
},
            shouldBeShown: function(args) {
    var context = args["context"];
    return true;
},
            calculatedValue: function(args) {
    var record = args["record"];
    var calValue = record.topup;
    return calValue;
}
    });
    var colTradCashValue = Class.define({
            columnName: function(args) {
    return "colTradCashValue";
},
            shouldBeShown: function(args) {
    var context = args["context"];
    return true;
},
            calculatedValue: function(args) {
    var record = args["record"];
    return record.cashValue;
}
    });
    var colTradCiBenefit = Class.define({
            columnName: function(args) {
    return "colTradCiBenefit";
},
            shouldBeShown: function(args) {
    var context = args["context"];
    return true;
},
            calculatedValue: function(args) {
    var record = args["record"];
    return record.ciBenefit;
}
    });
    var colTradCoupon = Class.define({
            columnName: function(args) {
    return "colTradCoupon";
},
            shouldBeShown: function(args) {
    var context = args["context"];
    return true;
},
            calculatedValue: function(args) {
    var record = args["record"];
    return record.coupon;
}
    });
    var colTradDb = Class.define({
            columnName: function(args) {
    return "colTradDb";
},
            shouldBeShown: function(args) {
    var context = args["context"];
    return true;
},
            calculatedValue: function(args) {
    var record = args["record"];
    return record.deathBenefit;
}
    });
    var colTradDividends = Class.define({
            columnName: function(args) {
    return "colTradDividends";
},
            shouldBeShown: function(args) {
    var context = args["context"];
    return true;
},
            calculatedValue: function(args) {
    var record = args["record"];
    return record.dividends;
}
    });
    var colAccountHighLocal = colAccountHigh.extend({
            columnName: function(args) {
//*** Parent(Start): colAccountHigh.columnName
//      return "Account Value (HIGH)";
//*** Parent(End): colAccountHigh.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colAccountHigh.calculatedValue
//      var record = args["record"];
//      var calValue = record.accountValues[PublicConstants.FUND_RETURN.HIGH] + record.cashoutAccount[PublicConstants.FUND_RETURN.HIGH];
//      return calValue;
//*** Parent(End): colAccountHigh.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colAccountHigh.shouldBeShown
//      return true;
//   }
//*** Parent(End): colAccountHigh.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });
    var colAccountLowLocal = colAccountLow.extend({
            columnName: function(args) {
//*** Parent(Start): colAccountLow.columnName
//      return "Account Value (LOW)";
//*** Parent(End): colAccountLow.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colAccountLow.calculatedValue
//      var record = args["record"];
//      var calValue = record.accountValues[PublicConstants.FUND_RETURN.LOW] + record.cashoutAccount[PublicConstants.FUND_RETURN.LOW];
//      return calValue;
//*** Parent(End): colAccountLow.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colAccountLow.shouldBeShown
//      return true;
//   }
//*** Parent(End): colAccountLow.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });
    var colAccountMediumLocal = colAccountMedium.extend({
            columnName: function(args) {
//*** Parent(Start): colAccountMedium.columnName
//      return "Account Value (MEDIUM)";
//*** Parent(End): colAccountMedium.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colAccountMedium.calculatedValue
//      var record = args["record"];
//      var calValue = record.accountValues[PublicConstants.FUND_RETURN.MEDIUM] + record.cashoutAccount[PublicConstants.FUND_RETURN.MEDIUM];
//      return calValue;
//*** Parent(End): colAccountMedium.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colAccountMedium.shouldBeShown
//      return true;
//   }
//*** Parent(End): colAccountMedium.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });
    var colCoiHighLocal = colCoiHigh.extend({
            columnName: function(args) {
//*** Parent(Start): colCoiHigh.columnName
//      return "COI (HIGH)";
//*** Parent(End): colCoiHigh.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colCoiHigh.calculatedValue
//      var record = args["record"];
//      var calValue = record.cois[PublicConstants.FUND_RETURN.HIGH];
//      return calValue;
//*** Parent(End): colCoiHigh.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colCoiHigh.shouldBeShown
//      return true;
//   }
//*** Parent(End): colCoiHigh.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });
    var colCoiLowLocal = colCoiLow.extend({
            columnName: function(args) {
//*** Parent(Start): colCoiLow.columnName
//      return "COI (LOW)";
//*** Parent(End): colCoiLow.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colCoiLow.calculatedValue
//      var record = args["record"];
//      var calValue = record.cois[PublicConstants.FUND_RETURN.LOW];
//      return calValue;
//*** Parent(End): colCoiLow.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colCoiLow.shouldBeShown
//      return true;
//   }
//*** Parent(End): colCoiLow.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });
    var colCoiMediumLocal = colCoiMedium.extend({
            columnName: function(args) {
//*** Parent(Start): colCoiMedium.columnName
//      return "COI (MEDIUM)";
//*** Parent(End): colCoiMedium.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colCoiMedium.calculatedValue
//      var record = args["record"];
//      var calValue = record.cois[PublicConstants.FUND_RETURN.MEDIUM];
//      return calValue;
//*** Parent(End): colCoiMedium.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colCoiMedium.shouldBeShown
//      return true;
//   }
//*** Parent(End): colCoiMedium.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });
    var colDbHighLocal = colDbHigh.extend({
            columnName: function(args) {
//*** Parent(Start): colDbHigh.columnName
//      return "Death Benefit (HIGH)";
//*** Parent(End): colDbHigh.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colDbHigh.calculatedValue
//      var record = args["record"];
//      var calValue = record.deathBenefits[PublicConstants.FUND_RETURN.HIGH];
//      return calValue;
//*** Parent(End): colDbHigh.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colDbHigh.shouldBeShown
//      return true;
//   }
//*** Parent(End): colDbHigh.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });
    var colDbLowLocal = colDbLow.extend({
            columnName: function(args) {
//*** Parent(Start): colDbLow.columnName
//      return "Death Benefit (LOW)";
//*** Parent(End): colDbLow.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colDbLow.calculatedValue
//      var record = args["record"];
//      var calValue = record.deathBenefits[PublicConstants.FUND_RETURN.LOW];
//      return calValue;
//*** Parent(End): colDbLow.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colDbLow.shouldBeShown
//      return true;
//   }
//*** Parent(End): colDbLow.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });
    var colDbMediumLocal = colDbMedium.extend({
            columnName: function(args) {
//*** Parent(Start): colDbMedium.columnName
//      return "Death Benefit (MEDIUM)";
//*** Parent(End): colDbMedium.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colDbMedium.calculatedValue
//      var record = args["record"];
//      var calValue = record.deathBenefits[PublicConstants.FUND_RETURN.MEDIUM];
//      return calValue;
//*** Parent(End): colDbMedium.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colDbMedium.shouldBeShown
//      return true;
//   }
//*** Parent(End): colDbMedium.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });
    var colNarHighLocal = colNarHigh.extend({
            columnName: function(args) {
//*** Parent(Start): colNarHigh.columnName
//      return "NAR (HIGH)";
//*** Parent(End): colNarHigh.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colNarHigh.calculatedValue
//      var record = args["record"];
//      var calValue = record.nars[PublicConstants.FUND_RETURN.HIGH];
//      return calValue;
//*** Parent(End): colNarHigh.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colNarHigh.shouldBeShown
//      return true;
//   }
//*** Parent(End): colNarHigh.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });
    var colNarLowLocal = colNarLow.extend({
            columnName: function(args) {
//*** Parent(Start): colNarLow.columnName
//      return "NAR (LOW)";
//*** Parent(End): colNarLow.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colNarLow.calculatedValue
//      var record = args["record"];
//      var calValue = record.nars[PublicConstants.FUND_RETURN.LOW];
//      return calValue;
//*** Parent(End): colNarLow.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colNarLow.shouldBeShown
//      return true;
//   }
//*** Parent(End): colNarLow.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });
    var colNarMediumLocal = colNarMedium.extend({
            columnName: function(args) {
//*** Parent(Start): colNarMedium.columnName
//      return "NAR (MEDIUM)";
//*** Parent(End): colNarMedium.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colNarMedium.calculatedValue
//      var record = args["record"];
//      var calValue = record.nars[PublicConstants.FUND_RETURN.MEDIUM];
//      return calValue;
//*** Parent(End): colNarMedium.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colNarMedium.shouldBeShown
//      return true;
//   }
//*** Parent(End): colNarMedium.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });
    var colPremiumLocal = colPremium.extend({
            columnName: function(args) {
//*** Parent(Start): colPremium.columnName
//      return "Premium";
//*** Parent(End): colPremium.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colPremium.calculatedValue
//      var record = args["record"];
//      var calValue = record.newPremium;
//      return calValue;
//*** Parent(End): colPremium.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colPremium.shouldBeShown
//      return true;
//   }
//*** Parent(End): colPremium.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });
    var colSurValueHighLocal = colSurValueHigh.extend({
            columnName: function(args) {
//*** Parent(Start): colSurValueHigh.columnName
//      return "Surrender Value (HIGH)";
//*** Parent(End): colSurValueHigh.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colSurValueHigh.calculatedValue
//      var record = args["record"];
//      var calValue = Number(Number(record.surrenderValues[PublicConstants.FUND_RETURN.HIGH]) + Number(record.surrenderValuesFromCashoutAccount[PublicConstants.FUND_RETURN.HIGH]));
//      return calValue;
//*** Parent(End): colSurValueHigh.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colSurValueHigh.shouldBeShown
//      return true;
//   }
//*** Parent(End): colSurValueHigh.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });
    var colSurValueLowLocal = colSurValueLow.extend({
            columnName: function(args) {
//*** Parent(Start): colSurValueLow.columnName
//      return "Surrender Value (LOW)";
//*** Parent(End): colSurValueLow.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colSurValueLow.calculatedValue
//      var record = args["record"];
//      var calValue = Number(Number(record.surrenderValues[PublicConstants.FUND_RETURN.LOW]) + Number(record.surrenderValuesFromCashoutAccount[PublicConstants.FUND_RETURN.LOW]));
//      return calValue;
//*** Parent(End): colSurValueLow.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colSurValueLow.shouldBeShown
//      return true;
//   }
//*** Parent(End): colSurValueLow.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });
    var colSurValueMediumLocal = colSurValueMedium.extend({
            columnName: function(args) {
//*** Parent(Start): colSurValueMedium.columnName
//      return "Surrender Value (MEDIUM)";
//*** Parent(End): colSurValueMedium.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colSurValueMedium.calculatedValue
//      var record = args["record"];
//      var calValue = Number(Number(record.surrenderValues[PublicConstants.FUND_RETURN.MEDIUM]) + Number(record.surrenderValuesFromCashoutAccount[PublicConstants.FUND_RETURN.MEDIUM]));
//      return calValue;
//*** Parent(End): colSurValueMedium.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colSurValueMedium.shouldBeShown
//      return true;
//   }
//*** Parent(End): colSurValueMedium.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });
    var colTotalDbHighLocal = colTotalDbHigh.extend({
            columnName: function(args) {
//*** Parent(Start): colTotalDbHigh.columnName
//      return "Total Death Benefit (HIGH)";
//*** Parent(End): colTotalDbHigh.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colTotalDbHigh.calculatedValue
//      var record = args["record"];
//      var calValue = record.totalDeathBenefits[PublicConstants.FUND_RETURN.HIGH];
//      return calValue;
//*** Parent(End): colTotalDbHigh.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colTotalDbHigh.shouldBeShown
//      return true;
//   }
//*** Parent(End): colTotalDbHigh.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });
    var colTotalDbLowLocal = colTotalDbLow.extend({
            columnName: function(args) {
//*** Parent(Start): colTotalDbLow.columnName
//      return "Total Death Benefit (LOW)";
//*** Parent(End): colTotalDbLow.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colTotalDbLow.calculatedValue
//      var record = args["record"];
//      var calValue = record.totalDeathBenefits[PublicConstants.FUND_RETURN.LOW];
//      return calValue;
//*** Parent(End): colTotalDbLow.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colTotalDbLow.shouldBeShown
//      return true;
//   }
//*** Parent(End): colTotalDbLow.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });
    var colTotalDbMediumLocal = colTotalDbMedium.extend({
            columnName: function(args) {
//*** Parent(Start): colTotalDbMedium.columnName
//      return "Total Death Benefit (MEDIUM)";
//*** Parent(End): colTotalDbMedium.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colTotalDbMedium.calculatedValue
//      var record = args["record"];
//      var calValue = record.totalDeathBenefits[PublicConstants.FUND_RETURN.MEDIUM];
//      return calValue;
//*** Parent(End): colTotalDbMedium.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colTotalDbMedium.shouldBeShown
//      return true;
//   }
//*** Parent(End): colTotalDbMedium.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });
    var colTotalPremiumLocal = colTotalPremium.extend({
            columnName: function(args) {
//*** Parent(Start): colTotalPremium.columnName
//      return "Total Premium";
//*** Parent(End): colTotalPremium.columnName

return this.self.parent.columnName(args);
},
            calculatedValue: function(args) {
//*** Parent(Start): colTotalPremium.calculatedValue
//      var record = args["record"];
//      var calValue = record.totalPremiums;
//      return calValue;
//*** Parent(End): colTotalPremium.calculatedValue

return this.self.parent.calculatedValue(args);
},
            shouldBeShown: function(args) {
//*** Parent(Start): colTotalPremium.shouldBeShown
//      return true;
//   }
//*** Parent(End): colTotalPremium.shouldBeShown

return this.self.parent.shouldBeShown(args);
}
    });

var ULRP_EventsLocal = CommonEvents.extend({
   events: function(args) {
    var proposal = args["proposal"];
    var events = [
         eventAvyInitializationLocal.create(),
         eventUpdateGuaranteeReturnInFunds.create(),

         eventApplyPolicyChangeRegularPremiumUL007.create(),
         eventApplyPolicyChangePlannedPremium.create(),
         eventApplyPolicyChangeFaceAmount.create(),

         eventReceiveInitialPremiumLocal.create(),
         eventReceiveRegularPremiumLocal.create(),
         eventReceiveTopupPremiumLocal.create(),

         // comment out this event to replace by local
         //eventCalculateNAR.create(),
         eventCalculateNARLocal.create(),
         eventCalculatePolicyFeeUL007.create(),

         eventCalculateCOILocal.create(),
         eventCalculateCOR_COR01Local.create(),
         eventCalculateCOR_COR02Local.create(),
         eventCalculateCOR_COR03Local.create(),
         eventAccumulateCOI.create(),

         //eventDeductCOI.create(),  
         eventDeductCOILocal.create(),

         eventDeductCOR.create(),
         eventDeductMonthlyPolicyFeeUL007.create(),
         eventDeductWithdrawalLocal.create(),
         eventTestLapsation.create(),
         eventRollInterest.create(),
         eventAccumulatePolicyValues.create(),
         eventCalculateFundBonusUL007.create(),
         eventDumpinFundBonusUL007.create(),
         eventCalculateSurrenderValuesUL007.create(),
         eventCalculateDeathBenefitUVL.create(),
         
         // ATP-281 comment out this event to replace by local
         // eventCalculateTotalDeathBenefit.create(),
         eventCalculateTotalDeathBenefitLocal.create(),        

         showMonthlyResults.create()
    ];
    return events;
},
   columns: function(args) {
    var proposal = args["proposal"];
    var columns = [
         columnYear.create(),
         columnAge.create(),
         colAccountLow.create(),
         colAccountMedium.create(),
         colAccountHigh.create(),
         colNarLow.create(),
         colNarMedium.create(),
         colNarHigh.create(),
         colCoiRate.create(),
         colCoiLow.create(),
         colCoiMedium.create(),
         colCoiHigh.create(),
         // add Loyalty Bonus column
         colLoyaltyBonusLow.create(),
         colLoyaltyBonusMedium.create(),
         colLoyaltyBonusHigh.create(),
         colWithdrawal.create(),
         colInitialDumpin.create(),
         colPremium.create(),
         // add Premium Load column
         colPremiumLoad.create(),
         // add Top-up Premium column
	 colTopupPremium.create(),
         colTotalPremium.create(),
         colSurValueLow.create(),
         colSurValueMedium.create(),
         colSurValueHigh.create(),
         colDbLow.create(),
         colDbMedium.create(),
         colDbHigh.create(),
         colTotalDbLow.create(),
         colTotalDbMedium.create(),
         colTotalDbHigh.create(),
         colCashOut.create(),
         colCashOutPaymentLow.create(),
         colCashOutPaymentMedium.create(),
         colCashOutPaymentHigh.create(),
         colFirstMonthCoi.create(),
         colPolicyLapseLow.create(),
         colPolicyLapseMedium.create(),
         colPolicyLapseHigh.create(),

         // Add column "Policy Fee" and "Premium Load & other fees"
         colPolicyFee.create(),
         colPremiumLoadAndOtherFees.create()
    ];
    return columns;
},
   init: function(args) {
    this.self['name'] = 'ULRP_EventsLocal';
    this.self['productId'] = null;
    this.self['productType'] = [CatalogConstants.PRODUCT_TYPE.UVLIFE,CatalogConstants.PRODUCT_TYPE.REGULAR_SAVING];
},
   makeSnapshot: function(args) {
    var context = args['context'];
    var snapshot = {
        year: context.runningPolicyValues.year,
        month: (context.runningPolicyValues.months % 12),
        age: Number(context.insuredAge) + Number(context.runningPolicyValues.year),
        accountValues: IFund.appliedTo(context.runningPolicyValues.fundPool).call("fundBalances", {}),
        nars: context.runningPolicyValues.nars,
        cois: context.runningPolicyValues.annualCois,
        fundBonus: context.runningPolicyValues.fundBonus || { HIGH: 0, LOW: 0, MEDIUM: 0 },
        coiRate: context.runningPolicyValues.coiRate,
        withdrawal: context.runningPolicyValues.withdrawal,
        initialDumpIn: context.runningPolicyValues.initialDumpIn,
        newPremium: context.runningPolicyValues.newPremium - _V( context.runningPolicyValues.topup, 0 ),
        netPremium: context.runningPolicyValues.netPremium - _V( context.runningPolicyValues.topup, 0 ),
        // Add Top-up premium
        topup: context.runningPolicyValues.topup,
        totalPremiums: context.runningPolicyValues.totalPremiums,
        deathBenefits: context.runningPolicyValues.deathBenefits,
        totalDeathBenefits: context.runningPolicyValues.totalDeathBenefits,
        cashOut: context.runningPolicyValues.cashOut,
        cashOutPayments: context.runningPolicyValues.accumulatedcashOutPayments,
        firstMonthCoi: context.runningPolicyValues.firstMonthCoi,
        cashoutAccount: IFund.appliedTo(context.runningPolicyValues.cashoutAccount).call("fundBalances", {}),
        surrenderValuesFromCashoutAccount: IFund.appliedTo(context.runningPolicyValues.cashoutAccount).call("surrenderValues", {}),
        surrenderValues: context.runningPolicyValues.surrenderValues,
        policyLapse: context.runningPolicyValues.lapse,
        policyFee: context.runningPolicyValues.policyFee
    };

     // just accept account value > 0
     for (var returnTypeKey in PublicConstants.FUND_RETURN) {
        snapshot['accountValues'][returnTypeKey] = Math.max( snapshot['accountValues'][returnTypeKey], 0);
     }

    _DBR('context.insuredAge', context.insuredAge, 'ULRP_EventsLocal->', context.runningPolicyValues.months);
    _DBR('context.runningPolicyValues.year', context.runningPolicyValues.year, 'ULRP_EventsLocal->', context.runningPolicyValues.months);
    _DBR('snapshot.age', snapshot.age, 'ULRP_EventsLocal->', context.runningPolicyValues.months);

    return snapshot;
}
});
var TradParCore_Events = CommonEvents.extend({
   events: function(args) {
    var proposal = args["proposal"];
    var events = [
         eventTradParPlanAvyInitialization.create(),
         eventCoreCalculatePremiumBPM44.create(),
         eventCoreAccumulatePremiums.create(),
         eventCalculateTradPlanCIBenefitENCXXX.create(),
         eventCalculateTradPlanDeathBenefitENCXXX.create(),
         eventCalculateTradPlanCashValue.create(),
         eventCalculateTradPlanDividends.create()
    ];
    return events;
},
   columns: function(args) {
    var proposal = args["proposal"];
    var columns = [
         columnYear.create(),
         columnAge.create(),
         colTradBasePlanPremiums.create(),
         colTradRiderPremiums.create(),
         colTradTotalPremiums.create(),
         colTradAccumulatePremiums.create(),
         colTradDb.create(),
         colTradCiBenefit.create(),
         colTradCashValue.create(),
         colTradDividends.create()
    ];
    return columns;
},
   init: function(args) {
    this.self['name'] = 'CommonEvents';
    this.self['productId'] = null;
    this.self['productType'] = [CatalogConstants.PRODUCT_TYPE.PARTICIPATE];
},
   matchProductType: function(args) {
    // return matches
    var productId = args['productId'];
    var catalog = args['catalog'];

    var isUVL = catalog.call("isUVL");
    var matchedTypes = 0;
    if (!isUVL) {
        if (this.self['productType']) {
            for (var ptIndex in this.self['productType']) {
                var prodType = this.self['productType'][ptIndex];
                if (Utils.matchInList(prodType, catalog.call('getCatalog', {}).ProductCatalog.ProductType.ProductTypeCode, false)) {
                    matchedTypes += 1;
                }
            }
        }
    }

    if (matchedTypes == this.self['productType'].length) {
        return matchedTypes;
    } else {
        return 0;
    }
},
   makeSnapshot: function(args) {
    var context = args['context'];
    var snapshot = {
        year: context.runningPolicyValues.year,
        month: (context.runningPolicyValues.months % 12),
        age: Number(context.insuredAge) + Number(context.runningPolicyValues.year),
        totalPremiums: context.runningPolicyValues.totalPremiums,
        accumulatePremiums: context.runningPolicyValues.accPremiums,
        basePlanPremiums: context.runningPolicyValues.basePlanPremiums,
        riderPremiums: context.runningPolicyValues.riderPremiums,
        deathBenefit: context.runningPolicyValues.deathBenefit,
        ciBenefit: context.runningPolicyValues.ciBenefit,
        cashValue: context.runningPolicyValues.cashValue,
        dividends: context.runningPolicyValues.dividends
    };
    return snapshot;
}
});

var em = Repository.getManager(SystemComponents.MANAGER.EVENTS_MANAGER);
if (!em) {
   console.log(''); console.log('++++ Create new Event Manager for setting local event providers into engine. ++++');
   em = IEventsManager.implementBy(EventsManagerImpl.create());
   Repository.addManager(SystemComponents.MANAGER.EVENTS_MANAGER, em);
}
console.log(''); console.log('++++ Loading event providers now ... ++++');
em.call('addEventProvider', {provider : ULRP_EventsLocal.create()});
em.call('addEventProvider', {provider : TradParCore_Events.create()});
console.log(''); console.log('++++ List event providers installed ++++');
em.call('showEventProviders', {});

var BackdateValidationRuleLocal = Class.define({
    ruleName: function(args) {
    return "BackdateValidationRuleLocal";
},
    shouldTest: function(args) {
    var proposal = args["proposal"];
    var basePlan = args["basePlan"];
    var rider = args["rider"];
    return true;
},
    scope: function(args) {
    var s = 0;
    s = s + PublicConstants.TARGET_SCOPE.PROPOSAL;
    //s = s + PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
    //s = s + PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
    return s;
},
    validate: function(args) {
    var proposal = args["proposal"];
    //var basePlan = args["basePlan"];
    //var rider = args["rider"];
    //if error,
    //e.g. return { code: "ERR00010", arguments: variables };
    //e.g. variables[PublicConstants.MESSAGE_FIELD.CURRENCY] = coverage.currency.currencyPK.currencyId;
    //if no error, just return

    var effDate = proposal.policyYearDate;
    if ( Utils.isNothing(effDate) ) {
        return { code: "ERR04941", arguments: {} };
    }

    var effectiveDate = proposal.policyYearDate.date();
    var sixMonthLater = new Date(effectiveDate.getFullYear(), effectiveDate.getMonth() + 6, effectiveDate.getDate());

    var today = new Date();
    today.setHours(0);
    today.setMinutes(0);
    today.setSeconds(0);

    var sixMonthLaterString = sixMonthLater.formatString();
    var todayString = today.formatString();

    _DB(">>>>> effectiveDate", effectiveDate);
    _DB(">>>>> today", todayString);
    _DB(">>>>> sixMonthLater", sixMonthLaterString);

    if (sixMonthLaterString < todayString) {
        return {
            code: "ERR00209",
            arguments: {}
        };
    }
}
});
var CheckCurrentDateEffectiveDateValidationRuleLocal = Class.define({
    ruleName: function(args) {
    return "CheckCurrentDateEffectiveDateValidationRuleLocal";
},
    shouldTest: function(args) {
    return true;
},
    scope: function(args) {
    return PublicConstants.TARGET_SCOPE.PROPOSAL;
},
    validate: function(args) {
    var proposal = args["proposal"];
    var effDate = proposal.policyYearDate;
    if (Utils.isNothing(effDate)) {
      return;
    }

    var effectiveDate = proposal.policyYearDate.date();
    effectiveDate.setHours(0);
    effectiveDate.setMinutes(0);
    effectiveDate.setSeconds(0);

    var today = new Date();
    today.setHours(0);
    today.setMinutes(0);
    today.setSeconds(0);

    if (effectiveDate > today) {
      return {
        code: "ERRC04948",
        arguments: {}
      };
    }
}
});
var CheckProductEffectiveDateValidationRuleLocal = Class.define({
    ruleName: function(args) {
    return "CheckProductEffectiveDateValidationRuleLocal";
},
    shouldTest: function(args) {
    var proposal = args["proposal"];
    var coverage = args["coverage"];
    return true;
},
    scope: function(args) {
    var s = 0;
    //s = s + PublicConstants.TARGET_SCOPE.PROPOSAL;
    s = s + PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
    s = s + PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
    return s;
},
    validate: function(args) {
    var coverage = args["coverage"];
    var proposal = coverage.proposal;
    var valueDate = _PV( coverage, 'proposal.policyYearDate');
    
    var catalog = coverage.catalog.call("getCatalog", null);
    var effectiveFromDate = _PV(catalog, 'ProductCatalog.CreateDate');

    _DB("valueDate", valueDate);
    _DB("effectiveFromDate", effectiveFromDate);

    if (valueDate < effectiveFromDate) {
        return {
            code: "ERR03101",
            arguments: {
                '%P0%': valueDate.substring(0, 8),
                '%P1%': effectiveFromDate
            }
        };
    }
    return;
}
});
var CvgClassCapValidationRuleRHCXOD = Class.define({
    ruleName: function(args) {
    return "CvgClassCapValidationRuleRHCXOD";
},
    shouldTest: function(args) {
    var proposal = args["proposal"];
    var coverage = args["coverage"];
    return true;
},
    scope: function(args) {
    var s = 0;
    //s = s + PublicConstants.TARGET_SCOPE.PROPOSAL;
    //s = s + PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
    s = s + PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
    return s;
},
    validate: function(args) {
    var coverage = args["coverage"];
    var productId = _V( _PV( coverage, "product.productKey.primaryProduct.productPK.productId" ) );

    var insured = getPrimaryInsured({coverage: coverage});
    var proposal = coverage.proposal;
    var rhcicvg;
    for(cvgIdx in proposal.riders.coverageInfo) {
        var riderRec = proposal.riders.coverageInfo[cvgIdx];
        var riderCatalog = riderRec.catalog.call("getCatalog");
        var riderProductId = _V( _PV( riderCatalog, "ProductCatalog.ProductPK.ProductId" ) );
        var riderAlias = _V( _PV( riderCatalog, "ProductCatalog.Alias" ) );
        if (riderAlias == this.self.getRHCIAlias(args)) {
            var riderInsured = getPrimaryInsured({coverage: riderRec});
            if (this.self.isEqualsParty( { party1:insured, party2:riderInsured } )) {
                rhcicvg = riderRec;
                break;
            }
        }
    }
    if (!Utils.isNothing(rhcicvg)) {
        var rhciCvgClass = _V( _PV( rhcicvg , "otherOptions.coverageClass" ) );
        var selfCvgClass = _V( _PV( coverage, "otherOptions.coverageClass" ) );
        if (selfCvgClass > rhciCvgClass) {
            var variables = {};
            variables["%PRODUCT_ID%"] = productId;
            variables["%INSURED%"] = insured;
            variables["%MAX_COVERAGE_CLASS%"] = rhciCvgClass;
            variables["%COVERAGE_CLASS%"] = selfCvgClass;
            return {
                code: "ERR00220",
                arguments: variables
            };
        }
    }
    return;
},
    getRHCIAlias: function(args) {
    return "RHCI";
},
    isEqualsParty: function(args) {
    var party1 = args['party1'];
    var party2 = args['party2'];

    if (Utils.isNothing(party1) || Utils.isNothing(party2)) return false;
    var result = (party1.type == party2.type) &&
        (party1.smokingStatus == party2.smokingStatus) &&
        (party1.insuredSex == party2.insuredSex) &&
        (party1.insuredAge == party2.insuredAge) &&
        (party1.insuredId == party2.insuredId)
    return result;
}
});
var FaceAmountValidationRuleMCXXX = Class.define({
    ruleName: function(args) {
return 'FaceAmountValidationRuleMCXXX';
},
    shouldTest: function(args) {
return true;
},
    scope: function(args) {
return PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
},
    validate: function(args) {
    var rider = args["coverage"];
    if (_V(rider.faceAmount, 0) <= 0) {
        var variables = {};
        variables["%PRODUCT_ID%"] = _PV(rider, "product.productKey.primaryProduct.productPK.productId");
        return {
            code: "ERR45022",
            arguments: variables
        };
    }

    var applicableFA = [200, 300, 500, 1000, 2000, 3000];
    if (applicableFA.indexOf(rider.faceAmount) == -1) {
        return {
            code: 'ERR03060',
            arguments: {}
        }
    }
}
});
var FundAllocationValidationRuleLocal = Class.define({
    ruleName: function(args) {
    return "FundAllocationValidationRuleLocal";
},
    shouldTest: function(args) {
    return true;
},
    scope: function(args) {
    return PublicConstants.TARGET_SCOPE.PROPOSAL;
},
    validate: function(args) {
    var proposal = args["proposal"];

    if (!(proposal.coverageInfo.catalog.call("isUVL"))) {
      return;
    }

    var records = Utils.iterator( _V( _PV( proposal, 'funds.fundRecord' ), [] ) );

    if (records.length > 0) {
      var totalFund = 0;

      for (recordKey in records) {
        totalFund = totalFund + Number(records[recordKey].allocation);
        if (records[recordKey].allocation < 0) {
          return {
            code: 'ERR11009',
            arguments: { '%P0%': "" + records[recordKey].allocation }
          }
        }
      }
    }
}
});
var HealthCareRiderValidationRuleHCXXX = Class.define({
    ruleName: function(args) {
    return 'HealthCareRiderValidationRuleHCXXX';
},
    shouldTest: function(args) {
    return true;
},
    scope: function(args) {
    return PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
},
    validate: function(args) {
    var coverage = args['coverage'];
    var foundHealthCare = false;
    var foundWaiver = false;
    var rider = coverage.schema.call("getSchema", null);
    var riderShortCode = _PV(rider, ['ProductSchema', 'ProductSchemaPK', 'ProductShortCode']);

    if (!riderShortCode) {
        return;
    }

    if (riderShortCode === 'HCR' || riderShortCode === 'HCU') {
        foundHealthCare = true;

        if (Utils.isNothing(schema.ProductSchema.BasicParticular.SupportOtherOptions || Utils.isNothing(rider.ProductSchema.BasicParticular.SupportOtherOptions.CoverageClass))) {
            return;
        }

        var listCvgClass = Utils.iterator(rider.ProductSchema.BasicParticular.SupportOtherOptions.CoverageClass);

        for (var i in listCvgClass) {
            if (listCvgClass[i].length < 3) {
                continue;
            }

            var cvgClassInpatient = listCvgClass[i].substring(0, 1);
            var cvgClassOutpatient = listCvgClass[i].substring(1, 2);
            var cvgClassDental = listCvgClass[i].substring(2, 3);

            if (cvgClassInpatient === SchemaConstants.COVERAGE_CLASS.CLASS_A && cvgClassOutpatient != '0') {
                return {
                    code: 'ERR02007E',
                    arguments: []
                }
            } else if (cvgClassOutpatient > cvgClassInpatient) {
                return {
                    code: 'ERR02007F',
                    arguments: []
                }
            }

            if (cvgClassInpatient == SchemaConstants.COVERAGE_CLASS.CLASS_A && cvgClassDental != '0') {
                return {
                    code: 'ERR02007G',
                    arguments: []
                }
            } else if (cvgClassDental > cvgClassInpatient) {
                return {
                    code: 'ERR02007H',
                    arguments: []
                }
            }
        }

        if (Utils.isNothing(schema.ProductSchema.BasicParticular.SupportOtherOptions || Utils.isNothing(rider.ProductSchema.BasicParticular.SupportOtherOptions.CoverageClass))) {
            return;
        }

        if (!Utils.isNothing(rider.ProductSchema.PolicyValue.PolicyValueTable)) {
            var listCvgPolicy = Utils.iterator(rider.ProductSchema.PolicyValue.PolicyValueTable);

            for (var i in listCvgPolicy) {
                if (!listCvgPolicy[i].PolicyValuePK.PolicyValueId.text) {
                    var policyCode = listCvgPolicy[i].PolicyValuePK.PolicyValueId.text;

                    if (policyCode === 'WOC' || policyCode === 'PWO') {
                        foundWaiver = true;
                    }
                }
            }
        }
    }

    if (foundHealthCare && foundWaiver) {
        return {
            code: 'ERR02007J',
            arguments: []
        }
    }
}
});
var lapseValidationRuleLocal = Class.define({
    ruleName: function(args) {
    return "lapseValidationRuleLocal";
},
    shouldTest: function(args) {
    var proposal = args["proposal"];
    var coverage = args["coverage"];
    return true;
},
    scope: function(args) {
    var s = 0;
    s = s + PublicConstants.TARGET_SCOPE.PROPOSAL;
    //s = s + PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
    //s = s + PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
    return s;
},
    validate: function(args) {
    var proposal = args["proposal"];
    var coverage = args["coverage"];
    var context = args["context"];
    var projectionResult = args["projectionResult"];
    
    //_SS(projectionResult.columns[26], 3);
    //_SS(projectionResult.columns[27], 3);
    //_SS(projectionResult.columns[28], 3);
    //_SS(proposal, 4);

    //if error,
    //e.g. return { code: "ERR00010", arguments: variables };
    //e.g. variables[PublicConstants.MESSAGE_FIELD.CURRENCY] = coverage.currency.currencyPK.currencyId;
    //if no error, just return
    
    var returnRate = this.self.returnRate(args);
    var columns = projectionResult.columns;
    for (colIdx in columns) {
        var column = columns[colIdx];
        if (column.Name == this.self.lapseColumnName(args)) {
            for (valIdx in column.Values) {
                var val = column.Values[valIdx];

                var year = val.year;
                var age = val.age;

                var value = val.value;
                if (value == "Y") {
                    return this.self.errorMessage( { year: year, age: age, returnRate: returnRate } );
                }
            }
        }
    }

    return;
},
    lapseColumnName: function(args) {
    return "Lapse (MEDIUM)";
},
    errorMessage: function(args) {
    var year = args["year"];
    var age = args["age"];
    var returnRate = args["returnRate"];

    return { code: "ERR00214", arguments: { "%POLICY_YEAR%" : year, "%ATTAIN_AGE%" : age, "%RETURN_RATE%": returnRate } };
},
    returnRate: function(args) {
    var fundRecord = args["proposal"].funds.fundRecord[0];
    return fundRecord.returnRateMedium;
}
});
var MaxFamilyMembersValidationRuleMCXXX = Class.define({
    ruleName: function(args) {
return "MaxFamilyMembersValidationRuleMCXXX";
},
    shouldTest: function(args) {
return true;
},
    scope: function(args) {
return PublicConstants.TARGET_SCOPE.PROPOSAL;
},
    validate: function(args) {
    var proposal = args["proposal"];
    var totNumberOfMember = 0;
    if (proposal.riders && proposal.riders.coverageInfo && proposal.riders.coverageInfo.length > 0) {
        for (var idx = 0; idx < proposal.riders.coverageInfo.length; idx++) {
            var coverageR = proposal.riders.coverageInfo[idx];
            var catalogR = coverageR.catalog.call("getCatalog", null);
            if (Utils.matchInList(CatalogConstants.PRODUCT_TYPE.HOSPITAL_BENEFIT, catalogR.ProductCatalog.ProductType.ProductTypeCode, false)) {
                //if (this.self.isInteger({
                //        value: coverageR.NoOfMember
                //    })) {
                //    totNumberOfMember += coverageR.NoOfMember;
                //}
                totNumberOfMember += 1;
            }
        }
    }    
    if (totNumberOfMember > 6) {
        return {
            code: "ERR03066",
            arguments: {}
        };
    }
}
});
var MissingDOBValidationRuleLocal = Class.define({
    ruleName: function(args) {
    return "MissingDOBValidationRuleLocal";
},
    shouldTest: function(args) {
    var proposal = args["proposal"];
    var coverage = args["coverage"];
    return true;
},
    scope: function(args) {
    var s = 0;
    //s = s + PublicConstants.TARGET_SCOPE.PROPOSAL;
    s = s + PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
    s = s + PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
    return s;
},
    validate: function(args) {
    var coverage = args["coverage"];
    var insureds = Utils.iterator( _V( _PV( coverage, 'parties.party' ), [] ));
    var now = Utils.now();
    for (idx in insureds) {
        var insured = insureds[idx];
        var variables = {};
        variables["%INSURED_ID%"] = "" + insured.insuredId;
        if (Utils.isNothing(insured.birthDate)) {
            return {
                code: "ERR04940",
                arguments: variables
            };
        }
        if (insured.birthDate > now.formatString()) {
            return {
                code: "ERR11010",
                arguments: variables
            };
        }
    }
}
});
var MissingFAValidationRuleUL007 = Class.define({
    ruleName: function(args) {
    return "MissingFAValidationRuleUL007";
},
    shouldTest: function(args) {
    return true;
},
    scope: function(args) {
    return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
},
    validate: function(args) {
    var coverage = args["coverage"];
    var faceAmount = _V( coverage.faceAmount, 0 );

    //_SS( coverage, 10 );
    //_DB( "faceAmount", faceAmount );

    if (faceAmount <= 0) {
        return {
            code: "ERR03005",
            arguments: []
        };
    }
}
});
var MissingInsuredNameValidationRuleLocal = Class.define({
    ruleName: function(args) {
    return 'MissingInsuredNameValidationRuleLocal';
},
    shouldTest: function(args) {
    return true;
},
    scope: function(args) {
    return PublicConstants.TARGET_SCOPE.BASE_COVERAGE | PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
},
    validate: function(args) {
  var coverage = args["coverage"];
  var productId = _PV( coverage, "product.productKey.primaryProduct.productPK.productId" );
  var parties = Utils.iterator( _V(_PV(coverage, "parties.party"),[]) );
  for (i in parties) {
      var party = parties[i];
      if (Utils.isNothing(party.insuredId) || party.insuredId == "") {
          return {
            code: "ERR04939",
            arguments: { "%PRODUCT_ID%" : productId }
          };
      }
  }
}
});
var MissingRelationToInsuredValidationRuleLocal = Class.define({
    ruleName: function(args) {
  return 'MissingRelationToInsuredValidationRuleLocal';
},
    shouldTest: function(args) {
  return true;
},
    scope: function(args) {
  return PublicConstants.TARGET_SCOPE.BASE_COVERAGE | PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
},
    validate: function(args) {
  var coverage = args["coverage"];
  var productId = _PV( coverage, "product.productKey.primaryProduct.productPK.productId" );
  var parties = Utils.iterator( _V(_PV(coverage, "parties.party"),[]) );
  for (i in parties) {
      var party = parties[i];
      var insuredId = _V (party.insuredId, "");
      var type = _V (party.type, "");
  
      //_DB("party.insuredId", insuredId);
      //_DB("party.type", type);

      if ( type == "") {
          return {
            code: "ERR11003",
            arguments: { "%PRODUCT_ID%" : productId, "%INSURED_ID%" : insuredId }
          };
      }
  }
}
});
var OneMonthYearOldValidationRuleLocal = Class.define({
    ruleName: function(args) {
    return "OneMonthYearOldValidationRuleLocal";
},
    shouldTest: function(args) {
    var proposal = args["proposal"];
    var coverage = args["coverage"];
    return true;
},
    scope: function(args) {
    var s = 0;
    //s = s + PublicConstants.TARGET_SCOPE.PROPOSAL;
    s = s + PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
    s = s + PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
    return s;
},
    validate: function(args) {
    var coverage = args["coverage"];
    var primary = getPrimaryInsured({
        coverage: coverage
    });
    var productId = _PV(coverage, "product.productKey.primaryProduct.productPK.productId");

    //_SS(primary, 10);

    var longInsuredDob = primary.birthDate;
    if (Utils.isNothing(longInsuredDob)) {
        _DB("OneMonthYearOldValidationRuleLocal: ", "SKIP due to missing DOB", "OneMonthYearOldValidationRuleLocal->");
        return;
    }

    var insuredDob = primary.birthDate.date();
    var oneMonthAfter = new Date(insuredDob.getFullYear(), insuredDob.getMonth() + 1, insuredDob.getDate());
    var lastDayInMonth = new Date(insuredDob.getFullYear(), insuredDob.getMonth() + 2, 0);
    if( oneMonthAfter > lastDayInMonth){
       oneMonthAfter = lastDayInMonth;
    }
    // var oneMonthAfterString = oneMonthAfter.formatString();

    //var today = new Date();
    //today.setHours(0);
    //today.setMinutes(0);
    //today.setSeconds(0);
    //var todayString = today.formatString();
    var policyYearDate = coverage.proposal.policyYearDate.date();
    policyYearDate.setHours(0);
    policyYearDate.setMinutes(0);
    policyYearDate.setSeconds(0);

   _DB("policyYearDate: ", policyYearDate );
   _DB("oneMonthAfter : ", oneMonthAfter );
    if (oneMonthAfter > policyYearDate) {
        variables = {};
        variables['%P0%'] = primary.insuredId;
        variables['%P1%'] = '1';
        variables['%P2%'] = productId;
        return {
            code: 'ERR03100',
            arguments: variables
        }
    }
}
});
var RegularPaymentValidationRuleUL007 = Class.define({
    ruleName: function(args) {
    return "RegularPaymentValidationRuleUL007";
},
    shouldTest: function(args) {
    var proposal = args["proposal"];
    var coverage = args["coverage"];
    return true;
},
    scope: function(args) {
    var s = 0;
    //s = s + PublicConstants.TARGET_SCOPE.PROPOSAL;
    s = s + PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
    //s = s + PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
    return s;
},
    validate: function(args) {
    //var proposal = args["proposal"];
    var coverage = args["coverage"];
    var currencyId = _PV( coverage, "currency.currencyPK.currencyId" );
    var mpre = this.self.getTotalPremium( args );
    //_DB("coverage", coverage, "RegularPaymentValidationRuleUL007");
    _DB("mpre", mpre, "RegularPaymentValidationRuleUL007->");
    _DB("coverage.regularPayment", coverage.regularPayment, "RegularPaymentValidationRuleUL007->");
    if (coverage.regularPayment < mpre) {
        var variables = {};
        variables['%PRODUCT_ID%'] = _PV( coverage, "product.productKey.associateProduct.productPK.productId" );
        variables['%MPRE%'] = currencyId + mpre;
        return {
            code: "ERR04948",
            arguments: variables
        }
    }
    return;
},
    getTotalPremium: function(args) {
    var coverage = args["coverage"];
    var totalPremium = 0;
    for (var totalPremiumsKey in coverage._premiums.totalPremiums) {
        var totalPremiumsRec = coverage._premiums.totalPremiums[totalPremiumsKey];
        if (totalPremiumsRec.paymentMode == coverage.options.paymentMode) {
            totalPremium = totalPremiumsRec.totalPremium;
        }
    }
    return totalPremium;
}
});
var RW_ExceptionValidationRuleLocal = Class.define({
    ruleName: function(args) {
return 'ExceptionValidationRuleLocal';
},
    shouldTest: function(args) {
return true;
},
    scope: function(args) {
return PublicConstants.TARGET_SCOPE.PROPOSAL;
},
    validate: function(args) {
    var proposal = args["proposal"];

    var distributionChannelCode = _PV(proposal, ['DistributionChannelCode']);
    if (!distributionChannelCode) {
      return;
    }

    if (distributionChannelCode == "24" || distributionChannelCode == "30" || distributionChannelCode == "39") {
      if (proposal.riders && proposal.riders.coverageInfo && proposal.riders.coverageInfo.length === 0) {
        return {
          code: "ERR03102",
          arguments: []
        };
      }
    }

    var prodCodeList = "";
    var premPrd = 0;

    var hasWaiverOfCharge = false;
    var hasSupportPrem = false;

    var hasOldWaiver = false;
    var hasWaiver = false;
    var hasHealthCareRider = false;

    if (proposal.riders && proposal.riders.coverageInfo && proposal.riders.coverageInfo.length > 0) {
      for (var idx = 0; idx < proposal.riders.coverageInfo.length; idx++) {
        var coverageR = proposal.riders.coverageInfo[idx];
        var schemaR = coverageR.catalog.call("getSchema", null);

        var productCode = _PV( schemaR, ['ProductSchema', 'ProductSchemaPK', 'ProductShortCode'] );

        if (!productCode) {
          return;
        }

        if (productCode == "PSI" || productCode == "PSP") {
          if (premPrd > 0) {
            var premProduct = _PV( schemaR, ['ProductSchema', 'BasicParticular', 'PremiumPaidPeriod', 'MinPremiumPaidPeriod'] );
            if (!premProduct) {
              return;
            }
            if (premPrd !== premProduct) {
              return {
                code: "ERR03104",
                arguments: { '%P0%': prodCodeList + "/" + productCode }
              };
            }

          } else {
            prodCodeList = productCode;
            premPrd = r.Value.Plan.PremPrd;
          }

        }

        if (productCode == "WOP" || productCode == "WOD") {
          hasOldWaiver = true;
        }

        if (productCode == "WP2" || productCode == "PW2") {
          hasWaiver = true;
        }

        if (productCode == "WOC" || productCode == "PW0") {
          hasWaiverOfCharge = true;
        }

        if (productCode == "PSI" || productCode == "PSP") {
          hasSupportPrem = true;
        }

        if (productCode == "HCR" || productCode == "HCU") {
          hasHealthCareRider = true;
        }
      }
    }

    if (hasWaiverOfCharge && hasSupportPrem) {
      return {
        code: "ERR03103",
        arguments: []
      };
    }

    if (hasWaiverOfCharge && hasHealthCareRider) {
      return {
        code: "ERR02007B",
        arguments: []
      };
    }

    if (hasOldWaiver && hasWaiver) {
      return {
        code: "ERR02007C",
        arguments: []
      };
    }
}
});
var RW_PlanPackageValidationRuleENXXX = Class.define({
    ruleName: function(args) {
    return "PlanPackageValidationRuleENXXX";
},
    shouldTest: function(args) {
    return true;
},
    scope: function(args) {
    return PublicConstants.TARGET_SCOPE.PROPOSAL;
},
    validate: function(args) {
    var proposal = args["proposal"];
    var coverage = proposal.coverageInfo;

    var product = coverage.schema.call("getSchema", null);
    var productShortCode = _PV(product, ['ProductSchema', 'ProductSchemaPK', 'ProductShortCode']);
    if (!productShortCode && productShortCode != 'EN3') {
        return;
    }

    var listRider = Utils.iterator( _V( _PV( proposal, 'riders.coverageInfo'), []) );
    var listApplicableRider = ['WP2', 'PW2', 'WOP', 'WOD'];
    var found = false;
    for (var i in listRider) {
        var riderSchema = listRider[i].schema.call('getSchema', null);
        var riderShortCode = _PV(riderSchema, ['ProductSchema', 'ProductSchemaPK', 'ProductShortCode' ]);
        if (listApplicableRider.indexOf(riderShortCode) < 0) {
            found = true;
            break;
        }
    }

    if (!found) {
        return {
            code: 'ERR02007A',
            arguments: []
        }
    }
}
});
var SameAsInsuredValidationRuleLocal = Class.define({
    ruleName: function(args) {
return "SameAsInsuredValidationRuleLocal";
},
    shouldTest: function(args) {
return true;
},
    scope: function(args) {
return PublicConstants.TARGET_SCOPE.BASE_COVERAGE;
},
    validate: function(args) {
    var coverage = args["coverage"];
    var partyTable = Utils.iterator( _V( _PV( coverage, 'parties.party' ), [] ) );
    var owner = coverage.proposal.owner;
    var insuredIsOwner = owner.insuredIsOwner;
    if (insuredIsOwner) {
        var partyRec = getPrimaryInsured({coverage: coverage});
        var matched = true;
 
        _DB("owner.ownerAge", owner.ownerAge);
        _DB("owner.ownerDOB", owner.ownerDOB);
        _DB("owner.ownerSex", owner.ownerSex);
        _DB("owner.ownerId", owner.ownerId);

        _DB("partyRec.insuredAge", partyRec.insuredAge);
        _DB("partyRec.birthDate", partyRec.birthDate);
        _DB("partyRec.insuredSex", partyRec.insuredSex);
        _DB("partyRec.insuredId", partyRec.insuredId);

        matched = matched && (owner.ownerAge == partyRec.insuredAge);
        matched = matched && (owner.ownerDOB == partyRec.birthDate);
        matched = matched && (partyRec.insuredSex.startsWith(owner.ownerSex));
        matched = matched && (owner.ownerId == partyRec.insuredId);
        if (!matched) {                
            return {
                code: "ERR11005",
                arguments: {}
            };
        }
    }
}
});
var TAKEOUT_MissingPayorAgeValidationRuleLocal = Class.define({
    ruleName: function(args) {
    return "MissingPayorAgeValidationRuleLocal";
},
    shouldTest: function(args) {
    return true;
},
    scope: function(args) {
    return PublicConstants.TARGET_SCOPE.BASE_COVERAGE | PublicConstants.TARGET_SCOPE.RIDER_COVERAGE;
},
    validate: function(args) {
   var coverage = args["coverage"];
    var catalog = coverage.catalog.call("getCatalog", null);
    if (Utils.matchInList(CatalogConstants.PRODUCT_TYPE.PAYORBENEFIT, catalog.ProductCatalog.ProductType.ProductTypeCode, false)) {
      if (!this.isNothingByPath(['parties', 'party'], coverage)) {
        var partyTable = Utils.iterator(coverage.parties.party);
        for (partyi in partyTable) {
          var partyRec = partyTable[partyi];
          if (this.isNullOrWhiteSpaceByPath(['insuredAge'], partyRec)) {
            return {
              code: "ERR11051",
              arguments: []
            };
          }
        }
      }
    }
}
});
var TAKEOUT_OwnerAgeValidationRuleLocal = Class.define({
    ruleName: function(args) {
    return "OwnerAgeValidationRuleLocal";
},
    shouldTest: function(args) {
    return true;
},
    scope: function(args) {
    return PublicConstants.TARGET_SCOPE.PROPOSAL;
},
    validate: function(args) {
    var proposal = args["proposal"];
    var coverage = proposal.coverageInfo;

    var catalog = coverage.catalog.call("getCatalog", null);
    var planCode = Utils.matchInList(
      CatalogConstants.PRODUCT_TYPE.BASEPLAN,
      catalog.ProductCatalog.ProductType.ProductTypeCode,
      false
    );

    if (planCode && (planCode == "EDS01" || planCode == "EDS02" || planCode == "BES01" || planCode == "BES02")) {
      var minAge = 18;
      var maxAge = 50;
      if (!proposal.owner) {
        return;
      }
      if (proposal.owner.insuredIsOwner) {
        var ownerAge = proposal.owner.ownerAge;
        if (ownerAge < minAge || ownerAge > maxAge) {
          return {
            code: "ERR03071",
            arguments: { '%P0%': planCode, '%P1%': minAge, '%P2%': maxAge }
          };
        }
      }

    }
}
});
var FaceAmountValidationRuleLocal = FaceAmountValidationRule.extend({
    ruleName: function(args) {
      return "FaceAmountValidationRuleLocal";
},
    validate: function(args) {
    var coverage = args["coverage"];
    var catalog = coverage.catalog.call('getCatalog');
    var productTypes = _PV(catalog, 'ProductCatalog.ProductType.ProductTypeCode');

    var isBenefit = false;
    for (i in productTypes) {
        var pt = _V(productTypes[i]);
        if (pt == 'Benefit') {
            isBenefit = true;
            break;
        }
    }

    if (!isBenefit) {
        if (_V(coverage.faceAmount, 0) <= 0) {
            //Error: Sum insured is not yet inputted (%PRODUCT_ID%)
            var variables = {};
            variables["%PRODUCT_ID%"] = _PV(coverage, "product.productKey.primaryProduct.productPK.productId");
            return {
                code: "ERR03005",
                arguments: variables
            };
        }
    }
    return this.self.parent.validate(args);
}
});
var FaceAmountValidationRuleUL007 = FaceAmountValidationRule.extend({
    ruleName: function(args) {
    return "FaceAmountValidationRuleUL007";
},
    shouldTest: function(args) {
    return true;
},
    validate: function(args) {
    var coverage = args['coverage'];
    var faceAmount = coverage.faceAmount;
    var factors = this.self.getMultiplier(args);
    if (factors.min == 0 && factors.max == 0) {
        _DB("", "Multiplier cannot be located. The limit of multiplier is skipped.", "");
    } else {
        var limitAmounts = this.self.getMultiplierLimits(args);
        var plannedPremium = _V(coverage.plannedPremium, 0);

        if (_V(coverage.faceAmount, 0) <= 0) {
            var variables = {};
            variables["%PRODUCT_ID%"] = _PV(coverage, "product.productKey.primaryProduct.productPK.productId");
            return {
                code: "ERR45022",
                arguments: variables
            };
        }

        if (plannedPremium > 0) {
            _DB("faceAmount", faceAmount, "FaceAmountValidationRuleUL007->");
            _DB("limitAmounts", limitAmounts, "FaceAmountValidationRuleUL007->");
            if (faceAmount < limitAmounts.minLimit || faceAmount > limitAmounts.maxLimit) {
                var variables = {};
                variables['%POLICY_YEAR%'] = "1";
                variables['%MIN_MULTIPIER%'] = factors.min;
                variables['%MAX_MULTIPIER%'] = factors.max;
                return {
                    code: 'ERR03081',
                    arguments: variables
                }
            }
        } else {
            _DB("", "planned premium is zero. multiplier check will be skipped", "");
        }
    }
    return this.self.parent.validate(args);
},
    getMultiplier: function(args) {
    var coverage = args['coverage'];
    var insured = getPrimaryInsured({ coverage: coverage });

    _DB("insured", insured);
    if (Utils.isNothing(insured)) {
        return { };
    }

    var insuredAge = insured.insuredAge;
    var schema = coverage.schema.call("getSchema", null);
    var multiplierTable = Utils.iterator(schema.ProductSchema.FaceAmountMultiplier.MultiplierRecord);

    //_SS(multiplierTable, 10);

    var result;
    for (var i in multiplierTable) {
        var multiplier = multiplierTable[i];
        if (!Utils.isNothing(multiplier)) {
            var minAge = _V(multiplier.MinIssueAge);
            var maxAge = _V(multiplier.MaxIssueAge);
            var minFAMultiplier = _V(multiplier.MinFAMultiplier, 0);
            var maxFAMultiplier = _V(multiplier.MaxFAMultiplier, 0);

            _DB("minAge[" + i + "]", minAge);
            _DB("maxAge[" + i + "]", maxAge);
            _DB("minFAMultiplier[" + i + "]", minFAMultiplier);
            _DB("maxFAMultiplier[" + i + "]", maxFAMultiplier);

            if (minAge <= insuredAge && insuredAge <= maxAge) {
                _DB("","Multiplier is matched");
               result = { min: minFAMultiplier, max: maxFAMultiplier};
                break;
            }
        }
    }
    if (Utils.isNothing(result)) {
        _DB("", "@@@@@ Multiplier cannot be found");
        return {min: 0, max: 0};
    }
    return result;
},
    getMultiplierLimits: function(args) {
  var coverage = args['coverage'];
  var insured = getPrimaryInsured({
    coverage: coverage
  });

  _DB("insured", insured);
  if (Utils.isNothing(insured)) {
    return {};
  }

  var plannedPremium = this.self.getTotalPremium(args);
  _DB("plannedPremium", plannedPremium);

  var multipliers = this.self.getMultiplier(args);
  var minFactor = (1000 / multipliers.max) + (coverage.extraRating.tempFlat + coverage.extraRating.flatExtra) / 0.4;
  var maxFactor = (1000 / multipliers.min) + (coverage.extraRating.tempFlat + coverage.extraRating.flatExtra) / 0.4;

  var minFA = plannedPremium / maxFactor * 1000;
  var maxFA = plannedPremium / minFactor * 1000;

  _DB("Min multiplier limit", minFA);
  _DB("Max multiplier limit", maxFA);
  
  return {
    minLimit: minFA,
    maxLimit: maxFA
  };
},
    getTotalPremium: function(args) {
    var coverage = args["coverage"];
    var pp = _V(coverage.plannedPremium, 0);
    var ppfreq = 1;
    if (!(Utils.isNothing(coverage.options)) && !(Utils.isNothing(coverage.options.paymentMode))) {
        if (coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.ANNUAL) {
            ppfreq = 1;
        } else if (coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.SEMIANNUAL) {
            ppfreq = 2;
        } else if (coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.QUARTERLY) {
            ppfreq = 4;
        } else if (coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.MONTHLY) {
            ppfreq = 12;
        }
    }
    return (ppfreq * pp);
}
});
var FundActivitiesValidationRuleUL007 = FundActivityValidationRule.extend({
    ruleName: function(args) {
    return "FundActivitiesValidationRuleUL007";
},
    validate: function(args) {
    var coverage = args['coverage'];
    var proposal = coverage.proposal;
    var schema = coverage.schema.call("getSchema");
    var primary = getPrimaryInsured({
        coverage: coverage
    });
    var fundActivities = _V(_PV(proposal, 'fundActivities.fundActivity'), []);
    var basePlanId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');
    var currencyId = _PV(coverage, "currency.currencyPK.currencyId");

    if (Utils.isNothing(primary)) {
        console.log("No primary insured is setup. The rule is skipped.");
        return;
    }
    var primaryInsuredAge = Number(_V(primary.insuredAge, 0));

    _DB("basePlanId", basePlanId);
    _DB("primaryInsuredAge", primaryInsuredAge);

    var multiplier = this.self.getMultiplier(args);
    var plannedPremium = coverage.plannedPremium;
    var regularPayment = coverage.regularPayment;
    var faceAmount = coverage.faceAmount;
    var accum3YearsPremium = 0;
    var total3YearsMpre = Number( this.self.getMPRE(args) ) * 3;
    var ppp = coverage.schema.call("calculatePremiumPayingPeriod", {coverage: coverage, issueAge: primary.issueAge});
    for (var pyr=0; pyr<ppp; pyr++) {
        for (var idx in fundActivities) {
            var fundActivity = fundActivities[idx];
            if (Utils.isNothing(fundActivity.attainAge)) {
                //Error: The attain age is missing in fund activity.
                return {
                    code: "ERR00217",
                    arguments: {}
                };
            }
    
            var policyYear = Number(fundActivity.attainAge) - primaryInsuredAge;
            if (policyYear == pyr) {
                var withdrawal = _V(fundActivity.withdrawal);
                
                regularPayment = _V(fundActivity.regularPayment, regularPayment);
                plannedPremium = _V(fundActivity.plannedPremium, plannedPremium);
                faceAmount = _V(fundActivity.faceAmount, faceAmount);
                
                var multiplierFaLimits = this.self.getMultiplierFALimits( {args: args, plannedPremium: plannedPremium} );
                var multiplierPpLimits = this.self.getMultiplierPPLimits( {args: args, faceAmount: faceAmount} );
                
                _DB("fundActivity[" + policyYear + "].plannedPremium", plannedPremium);
                _DB("fundActivity[" + policyYear + "].regularPayment", regularPayment);
                _DB("fundActivity[" + policyYear + "].withdrawal", withdrawal);
                _DB("fundActivity[" + policyYear + "].faceAmount", faceAmount);
        
                if (!Utils.isNothing( fundActivity.plannedPremium )) {
                    if (plannedPremium <= 0) {
                        //Error: Planned premium in year %P0% is less than zero.
                        return {
                            code: "ERR00218",
                            arguments: {
                                "%P0%": "" + policyYear,
                                "%MIN%": "" + 0
                            }
                        };
                    }
                    if (plannedPremium < multiplierPpLimits.minLimit || plannedPremium > multiplierPpLimits.maxLimit) {
                        //Error: Multiplier year %POLICY_YEAR% out of range %MIN_MULTIPIER% and %MAX_MULTIPIER%
                        /*
                        var variables = {};
                        variables["%PRODUCT_ID%"] = basePlanId;
                        variables['%MIN_MULTIPIER%'] = "" + multiplier.min;
                        variables['%MAX_MULTIPIER%'] = "" + multiplier.max;
                        variables['%POLICY_YEAR%'] = "" + pyr;
                        return {
                            code: 'ERR03081',
                            arguments: variables
                        }
                        */
                        var variables = {};
                        variables["%PRODUCT_ID%"] = basePlanId;
                        variables['%MIN%'] = "" + currencyId + "" + multiplierPpLimits.minLimit;
                        variables['%MAX%'] = "" + currencyId + "" + multiplierPpLimits.maxLimit;
                        variables['%POLICY_YEAR%'] = "" + pyr;
                        return {
                            code: 'ERR00225',
                            arguments: variables
                        }
                    }
                    if (policyYear < 3) {
                        //ERR00224 - Error: Planned Premium cannot be changed in first 3 years
                        return {
                            code: "ERR00224",
                            arguments: {
                                "%P0%": "" + policyYear
                            }
                        };
                    }
                }
        
                if (!Utils.isNothing(fundActivity.regularPayment)) {
                    if (regularPayment <= 0) {
                        //ERR03079 - Error: Premium in year %POLICY_YEAR%  must be greater than 0
                        return {
                            code: "ERR03079",
                            arguments: {
                                "%P0%": "" + policyYear,
                                "%MIN%": "" + 0
                            }
                        };
                    }
                    if (regularPayment < 300) {
                        //ERR00223 - Error: Regular Payment cannot be less than 300.
                        return {
                            code: "ERR00223",
                            arguments: {
                                "%P0%": "" + policyYear,
                                "%MIN%": "" + 300
                            }
                        };
                    }
                    if (policyYear < 3) {
                        //ERR00221 - Error: Regular Payment cannot be changed in first 3 year.
                        return {
                            code: "ERR00221",
                            arguments: {
                                "%P0%": "" + policyYear
                            }
                        };
                    }
                }
        
                if (!Utils.isNothing(fundActivity.withdrawal)) {
                    if (withdrawal > 0 && policyYear == 0) {
                        //Error: Can not witdrawal in year %P0%
                        return {
                            code: 'ERR03080',
                            arguments: {
                                '%P0%': "" + policyYear
                            }
                        };
                    }
                    if (withdrawal < 0) {
                        //Error: Withdrwal in year %P0% cannot be less than zero.
                        return {
                            code: 'ERR00219',
                            arguments: {
                                '%P0%': "" + policyYear
                            }
                        };
                    }
                }
                
                if (!Utils.isNothing(fundActivity.faceAmount)) {
                    if (faceAmount < multiplierFaLimits.minLimit || faceAmount > multiplierFaLimits.maxLimit) {
                        /*
                        //ERR03081 - Error: Multiplier year %POLICY_YEAR% out of range %MIN_MULTIPIER% and %MAX_MULTIPIER%
                        var variables = {};
                        variables["%PRODUCT_ID%"] = basePlanId;
                        variables['%MIN_MULTIPIER%'] = "" + multiplier.min;
                        variables['%MAX_MULTIPIER%'] = "" + multiplier.max;
                        variables['%POLICY_YEAR%'] = "" + pyr;
                        return {
                            code: 'ERR03081',
                            arguments: variables
                        }
                        */
                        var variables = {};
                        variables["%PRODUCT_ID%"] = basePlanId;
                        variables['%MIN%'] = "" + currencyId + "" + multiplierFaLimits.minLimit;
                        variables['%MAX%'] = "" + currencyId + "" + multiplierFaLimits.maxLimit;
                        variables['%POLICY_YEAR%'] = "" + pyr;
                        return {
                            code: 'ERR03083',
                            arguments: variables
                        }
                    }
                }
            }
            
            if (pyr < 3) {
                accum3YearsPremium = accum3YearsPremium + regularPayment;
                //ERR04949 - Error: Sum of payment for first 3 years cannot be less than the total premiums of base plan and riders of first 3 years.
                if (pyr == 2) {
                    if (accum3YearsPremium < total3YearsMpre) {
                        var variables = {};
                        variables['%PRODUCT_ID%'] = basePlanId;
                        variables["%MPRE_3YRS%"] = "" + currencyId + total3YearsMpre;
                        variables["%ACCUM_PREM%"] = "" + currencyId + accum3YearsPremium;
                        variables["%MIN_3YR_PREM%"] = "" + currencyId + total3YearsMpre;
                        return {
                            code: "ERR04949",
                            arguments: variables
                        }
                    }
                }
            }
        }
    }
    
    // should be obtained from schema
    var faceAmountLimits = this.self.getFaceAmountLimits({coverage: coverage});
    var lowerFALimit = faceAmountLimits.minLimit;
    var result = this.self.parent.validate( args );
    if (!Utils.isNothing( result )) {
        _DB( "result (parent)", result );
        if (result.code == "ERRC0146") {
            var age = result.arguments["%AGE%"];
            var policyYear = Number(age) - primaryInsuredAge;
            //Error: Face amount of plan %P0% in year %P1% is lower than minimum face amount %P2%
            result = {
                code: 'ERR03074',
                arguments: {
                    '%P0%': "" + basePlanId,
                    '%P1%': "" + policyYear,
                    '%P2%': "" + lowerFALimit
                }
            };
            _DB( "result (override)", result );
        }
        return result;
    }
},
    getMultiplier: function(args) {
    var coverage = args['coverage'];
    var insured = getPrimaryInsured({ coverage: coverage });

    _DB("insured", insured);
    if (Utils.isNothing(insured)) {
        return { };
    }

    var insuredAge = insured.insuredAge;
    var schema = coverage.schema.call("getSchema", null);
    var multiplierTable = Utils.iterator(schema.ProductSchema.FaceAmountMultiplier.MultiplierRecord);

    //_SS(multiplierTable, 10);

    var result;
    for (var i in multiplierTable) {
        var multiplier = multiplierTable[i];
        if (!Utils.isNothing(multiplier)) {
            var minAge = _V(multiplier.MinIssueAge);
            var maxAge = _V(multiplier.MaxIssueAge);
            var minFAMultiplier = _V(multiplier.MinFAMultiplier, 0);
            var maxFAMultiplier = _V(multiplier.MaxFAMultiplier, 0);

            _DB("minAge[" + i + "]", minAge);
            _DB("maxAge[" + i + "]", maxAge);
            _DB("minFAMultiplier[" + i + "]", minFAMultiplier);
            _DB("maxFAMultiplier[" + i + "]", maxFAMultiplier);

            if (minAge <= insuredAge && insuredAge <= maxAge) {
                _DB("","Multiplier is matched");
               result = { min: minFAMultiplier, max: maxFAMultiplier};
                break;
            }
        }
    }
    if (Utils.isNothing(result)) {
        _DB("", "@@@@@ Multiplier cannot be found");
        return {min: 0, max: 0};
    }
    return result;
},
    getMultiplierFALimits: function(args) {
  var vargs = args['args'];
  var latestPlannedPremium = args['plannedPremium'];
  
  var coverage = vargs['coverage'];
  var insured = getPrimaryInsured({
    coverage: coverage
  });

  _DB("insured", insured);
  if (Utils.isNothing(insured)) {
    return {};
  }

  var plannedPremium = _V(coverage.plannedPremium, 0);
  if (latestPlannedPremium) {
      plannedPremium = Number( latestPlannedPremium );
  }
  _DB("plannedPremium", plannedPremium);

  var multipliers = this.self.getMultiplier(vargs);
  if (multipliers.max == 0 && multipliers.min == 0) return { minLimit: 0, maxLimit: 9999999999 };

  var minFactor = (1000 / multipliers.max) + (coverage.extraRating.tempFlat + coverage.extraRating.flatExtra) / 0.4;
  var maxFactor = (1000 / multipliers.min) + (coverage.extraRating.tempFlat + coverage.extraRating.flatExtra) / 0.4;

  var minFA = plannedPremium / maxFactor * 1000;
  var maxFA = plannedPremium / minFactor * 1000;

  _DB("Min multiplier limit", minFA);
  _DB("Max multiplier limit", maxFA);
  
  return {
    minLimit: minFA,
    maxLimit: maxFA
  };
},
    getMultiplierPPLimits: function(args) {
  var vargs = args['args'];
  var latestFaceAmount = args['faceAmount'];
  
  var coverage = vargs['coverage'];
  var insured = getPrimaryInsured({
    coverage: coverage
  });

  _DB("insured", insured);
  if (Utils.isNothing(insured)) {
    return {};
  }

  var faceAmount = _V(coverage.faceAmount, 0);
  if (latestFaceAmount) {
      faceAmount = Number( latestFaceAmount );
  }
  _DB("faceAmount", faceAmount);

  var multipliers = this.self.getMultiplier(vargs);
  if (multipliers.max == 0 && multipliers.min == 0) return { minLimit: 0, maxLimit: 9999999999 };

  var minFactor = (1000 / multipliers.max) + (coverage.extraRating.tempFlat + coverage.extraRating.flatExtra) / 0.4;
  var maxFactor = (1000 / multipliers.min) + (coverage.extraRating.tempFlat + coverage.extraRating.flatExtra) / 0.4;

  var maxPP = faceAmount / 1000 * maxFactor;
  var minPP = faceAmount / 1000 * minFactor;

  _DB("Min multiplier PP", minPP);
  _DB("Max multiplier PP", maxPP);
  
  return {
    minLimit: minPP,
    maxLimit: maxPP
  };
},
    getFaceAmountLimits: function(args) {
    /*
    // UL007 only
    var result = this.self.parent.getFaceAmountLimits(args);
    _DB("+++++ Face Amount Limit (1)", result, "FundActivitiesValidationRuleUL007->");

    // multiplier limit
    var multiplierLimit = this.self.getMultiplierFALimits(args);
    result.minLimit = Math.max(result.minLimit, multiplierLimit.minLimit);
    result.maxLimit = Math.min(result.maxLimit, multiplierLimit.maxLimit);
    _DB("+++++ Face Amount Limit (2)", result, "FundActivitiesValidationRuleUL007->");

    return result;
    */
    return this.self.parent.getFaceAmountLimits(args);
},
    getPlannedPremiumLimits: function(args) {
    /*
    var limit = this.self.parent.getPlannedPremiumLimits(args);
    _DB("PP limit", limit, "FundActivitiesValidationRuleUL007->");
    
    var ppLimit = this.self.getMultiplierPPLimits(args);
    _DB("Multipier limit", ppLimit, "FundActivitiesValidationRuleUL007->");
    limit = {
        minPremium: Math.max(limit.minPremium, ppLimit.minLimit),
        maxPremium: Math.min(limit.maxPremium, ppLimit.maxLimit)
    };
    _DB("Combine limit", limit, "FundActivitiesValidationRuleUL007->");
    return limit;
    */
    return this.self.parent.getPlannedPremiumLimits(args);
},
    getMPRE: function(args) {
    var coverage = args["coverage"];
    var totalPremium = 0;
    for (var totalPremiumsKey in coverage._premiums.totalPremiums) {
        var totalPremiumsRec = coverage._premiums.totalPremiums[totalPremiumsKey];
        if (totalPremiumsRec.paymentMode == coverage.options.paymentMode) {
            totalPremium = totalPremiumsRec.totalPremium;
        }
    }
    return totalPremium;
}
});
var InsuredSexValidationRuleLocal = InsuredSexValidationRule.extend({
    ruleName: function(args) {

    return "InsuredSexValidationRuleLocal";
},
    validate: function(args) {
    var coverage = args["coverage"];
    var parties = Utils.iterator( _V( _PV(coverage, 'parties.party'), []) )    
    for (partyi in parties) {
        var partyRec = parties[partyi];
        if (Utils.isNothing(partyRec.insuredSex) || partyRec.insuredSex == "") {
            return {
                code: "ERR00210",
                arguments: []
            };
        }
    }
    return this.self.parent.validate(args);
}
});
var InsuredSmokingStatusValidationRuleLocal = InsuredSmokingStatusValidationRule.extend({
    ruleName: function(args) {
return "InsuredSmokingStatusValidationRuleLocal";
},
    shouldTest: function(args) {
return this.self.parent.shouldTest(args);
},
    scope: function(args) {
return this.self.parent.scope(args);
},
    validate: function(args) {

var coverage = args["coverage"];
if (!(Utils.isNothing(coverage)) && !(Utils.isNothing(coverage.parties)) && !(Utils.isNothing(coverage.parties.party)) && !(Utils.isNothing(coverage.parties.party.smokingStatus))) {
    _DB(">>>>> coverge party information is inputted as usual.", "");
    return this.self.parent.validate(args);
} else {
    _DB(">>>>> information is missing.", "");
    return { code: "ERR00211", arguments: [] };
}
}
});
var IssueAgeValidationRuleLocal = IssueAgeValidationRule.extend({
    ruleName: function(args) {
return "IssueAgeValidationRuleLocal";
},
    shouldTest: function(args) {
return this.self.parent.shouldTest(args);
},
    scope: function(args) {
return this.self.parent.scope(args);
},
    validate: function(args) {
    var coverage = args["coverage"];
    var result = this.self.parent.validate(args);

    _DB("result", result, "IssueAgeValidationRuleLocal->")
    if (!Utils.isNothing(result)) {
        var schema = coverage.schema.call('getSchema');
        var productId = _V( _PV( coverage, "product.productKey.primaryProduct.productPK.productId" ), "");
        var minIssueAge = _V( _PV( schema, "ProductSchema.BasicParticular.IssueAge.Min" ) );
        var maxIssueAge = _V( _PV( schema, "ProductSchema.BasicParticular.IssueAge.Max" ) );
        result.arguments['%PRODUCT_ID%'] = productId;
        result.arguments['%MIN_ISSUE_AGE%'] = "" + minIssueAge;
        result.arguments['%MAX_ISSUE_AGE%'] = "" + maxIssueAge;
        return result;
    }

    //var isNumber = function(n) {
    //    return !isNaN(parseFloat(n)) && isFinite(n)
    //}

    _DB("+++++", "Local checking is being performed", "IssueAgeValidationRuleLocal->")
    var parties = Utils.iterator( _V( _PV( coverage, 'parties.party' ), [] ) );
    _DB("parties", parties, "IssueAgeValidationRuleLocal->")
    for( i in parties ) {
        var party = parties[i];
        var insuredAge = _V(party.insuredAge);
        var insuredId = _V(party.insuredId, "");

        _DB("party", party, "IssueAgeValidationRuleLocal.parties(looping)->")

        var variables = {};
        variables["%INSURED_ID%"] = insuredId;
        if (Utils.isNothing(insuredAge)) {
            return {
                code: "ERR00212",
                arguments: variables
            }
        }
        
        /*
        var isNumber1 =  !isNaN(parseFloat(insuredAge));
        var isNumber2 =  isFinite(insuredAge);
        var isNumber3 = Number( "" + insuredAge ).valueOf();
        var isNumber4 = (isNumber3 == insuredAge);
 
        _DB("isNumber1", isNumber1);
        _DB("isNumber2", isNumber2);
        _DB("isNumber3", isNumber3);
        _DB("isNumber4", isNumber4);
        */

        if (!Utils.isNothing(insuredAge) && isNaN(parseFloat(insuredAge)) && !isFinite(insuredAge)) {
            return {
                code: "ERR03002",
                arguments: variables
            }
        }
    }
}
});
var JuvenileRiderTypeValidationRuleLocal = JuvenileRiderTypeValidationRule.extend({
    ruleName: function(args) {
      return "JuvenileRiderTypeValidationRuleLocal";
},
    validate: function(args) {
    var result = this.self.parent.validate(args);
    if (result) {
        if (result.code == "ERRC0196") {
            result = undefined;
        }
    }
    return result;
}
});
var lapseValidationRuleHighLocal = lapseValidationRuleLocal.extend({
    ruleName: function(args) {
    return "lapseValidationRuleHighLocal";
},
    lapseColumnName: function(args) {
    return "Lapse (HIGH)";
},
    returnRate: function(args) {
    var fundRecord = args["proposal"].funds.fundRecord[0];
    return fundRecord.returnRateHigh;
}
});
var lapseValidationRuleLowLocal = lapseValidationRuleLocal.extend({
    ruleName: function(args) {
    return "lapseValidationRuleLowLocal";
},
    lapseColumnName: function(args) {
    return "Lapse (LOW)";
},
    returnRate: function(args) {
    var fundRecord = args["proposal"].funds.fundRecord[0];
    return fundRecord.returnRate;
}
});
var OccupationClassValidationRuleLocal = OccupationClassValidationRule.extend({
    ruleName: function(args) {
    return "OccupationClassValidationRuleLocal";
},
    validate: function(args) {
    var result = this.self.parent.validate(args);
    if (!Utils.isNothing(result)) {
        return result;
    }

    var coverage = args["coverage"];
    var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');
    var insured = getPrimaryInsured( {coverage: coverage} );
    var insuredId = _V(insured.insuredId, "");
    var insuredAge = _V( insured.insuredAge, 0);

    if ((insuredAge < 18) && (coverage.occupation < SchemaConstants.OCCUPATION_CLASS.CLASS_2)) {
        var variables = {};
        variables['%INSURED_ID%'] = insuredId;
        variables['%MINIMUM_OCCUPATION_CLASS%'] = SchemaConstants.OCCUPATION_CLASS.CLASS_2;
        return {
            code: "ERR03064",
            arguments: variables
        }
    }
}
});
var PaymentModeValidationRuleLocal = PaymentModeValidationRule.extend({
    ruleName: function(args) {
    return "PaymentModeValidationRuleLocal";
},
    validate: function(args) {
    var coverage = args['coverage'];
    if (!Utils.isNothing( _PV( coverage, 'options.paymentMode'))) {
        return this.self.parent.validate(args);
    } else {
        return {
            code: 'ERR00216',
            arguments: []
        };
    }
}
});
var TotalPremiumValidationRuleUL007 = TotalPremiumValidationRule.extend({
    ruleName: function(args) {
    return "TotalPremiumValidationRuleUL007";
},
    shouldTest: function(args) {
    return true;
},
    scope: function(args) {
    return PublicConstants.TARGET_SCOPE.PROPOSAL;
},
    validate: function(args) {
    var proposal = args["proposal"];
    var coverage = proposal.coverageInfo;
    var productId = _PV(coverage, 'product.productKey.primaryProduct.productPK.productId');
    var schema = coverage.schema.call("getSchema", null);
    var catalog = coverage.catalog.call("getCatalog", null);
    var faceAmount = _V(coverage.faceAmount, 0);

    var constantPaymentMode = "";
    var minError = "";
    var maxError = "";
    var issueAge = 0;
    var maxPP = 0;
    var minPP = 0;

    // UL007 base plan only
    var totalPremium = this.self.getTotalPremium(args);
    _DB("basePlan Premium", totalPremium, "TotalPremiumValidationRuleUL007->");

    var variables = {
        "%PRODUCT_ID%": productId
    };
    variables[PublicConstants.MESSAGE_FIELD.TOTAL_AMOUNT] = coverage.currency.currencyPK.currencyId + " " + totalPremium;

    if (totalPremium <= 0) {
        return {
            // code: "ERR45104",
            code: "ERR04917",
            arguments: variables
        }
    }

    var factors = this.self.getMultiplier(args);
    if (factors.min == 0 && factors.max == 0) {
        _DB("", "Multiplier cannot be located. So, the checking on multiplier limits is skipped.", "");
    } else {
        if (faceAmount > 0) {
            var ppRange = this.self.getMultiplierLimits(args);
            _DB("ppRange", ppRange, "TotalPremiumValidationRuleUL007->");

            if (totalPremium < ppRange.minLimit || totalPremium > ppRange.maxLimit) {
                var variables = {};
                variables['%POLICY_YEAR%'] = "1";
                variables['%MIN_MULTIPIER%'] = factors.min;
                variables['%MAX_MULTIPIER%'] = factors.max;
                return {
                    code: 'ERR03081',
                    arguments: variables
                }
            }
        } else {
            _DB("", "face amount is zero. multiplier check will be skipped", "");
        }
    }

    return this.self.parent.validate(args);
},
    getMultiplier: function(args) {
    var proposal = args['proposal'];
    var coverage = proposal.coverageInfo;
    var insured = getPrimaryInsured({ coverage: coverage });

    _DB("insured", insured);
    if (Utils.isNothing(insured)) {
        return { };
    }

    var insuredAge = insured.insuredAge;
    var schema = coverage.schema.call("getSchema", null);
    var multiplierTable = Utils.iterator(schema.ProductSchema.FaceAmountMultiplier.MultiplierRecord);

    //_SS(multiplierTable, 10);

    var result;
    for (var i in multiplierTable) {
        var multiplier = multiplierTable[i];
        if (!Utils.isNothing(multiplier)) {
            var minAge = _V(multiplier.MinIssueAge);
            var maxAge = _V(multiplier.MaxIssueAge);
            var minFAMultiplier = _V(multiplier.MinFAMultiplier, 0);
            var maxFAMultiplier = _V(multiplier.MaxFAMultiplier, 0);

            _DB("minAge[" + i + "]", minAge);
            _DB("maxAge[" + i + "]", maxAge);
            _DB("minFAMultiplier[" + i + "]", minFAMultiplier);
            _DB("maxFAMultiplier[" + i + "]", maxFAMultiplier);

            if (minAge <= insuredAge && insuredAge <= maxAge) {
                _DB("","Multiplier is matched");
               result = { min: minFAMultiplier, max: maxFAMultiplier};
                break;
            }
        }
    }
    if (Utils.isNothing(result)) {
        _DB("", "@@@@@ Multiplier cannot be found");
        return {min: 0, max: 0};
    }
    return result;
},
    getMultiplierLimits: function(args) {
  var proposal = args['proposal'];
  var coverage = proposal.coverageInfo;
  var insured = getPrimaryInsured({
    coverage: coverage
  });

  _DB("insured", insured);
  if (Utils.isNothing(insured)) {
    return {};
  }

  var faceAmount = _V(coverage.faceAmount, 0);
  _DB("faceAmount", faceAmount);

  var multipliers = this.self.getMultiplier(args);
  var minFactor = (1000 / multipliers.max) + (coverage.extraRating.tempFlat + coverage.extraRating.flatExtra) / 0.4;
  var maxFactor = (1000 / multipliers.min) + (coverage.extraRating.tempFlat + coverage.extraRating.flatExtra) / 0.4;

  var maxPP = faceAmount / 1000 * maxFactor;
  var minPP = faceAmount / 1000 * minFactor;

  _DB("Min multiplier PP", minPP);
  _DB("Max multiplier PP", maxPP);
  
  return {
    minLimit: minPP,
    maxLimit: maxPP
  };
},
    getTotalPremium: function(args) {
    var proposal = args["proposal"];
    var coverage = proposal.coverageInfo;
    var pp = _V(coverage.plannedPremium, 0);
    var ppfreq = 1;
    if (!(Utils.isNothing(coverage.options)) && !(Utils.isNothing(coverage.options.paymentMode))) {
        if (coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.ANNUAL) {
            ppfreq = 1;
        } else if (coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.SEMIANNUAL) {
            ppfreq = 2;
        } else if (coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.QUARTERLY) {
            ppfreq = 4;
        } else if (coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.MONTHLY) {
            ppfreq = 12;
        }
    }
    return (ppfreq * pp);
}
});

var vr = Repository.getManager(SystemComponents.MANAGER.VALIDATOR);
if (!vr) {
   console.log(''); console.log('++++ Create new Validator for setting local rules into engine. ++++');
   vr = IValidator.implementBy(ValidatorImpl.create({productValidationMapping: mapping}));
   Repository.addManager(SystemComponents.MANAGER.VALIDATOR, vr);
}
console.log(''); console.log('++++ Loading rules now ... ++++');
vr.call('addValidationRule', {rule: BackdateValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: CheckCurrentDateEffectiveDateValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: CheckProductEffectiveDateValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: CvgClassCapValidationRuleRHCXOD.create()});
vr.call('addValidationRule', {rule: FaceAmountValidationRuleMCXXX.create()});
vr.call('addValidationRule', {rule: FundAllocationValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: HealthCareRiderValidationRuleHCXXX.create()});
vr.call('addValidationRule', {rule: lapseValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: MaxFamilyMembersValidationRuleMCXXX.create()});
vr.call('addValidationRule', {rule: MissingDOBValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: MissingFAValidationRuleUL007.create()});
vr.call('addValidationRule', {rule: MissingInsuredNameValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: MissingRelationToInsuredValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: OneMonthYearOldValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: RegularPaymentValidationRuleUL007.create()});
vr.call('addValidationRule', {rule: RW_ExceptionValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: RW_PlanPackageValidationRuleENXXX.create()});
vr.call('addValidationRule', {rule: SameAsInsuredValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: TAKEOUT_MissingPayorAgeValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: TAKEOUT_OwnerAgeValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: FaceAmountValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: FaceAmountValidationRuleUL007.create()});
vr.call('addValidationRule', {rule: FundActivitiesValidationRuleUL007.create()});
vr.call('addValidationRule', {rule: InsuredSexValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: InsuredSmokingStatusValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: IssueAgeValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: JuvenileRiderTypeValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: lapseValidationRuleHighLocal.create()});
vr.call('addValidationRule', {rule: lapseValidationRuleLowLocal.create()});
vr.call('addValidationRule', {rule: OccupationClassValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: PaymentModeValidationRuleLocal.create()});
vr.call('addValidationRule', {rule: TotalPremiumValidationRuleUL007.create()});
console.log(''); console.log('++++ List rules installed ++++');
vr.call('showRuleNames', {});

    var BPM31 = Class.define({
            formulaName: function(args) {
    return "BPM31";
},
            productId: function(args) {
    var coverage = args["coverage"];
    //e.g. return "RE100";
    return null;
},
            methodId: function(args) {
    var coverage = args["coverage"];
    //e.g. return "BPM1";
    return "BPM31";
},
            productTypes: function(args) {
    var coverage = args["coverage"];
    //e.g. return [CatalogConstants.PRODUCT_TYPE.UVLIFE, CatalogConstants.PRODUCT_TYPE.SINGLE_PREMIUM];
    return null;
},
            rateList: function(args) {
    var coverage = args["coverage"];
    var ipo = args["ipo"];
    return [
        {rateName: "premiumRate", rateType: RateConstants.RATETYPE.BASICPREMIUM, unit: true, hasRate: true},
    ];
},
            modalRateList: function(args) {
    var coverage = args["coverage"];
    var ipo = args["ipo"];
    return [
        {rateName: "modalFactor", rateType: RateConstants.RATETYPE.MODALFACTOR, unit: false, hasRate: true}
    ];
},
            calculate: function(args) {
    var coverage = args["coverage"];
    var rates = args["rates"];
    var rounding = args["rounding"];
    var paymentMode = args["paymentMode"];

    var rateUnit = rates.rateUnit;
    var rate = rates.premiumRate;
    //var modalFactor = rates.modalFactor;

    var premium;    
    if (!(Utils.isNothing(coverage.options)) && !(Utils.isNothing(coverage.options.paymentMode))) {
        if (coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.ANNUAL) {
            if (paymentMode == SchemaConstants.PAYMENT_MODE.ANNUAL) {
                premium = coverage.plannedPremium;
            } else
            if (paymentMode == SchemaConstants.PAYMENT_MODE.SEMIANNUAL) {
                premium = coverage.plannedPremium / 2;
            } else
            if (paymentMode == SchemaConstants.PAYMENT_MODE.QUARTERLY) {
                premium = coverage.plannedPremium / 4;
            } else
            if (paymentMode == SchemaConstants.PAYMENT_MODE.MONTHLY) {
                premium = coverage.plannedPremium / 12;
            }
        } else if (coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.SEMIANNUAL) {
            if (paymentMode == SchemaConstants.PAYMENT_MODE.ANNUAL) {
                premium = coverage.plannedPremium * 2;
            } else
            if (paymentMode == SchemaConstants.PAYMENT_MODE.SEMIANNUAL) {
                premium = coverage.plannedPremium;
            } else
            if (paymentMode == SchemaConstants.PAYMENT_MODE.QUARTERLY) {
                premium = coverage.plannedPremium / 2;
            } else
            if (paymentMode == SchemaConstants.PAYMENT_MODE.MONTHLY) {
                premium = coverage.plannedPremium / 6;
            }
        } else if (coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.QUARTERLY) {
            if (paymentMode == SchemaConstants.PAYMENT_MODE.ANNUAL) {
                premium = coverage.plannedPremium * 4;
            } else
            if (paymentMode == SchemaConstants.PAYMENT_MODE.SEMIANNUAL) {
                premium = coverage.plannedPremium * 2;
            } else
            if (paymentMode == SchemaConstants.PAYMENT_MODE.QUARTERLY) {
                premium = coverage.plannedPremium;
            } else
            if (paymentMode == SchemaConstants.PAYMENT_MODE.MONTHLY) {
                premium = coverage.plannedPremium / 3;
            }
        } else if (coverage.options.paymentMode == SchemaConstants.PAYMENT_MODE.MONTHLY) {
            if (paymentMode == SchemaConstants.PAYMENT_MODE.ANNUAL) {
                premium = coverage.plannedPremium * 12;
            } else
            if (paymentMode == SchemaConstants.PAYMENT_MODE.SEMIANNUAL) {
                premium = coverage.plannedPremium * 6;
            } else
            if (paymentMode == SchemaConstants.PAYMENT_MODE.QUARTERLY) {
                premium = coverage.plannedPremium * 3;
            } else
            if (paymentMode == SchemaConstants.PAYMENT_MODE.MONTHLY) {
                premium = coverage.plannedPremium;
            }
        }
    }

    //_DB("calculate.paymentMode", paymentMode, "BPM31->");
    //_SS(rates, 10);    

    return premium;
},
            calculateExtraPremium: function(args) {
    var coverage = args["coverage"];
    var rates = args["rates"];
    var rounding = args["rounding"];
    var paymentMode = args["paymentMode"];

    return 0.0;
}
    });
    var BPM41 = Class.define({
            formulaName: function(args) {
    // designed for rider premium of UL007 or other regular saving plans
    return "BPM41";
},
            productId: function(args) {
    var coverage = args["coverage"];
    //e.g. return "RE100";
    return null;
},
            methodId: function(args) {
    var coverage = args["coverage"];
    //e.g. return "BPM1";
    return "BPM41";
},
            productTypes: function(args) {
    var coverage = args["coverage"];
    //e.g. return [CatalogConstants.PRODUCT_TYPE.UVLIFE, CatalogConstants.PRODUCT_TYPE.SINGLE_PREMIUM];
    return null;
},
            rateList: function(args) {
    var coverage = args["coverage"];
    var ipo = args["ipo"];
    return [
       {rateName: "premiumRate", rateType: RateConstants.RATETYPE.BASICPREMIUM, unit: true, hasRate: true}
    ];
},
            modalRateList: function(args) {
    var coverage = args["coverage"];
    var ipo = args["ipo"];
    return [
       {rateName: "modalFactor", rateType: RateConstants.RATETYPE.MODALFACTOR, unit: false, hasRate: true}
    ];
},
            calculate: function(args) {
    var coverage = args["coverage"];
    var rates = args["rates"];
    var rounding = args["rounding"];
    var paymentMode = args["paymentMode"];

    //_SS(coverage, 10);
    //_SS(rates, 10)

    var premiumRate = rates.premiumRate;
    var modalFactor = rates.modalFactor;

    var modalRate = _R13(premiumRate * modalFactor);
    var premium = _R( coverage.faceAmount / 1000 * modalRate , 0);
    return premium;
},
            calculateExtraPremium: function(args) {
    var coverage = args["coverage"];
    var rates = args["rates"];
    var rounding = args["rounding"];
    var paymentMode = args["paymentMode"];

    var premiumRate = rates.premiumRate;
    var modalFactor = rates.modalFactor;
    var modalRate = _R13(premiumRate * modalFactor);

    var premium = 0;
    if (!Utils.isNothing(_PV( coverage, 'extraRating.percentageExtra' ))) {
         var ep = _R( coverage.faceAmount / 1000 * _R13( modalRate * (Number(coverage.extraRating.percentageExtra) - 1) ) , 0);
         _DB("percentage extra premium", ep, "BPM41");
         premium = premium + ep;
    }
    if (!Utils.isNothing(_PV( coverage, 'extraRating.flatExtra' ))) {
         var ep = _R( coverage.faceAmount / 1000 * Number(_PV( coverage, 'extraRating.flatExtra' ) ) , 0);
         _DB("flat extra premium", ep, "BPM41");
         premium = premium + ep;
    }
    if (!Utils.isNothing(_PV( coverage, 'extraRating.tempFlat' ))) {
         var ep = _R( coverage.faceAmount / 1000 * Number(_PV( coverage, 'extraRating.tempFlat' ) ) ,0);
         _DB("temp flat extra premium", ep, "BPM41");
          premium = premium + ep;
   }
   return premium;
}
    });
    var BPM42 = Class.define({
            formulaName: function(args) {
    // designed for rider premium of WOCXX (UL007) 
    return "BPM42";
},
            productId: function(args) {
    var coverage = args["coverage"];
    //e.g. return "RE100";
    return null;
},
            methodId: function(args) {
    var coverage = args["coverage"];
    return "BPM42";
},
            productTypes: function(args) {
    var coverage = args["coverage"];
    //e.g. return [CatalogConstants.PRODUCT_TYPE.UVLIFE, CatalogConstants.PRODUCT_TYPE.SINGLE_PREMIUM];
    return null;
},
            rateList: function(args) {
    var coverage = args["coverage"];
    var ipo = args["ipo"];
    return [
       {rateName: "premiumRate", rateType: RateConstants.RATETYPE.BASICPREMIUM, unit: true, hasRate: true},
       {rateName: "coiRate", rateType: RateConstants.RATETYPE.COI, unit: false, hasRate: true},
       {rateName: "wocFactorRate", rateType: RateConstants.RATETYPE.MINIMUMPREMIUM, unit: false, hasRate: true},
       {rateName: "wocPolicyFeeFactorRate", rateType: RateConstants.RATETYPE.MIN_PLANNED_PREMIUM, unit: false, hasRate: true}
    ];
},
            modalRateList: function(args) {
    var coverage = args["coverage"];
    var ipo = args["ipo"];
    return [
        {rateName: "modalFactor", rateType: RateConstants.RATETYPE.MODALFACTOR, unit: false, hasRate: true}
    ];
},
            calculate: function(args) {
    var coverage = args["coverage"];
    var proposal = coverage.proposal;
    var basePlan = proposal.coverageInfo;
    var extraRating = coverage.extraRating;
    var rates = args["rates"];
    var rounding = args["rounding"];
    var paymentMode = args["paymentMode"];
    var currentCors = 0;

    var targetPremiumRate = rates.premiumRate;
    var wocFactor = rates.wocFactorRate;
    var wocPolicyFeeFactor = rates.wocPolicyFeeFactorRate;
    var modalFactorForCoi = rates.modalFactor;

    var waiverTrendingFactor = -3.6/100;
    var policyFee = this.self.calculatePolicyFee({proposal: coverage.proposal});
    var yearOfPolicyFee = Math.max(Math.min(2033, proposal.policyYearDate.date().getFullYear()) - 2015, 0);
    var trendingFactor = Math.pow(1 + waiverTrendingFactor, yearOfPolicyFee);
    var wocPolicyFee = _R(_R(wocPolicyFeeFactor * trendingFactor, 1) * policyFee * 12, 3);

    var wocNaarBase = _R(wocFactor * Number(basePlan.faceAmount) / 1000,3);
    // 3. Premium for WOC (total premiums includes base premium excluding rider * 3)
    wocPremiums = (currentCors + Number(basePlan.plannedPremium)) * 3;
    totalWocFA = _R(wocNaarBase + wocPolicyFee + wocPremiums, 0);

    //_DB("yearOfPolicyFee", yearOfPolicyFee, "BPM42.");
    //_DB("targetPremiumRate", targetPremiumRate, "BPM42.");
    //_DB("wocFactor", wocFactor, "BPM42.");
    //_DB("wocPolicyFeeFactor", wocPolicyFeeFactor, "BPM42.");
    //_DB("extraRating", extraRating, "BPM42.");
    //_DB("currentCors", currentCors, "BPM42.");    
    //_DB("trendingFactor", trendingFactor, "BPM42.");
    //_DB("wocNaarBase", wocNaarBase, "BPM42.");
    //_DB("wocPolicyFee", wocPolicyFee, "BPM42.");
    //_DB("wocPremiums", wocPremiums, "BPM42.");
    //_DB("totalWocFA", totalWocFA, "BPM42.");

    targetPremium = this.self.calculateTargetPremium({targetPremiumRate: targetPremiumRate, modalFactorForCoi: modalFactorForCoi, totalWocFA: totalWocFA, percentRating: 1});
    //_DB("targetPremium", targetPremium, "BPM42.");    

    return targetPremium;
},
            calculateExtraPremium: function(args) {
    var coverage = args["coverage"];
    var proposal = coverage.proposal;
    var basePlan = proposal.coverageInfo;
    var extraRating = coverage.extraRating;
    var rates = args["rates"];
    var rounding = args["rounding"];
    var paymentMode = args["paymentMode"];
    var currentCors = 0;

    var targetPremiumRate = rates.premiumRate;
    var coiRate = rates.coiRate;
    var wocFactor = rates.wocFactorRate;
    var wocPolicyFeeFactor = rates.wocPolicyFeeFactorRate;
    var modalFactorForCoi = rates.modalFactor;

    var waiverTrendingFactor = -3.6/100;
    var policyFee = this.self.calculatePolicyFee({proposal: coverage.proposal});
    var yearOfPolicyFee = Math.max(Math.min(2033, proposal.policyYearDate.date().getFullYear()) - 2015, 0);
    var trendingFactor = Math.pow(1 + waiverTrendingFactor, yearOfPolicyFee);
    var wocPolicyFee = _R(_R(wocPolicyFeeFactor * trendingFactor, 1) * policyFee * 12, 3);

    var wocNaarBase = _R((wocFactor * (Number(extraRating.percentageExtra) - 1) + Number( _V( extraRating.flatExtra, 0) ) / 1000) * Number(basePlan.faceAmount) / 1000,3);
    // 3. Premium for WOC (total premiums includes base premium excluding rider * 3)
    wocPremiums = (currentCors + Number(basePlan.plannedPremium)) * 3;
    totalWocFA = _R(wocNaarBase + wocPolicyFee + wocPremiums, 0);

    targetPremium = this.self.calculateTargetPremium({targetPremiumRate: targetPremiumRate, modalFactorForCoi: modalFactorForCoi, totalWocFA: totalWocFA, percentRating: (Number(extraRating.percentageExtra)-1) });
    //_DB("extraPremium", targetPremium, "BPM42.");    

    return targetPremium;
},
            calculateTargetPremium: function(args) {
    var totalWocFA = args['totalWocFA'];
    var percentRating = args['percentRating'];
    var targetPremiumRate = args['targetPremiumRate'];
    var modalFactorForCoi = args['modalFactorForCoi'];

    var modalTargetPremiumRate = _R13(_R13( targetPremiumRate * modalFactorForCoi) * percentRating );
    //_DB("modalTargetPremiumRate", modalTargetPremiumRate, "BPM42.");

    var targetPremium = _R(totalWocFA * modalTargetPremiumRate / 1000, 0);
    return targetPremium;
},
            calculatePolicyFee: function(args) {
    var proposal = args['proposal'];
    var basePlan = proposal.coverageInfo;
    var year = 0;
    var months = 0;

    var issueDate = proposal.policyYearDate.date();

    var orgYear = 2016;
    var policyFee = 27;
    var currDate = issueDate;
    currDate.setMonth(issueDate.getMonth() + months);
    var currYear  = currDate.getFullYear();

    // since the rate is from 20160101 and the rate should be increased by $2 for each year after 2016 and cap at $60
    var fee = Math.min(60, policyFee + (currYear - orgYear) * 2);
    //_D("Policy Fee = Math.min(60, policyFee (" + policyFee +  ") + (currYear (" + currYear + ") - orgYear (" + orgYear + ")) * 2)", fee, "BPM42.");

    return fee;
}
    });
    var BPM43 = Class.define({
            formulaName: function(args) {
    return "BPM43";
},
            productId: function(args) {
    var coverage = args["coverage"];
    //e.g. return "RE100";
    return null;
},
            methodId: function(args) {
    var coverage = args["coverage"];
    //e.g. return "BPM1";
    return "BPM43";
},
            productTypes: function(args) {
    var coverage = args["coverage"];
    //e.g. return [CatalogConstants.PRODUCT_TYPE.UVLIFE, CatalogConstants.PRODUCT_TYPE.SINGLE_PREMIUM];
    return null;
},
            rateList: function(args) {
    var coverage = args["coverage"];
    var ipo = args["ipo"];
    return [
       {rateName: "premiumRate", rateType: RateConstants.RATETYPE.BASICPREMIUM, unit: true, hasRate: true}
    ];
},
            modalRateList: function(args) {
    var coverage = args["coverage"];
    var ipo = args["ipo"];
    return [
       {rateName: "modalFactor", rateType: RateConstants.RATETYPE.MODALFACTOR, unit: false, hasRate: true}
    ];
},
            calculate: function(args) {
    var coverage = args["coverage"];
    var rates = args["rates"];
    var rounding = args["rounding"];
    var paymentMode = args["paymentMode"];

    var premiumRate = rates.premiumRate;
    var modalFactor = rates.modalFactor;

    _SS(rates, 10);

    var premium = _R( premiumRate * modalFactor , 0);
    return premium;
},
            calculateExtraPremium: function(args) {
    var coverage = args["coverage"];
    var rates = args["rates"];
    var rounding = args["rounding"];
    var paymentMode = args["paymentMode"];

    var premiumRate = rates.premiumRate;
    var modalFactor = rates.modalFactor;
    var modalRate = _R13(premiumRate * modalFactor);

    var premium = 0;
    if (!Utils.isNothing(_PV( coverage, 'extraRating.percentageExtra' ))) {
         var ep = _R( _R13( modalRate * (Number(coverage.extraRating.percentageExtra) - 1) ) , 0);
         premium = premium + ep;
    }
    if (!Utils.isNothing(_PV( coverage, 'extraRating.flatExtra' ))) {
         var ep = _R( Number(_PV( coverage, 'extraRating.flatExtra' ) ) , 0);
         premium = premium + ep;
    }
    if (!Utils.isNothing(_PV( coverage, 'extraRating.tempFlat' ))) {
         var ep = _R( Number(_PV( coverage, 'extraRating.tempFlat' ) ) ,0);
          premium = premium + ep;
   }
   return premium;
}
    });
    var BPM44 = Class.define({
            formulaName: function(args) {
    //formular name
    return "BPM44";
},
            productId: function(args) {
    //productid
    var coverage = args["coverage"];
    return null;
},
            methodId: function(args) {
    var coverage = args["coverage"];
    return this.self.formulaName(args);
},
            productTypes: function(args) {
    var coverage = args["coverage"];
    //e.g. return [CatalogConstants.PRODUCT_TYPE.UVLIFE, CatalogConstants.PRODUCT_TYPE.SINGLE_PREMIUM];
    return null;
},
            rateList: function(args) {
    var coverage = args["coverage"];
    var ipo = args["ipo"];
    return [
        {rateName: "premiumRate", rateType: RateConstants.RATETYPE.BASICPREMIUM, unit: true, hasRate: true},
    ];
},
            modalRateList: function(args) {
    var coverage = args["coverage"];
    var ipo = args["ipo"];
    return [
        {rateName: "modalFactor", rateType: RateConstants.RATETYPE.MODALFACTOR, unit: false, hasRate: true}
    ];
},
            calculate: function(args) {
    //can create a method calculateCompletePremiumRecords to return a record with all premium fields such as {premium:a, extraPremium:b, totalPremium:(a+b)}
    var coverage = args["coverage"];
    var proposal = coverage.proposal;
    var rates = args["rates"];
    var rounding = args["rounding"];
    var paymentMode = args["paymentMode"];

    //if (proposal.enableDebug) {
    //    _SS(proposal, 10);
    //}

    var premiumRate = this.self.getNetPremiumRate(args);
    var modalFactor = rates.modalFactor;

    var modalRate = _R13(premiumRate * modalFactor);
    var premium = _R( coverage.faceAmount / 1000 * modalRate , 0);
    return premium;
},
            calculateExtraPremium: function(args) {
    var coverage = args["coverage"];
    var rates = args["rates"];
    var rounding = args["rounding"];
    var paymentMode = args["paymentMode"];
    var methodId = this.self.formulaName(args);

    var premiumRate = this.self.getNetPremiumRate(args);
    var modalFactor = rates.modalFactor;
    var modalRate = _R13(premiumRate * modalFactor);

    var premium = 0;
    if (!Utils.isNothing(_PV( coverage, 'extraRating.percentageExtra' ))) {
         var ep = _R( coverage.faceAmount / 1000 * _R13( modalRate * (Number(coverage.extraRating.percentageExtra) - 1) ) , 0);
         _DB("percentage extra premium", ep, methodId + "-->");
         premium = premium + ep;
    }
    if (!Utils.isNothing(_PV( coverage, 'extraRating.flatExtra' ))) {
         var ep = _R( coverage.faceAmount / 1000 * Number(_PV( coverage, 'extraRating.flatExtra' ) ) , 0);
         _DB("flat extra premium", ep, methodId + "-->");
         premium = premium + ep;
    }
    if (!Utils.isNothing(_PV( coverage, 'extraRating.tempFlat' ))) {
         var ep = _R( coverage.faceAmount / 1000 * Number(_PV( coverage, 'extraRating.tempFlat' ) ) ,0);
         _DB("temp flat extra premium", ep, methodId + "-->");
          premium = premium + ep;
   }
   return premium;
},
            getNetPremiumRate: function(args) {
    var coverage = args["coverage"];
    var proposal = coverage.proposal;
    var rates = args["rates"];
    
    var netRate = rates.premiumRate;
    var discountRate = 0;
    if (proposal.band == 3) {
        discountRate = 0.06;
    } else 
    if (proposal.band == 2) {
        discountRate = 0.03;
    }
    netRate = _R2(netRate * (1 - discountRate));
    return netRate;
},
            calculateCompletePremiumRecords: function(args) {
     _DB("@@@@@", "The method [calculateCompletePremiumRecords] is being called.", "BPM44->");
     var standardPremium = this.self.calculateStandardPremium(args);
     var premium = this.self.calculate(args);
     var extraPremium = this.self.calculateExtraPremium(args);
     var totalPremium = (premium + extraPremium);
     return { premium: premium, discount: (standardPremium - premium), extraPremium: extraPremium, totalPremium: totalPremium };
},
            calculateStandardPremium: function(args) {
    var coverage = args["coverage"];
    var proposal = coverage.proposal;
    var rates = args["rates"];
    var rounding = args["rounding"];
    var paymentMode = args["paymentMode"];

    var premiumRate = rates.premiumRate;
    var modalFactor = rates.modalFactor;

    var modalRate = _R13(premiumRate * modalFactor);
    var premium = _R(coverage.faceAmount / 1000 * modalRate, 0);
    return premium
}
    });

var pc = Repository.getManager(SystemComponents.MANAGER.PREMIUM_CALCULATOR);
if (!pc) {
   console.log(''); console.log('++++ Create new Premium Calculator for setting local premium formula into engine. ++++');
   pc = IPremiumCalculator.implementBy(PremiumCalculatorImpl.create());
   Repository.addManager(SystemComponents.MANAGER.PREMIUM_CALCULATOR, pc);
}
console.log(''); console.log('++++ Loading premium formula now ... ++++');
pc.call('addMethod', {method : CommonBpm.create({formula: BPM31.create()})});
pc.call('addMethod', {method : CommonBpm.create({formula: BPM41.create()})});
pc.call('addMethod', {method : CommonBpm.create({formula: BPM42.create()})});
pc.call('addMethod', {method : CommonBpm.create({formula: BPM43.create()})});
pc.call('addMethod', {method : CommonBpm.create({formula: BPM44.create()})});
console.log(''); console.log('++++ List premium formula installed ++++');
pc.call('showFormulas', {});
var managers = {};

var messageManager = IMessageManager.implementBy(MessageManagerImpl.create({
   messages: messages
}));

//var validator = IValidator.implementBy(ValidatorImpl.create({
//   productValidationMapping: mapping
//}));

//var engine = new PE(IProductEngine.implementBy(ProductEngineImpl.create({
//   managers: {
//      MESSAGE_MANAGER: messageManager,
//      VALIDATOR: validator
//   }
//})));

console.log("");
console.log("++++ Preparing engine now ... ++++");
var validator = Repository.getManager(SystemComponents.MANAGER.VALIDATOR);
if (validator) {
    console.log("");
    console.log("++++ Validator is found! ++++");
} else {
    console.log("");
    console.log("++++ Validator is created with mapping! ++++");
    validator = IValidator.implementBy(ValidatorImpl.create({
        productValidationMapping: mapping
    }));
}
managers[SystemComponents.MANAGER.VALIDATOR] = validator;
managers[SystemComponents.MANAGER.MESSAGE_MANAGER] = messageManager;

var eventManager = Repository.getManager(SystemComponents.MANAGER.EVENTS_MANAGER);
if (eventManager) {
    console.log("");
    console.log("++++ EventManager is found! ++++");
    managers[SystemComponents.MANAGER.EVENTS_MANAGER] = eventManager;
}

var premiumCalculator = Repository.getManager(SystemComponents.MANAGER.PREMIUM_CALCULATOR);
if (premiumCalculator) {
    console.log("");
    console.log("++++ PremiumCalculator is found! ++++");
    managers[SystemComponents.MANAGER.PREMIUM_CALCULATOR] = premiumCalculator;
}

console.log("++++ Engine is being started ... ++++");

var engine = new PE(IProductEngine.implementBy(ProductEngineImpl.create({
   managers: managers
})));

//console.log("++++ Global logger is being created ... ++++");
//
//var LoggerHelper = new LoggerFactory( engine );

console.log("");
console.log("++++ Engine is ready for use now ... ++++");
console.log("");
console.log("");

